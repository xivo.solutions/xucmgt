import com.typesafe.sbt.packager.docker._
import com.typesafe.sbt.packager.MappingsHelper._
import com.typesafe.sbt.web.pipeline.Pipeline
import com.typesafe.sbt.web.{CompileProblemsException, GeneralProblem, SbtWeb}
import play.sbt.PlayImport.PlayKeys.playRunHooks
import sbt.Append.appendSeqImplicit

import scala.sys.process._

val appName = "xucmgt"
// Do not change version directly but change TARGET_VERSION
val appVersion = sys.env.getOrElse("TARGET_VERSION", "dev-version")
val appOrganisation = "xivo"

val main = Project(appName, file("."))
  .enablePlugins(play.sbt.PlayScala)
  .enablePlugins(SbtWeb)
  .enablePlugins(DockerPlugin)
  .enablePlugins(BuildInfoPlugin)
  .settings(
    name := appName,
    version := appVersion,
    crossPaths := false,
    scalaVersion := Dependencies.scalaVersion,
    semanticdbEnabled := true,
    semanticdbVersion := scalafixSemanticdb.revision,
    organization := appOrganisation,
    resolvers ++= Dependencies.resolutionRepos,
    libraryDependencies ++= Dependencies.runDep ++ Dependencies.testDep,
    Assets / LessKeys.less / includeFilter := "*.less",
    Assets / LessKeys.less / excludeFilter := "_*.less",
    Assets / JshintKeys.jshint / excludeFilter := ((f: File) => ".*(/javascripts/(ccagent/|xccti/|xchelper/|ccmanager/|ucassistant/|xcchat/|xclogin/)|webpack).*".r.pattern.matcher(f.getAbsolutePath).matches),
    Compile / packageDoc / publishArtifact := false,
    playRunHooks += baseDirectory.map(base => Webpack(base)).value,
    webpack := {
      val log = new CustomLogger
      if("npm ci".!(log) != 0) throw new CompileProblemsException(Array(new GeneralProblem(log.buf.toString, file("./package.json"))))
      if("npm run build".!(log) != 0) throw new CompileProblemsException(Array(new GeneralProblem(log.buf.toString, file("./webpack.config.js"))))
    },
    webpackPipeline := { mappings =>
      val webpackBaseDir = baseDirectory.value / "target/web/public/main/javascripts/dist"
      val webpackRebaseDir = baseDirectory.value / "target/web/public/main/"
      val webpackFiles = PathFinder(webpackBaseDir).allPaths --- webpackBaseDir pair relativeTo(webpackRebaseDir)
      mappings ++ webpackFiles
    },
    npmTest := {
      val log = new CustomLogger
      if("npm ci".!(log) != 0) throw new CompileProblemsException(Array(new GeneralProblem(log.buf.toString, file("./package.json"))))
      if("npm run test-headless".!(log) != 0) throw new RuntimeException("NPM Tests failed")
    },
    (Test / test) := ((Test / test) dependsOn npmTest).value,
    pipelineStages := Seq(webpackPipeline, digest),
    dist := (dist dependsOn webpack).value,
    (Docker / stage) := ((Docker / stage) dependsOn webpack).value
  )
  .settings(
    Test / testOptions += Tests.Argument(TestFrameworks.ScalaTest, "-o", "-u","target/test-reports"),
    Test / testOptions += Tests.Setup(() => { System.setProperty("XUCMGT_VERSION", appVersion) }),
    scalacOptions ++= Seq(
      "-feature",
      "-language:existentials",
      "-language:higherKinds",
      "-language:implicitConversions",
      "-Wunused:imports",
    )
  )
  .settings(dockerSettings: _*)
  .settings(editSourceSettings: _*)
  .settings(docSettings: _*)
  .settings(
    setVersionVarTask := { System.setProperty("XUCMGT_VERSION", appVersion) },
    EditSource / edit := ((EditSource / edit) dependsOn (EditSource / EditSourcePlugin.autoImport.clean)).value,
    Compile / packageBin := ((Compile / packageBin) dependsOn (EditSource / edit)).value,
    Compile / run := ((Compile / run) dependsOn setVersionVarTask).evaluated
  )
  .settings(buildInfoSettings: _*)


lazy val setVersionVarTask = taskKey[Unit]("Set version to a env var")
lazy val webpack = taskKey[Unit]("Webpack stage")
lazy val webpackPipeline = taskKey[Pipeline.Stage]("Webpack pipeline")
lazy val npmTest = taskKey[Unit]("Run NPM headless test")


lazy val buildInfoSettings = Seq(
  buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
  buildInfoPackage := "server.info"
)

lazy val dockerSettings = Seq(
  Docker / maintainer := "R&D <randd@xivo.solutions>",
  dockerBaseImage := "eclipse-temurin:17.0.9_9-jdk-focal",
  dockerExposedPorts := Seq(9000),
  Docker / daemonUserUid := None,
  Docker / daemonUser := "daemon",
  dockerExposedVolumes := Seq("/conf","/logs"),
  dockerRepository := Some("xivoxc"),
  dockerCommands += Cmd("LABEL", s"""version="$appVersion""""),
  dockerEntrypoint := Seq("bin/xucmgt_docker"),
  dockerChmodType := DockerChmodType.UserGroupWriteExecute
)

lazy val editSourceSettings = Seq(
  EditSource / flatten := true,
  Universal / mappings += file("target/version/appli.version") -> "conf/appli.version",
  Universal / mappings ++= directory("updates"),
  EditSource / targetDirectory := baseDirectory(_/"target/version").value,
  EditSource / variables += version {_ => ("SBT_EDIT_APP_VERSION", appVersion)}.value,
  EditSource / sources ++= (baseDirectory map { bd =>
    (bd / "src/res" * "appli.version").get
  }).value
)

lazy val docSettings = Seq(
  Compile / packageDoc / publishArtifact := false,
  Compile / doc / sources := Seq.empty
)
