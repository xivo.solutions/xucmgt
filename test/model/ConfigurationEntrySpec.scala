package model

import models.ConfigurationEntry
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import play.api.libs.json.Json

class ConfigurationEntrySpec extends AnyWordSpec with Matchers {
  "ConfigurationEntry" should {
    "convert to json" in {
      val c = ConfigurationEntry("titi", "toto")
      Json.toJson(c) should be(
        Json.parse("""{"name":"titi", "value":"toto"}""")
      )
    }
  }
}
