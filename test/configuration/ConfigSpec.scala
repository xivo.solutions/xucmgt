package configuration

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import play.api.Configuration
import com.typesafe.config.ConfigFactory
import java.io.File 

class ConfigSpec extends AnyWordSpec with Matchers {

  val configFile = new File("test/conf/test.conf")
  val configData = Configuration(ConfigFactory.parseFile(configFile))
  
  "XucConfig.getNestedApplicationConf" should {

    "return Some string when the path exists" in {
      val config = new XucConfig(configData)

      val result = config.getNestedApplicationConf("titi.toto", "xuc")

      result shouldBe Some("toto.corp.fr")
    }

    "return Some string when the path exists (2-lvl)" in {
      val config = new XucConfig(configData)

      val result = config.getNestedApplicationConf("lili.lolo.lala", "xuc")

      result shouldBe Some("lala.corp.fr")
    }

    "return Some string when the path exists (3-lvl)" in {
      val config = new XucConfig(configData)

      val result = config.getNestedApplicationConf("nini.nono.nana.nene", "xuc")

      result shouldBe Some("nene.corp.fr")
    }

    "return None when the path does not exist" in {
      val config = new XucConfig(configData)

      val result = config.getNestedApplicationConf("nested.path", "xuc")

      result shouldBe None
    }

    "return None when the section does not exist" in {
      val config = new XucConfig(configData)

      val result = config.getNestedApplicationConf("test.nonexistent", "test")

      result shouldBe None
    }
  }
}
