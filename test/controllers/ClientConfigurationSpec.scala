package controllers

import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import org.mockito.Mockito.{verify, when}
import org.scalatestplus.mockito.MockitoSugar
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.libs.json.Json
import play.api.test.FakeRequest
import play.api.test.Helpers._
import play.api.Environment
import play.api.Mode
import play.api.mvc.ControllerComponents
import configuration.UcConfig

class ClientConfigurationSpec 
  extends AnyWordSpec 
  with Matchers
  with MockitoSugar
  with GuiceOneAppPerSuite {
  
  class Helper {
    val ucConfigMock = mock[UcConfig]
    val environment = Environment.simple(mode = Mode.Test)
    val cc = app.injector.instanceOf[ControllerComponents]
    val controller = new ClientConfiguration(ucConfigMock, environment, cc)
    controller.setControllerComponents(cc)
  }
  
  "ClientConfiguration" should {

    "call config.getNestedApplicationConf with the correct path format" in new Helper {
      when(ucConfigMock.getNestedApplicationConf("nested.path.with.point", "client")).thenReturn(Some("nestedValue"))
      val request = FakeRequest(GET, "/nested/path/with/point")
      val result = controller.getNestedConfig("nested/path/with/point")(request)
      status(result) shouldBe OK
      contentAsJson(result) shouldBe Json.obj("value" -> "nestedValue")
      verify(ucConfigMock).getNestedApplicationConf("nested.path.with.point", "client")
    }

    "call config.getNestedApplicationConf with the correct path format (path ending with '/')" in new Helper {
      when(ucConfigMock.getNestedApplicationConf("nested.path.with.point", "client")).thenReturn(Some("nestedValue"))
      val request = FakeRequest(GET, "/nested/path/with/point/")
      val result = controller.getNestedConfig("nested/path/with/point/")(request)
      status(result) shouldBe OK
      contentAsJson(result) shouldBe Json.obj("value" -> "nestedValue")
      verify(ucConfigMock).getNestedApplicationConf("nested.path.with.point", "client")
    }

  }
}
