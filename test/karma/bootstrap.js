var karmaBackend = angular.module('karma-backend', ['ngMockE2E']);

karmaBackend.run(function($httpBackend, $log) {
  var loginPage = '<login-form on-login="onLogin()" host-and-port="hostAndPort" error-code="error" use-sso="useSso" title="Login"/>';

  $httpBackend.whenGET(/^\/ccmanager\/login/).respond(loginPage);
  $httpBackend.whenGET(/^\/ucassistant\/login.html/).respond(loginPage);
  $httpBackend.whenGET('/ccagent/config/externalViewUrl').respond("");
  $httpBackend.whenGET("/config/thirdPartyWsLoginUrl").respond("");
  $httpBackend.whenGET("/ccagent/config/thirdPartyWsLoginUrl").respond("");
  $httpBackend.whenGET('/config/externalViewUrl').respond("");
  $httpBackend.whenGET('/ccagent/config/showQueueControls').respond(false);
  $httpBackend.whenGET('/ccagent/config/disableChat').respond(false);
  $httpBackend.whenGET("/ccagent/config/iceGatheringTimeout").respond("")
  $httpBackend.whenGET("/config/iceGatheringTimeout").respond("")
  $httpBackend.whenGET('/config/disableChat').respond(false);
  $httpBackend.whenGET("/ccagent/config/iceGatheringTimeout").respond("");
  $httpBackend.whenGET("/config/iceGatheringTimeout").respond("");
  $httpBackend.whenGET('/config/showAppDownload').respond(true);
  $httpBackend.whenGET('/ccagent/config/showAppDownload').respond(true);
  $httpBackend.whenGET(/\w*\/config\/email/).respond({});
  $httpBackend.whenGET(/\w*\/config\/headsets/).respond([{vendorId: 1151}]);
  $httpBackend.whenGET(/^assets\/i18n\//).passThrough();
  $httpBackend.whenGET(/^assets/).passThrough();
  $httpBackend.whenGET('/config/notifyOnHold').respond(0);
  $httpBackend.whenGET("/video/external_api.js").respond({});
  $httpBackend.whenGET("/ccmanager/config/queueIndicators.toString").respond("PercentageAnsweredBefore15,AvailableAgents,PercentageAbandonnedAfter15,WaitingCalls");
});
