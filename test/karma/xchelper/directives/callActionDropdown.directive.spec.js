import _ from 'lodash';
import { AppConfig, AppType } from '../../../../app/assets/javascripts/xccti/services/applicationConfiguration.provider';

describe('callActionDropdown directive', function() {

  var $rootScope;
  var $q;
  var $compile;
  var XucPhoneState;
  var XucPhoneEventListener;
  var applicationConfiguration;
  var $translate;
  var callContext;
  var jitsiProxy;
  var CtiProxy;
  var sampleContact = ContactBuilder("User 1", "user1", "1000");
  var defaultMailTpl = { subject: "", body: "" };

  var locationValue = {
    origin: 'http://localhost'
  };
  var windowProvider = {
    $get: function() {
      return {
        location: locationValue,
        navigator: {languages: 'en_EN'}};
    }
  };

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });

    angular.mock.module(function($provide) {
      $provide.provider('$window', windowProvider);
    });

    angular.mock.inject(function (_$compile_, _$rootScope_, _XucPhoneState_, _XucPhoneEventListener_, _applicationConfiguration_, _$translate_, _callContext_, _JitsiProxy_, _CtiProxy_, _$q_) {
      $rootScope = _$rootScope_;
      applicationConfiguration = _applicationConfiguration_;
      $compile = _$compile_;
      $translate = _$translate_;
      callContext = _callContext_;
      jitsiProxy = _JitsiProxy_;
      CtiProxy = _CtiProxy_;
      $q = _$q_;

      XucPhoneState = _XucPhoneState_;
      XucPhoneEventListener = _XucPhoneEventListener_;
    });
  });

  function sampleContactDirectiveBind(contact = sampleContact) {
    let newScope = $rootScope.$new();
    newScope.contact = contact;
    let elementStr = angular.element('<call-action-dropdown contact="contact"></call-action-dropdown>');
    let element = $compile(elementStr)(newScope);
    newScope.$digest();

    let innerScope = element.children().scope();
    return {element, innerScope};
  }

  it('bind directive', function() {
    let {element, innerScope} = sampleContactDirectiveBind();
    expect(element).toBeDefined();
    expect(_.isArray(innerScope.phoneNumbers)).toBe(true);
  });

  it('extract phone numbers from contact', function() {
    let {innerScope} = sampleContactDirectiveBind();
    expect(innerScope.phoneNumbers).toEqual(["1000", "2000", "3000"]);
  });

  it('extract email from contact', function() {
    let {innerScope} = sampleContactDirectiveBind();
    expect(innerScope.email).toEqual('noreply@avencall.com');
  });

  it('removes chat button if chat is disabled', function () {
    spyOn(applicationConfiguration, 'getCurrentAppConfig').and.returnValue({ switchboard: true, chat: false, mailTemplate: defaultMailTpl});
    let {element} = sampleContactDirectiveBind();

    expect(element.html()).not.toContain('<i class="fas fa-comment-dots"></i>');
  });

  it('should not enable invite to meeting room if user has no xivo username', function(){
    let {innerScope} = sampleContactDirectiveBind();
    innerScope.contact.actions.Video.args[0] = null;

    let result = innerScope.canInviteToMeetingRoom();
    expect(result).toBeFalsy();
  });

  it('should not enable chat if user has no xivo username', function(){
    let {innerScope} = sampleContactDirectiveBind();
    innerScope.contact.actions.Chat.args[0]= null;

    let result = innerScope.canShowChat();
    expect(result).toBeFalsy();
  });

  it('should not enable video if user has no xivo username', function(){
    let {innerScope} = sampleContactDirectiveBind();
    innerScope.contact.actions.Video.args[0] = null;

    let result = innerScope.canShowVideo();
    expect(result).toBeFalsy();
  });

  it('should enable chat if user is available', function(){
    let {innerScope} = sampleContactDirectiveBind();
    innerScope.contact.actions.Chat.args[0]= "user1";

    let result = innerScope.canShowChat();
    expect(result).toBeTruthy();
  });

  it('should enable video if user is available', function(){
    let {innerScope} = sampleContactDirectiveBind();
    innerScope.contact.actions.Video.args[0]= "user1";

    let result = innerScope.canShowVideo();
    expect(result).toBeTruthy();
  });


  it('should enable direct transfer if only one call is ongoing and you are in switchboard context', function () {
    const appConfig = new AppConfig(AppType.Switchboard);
    appConfig.mailTemplate = defaultMailTpl;
    spyOn(applicationConfiguration, 'getCurrentAppConfig').and.returnValue(appConfig);
    let calls = [{state: XucPhoneState.STATE_ESTABLISHED}];
    let {innerScope} = sampleContactDirectiveBind();

    let result = innerScope.canShowDirectTransfer(calls);
    expect(result).toBeTruthy();
  });

  it('should not enable direct transfer if one call is not ongoing', function(){
    let calls = [{state: XucPhoneState.STATE_RINGING}];
    let {innerScope} = sampleContactDirectiveBind();

    let result = innerScope.canShowDirectTransfer(calls);
    expect(result).toBeFalsy();
  });

  it('should not enable direct transfer if more than one call', function(){
    let calls = [{state: XucPhoneState.STATE_ESTABLISHED},{state: XucPhoneState.STATE_RINGING}];
    let {innerScope} = sampleContactDirectiveBind();

    let result = innerScope.canShowDirectTransfer(calls);
    expect(result).toBeFalsy();
  });

  it('should not enable direct transfer if no call', function(){
    let calls = [];
    let {innerScope} = sampleContactDirectiveBind();

    let result = innerScope.canShowDirectTransfer(calls);
    expect(result).toBeFalsy();
  });

  it('should not display invite icon if not organizer', function() {
    spyOn(XucPhoneState, 'getConferenceOrganizer').and.returnValue(false);
    let {element} = sampleContactDirectiveBind();
    expect(element.html()).not.toContain('<span class="xivo-phone-plus-wrapper">');
    expect(element.html()).not.toContain('<span><i class="xivo-telephone"></i></span>');
    expect(element.html()).not.toContain('<i class="fa fa-plus xivo-phone-plus"></i>');
  });


  it('should display invite icon if organizer', function() {
    spyOn(XucPhoneState, 'getConferenceOrganizer').and.returnValue(true);
    let {element} = sampleContactDirectiveBind();
    expect(element.html()).toContain('<span class="xivo-phone-plus-wrapper">');
    expect(element.html()).toContain('<span><i class="xivo-telephone"></i></span>');
    expect(element.html()).toContain('<i class="fa fa-plus xivo-phone-plus"></i>');

  });

  it('gets the email template', function() {
    let mailTemplate = {
      subject : 'subject',
      body : 'body'
    };
    spyOn(applicationConfiguration, 'getCurrentAppConfig').and.returnValue({mailTemplate: mailTemplate});

    let {innerScope} = sampleContactDirectiveBind();
    expect(decodeURI(innerScope.mailToLink)).toBe('mailTo:noreply@avencall.com?subject=' + mailTemplate.subject + '&body=' + mailTemplate.body);
  });

  it('gets the email template with values when there is an ongoing call', function() {
    let calls = [{
      state: XucPhoneState.STATE_ESTABLISHED,
      otherDN: '1000'
    }];

    let mailTemplate = {
      subject : 'subject {callernum}',
      body : 'body {dstname}'
    };

    spyOn(applicationConfiguration, 'getCurrentAppConfig').and.returnValue({mailTemplate: mailTemplate});
    spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);

    let {innerScope} = sampleContactDirectiveBind();
    expect(decodeURI(innerScope.mailToLink)).toBe('mailTo:noreply@avencall.com?subject=subject 1000&body=body User 1');
  });

  it('gets the email template with values when there is NO ongoing call', function() {
    let mailTemplate = {
      subject : 'subject {callernum}',
      body : 'body {callernum}'
    };
    spyOn($translate, 'instant').and.returnValue('NUMBER');
    spyOn(applicationConfiguration, 'getCurrentAppConfig').and.returnValue({mailTemplate: mailTemplate});
    spyOn(XucPhoneState, 'getCalls').and.returnValue([]);

    let {innerScope} = sampleContactDirectiveBind();

    expect(decodeURI(innerScope.mailToLink)).toBe('mailTo:noreply@avencall.com?subject=subject ###NUMBER###&body=body ###NUMBER###');
  });


  it('displays the email correctly with special characters', function() {
    let mailTemplate = {
      subject : 'subject ? with special & char é ` \\ ~ ^ %20 |',
      body : 'body'
    };
    spyOn(applicationConfiguration, 'getCurrentAppConfig').and.returnValue({mailTemplate: mailTemplate});

    let {innerScope} = sampleContactDirectiveBind();
    expect(decodeURI(innerScope.mailToLink)).toBe('mailTo:noreply@avencall.com?subject=' + mailTemplate.subject + '&body=' + mailTemplate.body);
  });


  it('displays the email correctly when anonymous', function() {
    let calls = [{
      state: XucPhoneState.STATE_ESTABLISHED,
      otherDN: 'anonymous'
    }];

    let mailTemplate = {
      subject : 'subject {callernum}',
      body : 'body {dstname}'
    };

    let translations = {
      'PLACEHOLDER_NUMBER': 'NUMBER'
    };

    spyOn(applicationConfiguration, 'getCurrentAppConfig').and.returnValue({mailTemplate: mailTemplate});
    spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
    spyOn($translate, 'instant').and.callFake((param) => {
      return translations[param];
    });

    let {innerScope} = sampleContactDirectiveBind();
    expect(decodeURI(innerScope.mailToLink)).toBe('mailTo:noreply@avencall.com?subject=subject ###NUMBER###&body=body User 1');
  });

  it('displays the email correctly when the callerNum and destination name are empty', function() {
    let calls = [{
      state: XucPhoneState.STATE_ESTABLISHED,
      otherDN: ''
    }];

    let mailTemplate = {
      subject : 'subject {callernum}',
      body : 'body {dstname}'
    };

    let translations = {
      'PLACEHOLDER_NUMBER': 'NUMBER',
      'PLACEHOLDER_NAME': 'NAME'
    };

    spyOn(applicationConfiguration, 'getCurrentAppConfig').and.returnValue({mailTemplate: mailTemplate});
    spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
    spyOn($translate, 'instant').and.callFake((param) => {
      return translations[param];
    });

    let {innerScope} = sampleContactDirectiveBind();

    expect(decodeURI(innerScope.mailToLink)).toBe('mailTo:noreply@avencall.com?subject=subject ###NUMBER###&body=body User 1');
  });

  it('register to regenerate mailto link when calls are established or released', function() {
    spyOn(XucPhoneEventListener, 'addEstablishedHandler');
    spyOn(XucPhoneEventListener, 'addReleasedHandler');
    sampleContactDirectiveBind();

    expect(XucPhoneEventListener.addEstablishedHandler).toHaveBeenCalled();
    expect(XucPhoneEventListener.addReleasedHandler).toHaveBeenCalled();
  });


  it('keep callernum of first call when a second call is established', function() {
    let calls = [{
      state: XucPhoneState.STATE_ONHOLD,
      otherDN: '1000'
    },
    {
      state: XucPhoneState.STATE_ESTABLISHED,
      otherDN: '1009'
    }];

    let mailTemplate = {
      subject : 'subject {callernum}',
      body : 'body {dstname}'
    };

    spyOn(applicationConfiguration, 'getCurrentAppConfig').and.returnValue({mailTemplate: mailTemplate});
    spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);

    let {innerScope} = sampleContactDirectiveBind();
    expect(decodeURI(innerScope.mailToLink)).toBe('mailTo:noreply@avencall.com?subject=subject 1000&body=body User 1');
  });

  it('generates meeting room share link', function() {
    let roomContact = ContactBuilder("My room", "room", "4004", true);

    let {innerScope} = sampleContactDirectiveBind(roomContact);

    expect(innerScope.generateMeetingShareLink()).toBe("http://localhost/invitation/video/eyJ0eXA");
  });


  it('dials number when not a meeting room', function() {
    let contact =  ContactBuilder("My contact", "con", "1004", true);

    let {innerScope} = sampleContactDirectiveBind(contact);
    spyOn(callContext, "normalizeDialOrAttTrans");
    innerScope.dial("1004");
    expect(callContext.normalizeDialOrAttTrans).toHaveBeenCalledWith("1004");

  });


  it('starts jitsi conference when contact is a personal meeting room', function() {
    let roomContact = ContactBuilder("My room", "room", "4004", true, "Available", true);

    let {innerScope} = sampleContactDirectiveBind(roomContact);
    spyOn(jitsiProxy, "startVideo").and.returnValue($q.reject('error'));
    innerScope.join();
    expect(jitsiProxy.startVideo).toHaveBeenCalledWith('room', 'personal');
  });

  it('starts jitsi conference when contact is a static meeting room', function() {
    let roomContact = ContactBuilder("My room", "room", "4004", true, "Busy", false);

    let {innerScope} = sampleContactDirectiveBind(roomContact);
    spyOn(jitsiProxy, "startVideo").and.returnValue($q.reject('error'));
    innerScope.join();
    expect(jitsiProxy.startVideo).toHaveBeenCalledWith('room', 'static');
  });

  it('should normalize number on dial', function(){
    spyOn(CtiProxy,'dial');
    let {innerScope} = sampleContactDirectiveBind();

    innerScope.dial("+33789456212");

    expect(CtiProxy.dial).toHaveBeenCalledWith("0033789456212", {});
  });

  it('should normalize number on attended Transfer', function(){
    spyOn(CtiProxy,'attendedTransfer');
    let {innerScope} = sampleContactDirectiveBind();

    innerScope.attendedTransfer("+33789456212");

    expect(CtiProxy.attendedTransfer).toHaveBeenCalledWith("0033789456212");
  });
  it('should normalize number on direct Transfer', function(){
    spyOn(CtiProxy,'directTransfer');
    let {innerScope} = sampleContactDirectiveBind();

    innerScope.directTransfer("+33789456212");

    expect(CtiProxy.directTransfer).toHaveBeenCalledWith("0033789456212");
  });

  it('should put on hold calls when joining meeting romm', function() {
    let calls = [{
      state: XucPhoneState.STATE_ONHOLD,
      otherDN: '1000',
      uniqueId: 123
    },
    {
      state: XucPhoneState.STATE_ONHOLD,
      otherDN: '1111',
      uniqueId: 456
    }];

    let roomContact = ContactBuilder("My room", "room", "4004", true);

    let {innerScope} = sampleContactDirectiveBind(roomContact);
    spyOn(jitsiProxy, "startVideo").and.returnValue($q.resolve());
    spyOn(XucPhoneState, 'getCallsNotOnHold').and.returnValue(calls);
    spyOn(CtiProxy, 'hold');
    innerScope.join();

    expect(CtiProxy.hold.calls.count()).toEqual(2);
    expect(CtiProxy.hold).toHaveBeenCalledWith(123);
    expect(CtiProxy.hold).toHaveBeenCalledWith(456);
  });

});
