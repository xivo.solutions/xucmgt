import 'xchelper/helper.module';

describe('failed destination directive', () => {
  
  var scope;
  var $rootScope;
  var XucFailedDestination;
  var $q;
  var $compile;
  var directiveScope;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('Agent'));

  beforeEach(function() {

    angular.mock.inject(function (_$compile_, _$rootScope_, _XucFailedDestination_, _$q_) {
      $compile = _$compile_;
      $rootScope = _$rootScope_;
      $q = _$q_;
      XucFailedDestination = _XucFailedDestination_;
      scope = $rootScope.$new();
      scope.$index = 1;
      scope.activity = {"id":6, "name":"queue1", "number":"2000",};
      scope.dissuasion = { open : false};

      var elementStr = angular.element('<failed-destination activity="activity" dissuasion="dissuasion" ></failed-destination>');
      var element = $compile(elementStr)(scope);
      scope.$digest();
      directiveScope = element.children().scope();
    });

  });

  it('get the list of dissuasion soundFile', function() {
    const expected = {
      soundFiles: [
        {
          "fileName": "promotion_soundFile1",
          "displayName": "soundFile1",
          "selected": false
        },
        {
          "fileName": "promotion_soundFile2",
          "displayName": "soundFile2",
          "selected": false
        }
      ],
      queues: [
        {
          "name": "switchboard",
          "selected": true
        }
      ]
    };

    var result = $q.defer();
    result.resolve(expected);

    spyOn(XucFailedDestination, 'getListDissuasion').and.callFake(() => {
      return result.promise;
    });
    
    directiveScope.getDissuasionList();
    expect(directiveScope.emptyDissuasion).toBeFalsy();

    $rootScope.$apply();
    expect(directiveScope.dissuasion_list).toBe(expected);
    expect(directiveScope.emptyDissuasion).toBeFalsy();
  });

  it('gets the empty list of dissuasion soundFile and queues', function() {

    const returned = {soundFiles: [], queues: []};

    var result = $q.defer();
    result.resolve(returned);

    spyOn(XucFailedDestination, 'getListDissuasion').and.callFake(() => {
      return result.promise;
    });

    directiveScope.getDissuasionList();
    expect(directiveScope.emptyDissuasion).toBeFalsy();

    $rootScope.$apply();
    expect(directiveScope.dissuasion_list).toEqual(returned);
    expect(directiveScope.emptyDissuasion).toBeTruthy();
  });

  it('verifies that the right url is called on click on a sound file from the dissuasion list', function() {
    var fileName = 'name';
    var idQueue = 3;

    spyOn(XucFailedDestination, 'setSoundFileDissuasion');
    
    directiveScope.setDissuasion(idQueue, fileName);

    expect(XucFailedDestination.setSoundFileDissuasion).toHaveBeenCalledWith(idQueue, fileName);
  });
  
  it('verifies that the right url is called on click on the default queue in dissuasion list', function() {
    var queueName = 'queue1';
    var idQueue = 3;

    spyOn(XucFailedDestination, 'setQueueDestination');
    
    directiveScope.changeDissuasionToDefault(idQueue, queueName);

    expect(XucFailedDestination.setQueueDestination).toHaveBeenCalledWith(idQueue, queueName);
  });

  it('verifies that the default queue dissuasion exists and is different from given', function() {
    var activityName = 'queue1';
    var queueName = 'queue2';

    expect(directiveScope.displayDefaultQueue(activityName, queueName)).toBeTruthy();
  });

  it('verifies that the default queue dissuasion exists and is similar to given', function() {
    var activityName = 'queue1';
    var queueName = 'queue1';
    
    expect(directiveScope.displayDefaultQueue(activityName, queueName)).toBeFalsy();
  });

  
  it('verifies that the list stays empty when there is no default queue or sound file', function() {
    directiveScope.dissuasion_list = {
      queues: [],
      soundFiles: []
    };
    expect(directiveScope.isDissuasionListEmpty("queue2")).toBeTruthy();
  });

  it('verifies that the list is not empty when there is no default queue and sound files', function() {
    directiveScope.dissuasion_list = {
      queues: [],
      soundFiles: [{fileName: "music_meeting"}]
    };

    expect(directiveScope.isDissuasionListEmpty("queue2")).toBeFalsy();
  });

  it('verifies that the list is not empty when there is a default queue and no sound files', function() {
    directiveScope.dissuasion_list = {
      queues: [{name: "queue1"}],
      soundFiles: []
    };

    expect(directiveScope.isDissuasionListEmpty("queue2")).toBeFalsy();
  });

  it('verifies that the list is empty when there is a default queue but not displayed', function() {
    directiveScope.dissuasion_list = {
      queues: [{name: "queue1"}],
      soundFiles: []
    };

    expect(directiveScope.isDissuasionListEmpty("queue1")).toBeTruthy();
  });

  it('inits the optional attribute dissuasion', function() {
    var localElem = angular.element('<failed-destination activity="activity"></failed-destination>');
    scope.dissuasion = undefined;
    var compiledElem = $compile(localElem)(scope);
    scope.$digest();
    directiveScope = compiledElem.children().scope();

    expect(directiveScope.dissuasion.open).toBeFalsy();
  });

  it('verifies that the dissuasion list is closed', function() {
  
    spyOn(directiveScope, 'closedissuasiondropdown');
  
    directiveScope.dissuasionToggle(false);

    $rootScope.$digest();

    expect(directiveScope.closedissuasiondropdown).toHaveBeenCalled();
  });

  it('verifies that the dissuasion list is open', function() {
    spyOn(directiveScope, 'opendissuasiondropdown');
 
    directiveScope.dissuasionToggle(true);

    $rootScope.$digest();
    
    expect(directiveScope.opendissuasiondropdown).toHaveBeenCalled();
  });
  
});