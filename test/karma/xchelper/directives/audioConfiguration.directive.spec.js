describe('audio configuration directive', () => {
  var $compile;
  var $rootScope;
  var el;
  var scope;
  var deferred;
  var mediaDevices;
  var webRtcAudio;
  var xcHelperPreferences;

  const devices = [{
    "deviceId": "default",
    "kind": "audiooutput",
    "label": "Par défaut",
    "groupId": "default"
  }, {
    "deviceId": "05841a6c85ff6dec33856edc2402a2649acde8e14e305283d4a67cd73b187ad9",
    "kind": "audiooutput",
    "label": "Audio interne Stéréo analogique",
    "groupId": "52f4abb78fcf201f3ddb3f45bafff3d512f824295ebc89ed28ea512b4e5604b5"
  }];

  function compile(directive) {
    el = angular.element(directive);
    $compile(el)($rootScope.$new());
    $rootScope.$digest();
    scope = el.isolateScope() || el.scope();
  }

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function (_mediaDevices_, _webRtcAudio_, _xcHelperPreferences_, _$compile_, _$rootScope_, _$q_) {
    mediaDevices = _mediaDevices_;
    webRtcAudio = _webRtcAudio_;
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    deferred = _$q_.defer();
    xcHelperPreferences = _xcHelperPreferences_;
  }));

  it('get list of output audio devices', () => {
    spyOn(mediaDevices, 'getAudioOutput').and.returnValue(deferred.promise);
    compile('<audio-configuration></audio-configuration>');
    deferred.resolve(devices);
    $rootScope.$apply();

    expect(mediaDevices.getAudioOutput).toHaveBeenCalled();
    expect(scope.ringtoneDevices).toEqual(devices);
  });

  it('get initial ringing device id', () => {
    let selectedDevice = '05841a6c85ff6dec33856edc2402a2649acde8e14e305283d4a67cd73b187ad9';
    spyOn(xcHelperPreferences, 'getRingingDeviceId').and.returnValue(selectedDevice);
    compile('<audio-configuration></audio-configuration>');

    expect(xcHelperPreferences.getRingingDeviceId).toHaveBeenCalled();
    expect(scope.ringingDeviceId).toEqual(selectedDevice);
  });

  it ('set device id and save it if it success', () => {
    let selectedDevice = '05841a6c85ff6dec33856edc2402a2649acde8e14e305283d4a67cd73b187ad9';
    spyOn(webRtcAudio, 'changeRingingDevice').and.returnValue(deferred.promise);
    spyOn(xcHelperPreferences, 'setRingingDeviceId');
    compile('<audio-configuration></audio-configuration>');
    scope.changeRingingDevice(selectedDevice);

    deferred.resolve();
    $rootScope.$apply();

    expect(webRtcAudio.changeRingingDevice).toHaveBeenCalledWith(selectedDevice);
    expect(xcHelperPreferences.setRingingDeviceId).toHaveBeenCalledWith(selectedDevice);
  });

  it ('set device id and not save it if it failed', () => {
    let selectedDevice = '05841a6c85ff6dec33856edc2402a2649acde8e14e305283d4a67cd73b187ad9';
    spyOn(webRtcAudio, 'changeRingingDevice').and.returnValue(deferred.promise);
    spyOn(xcHelperPreferences, 'setRingingDeviceId');
    compile('<audio-configuration></audio-configuration>');
    scope.changeRingingDevice(selectedDevice);

    deferred.reject('fail');
    $rootScope.$apply();

    expect(webRtcAudio.changeRingingDevice).toHaveBeenCalledWith(selectedDevice);
    expect(xcHelperPreferences.setRingingDeviceId).not.toHaveBeenCalled();
  });
});