import angular from 'angular';
import 'xchelper/helper.module';

describe('badge counter directive', () => {
  var $rootScope;
  var $scope;
  var $compile;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_$rootScope_, _$compile_) {
    $compile = _$compile_;
    $scope = _$rootScope_.$new();
    $rootScope = _$rootScope_;
  }));

  it('should display badge value', function() {
    $scope.val = 3;
    var directiveElem = $compile('<count-badge value="val"></count-badge>')($scope);
    $rootScope.$digest();
    expect(directiveElem).toBeDefined();
    expect(directiveElem.text()).toEqual("3");
  });

  it('should display 0 if no value', function() {
    var directiveElem = $compile('<count-badge></count-badge>')($scope);
    $rootScope.$digest();
    expect(directiveElem).toBeDefined();
    expect(directiveElem.text()).toEqual("0");
  });
});