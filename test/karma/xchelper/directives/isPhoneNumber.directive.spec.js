describe('is phone number directive', () => {

  var $scope;
  var $compile;
  var xucUtils;
  var directiveElement, directiveElementInput;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function() {

    angular.mock.inject(function (_XucUtils_, _$compile_, $rootScope) {
      $scope = $rootScope;
      xucUtils = _XucUtils_;
      $compile = _$compile_;
    });

    $scope.model = {value: undefined};

    directiveElement = angular.element('<form name="form"><input ng-model="model.value" type="text" is-phone-number /></form>');
    $compile(directiveElement)($scope);
    $scope.$digest();

    directiveElementInput = directiveElement.find('input');

  });

  it ('return true when the input is empty', function() {
   
    expect(directiveElementInput.hasClass('ng-valid')).toBeTruthy();
    expect(directiveElementInput.hasClass('ng-valid-is-phone-number')).toBeTruthy();    
  });

  it('returns true when the input contains a phone number', function() {
    
    spyOn(xucUtils, 'isaPhoneNb').and.returnValue(true);
    angular.element(directiveElementInput).val('+12 3').triggerHandler('input');
    $scope.$apply();

    expect(directiveElementInput.hasClass('ng-valid')).toBeTruthy();
    expect(directiveElementInput.hasClass('ng-valid-is-phone-number')).toBeTruthy();
  });

  it('returns false when the input does NOT contain a phone number', function() {
    
    spyOn(xucUtils, 'isaPhoneNb').and.returnValue(false);
    angular.element(directiveElementInput).val('+12d 3').triggerHandler('input');
    $scope.$apply();

    expect(directiveElementInput.hasClass('ng-valid')).toBeFalsy();
    expect(directiveElementInput.hasClass('ng-valid-is-phone-number')).toBeFalsy();
  });
  
});