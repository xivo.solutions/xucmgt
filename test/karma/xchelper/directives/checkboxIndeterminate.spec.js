import angular from 'angular';
import 'xchelper/helper.module';

describe('checkbox indeterminate directive', () => {
  var $scope;
  var $compile;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_$rootScope_, _$compile_) {
    $compile = _$compile_;
    $scope = _$rootScope_.$new();
  }));

  it('should toggle the indeterminate property', function() {
    var element = $compile('<input type="checkbox" indeterminate="value" />')($scope);
    expect(element[0].indeterminate).toBeFalsy();
    $scope.value = true;
    $scope.$apply();
    expect(element[0].indeterminate).toBe(true);
    $scope.value = false;
    $scope.$apply();
    expect(element[0].indeterminate).toBe(false);
  });
});