import { VolumeData } from '../../../../app/assets/javascripts/xchelper/services/processVolume.factory';

describe('volumeMeter directive', () => {
  var $compile;
  var $rootScope;
  var el;
  var innerScope;
  var processVolume;
  var outerScope;
  var XucPhoneState;
  var audioDirective = "<volume-meter sip-call-id='test-sipcallid' call-state='call.state'></volume-meter>";

  function compile(directive) {
    el = angular.element(directive);
    $compile(el)(outerScope);
    $rootScope.$digest();
    innerScope = el.isolateScope() || el.scope();
  }

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));

  beforeEach(angular.mock.inject(function(_$compile_, _$rootScope_, _processVolume_, _XucPhoneState_) {
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    processVolume = _processVolume_;
    XucPhoneState = _XucPhoneState_;
    outerScope = $rootScope.$new();
  }));

  it('should receive and store the volume data when they are broadcasted', () => {
    compile(audioDirective);

    let outputVolumeResult = new VolumeData('test-sipcallid', 50);
    $rootScope.$broadcast(processVolume.EVENT_OUTPUT_VOLUME, outputVolumeResult);
    $rootScope.$digest();
    expect(innerScope.outputVolume).toEqual(outputVolumeResult.volume);

    let inputVolumeResult = new VolumeData('test-sipcallid', 70);
    $rootScope.$broadcast(processVolume.EVENT_INPUT_VOLUME, inputVolumeResult);
    $rootScope.$digest();
    expect(innerScope.inputVolume).toEqual(inputVolumeResult.volume);
  });

  it('should call the service when the call state is established', () => {
    outerScope.call = {state: XucPhoneState.STATE_ESTABLISHED};
    spyOn(processVolume, 'monitorCall');
    compile(audioDirective);
    expect(processVolume.monitorCall).toHaveBeenCalled();
  });

  it('should not call the service if the call state is not yet established', () => {
    outerScope.call = { state: XucPhoneState.STATE_DIALING };
    spyOn(processVolume, 'monitorCall');
    compile(audioDirective);
    expect(processVolume.monitorCall).not.toHaveBeenCalled();
  });

  it('should call the service if the call state was not established but is established afterward', () => {
    outerScope.call = { state: XucPhoneState.STATE_DIALING };
    spyOn(processVolume, 'monitorCall');
    compile(audioDirective);
    outerScope.call.state = XucPhoneState.STATE_ESTABLISHED;
    $rootScope.$digest();
    expect(processVolume.monitorCall).toHaveBeenCalled();
  });

});
