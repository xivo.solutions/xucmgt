import angular from 'angular';

describe('containerResizable directive', () => {
  var $rootScope;
  var $compile;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('Agent'));

  beforeEach(angular.mock.inject((_$rootScope_, _$compile_) => {
    $rootScope = _$rootScope_;
    $compile = _$compile_;

  }));

  it('should return the result of the new content view height', () => {
    var elem = angular.element('<div class="ccagent-container main-app" container-resizable ng-cloak"><div class="header-container"></div><div ui-view class="content-view"></div></div>');
    var scope = $rootScope.$new();
    $compile(elem)(scope);

    let contentView = {
      style: {
        height: 0
      }
    };

    elem.isolateScope().applySize(1000, contentView, 200);
    expect(contentView.style.height).toBe('800px');
  });
  
});
