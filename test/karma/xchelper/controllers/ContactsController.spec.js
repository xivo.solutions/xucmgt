describe('Contacts controller', function () {
  var $rootScope;
  var $scope;
  var applicationConfiguration;
  var ctrl;
  var $state;
  var $stateParams;
  var XucDirectory;
  var ContactService;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('ucAssistant'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_$rootScope_, $controller, _applicationConfiguration_, _$stateParams_, _XucDirectory_, _$state_, _ContactService_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $rootScope = _$rootScope_;
    applicationConfiguration = _applicationConfiguration_;
    $stateParams = _$stateParams_;
    XucDirectory = _XucDirectory_;
    ContactService = _ContactService_;
    $state = _$state_;

    ctrl = $controller('ContactsController', {
      '$scope' :                  $scope,
      '$rootScope' :              $rootScope,
      'applicationConfiguration': applicationConfiguration,
      '$stateParams':             $stateParams,
      'XucDirectory':             XucDirectory,
      'ContactService':           ContactService,
      '$state':                   $state
    });
  }));

  it('can instantiate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });

  it('should navigate to chat when clicking on the chat icon', function() {
    spyOn($state, 'go');
    var $event = jasmine.createSpyObj('$event', ['stopPropagation']);

    var contact = {
      actions: {
        Chat: {
          args: ['someUsername']
        }
      }
    };

    var chatAction = $scope.actionsProperties['Chat'];
    chatAction.onClick(contact, $event);

    expect($state.go).toHaveBeenCalledWith('interface.conversation', { remoteParty: 'someUsername' });
  });

  it('should add all sources from a contact to favorite', () => {
    spyOn(Cti, 'addFavorite');
    const partialContact = {
      sources: [
        { id: 1, name: "source1" },
        { id: 2, name: "source2" },
      ]
    };
    $scope.addFavorite(partialContact, ({ stopPropagation: () => { } }));
    expect(Cti.addFavorite).toHaveBeenCalledWith(1, "source1");
    expect(Cti.addFavorite).toHaveBeenCalledWith(2, "source2");
  });

  it('should remove all sources from a contact to favorite', () => {
    spyOn(Cti, 'removeFavorite');
    const partialContact = {
      sources: [
        { id: 1, name: "source1" },
        { id: 2, name: "source2" },
      ]
    };
    $scope.removeFavorite(partialContact, ({ stopPropagation: () => { } }));
    expect(Cti.removeFavorite).toHaveBeenCalledWith(1, "source1");
    expect(Cti.removeFavorite).toHaveBeenCalledWith(2, "source2");
  });

});
