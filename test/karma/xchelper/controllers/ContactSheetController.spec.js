describe('Contact Sheet controller', function () {
  var ctrl;

  var $rootScope;
  var $scope;
  var $controller;
  var $uibModalInstance;
  var $state;
  var applicationConfiguration;
  var toast;
  var $translate;
  var XucLink;
  var contact;
  var ContactService;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('ucAssistant'));

  beforeEach(angular.mock.inject(function(_$rootScope_, _$controller_, _$state_, _applicationConfiguration_, _toast_, _$translate_, _XucLink_, _ContactService_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $controller = _$controller_;
    $state = _$state_;
    applicationConfiguration = _applicationConfiguration_;
    toast = _toast_;
    $translate = _$translate_;
    XucLink = _XucLink_;
    ContactService = _ContactService_;

    // Mock dependencies
    $uibModalInstance = { dismiss: jasmine.createSpy('dismiss') };
    contact = { name: "Test Contact", details: [], actions: {} };

    ctrl = $controller('contactSheetController', {
      '$scope': $scope,
      'contact': contact,
      'ContactService': ContactService,
      '$uibModalInstance': $uibModalInstance,
      '$state': $state,
      'applicationConfiguration': applicationConfiguration,
      'toast': toast,
      '$translate': $translate,
      'XucLink': XucLink
    });
  }));

  it('can instantiate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });

  it('should navigate to chat with contact when clicking on the chat icon', function() {
    spyOn($state, 'go');

    var contact = {
      actions: {
        Chat: {
          args: ['someUsername']
        }
      }
    };
    var $event = jasmine.createSpyObj('$event', ['stopPropagation']);
    var chatAction = $scope.actionsProperties['Chat'];
    $scope.appConfig = { routing: 'app' };

    chatAction.onClick(contact, $event);

    expect($state.go).toHaveBeenCalledWith('interface.conversation', { remoteParty: 'someUsername' });
    expect($event.stopPropagation).toHaveBeenCalled();
  });

  it('should close the contact sheet when clicking on Call | Video | VideoInvite | AudioInvite' , () => {
    var callAction = $scope.actionsProperties['Call'];
    var videoAction = $scope.actionsProperties['Video'];
    var videoInviteAction = $scope.actionsProperties['VideoInvite'];
    var audioinviteAction = $scope.actionsProperties['AudioInvite'];
    var $event = jasmine.createSpyObj('$event', ['stopPropagation']);
    var contact = {
      actions: {
        Call: {
          args: ['someUsername']
        },
        Video: {
          args: ['someUsername']
        },
        VideoInvite: {
          args: ['someUsername']
        },
        AudioInvite: {
          args: ['someUsername']
        },
      }
    };

    callAction.onClick(contact, $event);
    expect($scope.contactSheetOnClose()).toHaveBeenCalled;

    videoAction.onClick(contact, $event);
    expect($scope.contactSheetOnClose()).toHaveBeenCalled;

    videoInviteAction.onClick(contact, $event);
    expect($scope.contactSheetOnClose()).toHaveBeenCalled;

    audioinviteAction.onClick(contact, $event);
    expect($scope.contactSheetOnClose()).toHaveBeenCalled;

  });

  it('should display Edit action if contact is personal', () => {
    contact.isPersonal = true;

    var editAction = $scope.actionsProperties['Edit'];
    expect(editAction.displayCondition(contact)).toBe(true);

    contact.isPersonal = false;
    expect(editAction.displayCondition(contact)).toBe(false);
  });

  it('should display Call action for normal user', () => {
    contact.actions = {
      Call: {
        args: ["1001"],
        disable: false
      },
    };
    contact.isMeetingroom = false;

    spyOn(ContactService, 'meetingRoomWithCallIcon').and.returnValue(false);

    var callAction = $scope.actionsProperties['Call'];
    expect(callAction.displayCondition(contact)).toBe(true);
  });

  it('should display Call action for meeting room', () => {
    contact.actions = {
      Call: {
        args: ["9000"],
        disable: false
      },
    };
    contact.isMeetingroom = true;


    spyOn(ContactService, 'userWithCallIcon').and.returnValue(false);
    spyOn(ContactService, 'isJitsiAvailable').and.returnValue(true);

    var callAction = $scope.actionsProperties['Call'];
    expect(callAction.displayCondition(contact)).toBe(true);
  });

  it('should display Video action if jitsi is available', () => {
    contact.actions = {
      Video: {
        args: ["9000"],
        disable: false
      },
    };

    spyOn(ContactService, 'isJitsiAvailable').and.returnValues(true, false);

    var videoAction = $scope.actionsProperties['Video'];
    expect(videoAction.displayCondition(contact)).toBe(true);
    expect(videoAction.displayCondition(contact)).toBe(false);
  });

  it('should display Chat action', () => {
    contact.actions = {
      Chat: {
        args: ["bwillis"],
        disable: false
      },
    };

    var chatAction = $scope.actionsProperties['Chat'];
    expect(chatAction.displayCondition(contact)).toBe(true);
  });

  it('should display Mail action', () => {
    contact.actions = {
      Mail: {
        args: ["bwillis@gmail.com"],
        disable: false
      },
    };

    var mailAction = $scope.actionsProperties['Mail'];
    expect(mailAction.displayCondition(contact)).toBe(true);
  });

  it('should display ShareLink action', () => {
    contact.actions = {
      ShareLink: {
        args: ["/meet?id=6eh8-zflo"],
        disable: false
      },
    };

    var shareLinkAction = $scope.actionsProperties['ShareLink'];
    expect(shareLinkAction.displayCondition(contact)).toBe(true);
  });

  it('should display VideoInvite action', () => {
    contact.actions = {
      VideoInvite: {
        args: ["9000"],
        disable: false
      },
    };

    spyOn(ContactService, 'isJitsiAvailable').and.returnValues(true, false);
    spyOn(ContactService, 'canInviteToMeetingRoom').and.returnValues(true, false);

    var videoInviteAction = $scope.actionsProperties['VideoInvite'];
    expect(videoInviteAction.displayCondition(contact)).toBe(true);
    expect(videoInviteAction.displayCondition(contact)).toBe(false);
  });

  it('should display AudioInvite action for user', () => {
    contact.actions = {
      AudioInvite: {
        args: ["9000"],
        disable: false
      },
    };
    contact.isMeetingroom = false;

    spyOn(ContactService, 'canInviteToConference').and.returnValue(true);
    spyOn(ContactService, 'isCallable').and.returnValue(true);

    var audioInviteAction = $scope.actionsProperties['AudioInvite'];
    expect(audioInviteAction.displayCondition(contact)).toBe(true);
  });

  it('should display AudioInvite action for meetingroom', () => {
    contact.actions = {
      AudioInvite: {
        args: ["9000"],
        disable: false
      },
    };
    contact.isMeetingroom = true;

    spyOn(ContactService, 'canInviteToConference').and.returnValue(true);
    spyOn(ContactService, 'isCallable').and.returnValue(true);
    spyOn(ContactService, 'isJitsiAvailable').and.returnValue(true);

    var audioInviteAction = $scope.actionsProperties['AudioInvite'];
    expect(audioInviteAction.displayCondition(contact)).toBe(true);
  });

  it('should add all sources from a contact to favorite', () => {
    spyOn(Cti, 'addFavorite');
    contact.sources = [
      { id: 1, name: "source1" },
      { id: 2, name: "source2" },
    ];
    $scope.contactSheetAddFavorite(contact, ({ stopPropagation: () => { } }));
    expect(Cti.addFavorite).toHaveBeenCalledWith(1, "source1");
    expect(Cti.addFavorite).toHaveBeenCalledWith(2, "source2");
  });

  it('should remove all sources from a contact to favorite', () => {
    spyOn(Cti, 'removeFavorite');
    contact.sources = [
      { id: 1, name: "source1" },
      { id: 2, name: "source2" },
    ];
    $scope.contactSheetRemoveFavorite(contact, ({ stopPropagation: () => { } }));
    expect(Cti.removeFavorite).toHaveBeenCalledWith(1, "source1");
    expect(Cti.removeFavorite).toHaveBeenCalledWith(2, "source2");
  });
});
