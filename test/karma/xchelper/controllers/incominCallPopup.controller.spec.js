
describe('Incoming call popup controller', function() {
  var $rootScope;
  var $scope;
  var ctrl;
  var CtiProxy;
  var modalCloseMethod;
  var JitsiProxy;
  var XucPhoneState;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_$rootScope_, $controller, _CtiProxy_, _JitsiProxy_, _XucPhoneState_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    XucPhoneState = _XucPhoneState_;
    CtiProxy = _CtiProxy_;
    JitsiProxy = _JitsiProxy_;

    modalCloseMethod = jasmine.createSpy();
    ctrl = $controller('IncomingCallPopup', {
      '$scope' : $scope,
      '$uibModalInstance': {dismiss: modalCloseMethod},
      'incomingCallNumber': '23',
      'incomingCallName': 'incName',
      'uniqueId': '456789',
      'callType': xc_webrtc.mediaType.AUDIO,
      'requestId': 1,
      'username' : 'cjohnson',
      'token' : 'mytoken'
    });
  }));

  it('can instantiate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

  it('answers and close', function() {
    spyOn(CtiProxy, 'answer');
    $scope.accept();
    expect(CtiProxy.answer).toHaveBeenCalled();
    expect(modalCloseMethod).toHaveBeenCalledWith('accept');
  });

  it('hangup and close', function() {
    spyOn(CtiProxy, 'hangup');
    $scope.decline();
    expect(CtiProxy.hangup).toHaveBeenCalled();
    expect(modalCloseMethod).toHaveBeenCalledWith('decline');
  });

  it('hides the answer button on the ucassistant when a second call is incoming and the device is yealink', function() {
    
    $scope.calls = [{
      call1 : 'call1'
    }, {
      call2 : 'call2'
    }];
    
    $scope.currentDevice = 'Yealink';
    $scope.hideAnswer();
    
    expect($scope.hideAnswer()).toBeTruthy();
    
  });

  it('does not hide the answer button on ucassistant when there is only one call', function() {

    $scope.calls = [{
      call1 : 'call1'
    }];
    
    $scope.currentDevice = 'Yealink';
    $scope.hideAnswer();
    expect($scope.hideAnswer()).toBeFalsy();
  });

  it('displays a specific message on second call if the device is yealink', function() {
    $scope.calls = [{
      call1 : 'call1'
    }, {
      call2 : 'call2'
    }];
    $scope.currentDevice = 'Yealink';
    $scope.displayMessage();
    
    expect($scope.displayMessage()).toBeTruthy();
    expect($scope.maxAnswerableCalls).toBe(1);
  });

  it('accepts invitation to the meeting room and close', function() {
    spyOn(JitsiProxy, 'acceptInvitation');
    spyOn($scope, "isVideoCall").and.returnValue(true);
    $scope.accept();
    expect(JitsiProxy.acceptInvitation).toHaveBeenCalled();
    expect(modalCloseMethod).toHaveBeenCalledWith('accept');
  });

  it('timeout the invitation by dismissing the modal and close', function() {
    spyOn(JitsiProxy, 'acceptInvitation');
    spyOn(JitsiProxy, 'rejectInvitation');
    spyOn($scope, "isVideoCall").and.returnValue(true);
    $scope.cancel();
    expect(JitsiProxy.acceptInvitation).not.toHaveBeenCalled();
    expect(JitsiProxy.rejectInvitation).not.toHaveBeenCalled();
    expect(modalCloseMethod).toHaveBeenCalledWith('cancel');
  });


  it('rejects invitation to the meeting room and close', function() {
    spyOn(JitsiProxy, 'rejectInvitation');
    spyOn($scope, "isVideoCall").and.returnValue(true);
    $scope.decline();
    expect(JitsiProxy.rejectInvitation).toHaveBeenCalled();
    expect(modalCloseMethod).toHaveBeenCalledWith('decline');
  });

  it('puts the ongoing audio call on hold if accepting a video call', function() {
    var call = [{
      state : 'Established',
      uniqueId: '123'
    }];
    spyOn(XucPhoneState, 'getCallsNotOnHold').and.returnValue(call);

    spyOn(CtiProxy, 'hold');
    $rootScope.$digest();

    spyOn(JitsiProxy, 'acceptInvitation');
    spyOn($scope, "isVideoCall").and.returnValue(true);
    $scope.accept();

    expect(CtiProxy.hold).toHaveBeenCalledWith(call[0].uniqueId);
  }); 
});
