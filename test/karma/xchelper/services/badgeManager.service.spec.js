describe('change badge', function () {
  var $rootScope;
  var electronWrapper;
    
  
  beforeEach(angular.mock.module('xcCti', 'xcChat', 'xcHelper'));
  beforeEach(angular.mock.inject(function(_$rootScope_ , _electronWrapper_) {
    electronWrapper = _electronWrapper_;
    $rootScope = _$rootScope_;
  }));
  
  it('should enable the missed message badge when a message is missed', function () {
    spyOn(electronWrapper, 'setTrayIcon');
    $rootScope.$broadcast("ChatUnreadMessage", 1);
    expect(electronWrapper.setTrayIcon).toHaveBeenCalledWith('missed');
  });
    
  it('should disable the missed message badge when no messages are missed', function () {
    spyOn(electronWrapper, 'setTrayIcon');
    $rootScope.$broadcast("ChatUnreadMessage", 0);
    expect(electronWrapper.setTrayIcon).toHaveBeenCalledWith('default');
  });
  
  it('should enable the missed call badge when a call is missed', function () {
    spyOn(electronWrapper, 'setTrayIcon');
    $rootScope.$broadcast("newNbMissedCalls", 1);
    expect(electronWrapper.setTrayIcon).toHaveBeenCalledWith('missed');
  });
    
  it('should disable the missed call badge when no calls are missed', function () {
    spyOn(electronWrapper, 'setTrayIcon');
    $rootScope.$broadcast("newNbMissedCalls", 0);
    expect(electronWrapper.setTrayIcon).toHaveBeenCalledWith('default');
  });
  
  it('should disable the missed call badge when no calls and no messages are missed', function () {
    spyOn(electronWrapper, 'setTrayIcon');
    $rootScope.$broadcast("newNbMissedCalls", 0);
    $rootScope.$broadcast("ChatUnreadMessage", 0);
    expect(electronWrapper.setTrayIcon).toHaveBeenCalledWith('default');
  });
  
  it('should disable the missed call badge when  calls and  messages are missed', function () {
    spyOn(electronWrapper, 'setTrayIcon');
    $rootScope.$broadcast("newNbMissedCalls", 1);
    $rootScope.$broadcast("ChatUnreadMessage", 2);
    expect(electronWrapper.setTrayIcon).toHaveBeenCalledWith('missed');
  });
  
  it('should disable the missed call badge when there are no missed calls but missed messages', function () {
    spyOn(electronWrapper, 'setTrayIcon');
    $rootScope.$broadcast("newNbMissedCalls",0 );
    $rootScope.$broadcast("ChatUnreadMessage", 2);
    expect(electronWrapper.setTrayIcon).toHaveBeenCalledWith('missed');
  });
  
  it('should disable the missed call badge when there are no missed messages but missed calls', function () {
    spyOn(electronWrapper, 'setTrayIcon');
    $rootScope.$broadcast("newNbMissedCalls",2 );
    $rootScope.$broadcast("ChatUnreadMessage", 0);
    expect(electronWrapper.setTrayIcon).toHaveBeenCalledWith('missed');
  });
    
});