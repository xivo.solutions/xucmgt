describe('audio device manager', function () {
  var $rootScope;
  var audioDevicesMgrService;
  var mediaDevices;
  var localStorageService;
  var processVolume;
  var $window;

  let expectedAudioInput = {
    deviceId:
    "c964a067e333b6868ef8c6703d19a0013b033ddc4760a62a0df4d13d5bef9b31",
    kind: "audioinput",
    label: "Audio interne Stéréo analogique",
    groupId: "866e0814806f79637c7ff580dd7228ef5aa0ae063dc58660f7b78e703eeff1ad",
  };
  
  let expectedAudioOutput = {
    deviceId:
    "265941e2c29598081a56afaf61f9f1eed0f8e82bd0e55f56dcebbd770472de41",
    kind: "audiooutput",
    label: "Poly BT 700 Digital stereo",
    groupId: "866e0814806f79637c7ff580dd7228ef5aa0ae063dc58660f7b78e703eeff1ad",
  };

  let expectedRingingDevice = {
    deviceId:
    "365941e2c29598081a56afaf61f9f1eed0f8e82bd0e55f56dcebbd770472de41",
    kind: "audiooutput",
    label: "Poly BT 800 next gen",
    groupId: "966e0814806f79637c7ff580dd7228ef5aa0ae063dc58660f7b78e703eeff1ad",
  };
  
  let defaultInput = {
    deviceId: "default",
    kind: "audioinput",
    label: "Par défaut",
    groupId: "default",
  };
  
  let defaultOutput = {
    deviceId: "default",
    kind: "audiooutput",
    label: "Par défaut",
    groupId: "default",
  };

  let defaultRinging = {
    deviceId: "default",
    kind: "audiooutput",
    label: "Par défaut",
    groupId: "default",
  };
  
  let video = {
    deviceId:
    "63de9385d4859fd2c60e8460c2781b86aa2e0dd3554035a62c28db9c347eed6e",
    kind: "videoinput",
    label: "",
    groupId: "47ad004ec25ef221d0af70981d1e35434a4e093fa1d4fcf98f259e9f7ed05467",
  };
  
  let devices = [
    defaultInput,
    defaultOutput,
    expectedAudioInput,
    video,
    expectedAudioOutput,
    expectedRingingDevice
  ];
  
  beforeEach(angular.mock.module("html-templates"));
  beforeEach(angular.mock.module("karma-backend"));
  beforeEach(angular.mock.module("ucAssistant"));

  beforeEach(
    angular.mock.inject(function (
      _$rootScope_,
      $httpBackend,
      _mediaDevices_,
      _localStorageService_,
      _processVolume_,
      _$window_,
      _AudioDevicesMgrService_
    ) {
      $rootScope = _$rootScope_;
      mediaDevices = _mediaDevices_;
      localStorageService = _localStorageService_;
      processVolume = _processVolume_;
      $window = _$window_;
      audioDevicesMgrService = _AudioDevicesMgrService_;
      $httpBackend
        .whenGET("/config/maxImportFileSize")
        .respond({ name: "maxImportFileSize", value: "20000000" }); 

      spyOn(localStorageService, "set").and.callFake(() => {});
    })
  );

  it("retrieve audio devices", function () {
    spyOn(mediaDevices, "getAudioDevices").and.returnValue(
      Promise.resolve(devices)
    );
    
    audioDevicesMgrService.initAudioDevices();
    $rootScope.$digest();
    
    expect(mediaDevices.getAudioDevices).toHaveBeenCalled();
  });
  
  it("set audio devices properly", function () {
    spyOn(localStorageService, "get")
      .withArgs("AudioOutputDeviceId")
      .and.returnValue(expectedAudioOutput.deviceId)
      .withArgs("AudioInputDeviceId")
      .and.returnValue(expectedAudioInput.deviceId)
      .withArgs("ringingDeviceId")
      .and.returnValue(expectedRingingDevice.deviceId);

    var selectedAudioDevice;
    var selectedMicrophoneDevice;
    var selectedRingingDevice;

    $rootScope.$on(audioDevicesMgrService.AUDIO_DEVICE_PICKED_EVT, (_e, device) => {
      selectedAudioDevice = device;
    });

    $rootScope.$on(audioDevicesMgrService.MIC_DEVICE_PICKED_EVT, (_e, device) => {
      selectedMicrophoneDevice = device;
    });

    $rootScope.$on(audioDevicesMgrService.RINGING_DEVICE_PICKED_EVT, (_e, device) => {
      selectedRingingDevice = device;
    });

    audioDevicesMgrService.initPreviousAudioDevices(devices);
    $rootScope.$digest();
    
    expect(localStorageService.get).toHaveBeenCalledWith("AudioOutputDeviceId");
    expect(localStorageService.get).toHaveBeenCalledWith("AudioInputDeviceId");
    expect(localStorageService.get).toHaveBeenCalledWith("ringingDeviceId");
    expect(selectedAudioDevice).toEqual(expectedAudioOutput);
    expect(selectedMicrophoneDevice).toEqual(expectedAudioInput);
    expect(selectedRingingDevice).toEqual(expectedRingingDevice);
  });
  
  it("fallback to default devices if not found", function () {
    let filteredDevices = devices.filter(
      (device) =>
        device !== expectedAudioInput && device !== expectedAudioOutput && device !== expectedRingingDevice
    );
    
    spyOn(localStorageService, "get")
      .withArgs("AudioOutputDeviceId")
      .and.returnValue(expectedAudioOutput.deviceId)
      .withArgs("AudioInputDeviceId")
      .and.returnValue(expectedAudioInput.deviceId)
      .withArgs("ringingDeviceId")
      .and.returnValue(expectedRingingDevice.deviceId);

    var selectedAudioDevice;
    var selectedMicrophoneDevice;
    var selectedRingingDevice;

    $rootScope.$on(audioDevicesMgrService.AUDIO_DEVICE_PICKED_EVT, (_e, device) => {
      selectedAudioDevice = device;
    });

    $rootScope.$on(audioDevicesMgrService.MIC_DEVICE_PICKED_EVT, (_e, device) => {
      selectedMicrophoneDevice = device;
    });

    $rootScope.$on(audioDevicesMgrService.RINGING_DEVICE_PICKED_EVT, (_e, device) => {
      selectedRingingDevice = device;
    });
    
    audioDevicesMgrService.initPreviousAudioDevices(filteredDevices);
    $rootScope.$digest();
    
    expect(localStorageService.get).toHaveBeenCalledWith("AudioOutputDeviceId");
    expect(localStorageService.get).toHaveBeenCalledWith("AudioInputDeviceId");
    expect(localStorageService.get).toHaveBeenCalledWith("ringingDeviceId");
    expect(selectedAudioDevice).toEqual(defaultOutput);
    expect(selectedMicrophoneDevice).toEqual(defaultInput);
    expect(selectedRingingDevice).toEqual(defaultRinging);
  });
  
  it("can change microphone", function () {
    spyOn(xc_webrtc, "swapCurrentMicrophone").and.callFake(() => {
      return new Promise((res) => {
        res();
      });
    });
    spyOn($window.SIPml, "setGlobalMicrophoneDeviceId").and.callFake(() => {});
    spyOn(processVolume, "onMicChange").and.callFake(() => {});
    
    audioDevicesMgrService.microphoneDevices = devices;
    audioDevicesMgrService.changeAudioInputDevice(expectedAudioInput.deviceId);
    $rootScope.$digest();
    
    expect($window.SIPml.setGlobalMicrophoneDeviceId).toHaveBeenCalledWith(
      expectedAudioInput.deviceId
    );
    expect(localStorageService.set).toHaveBeenCalledWith(
      "AudioInputDeviceId",
      expectedAudioInput.deviceId
    );
  });
  
  it("can change audio device", function () {
    spyOn(xc_webrtc, "applyAudioOutputToElements").and.callFake(() => {});

    audioDevicesMgrService.audioDevices = devices;
    audioDevicesMgrService.changeAudioOutputDevice(expectedAudioOutput.deviceId);
    $rootScope.$digest();
    
    expect(localStorageService.set).toHaveBeenCalledWith(
      "AudioOutputDeviceId",
      expectedAudioOutput.deviceId
    );
    expect(xc_webrtc.applyAudioOutputToElements).toHaveBeenCalledWith(
      expectedAudioOutput.deviceId
    );
  });
});