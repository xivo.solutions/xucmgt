describe('callContext', function() {
  var XucPhoneState;
  var XucUtils;
  var CtiProxy;
  var callContext;
  var vars = {a: 21};

  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_callContext_, _XucPhoneState_, _XucUtils_, _CtiProxy_) {
    callContext = _callContext_;
    XucPhoneState = _XucPhoneState_;
    XucUtils = _XucUtils_;
    CtiProxy = _CtiProxy_;
    spyOn(XucPhoneState, 'getCalls').and.returnValue([]);
    spyOn(CtiProxy, 'dial');
  }));

  it('propagates parameters on dial request', function(){
    callContext.dialOrAttTrans('123', vars);
    expect(CtiProxy.dial).toHaveBeenCalledWith('123', vars);
  });

  it('propagates parameters on dial request with fallback on {} for vars', function(){
    callContext.dialOrAttTrans('123');
    expect(CtiProxy.dial).toHaveBeenCalledWith('123', {});
  });

  it('propagates parameters on normalized dial request', function(){
    spyOn(XucUtils, 'normalizePhoneNb').and.returnValue('456');
    callContext.normalizeDialOrAttTrans('123', vars);
    expect(CtiProxy.dial).toHaveBeenCalledWith('456', vars);
  });

  it('propagates parameters on normalized dial request with fallback on {} for vars', function(){
    spyOn(XucUtils, 'normalizePhoneNb').and.returnValue('456');
    callContext.normalizeDialOrAttTrans('123');
    expect(CtiProxy.dial).toHaveBeenCalledWith('456', {});
  });

  it('returns true if one of the contact is a meeting room', () => {
    const contact1 = ContactBuilder("Annabelle Beaumont", "ab", "456", true);
    
    expect(callContext.isMeetingRoom(contact1)).toBe(true);

    const contact2 = ContactBuilder("Marion Dubois", "md", "455", false);

    expect(callContext.isMeetingRoom(contact2)).toBe(false);    
  });
});
