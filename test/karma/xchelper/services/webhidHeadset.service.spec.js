describe('refreshWarning directive', () => {
  var WebHIDHeadsetService;
  var applicationConfiguration;

  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  
  beforeEach(angular.mock.inject(function(_$rootScope_, _$compile_, _$window_, _$log_, _XucLink_, _XucPhoneEventListener_, _CtiProxy_, _electronWrapper_, _WebHIDHeadsetService_, _applicationConfiguration_) {  
    WebHIDHeadsetService = _WebHIDHeadsetService_;
    applicationConfiguration= _applicationConfiguration_;
  }));
  
  it("accepts poly devices and listen for input reports", function(done) {
    var poly = {
      deviceId: 1234,
      vendorId: 1151,
      productName: "Poly test device",
      open: () => {
        let p = new Promise(res => (res()));
        return p;
      },
      addEventListener: (evt) => {
        expect(evt).toEqual("inputreport");
        done();
      }
    };

    spyOn(applicationConfiguration, 'getCurrentAppConfig').and.returnValue({headsetSupportedVendors: [1151]});
    spyOn(window.navigator.hid, "requestDevice").and.returnValue([poly]);
    WebHIDHeadsetService.init();
  });

  it("accepts jabra devices and listen for input reports", function(done) {
    var jabra = {
      deviceId: 1234,
      vendorId: 2830,
      productName: "Jabra test device",
      open: () => {
        let p = new Promise(res => (res()));
        return p;
      },
      addEventListener: (evt) => {
        expect(evt).toEqual("inputreport");
        done();
      }
    };

    spyOn(applicationConfiguration, 'getCurrentAppConfig').and.returnValue({headsetSupportedVendors: [2830]});
    spyOn(window.navigator.hid, "requestDevice").and.returnValue([jabra]);
    WebHIDHeadsetService.init();
  });

  it("turns an integer to binary and gives you the position of the first truthy bit from the right starting from zero", () => {
    expect(WebHIDHeadsetService.firstTruthyBitPos(8)).toBe(3); // 1000
    expect(WebHIDHeadsetService.firstTruthyBitPos(4)).toBe(2); // 100
    expect(WebHIDHeadsetService.firstTruthyBitPos(2)).toBe(1); // 10
    expect(WebHIDHeadsetService.firstTruthyBitPos(1)).toBe(0); // 1
    expect(WebHIDHeadsetService.firstTruthyBitPos(0)).toBe(-1);
  });

  it('retrieves a report', () => {
    WebHIDHeadsetService.device = {
      collections: [
        {
          inputReports: [
            {reportId: 1, items: [{usages: [100, 150, 200, 250]}]},
            {reportId: 2, items: [{usages: [110, 160, 210, 260]}]}
          ]
        },
        {
          inputReports: [
            {reportId: 3, items: [{usages: []}]}
          ]
        },
        {
          inputReports: []
        },
        {
          inputReports: [
            {reportId: 5, items: [{usages: [101, 102]}]}
          ]
        }
      ]
    };
    let report = WebHIDHeadsetService.getWebReport(1);
    expect(report).toEqual({reportId: 1, items: [{usages: [100, 150, 200, 250]}]});
  });

  it('it retrieves a mutli-items report and returns it with reduced & ordered items', () => {
    WebHIDHeadsetService.device = {
      collections: [
        {
          inputReports: [
            {reportId: 1, items: [{usages: [100, 150, 200, 250]}]},
            {reportId: 2, items: []}
          ]
        },
        {
          inputReports: [
            {reportId: 3, items: [{usages: []}]}
          ]
        },
        {
          inputReports: []
        },
        {
          inputReports: [
            {reportId: 5, items: [{usages: [110, 160, 210, 260]}, {usages: [120, 170, 220, 270]}, {usages: [999, 1000]}]}
          ]
        }
      ]
    };
    let report = WebHIDHeadsetService.getWebReport(5);
    expect(report).toEqual({reportId: 5, items: [{usages: [110, 160, 210, 260, 120, 170, 220, 270, 999, 1000]}]});
  });

  it('gets matching action with bit-ordering', () => {
    /*
      Here we pass 8, which turns into 0000 1000
      The 4th bit starting from the right is truthy
      The 4th action is 260 (we start from 0 both in arrays and in bit counting)
    */
    let action = WebHIDHeadsetService.getMatchingAction(8, {reportId: 5, items: [{usages: [110, 160, 210, 260, 120, 170, 220, 270, 999, 1000]}]});
    expect(action).toEqual(260);
  });

  it('turns an integer action into hexadecimal HID page-usage values', () => {
    /*
      786608 is the integer representation of hex c00b0 and we drop '00' at the
      start of the usage because it's unused for consumer and telephony input reports
      in the other way, c00b0 converted back to integer is 786608
    */
    expect(0xc00b0).toEqual(786608);
    let [page, usage] = WebHIDHeadsetService.toHIDCode(786608);
    expect(page).toEqual("C"); // Consumer page
    expect(usage).toEqual("B0"); // Play button usage
  });
  
});