describe('onHold popup service', function() {
  var $rootScope;
  var $uibModal;
  var $q;
  var onHold;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_$rootScope_, _$uibModal_, _onHold_, _$q_) {
    $rootScope = _$rootScope_;
    $uibModal = _$uibModal_;
    $q = _$q_;
    onHold = _onHold_;

    spyOn($uibModal, 'open').and.returnValue({result: $q.resolve({})});
  }));

  it('Open popup when OnHoldNotification is received', function(){
    onHold.init();
    $rootScope.$broadcast('OnHoldNotification');
    $rootScope.$digest();
    expect($uibModal.open).toHaveBeenCalled();
  });

});
