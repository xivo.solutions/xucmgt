import angular from 'angular';
import _ from 'lodash';

import { AppConfig, AppType } from '../../../../app/assets/javascripts/xccti/services/applicationConfiguration.provider';

describe('call control actions service', () => {
  var XucPhoneState;
  var CtiProxy;
  var callControlActions;
  var applicationConfiguration;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));

  beforeEach(angular.mock.inject((_XucPhoneState_, _CtiProxy_, _callControlActions_, _applicationConfiguration_) =>{
    XucPhoneState = _XucPhoneState_;
    CtiProxy = _CtiProxy_;
    callControlActions = _callControlActions_;
    applicationConfiguration = _applicationConfiguration_;
  }));

  function checkLineActionsForState(state, expectedActions, isMuted = false, isRecording = false, canRecord = false) {

    var actions = _.map(callControlActions.getLineActions(state, isMuted, isRecording, canRecord), (item) => {
      return item.label;
    });

    _.forEach(expectedActions, function(item) {
      expect(actions).toContain(item);
    });

    expect(actions.length).toBe(expectedActions.length);
  }

  function checkConferenceActionsForState(state, expectedActions, isRecording = false, canRecord = false) {
    var actions = _.map(callControlActions.getDeviceConferenceActions(state, isRecording, canRecord), (item) => {
      return item.label;
    });

    _.forEach(expectedActions, function(item) {
      expect(actions).toContain(item);
    });

    expect(actions.length).toBe(expectedActions.length);
  }

  describe('for SIP line', () => {
    it('should display hold and hangup actions when not-recorded call is established', () => {
      spyOn(XucPhoneState, 'getCalls').and.returnValue([{}]);
      checkLineActionsForState('Established', ['hold', 'hangup']);
    });

    it('should display hold, hangup, stopRecording actions when recording call is established', () => {
      spyOn(XucPhoneState, 'getCalls').and.returnValue([{}]);
      checkLineActionsForState('Established', ['hold', 'hangup', 'stoprecording'], false, true, true);
    });

    it('should display hold, hangup, startRecording actions when recording-capable call is established', () => {
      spyOn(XucPhoneState, 'getCalls').and.returnValue([{}]);
      checkLineActionsForState('Established', ['hold', 'hangup', 'startrecording'], false, false, true);
    });

    it('should display hold, hangup, direct transfer actions when switchboard call is established', () => {
      const appConfig = new AppConfig(AppType.Switchboard);
      spyOn(applicationConfiguration, 'getCurrentAppConfig').and.returnValue(appConfig);  
      spyOn(XucPhoneState, 'getCalls').and.returnValue([{}]);
      checkLineActionsForState('Established', ['hold', 'hangup', 'switchboardhold'], false, false, false);
    });

    it('should NOT display stopRecording action when recording call is established but action is disabled', () => {
      spyOn(XucPhoneState, 'getCalls').and.returnValue([{}]);
      checkLineActionsForState('Established', ['hold', 'hangup'], false, true, false);
    });

    it('should display hangup action when call is dialing', () => {
      checkLineActionsForState('Dialing', ['hangup']);
    });

    it('should display answer and hangup action when call is ringing', () => {
      checkLineActionsForState('Ringing', ['answer', 'hangup']);
    });

    it('should display unhold action when one call is on hold', () => {
      var calls = [
        {
          state: 'OnHold'
        }];
      spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
      checkLineActionsForState('OnHold', ['unhold']);
    });

    it('should not display hold action if multiple calls', () => {
      var calls = [
        {
          state: 'Established'
        },
        {
          state: 'Dialing'
        }];
      spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
      checkLineActionsForState('Established', ['hangup']);
    });

    it('should display attended transfer on hold when there are multiple calls', () => {
      var calls = [
        {
          state: 'Established'
        },
        {
          state: 'OnHold'
        }];
      spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
      checkLineActionsForState('OnHold', ['transfer']);
    });

    it('should display conference on hold when there are multiple calls', () => {
      var calls = [
        {
          state: 'Established'
        },
        {
          state: 'OnHold'
        }];
      spyOn(CtiProxy, 'isConferenceCapable').and.returnValue(true);
      spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
      checkLineActionsForState('OnHold', ['conference', 'transfer']);
    });
  });

  describe('for WebRTC line', () => {
    it('should display DTMF keyboard and mute action', () => {
      spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
      spyOn(XucPhoneState, 'getCalls').and.returnValue([{}]);
      checkLineActionsForState('Established', ['muteself', 'hold', 'hangup', 'dtmfkeypad']);
    });

    it('should display resume action on hold call if multiple ones', () => {
      spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
      spyOn(XucPhoneState, 'getCalls').and.returnValue([{}, {}]);
      checkLineActionsForState('Established', ['muteself', 'dtmfkeypad', 'hold', 'hangup']);
      checkLineActionsForState('OnHold', ['unhold', 'transfer']);
    });

    it('should display unmute action if muted', () => {
      spyOn(CtiProxy, 'toggleMicrophone');
      spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
      spyOn(XucPhoneState, 'getCalls').and.returnValue([{}]);
      checkLineActionsForState('Established', ['unmuteself', 'dtmfkeypad', 'hold', 'hangup'], true);
    });
  });

  describe('for Custom line', () => {
    it('should not propose answer nor hangup when ringing', () => {
      var calls = [
        {
          state: 'Ringing'
        }];
      spyOn(CtiProxy, 'isCustomLine').and.returnValue(true);
      spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
      checkLineActionsForState('Ringing', []);
    });

    it('should not propose hold nor conference when established', () => {
      var calls = [
        {
          state: 'Established'
        }];
      spyOn(CtiProxy, 'isCustomLine').and.returnValue(true);
      spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
      checkLineActionsForState('Established', ['hangup']);
    });

    it('should not propose unhold nor conference nor hangup when onHold', () => {
      var calls = [
        {
          state: 'OnHold'
        }];
      spyOn(CtiProxy, 'isCustomLine').and.returnValue(true);
      spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
      checkLineActionsForState('OnHold', []);
    });
  });

  describe('for device conference', () => {

    it('should display hold and hangup when conference is established', () => {
      checkConferenceActionsForState('Established', ['hold', 'hangup']);
    });

    it('should display stop record when conference is established and recording is active', () => {
      checkConferenceActionsForState('Established', ['hold', 'hangup', 'stoprecording'], true, true);
    });

    it('should display resume record when conference is established and recording is paused', () => {
      checkConferenceActionsForState('Established', ['hold', 'hangup', 'startrecording'], false, true);
    });
  });
});