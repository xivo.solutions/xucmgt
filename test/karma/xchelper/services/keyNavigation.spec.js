
describe('keyNavigation service', () => {
  var keyNavUtility;

  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.inject((_keyNavUtility_) => {
    keyNavUtility = _keyNavUtility_;
  }));

  it('should select the element below', function () {
    let index = 3;
    let amountOfOptions = 5;
    index = keyNavUtility.goDown(index, amountOfOptions);
    expect(index).toBe(4);
  });

  it('should go back to first element if there is no element below', function () {
    let index = 4;
    let amountOfOptions = 5;
    index = keyNavUtility.goDown(index, amountOfOptions);
    expect(index).toBe(0);
  });

  it('should select the element above', function () {
    let index = 3;
    let amountOfOptions = 5;
    index = keyNavUtility.goUp(index, amountOfOptions);
    expect(index).toBe(2);
  });

  it('should go to the last element if no element is above', function () {
    let index = 0;
    let amountOfOptions = 5;
    index = keyNavUtility.goUp(index, amountOfOptions);
    expect(index).toBe(4);
  });

  it('should, after a new search, select the last result on arrow up', function () {
    let index = 0;
    let amountOfOptions = 5;
    index = keyNavUtility.goUp(index, amountOfOptions);
    expect(index).toBe(4);
  });
  
});