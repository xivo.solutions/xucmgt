describe('errorModal', function () {
  var $uibModal;
  var $q;
  var errorModal;
  var $scope;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function (_$uibModal_, _$q_, _errorModal_, _$rootScope_) {
    $uibModal = _$uibModal_;
    $q = _$q_;
    errorModal = _errorModal_;
    $scope = _$rootScope_.$new();
  }));

  it('should open a modal when function is called', () => {
    let mockModalInstance = {
      result: $q.resolve({}),
      opened: $q.resolve({}),
      closed: $q.resolve({})
    };
    spyOn($uibModal, 'open').and.returnValue(mockModalInstance);
    errorModal.showErrorModal('CTI_SOCKET_CLOSED_WHILE_USING_WEBRTC');
    expect($uibModal.open).toHaveBeenCalled();
  });

  it('should open a new modal if an other modal is still open', () => {
    let mockOpened = $q.defer();
    let mockModalInstance = {
      result: $q.resolve(),
      opened: mockOpened.promise,
      closed: $q.defer().promise
    };
    spyOn($uibModal, 'open').and.returnValue(mockModalInstance);
    errorModal.showErrorModal('CTI_SOCKET_CLOSED_WHILE_USING_WEBRTC');
    expect($uibModal.open).toHaveBeenCalled();
    mockOpened.resolve();
    $scope.$apply();
    errorModal.showErrorModal('CTI_SOCKET_CLOSED_WHILE_USING_WEBRTC');
    expect($uibModal.open).toHaveBeenCalledTimes(1);
  });

  it('should open a new modal if an other modal is closed', () => {
    let mockOpened = $q.defer();
    let mockClosed = $q.defer();
    let mockModalInstance = {
      result: $q.resolve(),
      opened: mockOpened.promise,
      closed: mockClosed.promise
    };
    spyOn($uibModal, 'open').and.returnValue(mockModalInstance);
    errorModal.showErrorModal('CTI_SOCKET_CLOSED_WHILE_USING_WEBRTC');
    expect($uibModal.open).toHaveBeenCalled();
    mockOpened.resolve();
    $scope.$apply();
    mockClosed.resolve();
    $scope.$apply();
    errorModal.showErrorModal('CTI_SOCKET_CLOSED_WHILE_USING_WEBRTC');
    expect($uibModal.open).toHaveBeenCalledTimes(2);
  });
});
