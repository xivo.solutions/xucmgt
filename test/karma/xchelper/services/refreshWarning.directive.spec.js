describe('refreshWarning directive', () => {
  var xucPhoneState;
  var $window;
  var $compile;
  var $rootScope;
  var scope; 

  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));


  const createDirectiveScope = () => {
    var compiledDirective = $compile('<div refresh-warning event-registered="false"></div>')($rootScope.$new());
    $rootScope.$digest();
    return compiledDirective.scope();
  };

  beforeEach(angular.mock.inject(function(_XucPhoneState_, _$rootScope_, _$compile_, _$window_) {  
    xucPhoneState = _XucPhoneState_;
    $rootScope = _$rootScope_;
    $window = _$window_;
    $compile = _$compile_;
  }));
  
  it("register the refresh handler when the call list isn't empty", () => {
    spyOn($window, 'addEventListener');
    spyOn($window, 'removeEventListener');
    xucPhoneState.getCalls = jasmine.createSpy().and.returnValue([]);
    scope = createDirectiveScope();
    
    expect(scope.eventRegistered).toBe(false);
    xucPhoneState.getCalls = jasmine.createSpy().and.returnValue([{state: 'Ringing'}]);
    scope.$digest();

    expect(scope.eventRegistered).toBe(true);
    expect($window.addEventListener).toHaveBeenCalled();
  });
  
  it("removes the refresh handler when the call list is empty", () => {
    spyOn($window, 'addEventListener');
    spyOn($window, 'removeEventListener');
    xucPhoneState.getCalls = jasmine.createSpy().and.returnValue([{state: 'Ringing'}]);

    scope = createDirectiveScope();
    scope.$digest();

    expect(scope.eventRegistered).toBe(false);
    expect($window.addEventListener).toHaveBeenCalled();

    xucPhoneState.getCalls = jasmine.createSpy().and.returnValue([]);
    scope.$digest();

    expect(scope.eventRegistered).toBe(false);
    expect($window.removeEventListener).toHaveBeenCalled();
  });

});