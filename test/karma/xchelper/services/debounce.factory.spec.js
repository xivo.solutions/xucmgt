'use strict';

describe('Service: Debounce function execution', function () {
  var debounce, $timeout;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function (_debounce_, _$timeout_) {
    debounce = _debounce_;
    $timeout = _$timeout_;
  }));

  it('should execute function', function () {
    var spy = jasmine.createSpy('debounceFn');
    debounce(spy, 100)();
    expect(spy).toHaveBeenCalled();
  });


  it('should not execute function more than once if in wait range time', function () {
    var spy = jasmine.createSpy('debounceFn');
    var exec = debounce(spy, 100);
    exec();
    expect(spy).toHaveBeenCalled();
    exec();
    exec();
    exec();
    expect(spy.calls.count()).toBe(1);
  });

  it('should execute function once wait time is passed', function () {
    var spy = jasmine.createSpy('debounceFn');
    var exec = debounce(spy, 100);
    exec();
    expect(spy).toHaveBeenCalled();
    $timeout.flush(101);
    exec();
    expect(spy.calls.count()).toBe(2);
  });


});