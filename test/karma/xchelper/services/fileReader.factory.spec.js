describe('Service: file reader', function () {

  var fileReader, fakeData, $rootScope;
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_fileReader_, _$rootScope_) {
    fileReader = _fileReader_;
    $rootScope = _$rootScope_;

    fakeData = "company,email,fax,firstname,lastname,mobile,number\ncorp,j.doe@my.corp,3333,doe,john,2222,1111";
  }));

  it('should read content file', function(done) {
    let blob = new Blob([fakeData], { type: 'text/csv' });
    blob["lastModifiedDate"] = "";
    blob["name"] = "filename";

    fileReader.readAsText(blob, $rootScope).then(
      (result) => {
        expect(result).toBe(fakeData);
        done();
      });
  });
});
