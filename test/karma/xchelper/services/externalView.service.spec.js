'use strict';

describe('Service: External view URL', function () {

  var externalView;
  const url = 'http://myurl.com';

  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function (_externalView_) {
    externalView = _externalView_;
  }));

  it('should set content URL', function() {
    expect(externalView.getURL()).toBeUndefined();
    externalView.setURL(url);
    expect(externalView.getURL()).toBe(url);
  });

  it('should say if external content URL is set or not', function() {
    expect(externalView.isURLSet()).toBeFalsy();
    externalView.setURL("");
    expect(externalView.isURLSet()).toBeFalsy();
    externalView.setURL(url);
    expect(externalView.isURLSet()).toBeTruthy();
  });

});
