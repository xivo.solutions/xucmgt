describe('incomingCall', function() {
  var $rootScope;
  var XucPhoneEventListener;
  var $uibModal;
  var $q;
  var registeredCallback = null;
  var registeredVideoCallback = null;
  var XucVideoEventManager;
  var mockModalInstance;
  var remoteConfiguration;
  var incomingCall;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function(_$rootScope_, _XucPhoneEventListener_, _$uibModal_, _XucVideoEventManager_, _$q_, _incomingCall_, _remoteConfiguration_) {
    XucPhoneEventListener = _XucPhoneEventListener_;
    XucVideoEventManager = _XucVideoEventManager_;
    $rootScope = _$rootScope_;
    $uibModal = _$uibModal_;
    remoteConfiguration = _remoteConfiguration_;
    incomingCall = _incomingCall_;
    $q = _$q_;
    spyOn($rootScope, '$broadcast');
    spyOn(XucPhoneEventListener, 'addRingingHandler').and.callFake(function(scope, callback) {
      registeredCallback = callback;
    });

    spyOn(XucVideoEventManager, 'subscribeToVideoInviteEvent').and.callFake(function(scope, callback) {
      registeredVideoCallback = callback;
    });

    mockModalInstance = {
      result: $q.resolve({}),
      opened: $q.resolve({}),
      closed: $q.resolve({})
    };

  }));

  function triggerCall(call) {
    if(registeredCallback != null) {
      registeredCallback(call);
    }
  }

  function triggerVideoCall(call) {
    if(registeredVideoCallback != null) {
      registeredVideoCallback(call);
    }
  }

  it('Open popup upon phone call reception', angular.mock.inject(function(){
    incomingCall.init();
    incomingCall.subscribeForEvents(true);
    expect(XucPhoneEventListener.addRingingHandler).toHaveBeenCalled();
    spyOn($uibModal, 'open').and.returnValue(mockModalInstance);
    triggerCall({otherDN: "1001"});
    expect($uibModal.open).toHaveBeenCalled();
  }));

  it('Do not open popup upon second call ringing', angular.mock.inject(function(){
    incomingCall.init();
    incomingCall.subscribeForEvents(true);
    expect(XucPhoneEventListener.addRingingHandler).toHaveBeenCalled();
    spyOn($uibModal, 'open').and.returnValue(mockModalInstance);
    triggerCall({otherDN: "1001"});
    expect($uibModal.open).toHaveBeenCalled();
    $uibModal.open.calls.reset();
    triggerCall({otherDN: "1000"});
    expect($uibModal.open).not.toHaveBeenCalled();
  }));

  it('should not display incomingcall phone modal in ccagent', angular.mock.inject(function(){
    spyOn(remoteConfiguration, 'isAgent').and.returnValue(true);
    expect(XucPhoneEventListener.addRingingHandler).not.toHaveBeenCalled();
  }));

  it('Open popup upon video call reception', angular.mock.inject(function(){
    incomingCall.init();
    incomingCall.subscribeForEvents(true);
    expect(XucVideoEventManager.subscribeToVideoInviteEvent).toHaveBeenCalled();
    spyOn($uibModal, 'open').and.returnValue(mockModalInstance);
    triggerVideoCall({});
    expect($uibModal.open).toHaveBeenCalled();
  }));
});
