import * as angular from 'angular'
import ContactService from "../../../../app/assets/javascripts/xchelper/services/contact.service";
import {ContactSheetDetailFields, ContactSheetModel, ContactSheetDataType, ContactSheetAction, ContactSheetDetailCategorie} from "../../../../app/assets/javascripts/xchelper/models/contact-sheet.model";
import { PhoneHintStatus, VideoEvents } from '../../../../app/assets/javascripts/xchelper/models/CallHistory.model';
import { ActionsProperties } from '../../../../app/assets/javascripts/xchelper/directives/contactButtons.directive';
import { AppConfig, AppType } from '../../../../app/assets/javascripts/xccti/services/applicationConfiguration.provider';
import { StateService } from '@uirouter/angularjs';

describe('Service: Contact Service', () => {
  var ContactService: ContactService;
  var $state: StateService;

  beforeEach(angular.mock.module('ucAssistant'));

  beforeEach(angular.mock.inject(function (_ContactService_: ContactService, _$state_: StateService) {
    ContactService = _ContactService_;
    $state = _$state_;
  }));

  const contactSheetDetailFields = (name: string, data: string, dataType: ContactSheetDataType): ContactSheetDetailFields => { return {
    "name": name,
    "data": data,
    "dataType": dataType
  };}
  const fixtureContactSheetDetailFieldsPhoneNumber = (data: string, name: string = ""): ContactSheetDetailFields => contactSheetDetailFields(name,data, ContactSheetDataType.PhoneNumber)
  const fixtureContactSheetDetailFieldsEmail = (data: string, name: string = "Mail"): ContactSheetDetailFields => contactSheetDetailFields(name,data, ContactSheetDataType.Mail)
  const fixtureContactSheetDetailFieldsUrl = (data: string, name: string = ""): ContactSheetDetailFields => contactSheetDetailFields(name,data, ContactSheetDataType.Url)
  const fixtureContactSheetDetailFieldsString = (data: string, name: string = ""): ContactSheetDetailFields => contactSheetDetailFields(name,data, ContactSheetDataType.String)

  const fixtureContactSheetActions = (callArgs: string[] = [], chatArgs: string[] = [], mailArgs: string[] = [], videoArgs: string[] = []):  {[key:string]: ContactSheetAction } => {
    const fixInput = (input: string[]) => { if (input.length == 0) { return [""]; } else return input; }
    [callArgs, chatArgs, mailArgs, videoArgs] = [callArgs,chatArgs,mailArgs,videoArgs].map((input) => fixInput(input));
    return {
      "Call": {
        "args": callArgs,
        "disable": callArgs[0] == ""
      },
      "Chat": {
        "args": chatArgs,
        "disable": chatArgs[0] == ""
      },
      "Mail":{
        "args": mailArgs,
        "disable": mailArgs[0] == ""
      },
      "Video":{
        "args": videoArgs,
        "disable": videoArgs[0] == ""
      }
    };
  }

  const contactSheetUserActions: {[key:string]: ContactSheetAction } = fixtureContactSheetActions(["4000"],[],["ato@wisper.io"],["ato"]);
  const contactSheetUserContactsFields: ContactSheetDetailCategorie = {
    "name": "Contacts",
    "fields": [
      fixtureContactSheetDetailFieldsPhoneNumber("4000","Numéro"),
      fixtureContactSheetDetailFieldsPhoneNumber("","Mobile"),
      fixtureContactSheetDetailFieldsPhoneNumber("","Domicile"),
      fixtureContactSheetDetailFieldsPhoneNumber(""),
      fixtureContactSheetDetailFieldsEmail("ato@wisper.io"),
      fixtureContactSheetDetailFieldsString("")
    ]
  };
  const contactSheetUserGeneralFields: ContactSheetDetailCategorie = {
    "name": "Général",
    "fields": [
      fixtureContactSheetDetailFieldsString(""),
      fixtureContactSheetDetailFieldsString(""),
      fixtureContactSheetDetailFieldsString(""),
      fixtureContactSheetDetailFieldsString(""),
      fixtureContactSheetDetailFieldsUrl("")
    ]
  };
  const contactSheetUserWorkplaceFields: ContactSheetDetailCategorie = {
    "name": "Lieu d'affectation",
    "fields": [
      fixtureContactSheetDetailFieldsString(""),
      fixtureContactSheetDetailFieldsString(""),
    ]
  };
  const contactSheetUser: ContactSheetModel = {
    "name": "Antoine Toutain",
    "subtitle1": "",
    "subtitle2": "",
    "picture": "",
    "isPersonal": false,
    "isFavorite": false,
    "isMeetingroom": false,
    "canBeFavorite": true,
    "status": {
      "phone": 0,
      "video": VideoEvents.Available
    },
    "actions": contactSheetUserActions,
    "sources": [
      {
        "name": "internal",
        "id": "2"
      },
      {
        "name": "ldap",
        "id": "12"
      }
    ],
    "details": [
      contactSheetUserContactsFields,
      contactSheetUserGeneralFields,
      contactSheetUserWorkplaceFields
    ]
  };

  const contactSheetPersonalContact: ContactSheetModel = { ...contactSheetUser, sources: [{ name: "internal", id: "42" }, { name: "personal", id: "812" }] };
  const contactSheetMeetingRoom: ContactSheetModel = { ...contactSheetUser, sources: [{ name: "internal", id: "42" }, { name: "xivo_meetingroom", id: "963" }], isMeetingroom: true };

  it('should getPhoneStateBackColor', () => {
    expect(ContactService.getPhoneStateBackColor({status: {}})).toBe("user-statusundefined");
    expect(ContactService.getPhoneStateBackColor({status: {}, isMeetingroom: true})).toBe("user-meetingroom");
    expect(ContactService.getPhoneStateBackColor({status: {video: 'Busy'}})).toBe("user-videostatus-Busy");
  });

  it('should getPhoneStateLabel', () => {
    //@ts-expect-error
    expect(ContactService.getPhoneStateLabel({...contactSheetUser, status: {}})).toBe("USER_STATUS_undefined");
    expect(ContactService.getPhoneStateLabel({...contactSheetUser, status: {phone: PhoneHintStatus.BUSY, video: VideoEvents.Available}})).toBe("USER_STATUS_2");
  });

  it('should userWithInviteToConferenceIcon', () => {
    //@ts-expect-error
    expect(ContactService.userWithInviteToConferenceIcon({})).toBe(false);
  });

  it('should meetingRoomWithInviteToConferenceIcon', () => {
    //@ts-expect-error
    expect(ContactService.meetingRoomWithInviteToConferenceIcon({})).toBe(false);
  });

  it('should userWithCallIcon', () => {
    expect(ContactService.userWithCallIcon(contactSheetUser)).toBe(true);
  });

  it('should meetingRoomWithCallIcon', () => {
    expect(ContactService.meetingRoomWithCallIcon({...contactSheetUser, sources:[], isMeetingroom:false})).toBe(false);
  });

  it('should hasJustEmail', () => {
    expect(ContactService.hasJustEmail(contactSheetUser)).toBe(false);
  });

  it('should _isPersonal', () => {
    //@ts-expect-error
    expect(ContactService._isPersonal({})).toBe('static');
  });

  it('should canShareLink', () => {
    expect(ContactService.canShareLink({...contactSheetUser, actions: {}, isMeetingroom: false})).toBe(false);
    expect(ContactService.canShareLink({...contactSheetUser, actions: {}, isMeetingroom: true})).toBe(false);
    expect(ContactService.canShareLink({...contactSheetUser, actions: {'ShareLink': {disable: false, args: []}}, isMeetingroom: false})).toBe(false);
    expect(ContactService.canShareLink({...contactSheetUser, actions: {'ShareLink': {disable: false, args: []}}, isMeetingroom: true})).toBe(true);
  });

  it('should getFieldDataFormatted', () => {
    expect(ContactService.getFieldDataFormatted({...contactSheetUser, isMeetingroom: false}, {dataType: ContactSheetDataType.PhoneNumber, data: '1234', name: ""})).toBe('1234');
    expect(ContactService.getFieldDataFormatted({...contactSheetUser, isMeetingroom: true}, {dataType: ContactSheetDataType.PhoneNumber, data: '1234', name: ""})).toBe('**1234');
    expect(ContactService.getFieldDataFormatted({...contactSheetUser, isMeetingroom: false}, {dataType: ContactSheetDataType.PhoneNumber, data: '0633826619', name: ""})).toBe('06 33 82 66 19');
    expect(ContactService.getFieldDataFormatted({...contactSheetUser, isMeetingroom: false}, {dataType: ContactSheetDataType.String, data: 'toto', name: ""})).toBe('toto');
    expect(ContactService.getFieldDataFormatted({...contactSheetUser, isMeetingroom: false}, {dataType: ContactSheetDataType.Mail, data: 'toto@toto', name: ""})).toBe('toto@toto');
    expect(ContactService.getFieldDataFormatted({...contactSheetUser, isMeetingroom: false}, {dataType: ContactSheetDataType.Url, data: 'http://titi.tata', name: ""})).toBe('http://titi.tata');

  });

  it('should return contact initials', () => {
    expect(ContactService.setInitials({...contactSheetUser, name: "Jean"})).toBe("J");
    expect(ContactService.setInitials({...contactSheetUser, name: "Jean Claude"})).toBe("J C");
    expect(ContactService.setInitials({...contactSheetUser, name: "Jean Claude Van"})).toBe("J C V");
    expect(ContactService.setInitials({...contactSheetUser, name: "Jean Claude Van Damme"})).toBe("J C V D");
  });

  it('should display edit actions for personal contacts by default', () => {
    const action: ActionsProperties = ContactService.getActionsProperties();
    expect(action.Edit).toBeDefined();

    spyOnProperty(window.document, 'URL' ).and.returnValue('https://edge-bsc.test.avencall.com/')
    expect(action.Edit.displayCondition({...contactSheetUser, isPersonal: false})).toBe(false);
    expect(action.Edit.displayCondition({...contactSheetUser, isPersonal: true})).toBe(true);
  });


  it('should not display edit actions for personal contacts on ccagent', () => {
    ContactService.appConfig = new AppConfig(AppType.CCAgent);
    const action: ActionsProperties = ContactService.getActionsProperties();
    expect(action.Edit.displayCondition({...contactSheetUser, isPersonal: false})).toBe(false);
    expect(action.Edit.displayCondition({...contactSheetUser, isPersonal: true})).toBe(false);
  });

  it('should not display edit actions for personal contacts on switchboard', () => {
    ContactService.appConfig = new AppConfig(AppType.Switchboard);
    const action: ActionsProperties = ContactService.getActionsProperties();
    expect(action.Edit.displayCondition({...contactSheetUser, isPersonal: false})).toBe(false);
    expect(action.Edit.displayCondition({...contactSheetUser, isPersonal: true})).toBe(false);
  });

  it('should naviate to meeting room editor if contact sources contains xivo_meetingroom', () => {
    console.log(contactSheetMeetingRoom.sources)
    spyOn($state, "go")
    ContactService.navigateToContactEditor(contactSheetMeetingRoom);
    expect($state.go).toHaveBeenCalledWith('interface.meetingRooms', { id: '963' });
  });

  it('should navigate to personal contact editor if contact sources contains personal', () => {
    spyOn($state, "go")
    ContactService.navigateToContactEditor(contactSheetPersonalContact);
    expect($state.go).toHaveBeenCalledWith('interface.personalContact', { id: '812' });
  });

  it('should display the Call button when the user can make a call (userWithCallIcon condition)', () => {
      spyOn(ContactService, 'userWithCallIcon').and.returnValue(true);
      const contact: ContactSheetModel = {...contactSheetUser, actions: fixtureContactSheetActions(["1234"])
      };
      const actions = ContactService.getActionsProperties();
      expect(actions.Call.displayCondition(contact)).toBe(true);
    });


  it('should display the Call button when the user is in a meeting room (meetingRoomWithCallIcon condition)', () => {
      spyOn(ContactService, 'meetingRoomWithCallIcon').and.returnValue(true);
      const contact: ContactSheetModel = {...contactSheetUser, isMeetingroom: true, actions: fixtureContactSheetActions(["1234"])};
      const actions = ContactService.getActionsProperties();
      expect(actions.Call.displayCondition(contact)).toBe(true);
  });


  it('should display the Call button when AudioInvite is available and userWithInviteToConferenceIcon is true', () => {
      spyOn(ContactService, 'userWithInviteToConferenceIcon').and.returnValue(true);
      const contact: ContactSheetModel = {...contactSheetUser, actions: fixtureContactSheetActions([], [], [], ["audioInvite"])};
      const actions = ContactService.getActionsProperties();
      expect(actions.Call.displayCondition(contact)).toBe(true);
  });

  it('should not display the Call button if none of the conditions are met', () => {
      spyOn(ContactService, 'userWithCallIcon').and.returnValue(false);
      spyOn(ContactService, 'meetingRoomWithCallIcon').and.returnValue(false);
      spyOn(ContactService, 'userWithInviteToConferenceIcon').and.returnValue(false);
      spyOn(ContactService, 'meetingRoomWithInviteToConferenceIcon').and.returnValue(false);

      const contact: ContactSheetModel = {...contactSheetUser, actions: fixtureContactSheetActions(["1234"])};
      const actions = ContactService.getActionsProperties();
      expect(actions.Call.displayCondition(contact)).toBe(false);
   });
});
