describe('preventCache factory', () => {
  var $window;

  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject((_$window_) => {
    $window = _$window_;
    $window.appVersion = 1;
  }));

  afterEach(() => {
    delete $window.appVersion;
  });

  it('add version cache busting for matching url', angular.mock.inject((preventCache) => {
    let config = {
      url: 'assets/xchelper/tpl.html'
    };
    expect(preventCache.request(config).url).toBe("assets/xchelper/tpl.html?v=1");
  }));

  it('does nothing for not matching url', angular.mock.inject((preventCache) => {
    let config = {
      url: 'assets/xchelper/tpl.fake'
    };
    expect(preventCache.request(config).url).toBe("assets/xchelper/tpl.fake");
  }));

});