'use strict';

describe('Service: broadcaster', function () {

  var $rootScope;
  var broadcaster;

  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));


  beforeEach(angular.mock.inject(function (_$rootScope_, _Broadcaster_) {
    $rootScope = _$rootScope_;
    broadcaster = _Broadcaster_;
    jasmine.clock().uninstall();
  }));

  it('should add an event to ongoing events', function() {
    spyOn($rootScope, "$broadcast").and.callThrough();
    broadcaster.send("someEvent");
    expect(broadcaster.ongoingEvents).toEqual(["someEvent"]);
    expect($rootScope.$broadcast).not.toHaveBeenCalled();
  });

  it('should start a broadcast timeout for a given event', function() {
    spyOn($rootScope, "$broadcast");
    jasmine.clock().install();

    broadcaster.startTimeout("anotherEvent");
    expect($rootScope.$broadcast).not.toHaveBeenCalled();
    
    jasmine.clock().tick(broadcaster.timeoutMs + 1);
    expect($rootScope.$broadcast).toHaveBeenCalledWith("anotherEvent");
    jasmine.clock().uninstall();
  });

  it('should keep its current timeout if multiple events are incoming', function() {
    spyOn($rootScope, "$broadcast");
    jasmine.clock().install();

    broadcaster.send("thirdEvent");
    expect($rootScope.$broadcast).not.toHaveBeenCalled();

    jasmine.clock().tick(broadcaster.timeoutMs / 4);
    broadcaster.send("thirdEvent");
    expect($rootScope.$broadcast).not.toHaveBeenCalled();

    jasmine.clock().tick(broadcaster.timeoutMs / 4);
    broadcaster.send("thirdEvent");
    expect($rootScope.$broadcast).not.toHaveBeenCalled();

    jasmine.clock().tick(broadcaster.timeoutMs / 4);
    broadcaster.send("thirdEvent");
    expect($rootScope.$broadcast).not.toHaveBeenCalled();

    jasmine.clock().tick((broadcaster.timeoutMs / 4) + 1);
    expect($rootScope.$broadcast).toHaveBeenCalledWith("thirdEvent");
    jasmine.clock().uninstall();
  });

  it('should finish the timeout and restart a new one if multiples events are sent with ', function() {
    const spy = spyOn($rootScope, "$broadcast");
    jasmine.clock().install();

    broadcaster.send("fourthEvent");
    jasmine.clock().tick(broadcaster.timeoutMs / 2);
    broadcaster.send("fourthEvent");
    expect($rootScope.$broadcast).not.toHaveBeenCalled();

    jasmine.clock().tick((broadcaster.timeoutMs / 2) + 1);
    expect($rootScope.$broadcast).toHaveBeenCalledWith("fourthEvent");

    spy.calls.reset();

    broadcaster.send("fourthEvent");
    jasmine.clock().tick(broadcaster.timeoutMs / 2);
    expect($rootScope.$broadcast).not.toHaveBeenCalled();

    jasmine.clock().tick((broadcaster.timeoutMs / 2) + 1);
    expect($rootScope.$broadcast).toHaveBeenCalledWith("fourthEvent");

    jasmine.clock().uninstall();
  });

});
