'use strict';

describe('Service: onHold notifier', function () {
  var $rootScope;
  var $interval;
  var onHoldNotifier;
  var XucPhoneState;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject((_$rootScope_, _onHoldNotifier_, _$interval_, _XucPhoneState_) => {
    $rootScope = _$rootScope_;
    onHoldNotifier = _onHoldNotifier_;
    $interval = _$interval_;
    XucPhoneState = _XucPhoneState_;
  }));

  it('triggers notification when onHold period for a call is reached', () => {
    var promiseCall = {};
    const counter = 2;
    const thresholdInMs = 100;
    const call = {uniqueId: 1};

    spyOn(XucPhoneState, 'getCallsOnHold').and.returnValue([call]);

    onHoldNotifier.notify(thresholdInMs, call).finally(null, (notify) => {
      promiseCall = notify;
    });
    $interval.flush(thresholdInMs * counter);
    $rootScope.$apply();
    expect(promiseCall.count).toBe(counter);
    expect(promiseCall.call).toBe(call);
  });

  it ('do not notify if no more on hold calls', () => {
    var promiseCall;
    var completed = false;
    const thresholdInMs = 100;

    spyOn(XucPhoneState, 'getCallsOnHold').and.returnValue([]);

    onHoldNotifier.notify(thresholdInMs, {uniqueId: 1}).finally(
      () => {
        completed = true;
      },(notify) => {
        promiseCall = notify;
      });
    $interval.flush(thresholdInMs);
    $rootScope.$apply();
    expect(promiseCall).toBeUndefined();
    expect(completed).toBe(true);
  });

  it ('stops notifying when manually invoked and no more on hold calls', () => {
    var promiseCall = {};
    const thresholdInMs = 100;
    const call = {uniqueId: 1};

    var spy = spyOn(XucPhoneState, 'getCallsOnHold').and.returnValue([call]);

    onHoldNotifier.notify(thresholdInMs, call).finally(
      () => {},
      (notify) => {
        promiseCall = notify;
      });
    $interval.flush(thresholdInMs);
    $rootScope.$apply();
    expect(promiseCall.count).toBe(1);

    spy.and.returnValue([]);
    onHoldNotifier.stop();
    $interval.flush(thresholdInMs);
    $rootScope.$apply();
    expect(promiseCall.count).toBe(1);
  });


  it ('keeps notifying even if stop invoked but still on hold calls', () => {
    var promiseCall = {};
    const thresholdInMs = 100;
    const call = {uniqueId: 1};

    spyOn(XucPhoneState, 'getCallsOnHold').and.returnValue([call]);

    onHoldNotifier.notify(thresholdInMs, call).finally(null, (notify) => {
      promiseCall = notify;
    });
    $interval.flush(thresholdInMs);
    $rootScope.$apply();
    expect(promiseCall.count).toBe(1);

    onHoldNotifier.stop();
    $interval.flush(thresholdInMs);
    $rootScope.$apply();
    expect(promiseCall.count).toBe(2);
    $rootScope.$digest();
  });

  it ('notifies only the oldest call if multiple on hold calls', () => {
    var promiseCall = {};
    const thresholdInMs = 100;
    const call1 = {uniqueId: 1};
    const call2 = {uniqueId: 2};

    spyOn(XucPhoneState, 'getCallsOnHold').and.returnValue([call1, call2]);

    onHoldNotifier.notify(thresholdInMs, call1).finally(null, (notify) => {
      promiseCall = notify;
    });
    $interval.flush(thresholdInMs);
    $rootScope.$apply();
    expect(promiseCall.count).toBe(1);

    onHoldNotifier.notify(thresholdInMs, call2).finally(null, (notify) => {
      promiseCall = notify;
    });
    $interval.flush(thresholdInMs);
    $rootScope.$apply();
    expect(promiseCall.count).toBe(2);
    expect(promiseCall.call).toBe(call1);
    $rootScope.$digest();
  });

});