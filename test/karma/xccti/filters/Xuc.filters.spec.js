import moment from 'moment';
import countdown from 'countdown';

describe('filter', function() {

  beforeEach(angular.mock.module('xcCti'));

  describe('totalTime filter', function() {

    it('should convers seconds to a totalTime display',
      angular.mock.inject(function(totalTimeFilter) {

        expect(totalTimeFilter(0)).toBe('00:00:00');
        expect(totalTimeFilter(10)).toBe('00:00:10');
        expect(totalTimeFilter(130)).toBe('00:02:10');
        expect(totalTimeFilter(3730)).toBe('01:02:10');
        expect(totalTimeFilter(99*3600)).toBe('99:00:00');
        expect(totalTimeFilter(100*3600)).toBe('100:00:00');
        expect(totalTimeFilter(100*3600 + 59*60 + 59)).toBe('100:59:59');
        expect(totalTimeFilter(1000*3600)).toBe('###:##:##');

      }));
  });

  describe('prepareServerUrl filter', function() {
    it('generates url corresponding to the current window location',

      angular.mock.inject(function(prepareServerUrlFilter) {
        expect(prepareServerUrlFilter("http:", "192.168.56.101:8000", 'http')).toBe('http://192.168.56.101:8000');
        expect(prepareServerUrlFilter("https:", "192.168.56.101:8000", 'http')).toBe('https://192.168.56.101');
        expect(prepareServerUrlFilter("http:", "192.168.56.101:8000", 'ws')).toBe('ws://192.168.56.101:8000');
        expect(prepareServerUrlFilter("https:", "192.168.56.101:8000", 'ws')).toBe('wss://192.168.56.101');

      }));
  });

  describe('timeInState filter', function() {
    var $translate;
    var filter;
    beforeEach(angular.mock.module('xcCti'));

    beforeEach(angular.mock.inject(function($filter, _$translate_) {
      $translate = _$translate_;
      filter = $filter('timeInState', {});
    }));

    it('can use filter', function(){
      expect(filter).not.toBeUndefined();
    });

    it('returns minutes and seconds',function(){
      var start = moment().add(-(90), 'seconds');
      expect(filter(countdown(start))).toBe('01:30');
    });

    it('returns hours minutes and seconds',function(){
      var start = moment().add(-(3690), 'seconds');
      expect(filter(countdown(start))).toBe('01:01:30');
    });

    it('returns days hours minutes and seconds',function(){
      var start = moment().add(-(90090), 'seconds');
      spyOn($translate, 'instant').and.returnValue('d');
      expect(filter(countdown(start))).toBe('1d 01:01:30');
    });
  });


});
