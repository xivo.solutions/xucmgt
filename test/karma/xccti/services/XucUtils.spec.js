'use strict';

describe('Xuc service utils', function() {
  var xucUtils;

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_XucUtils_) {
    xucUtils = _XucUtils_;
  }));


  it('return same number if only numbers', function() {
    expect(xucUtils.normalizePhoneNb('1260')).toBe('1260');
  });
  it('not allow alphabetic together with allowed characters', function() {
    expect(xucUtils.normalizePhoneNb(' 1*2(6+4q 8$')).toBe('1*264q8$');
  });
  it('replace leading + by 00', function() {
    expect(xucUtils.normalizePhoneNb('+33684985623')).toBe('0033684985623');
  });
  it('remove leading spaces', function() {
    expect(xucUtils.normalizePhoneNb('    06 8498 5623')).toBe('0684985623');
  });
  it('allow leading (after space removal) star', function() {
    expect(xucUtils.normalizePhoneNb(' * 98')).toBe('*98');
    expect(xucUtils.normalizePhoneNb('*123')).toBe('*123');
    expect(xucUtils.normalizePhoneNb(' *  **12*3')).toBe('***12*3');
    expect(xucUtils.normalizePhoneNb('****')).toBe('****');
  });

  it('not allow alphanumeric characters', function() {
    expect(xucUtils.normalizePhoneNb('fss154')).toBe('fss154');
  });
  it('remove brackets within number', function() {
    expect(xucUtils.normalizePhoneNb('036(06)84985623')).toBe('0360684985623');
  });
  it('remove dots within number', function() {
    expect(xucUtils.normalizePhoneNb('036.06.8498.5623')).toBe('0360684985623');
  });
  it('remove plus within number', function() {
    expect(xucUtils.normalizePhoneNb('*30+33123456')).toBe('*3033123456');
  });
  it('not allow alphabetic characters', function() {
    expect(xucUtils.normalizePhoneNb('alphabetic')).toBe('alphabetic');
  });
  it('handle undefined', function() {
    expect(xucUtils.normalizePhoneNb()).toBe('');
  });
  it('allow SIP uri', function() {
    expect(xucUtils.normalizePhoneNb('jbond@mi6.gov.uk')).toBe('jbond@mi6.gov.uk');
  });
  it('a phone number is composed by numbers', function() {
    expect(xucUtils.isaPhoneNb('1260')).toBe(true);
  });
  it('a number starting by * is a phone number', function() {
    expect(xucUtils.isaPhoneNb('*1260')).toBe(true);
    expect(xucUtils.isaPhoneNb('**1260')).toBe(true);
  });

  it('a number containing a single * is a phone number', function() {
    expect(xucUtils.isaPhoneNb('*12*60')).toBe(true);
  });
  it('a number with one digit only containing a single * is a phone number', function() {
    expect(xucUtils.isaPhoneNb('*9')).toBe(true);
  });
  it('a number with one digit only is a phone number', function() {
    expect(xucUtils.isaPhoneNb('9')).toBe(true);
  });
  it('empty is not a phone number', function() {
    expect(xucUtils.isaPhoneNb('')).toBe(false);
  });
  it('a number containing two * is a phone number', function() {
    expect(xucUtils.isaPhoneNb('*12**60')).toBe(true);
  });
  it('a number containing more than two * is NOT a phone number', function() {
    expect(xucUtils.isaPhoneNb('*12***60')).toBe(false);
  });
  it('a number containing * randomly positioned is NOT a phone number', function() {
    expect(xucUtils.isaPhoneNb('*12*60**70*1**2')).toBe(false);
  });
  it('a number with letters is NOT a phone number', function() {
    expect(xucUtils.isaPhoneNb('*12abcd60')).toBe(false);
  });
  it('a number with dots is NOT a phone number', function() {
    expect(xucUtils.isaPhoneNb('036.06.8498.5623')).toBe(false);
  });
  it('a number brackets brackets is NOT a phone number', function() {
    expect(xucUtils.isaPhoneNb('036(06)84985623')).toBe(false);
  });
  it('a SIP uri is a phone number', function() {
    expect(xucUtils.isaPhoneNb('jbond@mi6.gov.uk')).toBe(true);
  });

  it('pretty formats phone number', function () {
    expect(xucUtils.prettyPhoneNb('')).toBe('');
    expect(xucUtils.prettyPhoneNb('1')).toBe('1');
    expect(xucUtils.prettyPhoneNb('12')).toBe('12');
    expect(xucUtils.prettyPhoneNb('123')).toBe('123');
    expect(xucUtils.prettyPhoneNb('1234')).toBe('1234');
    expect(xucUtils.prettyPhoneNb('12345')).toBe('12345');
    expect(xucUtils.prettyPhoneNb('123456')).toBe('12 34 56');
    expect(xucUtils.prettyPhoneNb('1234567')).toBe('1 23 45 67');
    expect(xucUtils.prettyPhoneNb('12345678')).toBe('12 34 56 78');
    expect(xucUtils.prettyPhoneNb('123456789')).toBe('123 456 789');
    expect(xucUtils.prettyPhoneNb('1234567890')).toBe('12 34 56 78 90');
    expect(xucUtils.prettyPhoneNb('12345678901')).toBe('1 23 45 67 89 01');
    expect(xucUtils.prettyPhoneNb('+12345678901')).toBe('+12345678901');
    expect(xucUtils.prettyPhoneNb('jbond@mi6.gov.uk')).toBe('jbond@mi6.gov.uk');
  });
 
  it('reshape phone numbers input without spaces', function () {
    expect(xucUtils.reshapeInput('06 02 03 04 05')).toEqual({ type: 'phone', value: '0602030405' });
  });

  it('reshapeInput should preserve SIP uri', function () {
    expect(xucUtils.reshapeInput('jbond@mi6.gov.uk')).toEqual({ type: 'phone', value: 'jbond@mi6.gov.uk' });
  });

  it('detect text input and return an input object', function () {
    expect(xucUtils.reshapeInput('Richard Papillon')).toEqual({ type: 'search', value: 'Richard Papillon' });
  });

});
