'use strict';

describe('OIDCHelper', function() {
  var $rootScope;
  var $httpBackend;
  var OIDCHelper;

  var mockedWindow = {};

  beforeEach(angular.mock.module("xcCti", function ($provide) {
    $provide.value('$window', mockedWindow);
  }));
  
  beforeEach(angular.mock.inject(function (_OIDCHelper_, _$rootScope_, _$httpBackend_) {
    OIDCHelper = _OIDCHelper_;
    $rootScope = _$rootScope_;
    $httpBackend = _$httpBackend_;
    $rootScope.$digest();
  }));

  afterEach(function () {
    $httpBackend.resetExpectations();
  });

  it('should build the service url as expected', function() {

    mockedWindow.location = {
      protocol: "https:",
      host: "foobar.bat",
      pathname: "/main/login",
      hash: ""
    };

    var serviceURL = OIDCHelper.buildServiceUrl();
    expect(serviceURL).toEqual("https://foobar.bat/main/login");

    mockedWindow.location.hash = "/#!/login";
    mockedWindow.location.pathname = "";
    serviceURL = OIDCHelper.buildServiceUrl();
    expect(serviceURL).toEqual("https://foobar.bat/");

    mockedWindow.location.hash = "/#!/login?error=Logout";
    serviceURL = OIDCHelper.buildServiceUrl();
    expect(serviceURL).toEqual("https://foobar.bat/");

    mockedWindow.location.hash = "/#!/login";
    serviceURL = OIDCHelper.buildServiceUrl();
    expect(serviceURL).toEqual("https://foobar.bat/");
  });

  it('should build the config url as expected', function() {
    expect(OIDCHelper.buildConfigUrl("https://foobar.bat")).toEqual("https://foobar.bat/.well-known/openid-configuration");
  });

  it('should get the state out of an URL', function() {
    let url = "https://foobar.bat/#!/login?state=somestate&token=sometoken";
    let state = OIDCHelper.getPhoneNbFromState(url);
    expect(state).toEqual("somestate");

    url = "https://foobar.bat/#!/login?token=sometoken&state=someotherstate";
    state = OIDCHelper.getPhoneNbFromState(url);
    expect(state).toEqual("someotherstate");

    url = "https://foobar.bat/#!/login?token=sometoken";
    state = OIDCHelper.getPhoneNbFromState(url);
    expect(state).toBeUndefined();
  });

  it('should redirect to oidc server if no token', function() {
    mockedWindow.location = {
      protocol: "https:",
      host: "foobar.bat",
      pathname: "/main/login",
      hash: ""
    };
    let OIDCconf = {data: {authorization_endpoint: "https://some.endpoint/foobar/openid-connect/auth"}};
    let loginUrl = OIDCHelper.buildLoginUrl(OIDCconf, "1234", "xuc");
    expect(loginUrl).toEqual(`https://some.endpoint/foobar/openid-connect/auth?client_id=xuc&redirect_uri=https%3A%2F%2Ffoobar.bat%2Fmain%2Flogin&response_type=token+id_token&response_mode=fragment&scope=openid&nonce=nonce&state=1234`);

    loginUrl = OIDCHelper.buildLoginUrl(OIDCconf, undefined, "xuc");
    expect(loginUrl).toEqual(`https://some.endpoint/foobar/openid-connect/auth?client_id=xuc&redirect_uri=https%3A%2F%2Ffoobar.bat%2Fmain%2Flogin&response_type=token+id_token&response_mode=fragment&scope=openid&nonce=nonce`);
  });

});
