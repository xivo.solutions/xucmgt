'use strict';
describe('XucPhoneHintService', function() {
  var $rootScope;
  var XucPhoneHintService;
  var $scope;

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function (_$rootScope_, _XucPhoneHintService_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();

    XucPhoneHintService = _XucPhoneHintService_;
    spyOn(Cti, 'sendCallback');

    Cti.Topic(Cti.MessageType.PHONEHINTSTATUSEVENT).clear();

    // Initialize XucLink
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    $rootScope.$digest();
  }));

  afterEach(function() {
    $scope.$destroy();
    $rootScope.$digest();
  });

  function getEvent(number, status) {
    return {
      "number": number,
      "status": status
    };
  }


  it('should notify when receiving an event', function () {
    var rawEvent = getEvent("1000", 1);

    var handler = {
      callback: function () {}
    };

    spyOn(handler, 'callback');
    XucPhoneHintService.addEventListener($scope, handler.callback);

    Cti.Topic(Cti.MessageType.PHONEHINTSTATUSEVENT).publish(rawEvent);
    expect(handler.callback).toHaveBeenCalledWith(rawEvent);

  });

});
