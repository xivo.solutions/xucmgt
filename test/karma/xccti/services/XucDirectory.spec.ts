import * as angular from 'angular'
import { IRootScopeService } from 'angular';
import { RichEntry, SearchContactResult } from '../../../../app/assets/javascripts/xchelper/models/RichEntry.model';
import { ContactSheetDetailFields, ContactSheetDetailCategorie, ContactSheetAction, ContactSheetDataType, ContactSheetModel } from '../../../../app/assets/javascripts/xchelper/models/contact-sheet.model';
import { VideoEvents } from '../../../../app/assets/javascripts/xchelper/models/CallHistory.model';
import { FavoritesUpdated } from '../../../../app/assets/javascripts/xchelper/models/FavoritesUpdated.model';
import exp from 'constants';

describe('Xuc directory', () => {
  var xucDirectory: any;
  var $rootScope: IRootScopeService;
  var token: string = "aaaa-bbbb-cccc-dddd-1234";
  var user: any = {username: "jbond", phoneNumber: "1001", token: token};
  var Cti = (window as any).Cti;

  const favEntry: Array<string|boolean> = ["u1","45654","","",true,""]
  const favContact: RichEntry = {status:4,entry:favEntry,contact_id:"25",source:"internal",favorite:true,personal: false}
  const favResult: SearchContactResult = {headers:[], entries:
    [
      favContact,
      { ...favContact, entry: ["u2","568778","","",true,""], contact_id: "24"}
    ]
  };
  const extEntry: Array<string|boolean> =["u1","45654","","",false,""]
  const extContact: RichEntry = {...favContact, entry: extEntry, favorite: false }
  const searchResult: SearchContactResult = {headers:[], entries:
    [
      extContact,
      { ...extContact, entry: ["u2","568778","","",false,""], contact_id: "24"}
    ]
  };

  const contactSheetDetailFields = (name: string, data: string, dataType: ContactSheetDataType): ContactSheetDetailFields => { return {
    "name": name,
    "data": data,
    "dataType": dataType
  };}
  const fixtureContactSheetDetailFieldsPhoneNumber = (data: string, name: string = ""): ContactSheetDetailFields => contactSheetDetailFields(name,data, ContactSheetDataType.PhoneNumber)
  const fixtureContactSheetDetailFieldsEmail = (data: string, name: string = "Mail"): ContactSheetDetailFields => contactSheetDetailFields(name,data, ContactSheetDataType.Mail)
  const fixtureContactSheetDetailFieldsUrl = (data: string, name: string = ""): ContactSheetDetailFields => contactSheetDetailFields(name,data, ContactSheetDataType.Url)
  const fixtureContactSheetDetailFieldsString = (data: string, name: string = ""): ContactSheetDetailFields => contactSheetDetailFields(name,data, ContactSheetDataType.String)

  const fixtureContactSheetActions = (callArgs: string[] = [], chatArgs: string[] = [], mailArgs: string[] = [], videoArgs: string[] = []):  {[key:string]: ContactSheetAction } => {
    const fixInput = (input: string[]) => { if (input.length == 0) { return [""]; } else return input; }
    [callArgs, chatArgs, mailArgs, videoArgs] = [callArgs,chatArgs,mailArgs,videoArgs].map((input) => fixInput(input));
    return {
      "Call": {
        "args": callArgs,
        "disable": callArgs[0] == ""
      },
      "Chat": {
        "args": chatArgs,
        "disable": chatArgs[0] == ""
      },
      "Mail":{
        "args": mailArgs,
        "disable": mailArgs[0] == ""
      },
      "Video":{
        "args": videoArgs,
        "disable": videoArgs[0] == ""
      }
    };
  }

  const contactSheetUserActions: {[key:string]: ContactSheetAction } = fixtureContactSheetActions(["4000"],[],["ato@wisper.io"],["ato"]);
  const contactSheetUserContactsFields: ContactSheetDetailCategorie = {
    "name": "Contacts",
    "fields": [
      fixtureContactSheetDetailFieldsPhoneNumber("4000","Numéro"),
      fixtureContactSheetDetailFieldsPhoneNumber("","Mobile"),
      fixtureContactSheetDetailFieldsPhoneNumber("","Domicile"),
      fixtureContactSheetDetailFieldsPhoneNumber(""),
      fixtureContactSheetDetailFieldsEmail("ato@wisper.io"),
      fixtureContactSheetDetailFieldsString("")
    ]
  };
  const contactSheetUserGeneralFields: ContactSheetDetailCategorie = {
    "name": "Général",
    "fields": [
      fixtureContactSheetDetailFieldsString(""),
      fixtureContactSheetDetailFieldsString(""),
      fixtureContactSheetDetailFieldsString(""),
      fixtureContactSheetDetailFieldsString(""),
      fixtureContactSheetDetailFieldsUrl("")
    ]
  };
  const contactSheetUserWorkplaceFields: ContactSheetDetailCategorie = {
    "name": "Lieu d'affectation",
    "fields": [
      fixtureContactSheetDetailFieldsString(""),
      fixtureContactSheetDetailFieldsString(""),
    ]
  };
  const contactSheetUser: ContactSheetModel = {
    "name": "Antoine Toutain",
    "subtitle1": "",
    "subtitle2": "",
    "picture": "",
    "isPersonal": false,
    "isFavorite": false,
    "isMeetingroom": false,
    "canBeFavorite": true,
    "status": {
      "phone": 0,
      "video": VideoEvents.Available
    },
    "actions": contactSheetUserActions,
    "sources": [
      {
        "name": "someldap",
        "id": ""
      },
      {
        "name": "internal",
        "id": "2"
      }
    ],
    "details": [
      contactSheetUserContactsFields,
      contactSheetUserGeneralFields,
      contactSheetUserWorkplaceFields
    ]
  };

  const contactSheetMRActions: {[key:string]: ContactSheetAction } = fixtureContactSheetActions(["4040"]);
  const contactSheetMRContactsFields: ContactSheetDetailCategorie = {
    "name": "Contacts",
    "fields": [
      fixtureContactSheetDetailFieldsPhoneNumber("4040","Numéro"),
      fixtureContactSheetDetailFieldsPhoneNumber("","Mobile"),
      fixtureContactSheetDetailFieldsPhoneNumber("","Domicile"),
      fixtureContactSheetDetailFieldsPhoneNumber(""),
      fixtureContactSheetDetailFieldsEmail(""),
      fixtureContactSheetDetailFieldsString("")
    ]
  };

  const contactSheetMR: ContactSheetModel = {...contactSheetUser,
    name:"toto",
    isFavorite: false,
    "status": {
      "phone": -2,
      "video": VideoEvents.Available
    },
    actions: contactSheetMRActions,
    sources: [
      {
        "name": "xivo_meetingroom",
        "id": "1"
      }
    ],
    details: [
      contactSheetMRContactsFields,
      contactSheetUserGeneralFields,
      contactSheetUserWorkplaceFields
    ]
  };

  beforeEach(() => {
    Cti.Topic(Cti.MessageType.USERDISPLAYNAME).clear();
  });

  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(angular.mock.module(function($provide: any) {
    let XucLinkProvider = {
      $get: function($q: any) {
        let loggedOutDefer = $q.defer();
        return {
          loggedOutDefer: loggedOutDefer,
          whenLogged: () => $q.resolve(user),
          whenLoggedOut: () => loggedOutDefer.promise
        };
      }
    };
    $provide.provider('XucLink', XucLinkProvider);
  }));

  beforeEach(angular.mock.inject(function(_XucDirectory_, _$rootScope_) {
    xucDirectory = _XucDirectory_;
    $rootScope = _$rootScope_;
    spyOn($rootScope, '$broadcast').and.callThrough();

    // Initialize XucLink by resolving promise
    $rootScope.$digest();
  }));

  it('should remove Favoris and Personal from header list', () => {
    const result = {
      "headers": ["Nom","Numéro","Mobile","Autre numéro","Favoris", "Perso", "Email"],
      "entries":[]
    };
    xucDirectory.onSearchResult(result);
    expect(xucDirectory.getHeaders()).toEqual(["Nom","Numéro","Mobile","Autre numéro","Email"]);
  });

  it('should not modify header list if Favoris is not in the list', () => {
    const result = {
      "headers": ["Nom","Numéro","Mobile","Autre numéro","Email"],
      "entries":[]
    };
    xucDirectory.onSearchResult(result);
    expect(xucDirectory.getHeaders()).toEqual(["Nom","Numéro","Mobile","Autre numéro","Email"]);
  });

  it('should broadcast notification on search result', () => {
    xucDirectory.onSearchResult(searchResult);
    expect(xucDirectory.getSearchResult()).toEqual(searchResult.entries);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('searchResultUpdated');
  });

  it('should broadcast notification on favorites result', () => {
    xucDirectory.onFavorites(favResult);
    expect(xucDirectory.getFavorites()).toEqual(favResult.entries);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('favoritesUpdated');
  });

  it('should update favorites and broadcast notification on favorites add', () => {
    const entries: ContactSheetModel[] = [{...contactSheetUser,isFavorite: false},{...contactSheetMR,isFavorite: false}];
    const result = {"headers":[], "sheets": entries};

    xucDirectory.onContactSheet(result);
    expect(xucDirectory.getFavoriteContactSheet()).toEqual([]);

    const successfulAdd: FavoritesUpdated = {"action":"Added","contact_id":"2","source":"internal"};
    xucDirectory.onFavoriteUpdated(successfulAdd);

    expect(xucDirectory.getFavoriteContactSheet()).toEqual([{ ...contactSheetUser, isFavorite: true}]);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('favoritesUpdated');
  });

  it('should update favorites and  broadcast notification on favorites remove', () => {
    const entries: ContactSheetModel[] = [{...contactSheetMR,isFavorite: true},{...contactSheetUser,isFavorite: true}];
    const result = {"headers":[], "sheets": entries};

    xucDirectory.onFavoriteContactSheet(result);
    expect(xucDirectory.getFavoriteContactSheet()).toEqual(entries);

    const successfulRm: FavoritesUpdated = {"action":"Removed","contact_id":"2","source":"internal"};
    xucDirectory.onFavoriteUpdated(successfulRm);

    expect(xucDirectory.getFavoriteContactSheet()).toEqual([{ ...contactSheetMR, isFavorite: true}]);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('favoritesUpdated');
  });

  it('should not fetch favorites from an update without source', () => {
    const contactSheetMergedUser: ContactSheetModel = {...contactSheetUser,
      name:"alice",
      sources: [
        {
          "name": "ldap",
          "id": "3"
        },
        {
          "name": "internal",
          "id": ""
        },
      ],
      isFavorite: false,
      isMeetingroom: false,
      canBeFavorite: true
    };
    const otherEntries: ContactSheetModel[] = [{...contactSheetUser,isFavorite: false},contactSheetMergedUser];
    const result = {"headers":[], "sheets": otherEntries};
    xucDirectory.onContactSheet(result);
    const addContactSheetUser: FavoritesUpdated = {"action":"Added","contact_id":"2","source":"internal"};
    xucDirectory.onFavoriteUpdated(addContactSheetUser);
    expect(xucDirectory.getFavoriteContactSheet()).toEqual([{ ...contactSheetUser, isFavorite: true}]);
    const addWithoutContatctId: FavoritesUpdated = {"action":"Added","contact_id":"","source":"internal"};

    xucDirectory.onFavoriteUpdated(addWithoutContatctId);
    expect(xucDirectory.getFavoriteContactSheet()).toEqual([{ ...contactSheetUser, isFavorite: true}]);
  });

  it('should not empty favorites on searchResult', () => {
    xucDirectory.onFavorites(favResult);
    xucDirectory.onSearchResult(searchResult);
    expect(xucDirectory.getFavorites()).toEqual(favResult.entries);
  });

  it('should lookup for search term and store it', () => {
    spyOn(Cti, 'directoryLookUp');
    xucDirectory.directoryLookup(null);
    expect(xucDirectory.getSearchTerm()).toEqual('');
    expect(Cti.directoryLookUp).toHaveBeenCalledWith('');

    xucDirectory.directoryLookup('user');
    expect(xucDirectory.getSearchTerm()).toEqual('user');
    expect(Cti.directoryLookUp).toHaveBeenCalledWith('user');
  });

  it('should show that a directory lookup is ongoing', () => {
    spyOn(Cti, 'directoryLookUp');
    expect(xucDirectory.isSearching()).toEqual(false);
    xucDirectory.directoryLookup('user');
    expect(xucDirectory.isSearching()).toEqual(true);
  });

  it('should show that lookup is finished', () => {
    spyOn(Cti, 'directoryLookUp');
    xucDirectory.directoryLookup('user');
    expect(xucDirectory.isSearching()).toEqual(true);

    xucDirectory.onSearchResult(searchResult);
    expect(xucDirectory.isSearching()).toEqual(false);
  });

  it('should broadcast notification on clear result', () => {
    xucDirectory.clearResults();
    expect(xucDirectory.getSearchResult()).toEqual([]);
    expect(xucDirectory.isSearching()).toEqual(false);
    expect(xucDirectory.getSearchTerm()).toEqual(null);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('searchResultUpdated');
  });

  it('should get display name from remote party username', () => {
    spyOn(Cti, 'displayNameLookup');
    const promise = xucDirectory.getUserDisplayName('jbond');
    let resolvedValue: string | undefined;
    promise.then((displayName: string) => {
      resolvedValue = displayName;
    });

    Cti.Topic(Cti.MessageType.USERDISPLAYNAME).publish({userName: 'jbond', displayName: 'James Bond'});
    $rootScope.$apply();

    expect(resolvedValue).toEqual('James Bond');
  });

  it('should handle if result has unexpected username', () => {
    spyOn(Cti, 'displayNameLookup');
    const promise = xucDirectory.getUserDisplayName('jbond');
    let resolvedValue: string | undefined;
    promise.then((displayName: string) => {
      resolvedValue = displayName;
    });

    Cti.Topic(Cti.MessageType.USERDISPLAYNAME).publish({userName: 'jdoe', displayName: 'Jane Doe'});
    $rootScope.$apply();

    expect(resolvedValue).toEqual(undefined);
  });

  it('should get correct display name for concurrent requests', () => {
    spyOn(Cti, 'displayNameLookup');
    const promise1 = xucDirectory.getUserDisplayName('jbond');
    let resolvedValue1: string | undefined;
    promise1.then((displayName: string) => {
      resolvedValue1 = displayName;
    });
    const promise2 = xucDirectory.getUserDisplayName('bwayne');
    let resolvedValue2: string | undefined;
    promise2.then((displayName: string) => {
      resolvedValue2 = displayName;
    });

    Cti.Topic(Cti.MessageType.USERDISPLAYNAME).publish({userName: 'bwayne', displayName: 'Bruce Wayne'});
    Cti.Topic(Cti.MessageType.USERDISPLAYNAME).publish({userName: 'jbond', displayName: 'James Bond'});
    $rootScope.$apply();

    expect(resolvedValue1).toEqual('James Bond');
    expect(resolvedValue2).toEqual('Bruce Wayne');
  });

  it('should retrieve display name from cache', () => {
    spyOn(Cti, 'displayNameLookup');
    const promiseNotCached = xucDirectory.getUserDisplayName('jbond');
    let resolvedValueNotCached: string|undefined;
    promiseNotCached.then((displayName:string) => { resolvedValueNotCached = displayName; });

    Cti.Topic(Cti.MessageType.USERDISPLAYNAME).publish({userName: 'jbond', displayName: 'James Bond'});
    $rootScope.$apply();

    expect(resolvedValueNotCached).toEqual('James Bond');

    const promiseCached = xucDirectory.getUserDisplayName('jbond');
    let resolvedValueCached: string|undefined;
    promiseCached.then((displayName:string) => { resolvedValueCached = displayName; });

    $rootScope.$apply();

    expect(resolvedValueCached).toEqual('James Bond');
    expect(Cti.displayNameLookup).toHaveBeenCalledTimes(1);
  });

  it('should not update every searchResults phone status on receiving a PhoneEvent displaying the login of one of them', () => {
    const zeusEntry: Array<string|boolean> = ["zeus","1501","","",false,"zeus@olympus.io"]
    const athenaEntry: Array<string|boolean> = ["athena","1502","","",false,"athena@olympus.io"]

    const inactiveContact: RichEntry = {...extContact,status:4,username:"ext1"};
    const athena: RichEntry =  {...inactiveContact, entry: athenaEntry, contact_id: "26",username:"athena"};
    const zeus: RichEntry = {...inactiveContact, entry: zeusEntry, contact_id: "27",username:"zeus"};
    const inactiveContacts: RichEntry[] = [inactiveContact, athena, zeus];
    const searchContactResult: SearchContactResult = {headers:[], entries: inactiveContacts};

    xucDirectory.onSearchResult(searchContactResult);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('searchResultUpdated');
    expect(xucDirectory.getSearchResult()).toEqual(inactiveContacts);

    const rawEvent = {
      number:"1502",
      status:1
    };
    Cti.Topic(Cti.MessageType.PHONEHINTSTATUSEVENT).publish(rawEvent);

    const updatedContacts = inactiveContacts.map((contact) => { if (contact.contact_id == "26") {
      return {...athena,status:1};
    } else {
      return contact;
    }});
    expect(xucDirectory.getSearchResult()).toEqual(updatedContacts);
  });

  it('should update videoStatus of user on event from XucVideoEventManager', () => {
    const zeusEntry: Array<string|boolean> = ["zeus","1501","","",false,"zeus@olympus.io"]
    const athenaEntry: Array<string|boolean> = ["athena","1502","","",false,"athena@olympus.io"]

    const inactiveContact: RichEntry = {...extContact,videoStatus:"Available",username:"ext1"};
    const athena: RichEntry =  {...inactiveContact, entry: athenaEntry, contact_id: "26",username:"athena"};
    const zeus: RichEntry = {...inactiveContact, entry: zeusEntry, contact_id: "27",username:"zeus"};
    const inactiveContacts: RichEntry[] = [inactiveContact, athena, zeus];
    const searchContactResult: SearchContactResult = {headers:[], entries: inactiveContacts};

    xucDirectory.onSearchResult(searchContactResult);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('searchResultUpdated');
    expect(xucDirectory.getSearchResult()).toEqual(inactiveContacts);

    const rawEvent = {
      status: "Busy",
      fromUser: "zeus"
    };
    Cti.Topic(Cti.MessageType.VIDEOEVENT).publish(rawEvent);
    $rootScope.$digest();

    const updatedContacts = inactiveContacts.map((contact) => { if (contact.contact_id == "27") {
      return {...zeus,videoStatus:'Busy'};
    } else {
      return contact;
    }});
    expect(xucDirectory.getSearchResult()).toEqual(updatedContacts);
  });

});
