import angular, {IRootScopeService} from 'angular'
import CtiStatusService from "../../../../app/assets/javascripts/xccti/services/CtiStatus.service";
import {CtiStatusEnum, CtiStatus} from  "../../../../app/assets/javascripts/xchelper/models/CtiStatus.model";
import {RichWindow} from '../../../../app/assets/javascripts/RichWindow';

describe('CtiStatus service', function () {
  let ctiStatusService: CtiStatusService;
  let $rootScope: IRootScopeService;
  let $window: RichWindow;

  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('karma-backend'));

  beforeEach(angular.mock.inject(function(_$rootScope_: IRootScopeService, _$window_: RichWindow, CtiStatusService: CtiStatusService) {
    $rootScope = _$rootScope_;
    $window = _$window_;
    ctiStatusService = CtiStatusService;
    spyOn($rootScope, '$broadcast');
  }));

  it('should have empty CtiStatuses at init', function() {

    expect(ctiStatusService.ctiStatuses).toEqual([]);

  });

  it('should have CtiStatuses', function() {

    ctiStatusService.onCtiStatuses(_ctiStatuses)
    expect($rootScope.$broadcast).toHaveBeenCalledWith('AgentUserStatusesLoaded');
    expect(ctiStatusService.ctiStatuses).toEqual(_ctiStatuses.sort((a, b) => a.status > b.status ? 1 : b.status > a.status ? -1 : 0));

  });

  it('should return CtiStatuses filtered', function() {

    ctiStatusService.onCtiStatuses(_ctiStatuses)
    expect($rootScope.$broadcast).toHaveBeenCalledWith('AgentUserStatusesLoaded');
    expect(ctiStatusService.getPossibleCtiStatus(CtiStatusEnum.AgentReady)).toEqual(_ctiStatuses.filter((elem: CtiStatus) => elem.status !== CtiStatusEnum.AgentReady));

  });

  it('should switch CtiStatuses', function() {

    expect(ctiStatusService.currentStatus).toEqual('');
    ctiStatusService.onCtiStatuses(_ctiStatuses)
    expect($rootScope.$broadcast).toHaveBeenCalledWith('AgentUserStatusesLoaded');
    ctiStatusService.switchAgentStateCtiStatus(CtiStatusEnum.AgentReady, 'toto');
    expect(ctiStatusService.currentStatus).toEqual('toto');

  });


  let _ctiStatuses: CtiStatus[] = [
    {
      name: '2',
      display_name: '2',
      status: 1
    },
    {
      name: 'donotdisturb',
      display_name: 'Ne pas déranger',
      status: 0
    },
    {
      name: 'outtolunch',
      display_name: 'Parti Manger',
      status: 0
    },
    {
      name: 'test',
      display_name: 'test',
      status: 0
    },
    {
      name: 'miam',
      display_name: 'j\'ai faim',
      status: 0
    },
    {
      name: 'available',
      display_name: 'Disponible',
      status: 0
    },
    {
      name: 'disconnected',
      display_name: 'Déconnecté',
      status: 2
    },
    {
      name: 'plop',
      display_name: 'PLOP',
      status: 1
    }
  ];
});