'use strict';

describe('XucNotification', function() {
  var XucNotification;
  var XucChat;
  var webNotification;
  var onHoldNotifier;
  var remoteConfiguration;
  var $rootScope;
  var $q;

  var holdTimeDefer;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });


  function getEvent(eventType) {
    return {
      "eventType": eventType,
      "DN":"1000",
      "otherDN":"1001",
      "linkedId":"1471858488.233",
      "uniqueId":"1471858490.236",
      "userData": {}
    };
  }

  function getVideoInvite(id, token, displayName, userName) {
    return { 
      "requestId" : id,
      "token": token,
      "displayName": displayName,
      "username": userName
    };
  }

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_XucNotification_, _$rootScope_, _$q_, _webNotification_, _onHoldNotifier_, _remoteConfiguration_, _XucChat_) {
    XucNotification = _XucNotification_;
    webNotification = _webNotification_;
    onHoldNotifier = _onHoldNotifier_;
    remoteConfiguration = _remoteConfiguration_;
    XucChat = _XucChat_;
    $rootScope = _$rootScope_;
    $q = _$q_;
    spyOn(Cti, 'sendCallback');
    holdTimeDefer = $q.defer();
    spyOn(remoteConfiguration, 'getIntOrElse').and.returnValue(holdTimeDefer.promise);
    spyOn(xc_webrtc, 'disableICE').and.callFake(() => {});

    Cti.Topic(Cti.MessageType.PHONEEVENT).clear();
    // Initialize XucLink
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    $rootScope.$digest();
  }));


  afterEach(function () {
    $rootScope.$digest();
  });

  it('check for permission when enabled', function() {    
    spyOn(window.Notification, "requestPermission");
    XucNotification.enableCallNotification();
    expect(window.Notification.requestPermission).toHaveBeenCalled();
    expect(XucNotification.isCallNotificationEnabled()).toEqual(true);
  });

  it('display desktop notification when call rings', function() {
    spyOn(webNotification, "showNotification");
    XucNotification.enableCallNotification();
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $rootScope.$digest();
    expect(webNotification.showNotification).toHaveBeenCalled();
  });

  it('display desktop notification when incoming video invite', function() {
    spyOn(webNotification, "showNotification");
    XucNotification.enableCallNotification();
    Cti.Topic(Cti.MessageType.MEETINGROOMINVITE).publish(getVideoInvite(1, "some-token", "Some User", "someuser"));
    $rootScope.$digest();
    expect(webNotification.showNotification).toHaveBeenCalled();
  });

  it('display desktop notification when call is on hold for a long time', function() {
    XucNotification.enableCallNotification();
    holdTimeDefer.resolve(1);
    $rootScope.$digest();
    spyOn(onHoldNotifier, 'notify').and.callFake(() => {
      var defer = $q.defer();
      defer.resolve({});
      return defer.promise;
    });

    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventOnHold'));
    $rootScope.$digest();
    expect(onHoldNotifier.notify).toHaveBeenCalled();
  });

  it('hide any desktop notification when call answered', function() {
    var cb = {hide: function() {}};
    spyOn(cb, "hide");
    spyOn(webNotification, "showNotification").and.callFake(function(title, options, callback) {
      callback(false, cb.hide);
    });
    XucNotification.enableCallNotification();
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $rootScope.$digest();
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventEstablished'));
    $rootScope.$digest();
    expect(cb.hide).toHaveBeenCalled();
  });

  it('hide any desktop notification when call released', function() {
    var cb = {hide: function() {}};
    spyOn(cb, "hide");
    spyOn(webNotification, "showNotification").and.callFake(function(title, options, callback) {
      callback(false, cb.hide);
    });
    XucNotification.enableCallNotification();
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventRinging'));
    $rootScope.$digest();
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(getEvent('EventReleased'));
    $rootScope.$digest();
    expect(cb.hide).toHaveBeenCalled();
  });

  it('display notification when new message is received and chat notification allowed', function(){
    var message = {
      content: "hello world",
    };

    spyOn(webNotification, "showNotification");
    XucNotification.enableChatNotification();
    $rootScope.$broadcast(XucChat.CHAT_RECEIVED_MESSAGE, message, 'jbond', true);
    $rootScope.$digest();
    expect(webNotification.showNotification).toHaveBeenCalled();
  });

  it('hide notification when new message is received but chat notification disabled', function(){
    var message = {
      content: "hello world"
    };

    spyOn(webNotification, "showNotification");
    XucNotification.enableCallNotification();
    $rootScope.$broadcast(XucChat.CHAT_RECEIVED_MESSAGE, message, 'jbond', true);
    $rootScope.$digest();
    expect(webNotification.showNotification).not.toHaveBeenCalled();
  });

});
