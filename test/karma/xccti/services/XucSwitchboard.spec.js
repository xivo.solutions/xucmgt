describe('Xuc switchboard', function () {
  var switchboard;
  var $q, $rootScope;
  var XucAgentUser, XucLink, XucQueue, XucUser, XucAgent;
  var logged;
  var errorModal;

  beforeEach(function () {
    jasmine.addMatchers({
      toEqualData: customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));

  beforeEach(angular.mock.inject(function (_$q_, _$rootScope_, _XucAgentUser_, _XucLink_, _XucQueue_, _XucUser_, _XucAgent_, _errorModal_) {
    $q =  _$q_;
    $rootScope = _$rootScope_;
    XucLink = _XucLink_;
    XucAgentUser = _XucAgentUser_;
    XucQueue = _XucQueue_;
    errorModal = _errorModal_;
    XucUser = _XucUser_;
    XucAgent = _XucAgent_;
    logged = $q.defer();
    spyOn(XucLink, 'whenLogged').and.returnValue(logged.promise);
  }));  

  beforeEach(angular.mock.inject(function (_XucSwitchboard_) {
    switchboard = _XucSwitchboard_;
  }));


  it('should receive the incoming and hold queues and instantiate them', function () {

    var agentQueues = $q.defer();
    agentQueues.resolve([
      {
        'id' : 1,
        'name': 'myincqueue'
      }
    ]);    

    var allQueues = $q.defer();
    allQueues.resolve([
      {
        'id': 1,
        'name': 'myincqueue'
      },

      {
        'id': 2,
        'name': 'queue3'
      },

      {
        'id': 3,
        'name': 'myincqueue_hold'
      },

      {
        'id': 4,
        'name': 'queue4'
      }
    ]);  
    
    let resultQueues = {
      'incQueue': {
        'id': 1,
        'name': 'myincqueue'
      },
      'holdQueue': {
        'id': 3,
        'name': 'myincqueue_hold'
      }
    };

    var agentId = $q.defer();
    agentId.resolve([{ agentId: '1' }]);

    var agentNumber = $q.defer();
    agentNumber.resolve([{ number: '35' }]);

    spyOn(XucAgentUser, 'getQueuesAsync').and.returnValue(agentQueues.promise);
    spyOn(XucQueue, 'getQueuesAsync').and.returnValue(allQueues.promise);
    spyOn(XucAgentUser, 'isMemberOfQueueAsync').and.returnValue(true);
    spyOn(XucUser, 'getUserAsync').and.returnValue(agentId.promise);
    spyOn(XucAgent, 'getAgentAsync').and.returnValue(agentNumber.promise);

    spyOn(Cti, 'subscribeToQueueCalls');
    spyOn(Cti, 'setHandler');
    switchboard.registerOnSwitchboard();
    $rootScope.$digest();
    expect(Cti.subscribeToQueueCalls).toHaveBeenCalledWith(resultQueues.incQueue.id);
    expect(Cti.subscribeToQueueCalls).toHaveBeenCalledWith(resultQueues.holdQueue.id);
    expect(Cti.setHandler).toHaveBeenCalled();
  });

  it('should receive the existing personal hold queues and instantiate it', function () {

    var agentQueues = $q.defer();
    agentQueues.resolve([
      {
        'id': 1,
        'name': 'myincqueue'
      }
    ]);

    var allQueues = $q.defer();
    allQueues.resolve([
      {
        'id': 1,
        'name': 'myincqueue'
      },

      {
        'id': 2,
        'name': 'myincqueue_hold'
      },

      {
        'id': 3,
        'name': 'myincqueue_hold_35'
      },

      {
        'id': 4,
        'name': 'myincqueue_hold_10'
      }
    ]);

    let resultQueues = {
      'incQueue': {
        'id': 1,
        'name': 'myincqueue'
      },
      'holdQueue': {
        'id': 3,
        'name': 'myincqueue_hold_35'
      }
    };

    var agentId = $q.defer();
    agentId.resolve({ agentId: '1' });

    var agentNumber = $q.defer();
    agentNumber.resolve({ number: '35' });

    spyOn(XucAgentUser, 'getQueuesAsync').and.returnValue(agentQueues.promise);
    spyOn(XucQueue, 'getQueuesAsync').and.returnValue(allQueues.promise);
    spyOn(XucAgentUser, 'isMemberOfQueueAsync').and.returnValue(true);
    spyOn(XucUser, 'getUserAsync').and.returnValue(agentId.promise);
    spyOn(XucAgent, 'getAgentAsync').and.returnValue(agentNumber.promise);

    spyOn(Cti, 'subscribeToQueueCalls');
    spyOn(Cti, 'setHandler');
    switchboard.registerOnSwitchboard();
    $rootScope.$digest();
    expect(Cti.subscribeToQueueCalls).toHaveBeenCalledWith(resultQueues.incQueue.id);
    expect(Cti.subscribeToQueueCalls).toHaveBeenCalledWith(resultQueues.holdQueue.id);
    expect(Cti.setHandler).toHaveBeenCalled();
  });


  it('should call retrieveQueueCall', function () {
    spyOn(Cti, 'retrieveQueueCall');
    let qc = {position: 1, channel : "SIP/abcd"};

    switchboard.retrieveQueueCall(qc);
    expect(Cti.retrieveQueueCall).toHaveBeenCalledWith(qc);
  });


  it('should logout the agent and throw an error if they are a member of more than one queue', function () {
    var agentQueues = $q.defer();
    agentQueues.resolve([
      {
        'id' : 1,
        'name': 'myincqueue'
      },

      {
        'id' : 2,
        'name': 'queue3'
      }
    ]);

    var allQueues = $q.defer();
    allQueues.resolve([
      {
        'id': 1,
        'name': 'myincqueue'
      },

      {
        'id': 2,
        'name': 'queue3'
      },

      {
        'id': 3,
        'name': 'myincqueue_hold'
      },

      {
        'id': 4,
        'name': 'queue4'
      }
    ]);

    spyOn(XucAgentUser, 'getQueuesAsync').and.returnValue(agentQueues.promise);
    spyOn(XucQueue, 'getQueuesAsync').and.returnValue(allQueues.promise);

    spyOn(errorModal, 'showErrorModal');

    switchboard.registerOnSwitchboard();

    $rootScope.$digest();
    expect(errorModal.showErrorModal).toHaveBeenCalled();
  });


  it('should logout the agent and throw an error if they are not a member of ANY queue', function () {

    var agentQueues = $q.defer();
    agentQueues.resolve([]);


    var allQueues = $q.defer();
    allQueues.resolve([
      {
        'id': 1,
        'name': 'myincqueue'
      },

      {
        'id': 2,
        'name': 'queue3'
      },

      {
        'id': 3,
        'name': 'myincqueue_hold'
      },

      {
        'id': 4,
        'name': 'queue4'
      }
    ]);

    spyOn(XucAgentUser, 'getQueuesAsync').and.returnValue(agentQueues.promise);
    spyOn(XucQueue, 'getQueuesAsync').and.returnValue(allQueues.promise);


    spyOn(errorModal, 'showErrorModal');

    switchboard.registerOnSwitchboard();

    $rootScope.$digest();

    expect(errorModal.showErrorModal).toHaveBeenCalled();
  });


  it('should throw an error if the hold queue are not found', function (done) {
    var agentQueues = $q.defer();
    agentQueues.resolve([
      {
        'id' : 1,
        'name': 'myincqueue'
      },
    ]);

    var allQueues = $q.defer();
    allQueues.resolve([
      {
        'id': 1,
        'name': 'myincqueue'
      },

      {
        'id': 2,
        'name': 'queue3'
      },

      {
        'id': 4,
        'name': 'queue4'
      }
    ]);

    var agentId = $q.defer();
    agentId.resolve({ agentId: '1' });

    var agentNumber = $q.defer();
    agentNumber.resolve({ number: '35' });

    spyOn(XucAgentUser, 'getQueuesAsync').and.returnValue(agentQueues.promise);
    spyOn(XucQueue, 'getQueuesAsync').and.returnValue(allQueues.promise);
    spyOn(XucUser, 'getUserAsync').and.returnValue(agentId.promise);
    spyOn(XucAgent, 'getAgentAsync').and.returnValue(agentNumber.promise);

    spyOn(errorModal, 'showErrorModal');

    var p = switchboard.getHoldQueueAsync('myincqueue');

    p.catch((err) => {
      expect(err).not.toBeUndefined();
      done();
    });

    $rootScope.$digest();
  });
});