'use strict';

import _ from 'lodash';
import moment from 'moment';

describe('Xuc service callback', function() {
  var xucQueue;
  var $rootScope;
  var xucCallback;

  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData : customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_XucQueue_,_$rootScope_,_XucCallback_) {
    xucQueue = _XucQueue_;
    $rootScope = _$rootScope_;
    xucCallback = _XucCallback_;
    spyOn($rootScope, '$broadcast');
    spyOn(Cti, 'sendCallback');

  }));

  it('init callback', function() {
    spyOn(Callback, 'init');

    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    $rootScope.$digest();

    expect(Callback.init).toHaveBeenCalledWith(Cti);
  });

  it('should order callbacks based on due status', function() {
 
    var statuses = [
      xucCallback.DUE_STATUS_NOW,
      xucCallback.DUE_STATUS_LATER,
      xucCallback.DUE_STATUS_NOW,
      xucCallback.DUE_STATUS_OVERDUE,
      xucCallback.DUE_STATUS_NOW
    ];
    var res = _.sortBy(statuses, xucCallback.callbackDueStatusOrder);
    
    expect(res).toEqual([
      xucCallback.DUE_STATUS_OVERDUE,
      xucCallback.DUE_STATUS_NOW,
      xucCallback.DUE_STATUS_NOW,
      xucCallback.DUE_STATUS_NOW,
      xucCallback.DUE_STATUS_LATER
    ]);
 
  });

  it('should ask for callback lists when queues are loaded', function() {
    spyOn(Callback, 'init');
    spyOn(Callback, 'getCallbackLists');

    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    $rootScope.$digest();
    xucQueue.onQueueList({});
    $rootScope.$digest();

    expect(Callback.init).toHaveBeenCalledWith(Cti);
    expect(Callback.getCallbackLists).toHaveBeenCalled();
  });

  it('should find callbacks and return them asynchronously', function() {
    var queue = QueueBuilder('test', 'Test').build();
    var cb1 = CallbackRequestBuilder('1000').build();
    cb1.uuid = '123';
    cb1.agentId = 12;
    var cb2 = CallbackRequestBuilder('1001').build();
    cb2.uuid = '456';
    var list = CallbackListBuilder('Test', queue.id, [cb1, cb2]).build();

    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    xucQueue.onQueueList([queue]);
    xucCallback._onCallbackLists([list]);
    $rootScope.$digest();
    var rqId = 1;
    var query = { filters: [{field:"firstName", operator:"like", value:"%James%"}], offset: 0, limit: 100 };
    var innerResponse = {total: 5, list: []};
    var resp = {id: rqId, response: innerResponse};
    spyOn(Callback, 'findCallbackRequest');

    var cbGet = null;
    xucCallback.findCallbackRequestsAsync(query).then(function(r) { cbGet = r;});
    $rootScope.$digest();
    expect(Callback.findCallbackRequest).toHaveBeenCalledWith(rqId, query.filters, query.offset, query.limit);
    Cti.Topic(Callback.MessageType.CALLBACKFINDRESPONSE).publish(resp);
    $rootScope.$digest();
    expect(cbGet).toEqual(innerResponse);
  });

  it('should mix callbacks with their queue when they are loaded', function() {
    var queue = QueueBuilder('test', 'Test').build();
    var cb1 = CallbackRequestBuilder('1000').build();
    var cb2 = CallbackRequestBuilder('1001').build();
    var l1 = CallbackListBuilder('Test', queue.id, [cb1]).build();
    var l2 = CallbackListBuilder('Test 2', queue.id, [cb2]).build();

    xucQueue.onQueueList([queue]);
    xucCallback._onCallbackLists([l1, l2]);
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    $rootScope.$digest();

    expect(xucCallback.getCallbackLists()).toEqual([l1, l2]);
    expect(l1.queue).toEqual(queue);
    expect(l2.queue).toEqual(queue);
    expect(l1.callbacks.length).toEqual(1);
    expect(l2.callbacks.length).toEqual(1);
    expect(l1.callbacks[0].queue).toEqual(queue);
    expect(l2.callbacks[0].queue).toEqual(queue);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('CallbacksLoaded');
  });

  it('should compute callbacks due status', function() {
    var queue = QueueBuilder('test', 'Test').build();
    var cb1 = CallbackRequestBuilder('1000').withDueDate(moment().subtract(1, "days").format("YYYY-MM-DD")).build();
    var cb2 = CallbackRequestBuilder('1001').withDueDate(moment().format("YYYY-MM-DD")).build();
    var cb3 = CallbackRequestBuilder('1002').withDueDate(moment().add(1, "days").format("YYYY-MM-DD")).build();
    var l1 = CallbackListBuilder('Test', queue.id, [cb1, cb2, cb3]).build();
    

    xucQueue.onQueueList([queue]);
    xucCallback._onCallbackLists([l1]);
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    $rootScope.$digest();
    var cbres = xucCallback.getCallbackLists();
    expect(cbres.length).toEqual(1);
    expect(cbres[0].callbacks.length).toEqual(3);
    expect(cbres[0].callbacks[0].dueStatus).toEqual(xucCallback.DUE_STATUS_OVERDUE);
    expect(cbres[0].callbacks[1].dueStatus).toEqual(xucCallback.DUE_STATUS_NOW);
    expect(cbres[0].callbacks[2].dueStatus).toEqual(xucCallback.DUE_STATUS_LATER);
  });

  it('should take a callback', function() {
    spyOn(Callback, 'takeCallback');
    var uuid = '12345-abcd';

    xucCallback.takeCallback(uuid);
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    $rootScope.$digest();

    expect(Callback.takeCallback).toHaveBeenCalledWith(uuid);
  });

  it('should set the agentId when a callback is taken and broadcast the event', function() {
    var queue = QueueBuilder('test', 'Test').build();
    var cb1 = CallbackRequestBuilder('1000').build();
    cb1.uuid = '123';
    var cb2 = CallbackRequestBuilder('1001').build();
    cb2.uuid = '456';
    var list = CallbackListBuilder('Test', queue.id, [cb1, cb2]).build();

    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    xucQueue.onQueueList([queue]);
    xucCallback._onCallbackLists([list]);
    $rootScope.$digest();
    xucCallback._onCallbackTaken({uuid: cb1.uuid, agentId: 12});
    var cbGet = null;
    xucCallback.getCallbackByUuidAsync(cb1.uuid).then(function(cb) { cbGet = cb;});
    $rootScope.$digest();

    expect($rootScope.$broadcast).toHaveBeenCalledWith('CallbackTaken', {uuid: cb1.uuid, agentId: 12});
    expect(cbGet.agentId).toEqual(12);
  });

  it('should release a callback', function() {
    spyOn(Callback, 'releaseCallback');
    var uuid = '12456';

    xucCallback.releaseCallback(uuid);

    expect(Callback.releaseCallback).toHaveBeenCalledWith(uuid);
  });

  it('should unset the agentId when a callback is released and broadcast the event', function() {
    var queue = QueueBuilder('test', 'Test').build();
    var cb1 = CallbackRequestBuilder('1000').build();
    cb1.uuid = '123';
    cb1.agentId = 12;
    var cb2 = CallbackRequestBuilder('1001').build();
    cb2.uuid = '456';
    var list = CallbackListBuilder('Test', queue.id, [cb1, cb2]).build();

    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    xucQueue.onQueueList([queue]);
    xucCallback._onCallbackLists([list]);
    $rootScope.$digest();
    xucCallback._onCallbackReleased({uuid: cb1.uuid});

    var cbGet = null;
    xucCallback.getCallbackByUuidAsync(cb1.uuid).then(function(cb) { cbGet = cb;});
    $rootScope.$digest();

    expect(cbGet.agentId).toEqual(null);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('CallbackReleased', {uuid: cb1.uuid});
  });

  it('should start a callback', function() {
    spyOn(Callback, 'startCallback');
    var uuid = '12456';
    var number = '3000';

    xucCallback.startCallback(uuid, number);

    expect(Callback.startCallback).toHaveBeenCalledWith(uuid,number);
  });

  it('should start listen callback message', function() {
    spyOn(Callback, 'listenCallbackMessage');
    var mevoRef = '12456';

    xucCallback.listenCallbackMessage(mevoRef);

    expect(Callback.listenCallbackMessage).toHaveBeenCalledWith(mevoRef);
  });


  it('should broadcast the event when a callback is started', function() {
    var cbStarted = {uuid: '123456'};
    xucCallback._onCallbackStarted(cbStarted);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('CallbackStarted', cbStarted);
  });

  it('should update a callback', function() {
    spyOn(Callback, 'updateCallbackTicket');
    var uuid = '123456', status = 'Fax', comment = 'The comment';
    var ticket = {
      uuid: uuid,
      status: status,
      comment: comment
    };

    xucCallback.updateCallbackTicket(ticket);

    expect(Callback.updateCallbackTicket).toHaveBeenCalledWith(uuid,status, comment, undefined, undefined);
  });

  it('should update a callback and reschedule when asked', function() {
    spyOn(Callback, 'updateCallbackTicket');
    var uuid = '123456',
      status = 'Fax',
      comment = 'The comment',
      dueDate = "2016-08-09",
      periodUuid= "1234567890";

    var ticket = {
      uuid: uuid,
      status: status,
      comment: comment,
      dueDate: dueDate,
      periodUuid: periodUuid
    };

    xucCallback.updateCallbackTicket(ticket);

    expect(Callback.updateCallbackTicket).toHaveBeenCalledWith(uuid,status, comment, dueDate, periodUuid);
  });

  it('should remove a callback when clotured and broadcast the event', function() {
    var queue = QueueBuilder('test', 'Test').build();
    var cb1 = CallbackRequestBuilder('1000').build();
    cb1.uuid = '123';
    var cb2 = CallbackRequestBuilder('1001').build();
    cb2.uuid = '456';
    var list = CallbackListBuilder('Test', queue.id, [cb1, cb2]).build();

    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    xucQueue.onQueueList([queue]);
    xucCallback._onCallbackLists([list]);
    $rootScope.$digest();

    xucCallback._onCallbackClotured({uuid: cb1.uuid});

    expect(list.callbacks).toEqual([cb2]);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('CallbackClotured', {uuid: cb1.uuid});
  });

  it('should build a find query', function() {
    var q = xucCallback.queryBuilder()
      .filterEq("clotured", false)
      .filterIsNull("agentId")
      .filterLte("dueDate", "2016-03-09")
      .build();

    var expected = {
      filters: [
        {field: 'clotured', operator: '=', value: 'false'},
        {field: 'agentId', operator: 'is null'},
        {field: 'dueDate', operator: '<=', value: '2016-03-09'}
      ],
      offset: 0,
      limit: 100
    };

    expect(q).toEqual(expected);
  });
});
