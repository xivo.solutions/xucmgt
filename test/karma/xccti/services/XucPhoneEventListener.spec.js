'use strict';
describe('XucPhoneEventListener', function() {
  var $rootScope;
  var XucPhoneEventListener;
  var $scope;

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function (_$rootScope_, _XucPhoneEventListener_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();

    XucPhoneEventListener = _XucPhoneEventListener_;
    spyOn(Cti, 'sendCallback');
    spyOn(xc_webrtc, 'getMediaTypeBySipCallId').and.callFake(function() {
      return xc_webrtc.mediaType.AUDIO;
    });

    Cti.Topic(Cti.MessageType.PHONEEVENT).clear();

    // Initialize XucLink
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    $rootScope.$digest();
  }));

  afterEach(function() {
    $scope.$destroy();
    $rootScope.$digest();
  });

  function getEvent(eventType, newSipCallId) {
    return {
      "eventType": eventType,
      "DN":"1000",
      "otherDN":"1001",
      "linkedId":"1471858488.233",
      "uniqueId":"1471858490.236",
      "queueName":"trucks",
      "userData":{
        "XIVO_CONTEXT":"default",
        "XIVO_USERID":"2",
        "XIVO_SRCNUM":"1001",
        "XIVO_DSTNUM":"3001",
        "SIPCALLID": newSipCallId || 'otherId'},
    };
  }
  
  function testPublishEvents(spyee, eventTypes) {
    angular.forEach(eventTypes, function(expected, eventType) {
      spyee.calls.reset();

      var rawEvent = getEvent(eventType);
      Cti.Topic(Cti.MessageType.PHONEEVENT).publish(rawEvent);
      $rootScope.$digest();

      if(expected) {
        var event = getEvent(eventType);
        event.callType = xc_webrtc.mediaType.AUDIO;
        expect(spyee).toHaveBeenCalledWith(event);
      } else {
        expect(spyee).not.toHaveBeenCalled();
      }
    });

  }

  it('should request currentCallsPhoneEvents when asked for', function() {
    spyOn(xc_webrtc, 'doesSessionExists').and.callFake(function(sipCallId) {
      return sipCallId=='doesNotExist';    
    });
    spyOn(Cti, 'getCurrentCallsPhoneEvents');
    XucPhoneEventListener.requestPhoneEventsForCurrentCalls();
    expect(Cti.getCurrentCallsPhoneEvents).toHaveBeenCalled();
  });

  it('should notify when receiving any event', function () {
    spyOn(xc_webrtc, 'doesSessionExists').and.callFake(function(sipCallId) {
      return sipCallId=='doesNotExist';    
    });
    var eventTypes = {
      'EventDialing': true,
      'EventRinging': true,
      'EventEstablished': true,
      'EventReleased': true,
      'EventOnHold': true
    };

    var handler = {
      callback: function () {}
    };

    spyOn(handler, 'callback');

    XucPhoneEventListener.addHandler($scope, handler.callback);

    testPublishEvents(handler.callback, eventTypes);

  });

  it('should notify when receiving any event (when register by addHandlerCustom)', function () {
    spyOn(xc_webrtc, 'doesSessionExists').and.callFake(function(sipCallId) {
      return sipCallId=='doesNotExist';    
    });
    var eventTypes = {
      'EventRinging': true,
      'EventEstablished': true,
      'EventReleased': true,
    };

    var handler = {
      callback: function () {}
    };

    spyOn(handler, 'callback');

    XucPhoneEventListener.addHandlerCustom(handler.callback);

    testPublishEvents(handler.callback, eventTypes);
  });


  it('should notify only when receiving EventDialing event', function () {
    spyOn(xc_webrtc, 'doesSessionExists').and.callFake(function(sipCallId) {
      return sipCallId=='doesNotExist';    
    });
    var eventTypes = {
      'EventDialing': true,
      'EventRinging': false,
      'EventEstablished': false,
      'EventReleased': false,
      'EventOnHold': false
    };

    var handler = {
      callback: function () {}
    };

    spyOn(handler, 'callback');

    XucPhoneEventListener.addDialingHandler($scope, handler.callback);

    testPublishEvents(handler.callback, eventTypes);
  });

  it('should notify only when receiving EventRinging event', function () {
    spyOn(xc_webrtc, 'doesSessionExists').and.callFake(function(sipCallId) {
      return sipCallId=='doesNotExist';    
    });
    var eventTypes = {
      'EventDialing': false,
      'EventRinging': true,
      'EventEstablished': false,
      'EventReleased': false,
      'EventOnHold': false
    };

    var handler = {
      callback: function () {}
    };

    spyOn(handler, 'callback');

    XucPhoneEventListener.addRingingHandler($scope, handler.callback);

    testPublishEvents(handler.callback, eventTypes);
  });

  it('should notify only when receiving EventEstablished event', function () {
    spyOn(xc_webrtc, 'doesSessionExists').and.callFake(function(sipCallId) {
      return sipCallId=='doesNotExist';    
    });
    var eventTypes = {
      'EventDialing': false,
      'EventRinging': false,
      'EventEstablished': true,
      'EventReleased': false,
      'EventOnHold': false
    };

    var handler = {
      callback: function () {}
    };

    spyOn(handler, 'callback');

    XucPhoneEventListener.addEstablishedHandler($scope, handler.callback);

    testPublishEvents(handler.callback, eventTypes);
  });

  it('should notify only when receiving EventReleased event', function () {
    spyOn(xc_webrtc, 'doesSessionExists').and.callFake(function(sipCallId) {
      return sipCallId=='doesNotExist';    
    });
    var eventTypes = {
      'EventDialing': false,
      'EventRinging': false,
      'EventEstablished': false,
      'EventReleased': true,
      'EventOnHold': false
    };

    var handler = {
      callback: function () {}
    };

    spyOn(handler, 'callback');

    XucPhoneEventListener.addReleasedHandler($scope, handler.callback);

    testPublishEvents(handler.callback, eventTypes);
  });

  it('should notify only when receiving EventOnHold event', function () {
    spyOn(xc_webrtc, 'doesSessionExists').and.callFake(function(sipCallId) {
      return sipCallId=='doesNotExist';    
    });
    var eventTypes = {
      'EventDialing': false,
      'EventRinging': false,
      'EventEstablished': false,
      'EventReleased': false,
      'EventOnHold': true
    };

    var handler = {
      callback: function () {}
    };

    spyOn(handler, 'callback');

    XucPhoneEventListener.addOnHoldHandler($scope, handler.callback);

    testPublishEvents(handler.callback, eventTypes);
  });

  it('should notify when receiving any event after reconnecting', function () {
    spyOn(xc_webrtc, 'doesSessionExists').and.callFake(function(sipCallId) {
      return sipCallId=='doesNotExist';    
    });
    var eventTypes = {
      'EventDialing': true,
      'EventRinging': true,
      'EventEstablished': true,
      'EventReleased': true,
      'EventOnHold': true
    };

    var handler = {
      callback: function () {}
    };

    spyOn(handler, 'callback');
    Cti.Topic(Cti.MessageType.LINKSTATUSUPDATE).publish({status: 'closed'});
    Cti.Topic(Cti.MessageType.PHONEEVENT).clear();

    // Re-Initialize XucLink
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    $rootScope.$digest();

    XucPhoneEventListener.addHandler($scope, handler.callback);

    testPublishEvents(handler.callback, eventTypes);

  });

  it('should notify when receiving CurrentCallsPhoneEvents after registering its handler', function () {
    spyOn(xc_webrtc, 'doesSessionExists').and.callFake(function(sipCallId) {
      return sipCallId=='doesNotExist';    
    });
    var handler = {
      callback: function () {}
    };
    spyOn(handler, 'callback');

    XucPhoneEventListener.addCurrentCallsPhoneEventsHandler($scope, handler.callback);

    var events = ['testEntry'];
    var currentCallsPhoneEvents = {'events': events};
    Cti.Topic(Cti.MessageType.CURRENTCALLSPHONEEVENTS).publish(currentCallsPhoneEvents );
    $rootScope.$digest();

    expect(handler.callback).toHaveBeenCalledWith(events);
  });

  it('should notify when receiving CurrentCallsPhoneEvents after registering as custom handler', function () {
    spyOn(xc_webrtc, 'doesSessionExists').and.callFake(function(sipCallId) {
      return sipCallId=='doesNotExist';    
    });
    var handler = {
      callback: function () {}
    };
    spyOn(handler, 'callback');

    XucPhoneEventListener.addHandlerCustom(handler.callback, 'CurrentCallsEvents');

    var events = ['testEntry'];
    var currentCallsPhoneEvents = {'events': events};
    Cti.Topic(Cti.MessageType.CURRENTCALLSPHONEEVENTS).publish(currentCallsPhoneEvents );
    $rootScope.$digest();

    expect(handler.callback).toHaveBeenCalledWith(events);
  });

  it('adds the media type based on the sip callId', function() {
    spyOn(xc_webrtc, 'doesSessionExists').and.callFake(function(sipCallId) {
      return sipCallId=='doesNotExist';    
    });
    var eventTypes = {
      'EventRinging': true,
      'EventEstablished': true,
      'EventReleased': true,
    };

    var handler = {
      callback: function () {}
    };

    spyOn(handler, 'callback');

    XucPhoneEventListener.addHandlerCustom(handler.callback);

    testPublishEvents(handler.callback, eventTypes, true);
  });

  it('fallback on audio media type when the sip callId is missing', function() {
    spyOn(xc_webrtc, 'doesSessionExists').and.callFake(function(sipCallId) {
      return sipCallId=='doesNotExist';    
    });
    var handler = {
      callback: function () {}
    };

    spyOn(handler, 'callback');

    XucPhoneEventListener.addHandlerCustom(handler.callback);
    var rawEvent = getEvent('EventRinging');
    Cti.Topic(Cti.MessageType.PHONEEVENT).publish(rawEvent);
    $rootScope.$digest();

    var expected = getEvent('EventRinging');
    expected.callType = xc_webrtc.mediaType.AUDIO;
    expect(handler.callback).toHaveBeenCalledWith(expected);
  });

    
  it("does not propagate event if xc_webrtc doesn't know the sipCallId when using mobile App except release one", function () {
    spyOn(xc_webrtc, 'doesSessionExists').and.callFake(function(sipCallId) {
      return sipCallId=='doesNotExist';    
    });

    $rootScope.$broadcast('isUsingMobileApp', true);
    $rootScope.$digest();

    var eventTypes = {
      'EventDialing': false,
      'EventRinging': false,
      'EventReleased': true,
      'EventOnHold': false
    };

    var handler = {
      callback: function () {}
    };

    spyOn(handler, 'callback');

    XucPhoneEventListener.addHandler($scope, handler.callback);

    testPublishEvents(handler.callback, eventTypes);

  });

  it("does propagate event if xc_webrtc know the sipCallId when using mobile App", function () {
    spyOn(xc_webrtc, 'doesSessionExists').and.callFake(function() {
      return true;    
    });

    $rootScope.$broadcast('isUsingMobileApp', true);
    $rootScope.$digest();

    var eventTypes = {
      'EventDialing': true,
      'EventRinging': true,
      'EventReleased': true,
      'EventOnHold': true,
      'EventEstablished': true
    };

    var handler = {
      callback: function () {}
    };

    spyOn(handler, 'callback');

    XucPhoneEventListener.addHandler($scope, handler.callback);

    testPublishEvents(handler.callback, eventTypes);

  });

});
