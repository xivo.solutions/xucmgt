
describe('Xuc service user', function() {
  var xucUser;
  var $rootScope;


  beforeEach(function() {
    jasmine.addMatchers({
      toEqualData :  customMatchers.toEqualData
    });
    spyOn(Cti,'setHandler').and.callThrough();
    Cti.debugMsg = true;
  });

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_XucUser_,_$rootScope_) {
    $rootScope = _$rootScope_;
    xucUser = _XucUser_;
    spyOn($rootScope, '$broadcast');
    spyOn(Cti, 'sendCallback');
  }));


  it('setup handlers when starting', function() {
    expect(Cti.setHandler).toHaveBeenCalled();
  });

  it('initialise user', function(){
    var initUser = {};
    initUser.fullName = '--------';
    initUser.naFwdEnabled = false;
    initUser.uncFwdEnabled = false;
    initUser.busyFwdEnabled = false;

    expect(xucUser.getUser()).toEqualData(initUser);
  });

  it('get user asynchronously', function() {
    var cb = {
      getUserCb: function(o) {
        expect(o.id).toEqual(5);
        expect(o.firstName).toEqual("James");
        expect(o.lastName).toEqual("Bond");
      }
    };
    spyOn(cb, 'getUserCb');

    var userConfig = {
      id:5,
      userId: 1,
      agentId: 5,
      firstName: "James",
      lastName: "Bond",
      fullName: "James Bond"
    };

    xucUser.getUserAsync().then(cb.getUserCb);
    xucUser.onUserConfig(userConfig);

    $rootScope.$digest();
    expect(cb.getUserCb).toHaveBeenCalled();

  });

  it('update user on userconfig received and broadcast userConfigUpdated event', function() {

    var userConfig = {};
    userConfig.userId = 1;
    userConfig.firstName = "Jack";
    userConfig.lastName = "London";
    userConfig.fullName = "Jack London";

    xucUser.onUserConfig(userConfig);
    $rootScope.$digest();

    expect(xucUser.getUser()).toEqualData(userConfig);
    expect($rootScope.$broadcast).toHaveBeenCalledWith('userConfigUpdated');
  });

  it('does not update user when fullName is null ', function() {
    var userConfig = {};
    userConfig.userId = 1;
    userConfig.firstName = "Jack";
    userConfig.lastName = "London";
    userConfig.fullName = "Jack London";

    xucUser.onUserConfig(userConfig);

    userConfig.firstName = null;
    userConfig.fullName = null;

    xucUser.onUserConfig(userConfig);

    expect(xucUser.getUser().firstName).toBe("Jack");

  });

  it('update user na forwards when fullname is null', function(){
    var userConfig = {};
    userConfig.fullName = null;
    userConfig.naFwdEnabled = true;
    userConfig.naFwdDestination = "4567";

    xucUser.onUserConfig(userConfig);

    expect(xucUser.getUser().naFwdEnabled).toBe(true);
    expect(xucUser.getUser().naFwdDestination).toBe("4567");
  });

  it('update user unc forwards when fullname is null', function(){
    var userConfig = {};
    userConfig.fullName = null;
    userConfig.uncFwdEnabled = true;
    userConfig.uncFwdDestination = "8877";

    xucUser.onUserConfig(userConfig);

    expect(xucUser.getUser().uncFwdEnabled).toBe(true);
    expect(xucUser.getUser().uncFwdDestination).toBe("8877");
  });

  it('update user busy forwards when fullname is null', function(){
    var userConfig = {};
    userConfig.fullName = null;
    userConfig.busyFwdEnabled = true;
    userConfig.busyFwdDestination = "11111";

    xucUser.onUserConfig(userConfig);

    expect(xucUser.getUser().busyFwdEnabled).toBe(true);
    expect(xucUser.getUser().busyFwdDestination).toBe("11111");
  });

  it('update only relevant forward when other dest are null', function() {
    var userConfig = {};
    userConfig.fullName = null;
    userConfig.busyFwdEnabled = true;
    userConfig.busyFwdDestination = "77777";

    xucUser.onUserConfig(userConfig);

    userConfig.fullName = null;
    userConfig.busyFwdEnabled = false;
    userConfig.busyFwdDestination = null;
    userConfig.uncFwdEnabled = false;
    userConfig.uncFwdDestination = "999999";

    xucUser.onUserConfig(userConfig);

    expect(xucUser.getUser().busyFwdEnabled).toBe(true);
    expect(xucUser.getUser().busyFwdDestination).toBe("77777");
    expect(xucUser.getUser().uncFwdEnabled).toBe(false);
    expect(xucUser.getUser().uncFwdDestination).toBe("999999");

  });
});