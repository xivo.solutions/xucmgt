'use strict';
describe('XucVideoEventManager', function() {
  var $rootScope;
  var XucVideoEventManager;
  var $scope;

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function (_$rootScope_, _XucVideoEventManager_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();

    XucVideoEventManager = _XucVideoEventManager_;

    Cti.Topic(Cti.MessageType.VIDEOEVENT).clear();
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");
    $rootScope.$digest();
  }));

  afterEach(function() {
    $scope.$destroy();
    $rootScope.$digest();
  });

  function getEvent(status) {
    return {
      "status": status,
      "fromUser": "dummyUser"
    };
  }
  
  function testPublishEvents(spyee, eventTypes) {
    angular.forEach(eventTypes, function(expected, eventType) {
      spyee.calls.reset();

      var rawEvent = getEvent(eventType);
      Cti.Topic(Cti.MessageType.VIDEOEVENT).publish(rawEvent);
      $rootScope.$digest();

      if(expected) {
        var event = getEvent(eventType);
        expect(spyee).toHaveBeenCalledWith(event);
      } else {
        expect(spyee).not.toHaveBeenCalled();
      }
    });
  }


  it('should notify user status when receiving video event', function () {
    var eventTypes = {
      'Available': true,
      'Busy': true
    };

    var handler = {
      callback: function () {}
    };

    spyOn(handler, 'callback');

    XucVideoEventManager.subscribeToVideoStatusEvent($scope, handler.callback);
    testPublishEvents(handler.callback, eventTypes);
  });

  it('should notify when receiving MeetingRoomInvite event', function () {
    var eventTypes = {
      'Busy': false,
      'Available': false,
      'MeetingRoomInvite': true
    };

    var handler = {
      callback: function () {}
    };

    spyOn(handler, 'callback');

    XucVideoEventManager.subscribeToVideoInviteEvent($scope, handler.callback);
    testPublishEvents(handler.callback, eventTypes);
  });
});