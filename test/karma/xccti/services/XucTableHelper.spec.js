'use strict';
describe('Xuc service TableHelper', function() {
  var xucTableHelper;

  beforeEach(function () {
    jasmine.addMatchers({
      toEqualData: customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function (_XucTableHelper_) {
    xucTableHelper = _XucTableHelper_;
  }));

  it('should create settings with given data', function () {
    var data = [{a:1, b:1}, {a:2, b:2}, {a:3, b:42}];
    var settings = xucTableHelper.createSettings(data);

    var params = {
      sorting: function() { return false; },
      orderBy: function() { return 'a'; },
      page: function() { return 1; },
      count: function() { return 5; },
      _total: 0,
      total: function(c) { if(typeof(c) !== undefined) params._total = c ; return params._total;}
    };


    settings.getData(params);

    expect(settings.total).toEqual(data.length);
  });

  it('should create settings with given function data', function () {
    var data = [{a:1, b:1}, {a:2, b:2}, {a:3, b:42}];
    var fnData = function() { return data;};
    var settings = xucTableHelper.createSettings(fnData);

    var params = {
      sorting: function() { return false; },
      orderBy: function() { return 'a'; },
      page: function() { return 1; },
      count: function() { return 5; },
      _total: 0,
      total: function(c) { if(typeof(c) !== undefined) params._total = c ; return params._total;}
    };

    settings.getData(params);

    expect(settings.total).toEqual(data.length);
  });

  it('should filter out data overflowing page size', function () {
    var data = [{a:1, b:1}, {a:2, b:2}, {a:3, b:42}, {a:4, b:0}];
    var settings = xucTableHelper.createSettings(data);

    var params = {
      sorting: function() { return false; },
      orderBy: function() { return 'a'; },
      page: function() { return 1; },
      count: function() { return 2; },
      _total: 0,
      total: function(c) { if(typeof(c) !== undefined) params._total = c ; return params._total;}
    };


    settings.getData(params);


    expect(settings.total).toEqual(data.length);
  });

  it('should return second page when asked', function () {
    var data = [{a:1, b:1}, {a:2, b:2}, {a:3, b:42}, {a:4, b:0}];
    var settings = xucTableHelper.createSettings(data);

    var params = {
      sorting: function() { return false; },
      orderBy: function() { return 'a';},
      page: function() { return 2; },
      count: function() { return 2; },
      _total: 0,
      total: function(c) { if(typeof(c) !== undefined) params._total = c ; return params._total;}
    };


    settings.getData(params);


    expect(settings.total).toEqual(data.length);
  });

  it('should return second page when asked, even when ordering', function () {
    var data = [{a:1, b:1}, {a:2, b:2}, {a:3, b:42}, {a:4, b:0}];
    var settings = xucTableHelper.createSettings(data);

    var params = {
      sorting: function() { return true; },
      orderBy: function() { return 'b'; },
      page: function() { return 2; },
      count: function() { return 2; },
      _total: 0,
      total: function(c) { if(typeof(c) !== undefined) params._total = c ; return params._total;}
    };

    settings.getData(params);


    expect(settings.total).toEqual(data.length);
  });
});
