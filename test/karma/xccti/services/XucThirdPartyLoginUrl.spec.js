'use strict';

describe('XucThirdPartyLoginUrl', function() {
  var $rootScope;
  var $scope;
  var $httpBackend;
  var XucLink;
  var $window;

  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function (_$rootScope_, _XucThirdPartyLoginUrl_, _$httpBackend_, _XucLink_, _$window_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();

    $httpBackend = _$httpBackend_;
    XucLink = _XucLink_;
    $window = _$window_;

    $httpBackend.whenGET("/config/thirdPartyWsLoginUrl").respond(200, {value:"/logininfo?login=%{login}&token=%{token}&xivocchost=%{xivocchost}"});
    

    spyOn(Cti, 'sendCallback');

    // Initialize XucLink
    Cti.Topic(Cti.MessageType.LOGGEDON).publish("");

    $window.externalConfig = { host: '192.168.56.100'};    

  }));

  afterEach(function () {
    $httpBackend.resetExpectations();
    $scope.$destroy();
  });

  it('calls the url on login and replace its params', function(){
    spyOn(XucLink, 'getXucUser').and.returnValue({token:'123456', username: 'jbond'});
    $httpBackend.flush();
    $rootScope.$broadcast('ctiLoggedOn');

    $httpBackend.expectGET('/logininfo?login=jbond&token=123456&xivocchost=192.168.56.100').respond('OK');

    $httpBackend.verifyNoOutstandingExpectation();
  });

});
