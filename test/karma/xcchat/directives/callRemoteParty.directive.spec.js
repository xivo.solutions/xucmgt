describe('call remote party directive', () => {
  
  var scope;
  var directiveScope;
  var $compile;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcChat'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));

  
  beforeEach(function() {
    
    angular.mock.inject(function (_$compile_, $rootScope) {
      scope = $rootScope.$new();
      $compile = _$compile_;
    });
  });
  
  var getCompiledElement = function () {
    var element = angular.element('<call-remote-party></call-remote-party>');
    var compiledElem = $compile(element)(scope);
    scope.$digest();
    return compiledElem;
  };
  
  it('generates html when instantiated', function() {
    var element = getCompiledElement();
    directiveScope = element.children().scope();
    scope.$digest();
    
    expect(element.html()).toContain('nd-action-button');
  });
  
  it('calls the remote party username when clicking the button', function() {
    spyOn(Cti, 'dialByUsername');
    var element = getCompiledElement();
    directiveScope = element.children().scope();
    scope.$digest();
    directiveScope.remoteParty = 'jbond';
    directiveScope.callRemoteParty();
    scope.$apply();
    expect(Cti.dialByUsername).toHaveBeenCalledWith('jbond');
  });

});
