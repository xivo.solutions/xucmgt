describe('ccm ccviewcontroller', function() {
  var $rootScope;
  var $scope;
  var ctrl;
  var preferences;
  var xucLink;

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(angular.mock.inject(function(_XucLink_, $controller, _$rootScope_, _Preferences_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    xucLink = _XucLink_;
    preferences = _Preferences_;

    ctrl = $controller('ccViewController', {
      '$scope': $scope,
      'XucLink': xucLink,
      'Preferences': preferences
    });
  }));

  it('can instanciate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });

  it('show alert if no right found for user', function() {
    $scope.userRightProfile = 'NoRightForUser';
    $scope.showAlert = true;

    expect($scope.displayAlert()).toBe(true);
    $scope.closeAlert();
    expect($scope.displayAlert()).toBe(false);
  });

});
