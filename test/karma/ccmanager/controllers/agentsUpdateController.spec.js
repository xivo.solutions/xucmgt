import _ from 'lodash';

describe('agents update controller', function() {
  var $scope;
  var $rootScope;
  var ctrl;
  var $uibModal;
  var xucQueue;
  var $q;


  beforeEach(angular.mock.module('ccManager'));

  beforeEach(angular.mock.inject(function($controller, _$rootScope_, _$uibModal_, _XucQueue_, _$q_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $uibModal = _$uibModal_;
    xucQueue = _XucQueue_;
    $q = _$q_;

    ctrl = $controller('agentsUpdateController', {
      '$scope' : $scope,
      '$uibModal' : $uibModal,
      'xucQueue' : xucQueue
    });
  }));

  it('should instanciate controller', function() {

    expect(ctrl).not.toBeUndefined();

  });

  it('should open modal on update agents', function(){

    spyOn($uibModal, 'open').and.returnValue({result: $q.resolve({})});

    $scope.updateAgents();

    expect($uibModal.open).toHaveBeenCalled();
  });

  it('should open modal on create base configuration', function(){
    var ag1 = new MockAgentBuilder(1, "James", "Bond").build();
    ag1.selected = true;
    var ag2 = new MockAgentBuilder(2, "Jason", "Bourne").build();
    ag2.selected = true;
    var ag3 = new MockAgentBuilder(3, "Jack", "Bauer").build();
    ag3.selected = false;
    $scope.displayedData = [ag1, ag2, ag3];


    spyOn($uibModal, 'open').and.callFake(function(params) {
      var ags = params.resolve.selectedAgents();
      expect(_.sortBy(ags, 'id')).toEqual([ag1, ag2]);
      expect(params.resolve.selectedQueues()).toEqual([]);
    });

    $scope.createBaseConfiguration();

    expect($uibModal.open).toHaveBeenCalled();
  });

  it('should inject queues from selected agents "create from active configuration"', function(){
    var q1 = new QueueBuilder("cars", "Cars").build();
    var q2 = new QueueBuilder("trucks", "Trucks").build();
    var q3 = new QueueBuilder("bikes", "Bikes").build();
    var queues = [q1, q2, q3];

    var ag1 = new MockAgentBuilder(1, "James", "Bond").build();
    ag1.queueMembers[q1.id] = 2;
    ag1.queueMembers[q2.id] = 5;
    ag1.selected = true;
    var ag2 = new MockAgentBuilder(2, "Jason", "Bourne").build();
    ag2.queueMembers[q1.id] = 1;
    ag2.queueMembers[q3.id] = 3;
    ag2.selected = true;
    var ag3 = new MockAgentBuilder(3, "Jack", "Bauer").build();
    ag3.queueMembers[q1.id] = 0;
    ag3.queueMembers[q3.id] = 0;
    ag3.selected = false;
    $scope.displayedData = [ag1, ag2, ag3];

    q1 = _.clone(q1);
    q2 = _.clone(q2);
    q3 = _.clone(q3);
    q1.penalty = 1;
    q2.penalty = 5;
    q3.penalty = 3;
    var expectedQueues = [q1, q2, q3];


    spyOn(xucQueue, 'getQueue').and.callFake(function (qid) {
      return _.find(queues, {id: qid});
    });

    spyOn($uibModal, 'open').and.callFake(function(params) {
      var qs = params.resolve.selectedQueues();
      expect(_.sortBy(qs, 'id')).toEqual(_.sortBy(expectedQueues, 'id'));
    });

    $scope.createBaseFromCurrent();

    expect($uibModal.open).toHaveBeenCalled();
  });

});
