describe('queue dyn view controllers', function() {
  var xucQueueTotal;
  var $scope;
  var ctrl;
  var $filter;
  var $q;
  var NgTableParams;
  var $rootScope;
  var viewColBuilder;
  var preferences;
  var thresholds;
  var remoteConfiguration;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('ccManager'));

  beforeEach(angular.mock.inject(function(_NgTableParams_, _$filter_, _XucQueueTotal_, $controller, _$rootScope_, _$q_, _ViewColBuilder_, _Preferences_, _Thresholds_, _remoteConfiguration_) {
    $scope = _$rootScope_.$new();
    $rootScope = _$rootScope_;
    xucQueueTotal = _XucQueueTotal_;
    $filter = _$filter_;
    NgTableParams = _NgTableParams_;
    viewColBuilder = _ViewColBuilder_;
    preferences = _Preferences_;
    thresholds = _Thresholds_;
    $q = _$q_;
    remoteConfiguration= _remoteConfiguration_;

    $scope.queues = [];

    var selectedColumns = [{id:'col1', show: false},{id:'col2', show: true}];
    spyOn(preferences, 'getViewColumns').and.returnValue(selectedColumns);
    spyOn(remoteConfiguration, 'getFromXuc').and.returnValue($q.resolve([15,30]));

    ctrl = $controller('ccQueueDynViewController', {
      '$scope' : $scope,
      'NgTableParams': NgTableParams,
      '$filter': $filter,
      'xucQueueTotal': xucQueueTotal,
      'viewColBuilder': viewColBuilder,
      'preferences' : preferences,
      'thresholds' : thresholds,
      'remoteConfiguration': remoteConfiguration
    });
  }));

  it('can instanciate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });

  it('should get queue columns to be displayed from preferences', function() {
    $rootScope.$digest();
    expect(preferences.getViewColumns).toHaveBeenCalled();

  });

  it('should reload cols on preferences saved', function(){

    $rootScope.$broadcast('preferences.queueViewColumns');
    $rootScope.$digest();
    expect(preferences.getViewColumns.calls.count()).toEqual(2);
  });


  it('should calculate display class for a counter', function() {

    spyOn(thresholds, 'thresholdClass').and.returnValue('middleClass');

    var cl = $scope.thresholdClass('WaitingCalls',1);

    expect(cl).toBe('middleClass');

  });

  it('should calculate nothing for a unkown counter', function() {
    $scope.thresholds = {};
    $scope.thresholds['WaitingCalls'] = {low:7, high:11};

    var cl = $scope.thresholdClass('TalkingAgents',1);

    expect(cl).toBe('');

  });

  it('should generate columns for custom stats thresholds', function() {
    $rootScope.$digest();

    let stats = {
      WaitingCalls: { value: 'stats.sum.WaitingCalls' },
      LongestWaitTime: { value: 'stats.max.LongestWaitTime | queueTime' },
      EWT: { value: 'stats.max.EWT | queueTime' },
      TotalNumberCallsEntered: { value: 'stats.sum.TotalNumberCallsEntered' },
      TotalNumberCallsAnswered: { value: 'stats.sum.TotalNumberCallsAnswered' },
      PercentageAnsweredBefore15: { value: 'stats.global.PercentageAnsweredBefore15 | number:1' },
      PercentageAnsweredBefore30: { value: 'stats.global.PercentageAnsweredBefore30 | number:1' },
      TotalNumberCallsAbandonned: { value: 'stats.sum.TotalNumberCallsAbandonned' },
      PercentageAbandonnedAfter15: { value: 'stats.global.PercentageAbandonnedAfter15 | number:1' },
      PercentageAbandonnedAfter30: { value: 'stats.global.PercentageAbandonnedAfter30 | number:1' },
      TotalNumberCallsClosed: { value: 'stats.sum.TotalNumberCallsClosed' },
      TotalNumberCallsTimeout: { value: 'stats.sum.TotalNumberCallsTimeout' },
      TotalNumberCallsNotAnswered: { value: 'stats.sum.TotalNumberCallsNotAnswered' }
    };
    expect($scope.totals).toEqual(stats);
  });


});
