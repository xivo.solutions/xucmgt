import _ from 'lodash';

describe('CCmanager Agent Select Controller', function() {
  var $rootScope;
  var $scope;
  var ctrl;

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(function() {
    var _selectedToBe = function() {
      return {
        compare: function(items, nbSelected) {
          var realSelected =  _.filter(items, function(o) { return o.selected; }).length;
          return {
            pass : realSelected === nbSelected,
            message: 'Number of items selected is ' + realSelected + ' but expected to be ' + nbSelected
          };
        }
      };
    };
    var _allSelected = function() {
      return {
        compare: function(items) {
          var realSelected =  _.filter(items, function(o) { return o.selected; }).length;
          return {
            pass : realSelected === items.length,
            message: 'Expected all items to be selected, but only ' + realSelected + ' selected'
          };
        }
      };
    };
    var _noneSelected = function() {
      return {
        compare: function(items) {
          var realSelected =  _.filter(items, function(o) { return o.selected; }).length;
          return {
            pass : realSelected === 0,
            message: 'Expected no items to be selected, but ' + realSelected + ' selected'
          };
        }
      };
    };
    jasmine.addMatchers({
      selectedToBe : _selectedToBe,
      allSelected: _allSelected,
      noneSelected : _noneSelected
    });
  });


  beforeEach(angular.mock.inject(function(_$rootScope_, $controller) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();

    ctrl = $controller('agentSelectController', {
      '$scope' : $scope,
      '$rootScope' : $rootScope
    });
  }));

  it('can instanciate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

  describe('Select class', function(){
    it('should return fa-check-square-o when all data are selected ', function(){
      $scope.displayedData = [{item:'one','selected':true}];

      expect($scope.agentSelectorClass()).toBe('fa-check-square-o');
    });

    it('should return fa-square-o when no data selected ', function(){
      $scope.displayedData = [{item:'one','selected':false},{item:'two'}];

      expect($scope.agentSelectorClass()).toBe('fa-square-o');
    });
    it('should return fa-minus-square-o when some data are selected ', function(){
      $scope.displayedData = [{item:'one','selected':true},{item:'two','selected':false},{item:'three'}];

      expect($scope.agentSelectorClass()).toBe('fa-minus-square-o');
    });
  });
  describe('Toggle select agent', function(){
    it('should select all agents if none are selected', function(){
      $scope.displayedData = [{agent:'one','selected':false},{agent:'two','selected':false},{agent:'three'}];

      $scope.toggleSelectAgents();

      expect($scope.displayedData).allSelected();
    });

    it('should deselect all agents if some are selected', function(){
      $scope.displayedData = [{agent:'one','selected':false},{agent:'two','selected':true},{agent:'three'}];

      $scope.toggleSelectAgents();

      expect($scope.displayedData).noneSelected();
    });

  });

  describe('No agent selected', function(){
    it('should be true when no agent are selected', function() {
      $scope.displayedData = [{agent:'one','selected':false},{agent:'two','selected':false},{agent:'three'}];

      expect($scope.noAgentSelected()).toBe(true);
    });
    it('should be false when some agent are selected', function() {
      $scope.displayedData = [{agent:'one','selected':false},{agent:'two','selected':true},{agent:'three'}];

      expect($scope.noAgentSelected()).toBe(false);
    });
  });

});
