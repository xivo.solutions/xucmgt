describe('agent dyn view controllers', function() {
  var preferences;
  var xucAgent;
  var $scope;
  var ctrl;
  var $filter;
  var NgTableParams;
  var $rootScope;
  var agentViewColBuilder;
  var xucAgentTotal;
  var ccmUtils;

  var updateFn = jasmine.createSpy('update');

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(angular.mock.inject(function(_NgTableParams_, _$filter_, _XucAgent_,_Preferences_, $controller, _$rootScope_, _AgentViewColBuilder_,_XucAgentTotal_, _CCMUtils_) {
    $scope = _$rootScope_.$new();
    $rootScope = _$rootScope_;
    xucAgent = _XucAgent_;
    preferences = _Preferences_;
    $filter = _$filter_;
    NgTableParams = _NgTableParams_;
    agentViewColBuilder = _AgentViewColBuilder_;
    xucAgentTotal = _XucAgentTotal_;
    ccmUtils = _CCMUtils_;

    var selectedColumns = [{id:'col1', show: false},{id:'col2', show: true}];
    spyOn(preferences, 'getViewColumns').and.returnValue(selectedColumns);
    spyOn(agentViewColBuilder, 'buildTable').and.callFake(function() {});
    spyOn(ccmUtils,'doDeffered').and.returnValue(updateFn);

    updateFn.calls.reset();

    ctrl = $controller('ccAgentDynViewController', {
      '$scope' : $scope,
      'NgTableParams': NgTableParams,
      '$filter': $filter,
      'XucAgent' : xucAgent,
      'Preferences' : preferences,
      'AgentViewColBuilder': agentViewColBuilder,
      'XucAgentTotal' : xucAgentTotal,

    });
  }));

  it('can instanciate controller', function() {
    expect(ctrl).not.toBeUndefined();
    expect(updateFn).toHaveBeenCalled();
  });

  it('should select agents from queue selected', function() {

    $rootScope.$broadcast('preferences.queueSelected');

    expect(updateFn).toHaveBeenCalledTimes(2);

  });

  it('on agent loaded should select agent from queues', function() {

    $rootScope.$broadcast('AgentsLoaded');

    expect(updateFn).toHaveBeenCalledTimes(2);

  });
  it('should reload cols on preferences saved', function(){

    $rootScope.$broadcast('preferences.agentViewColumns');

    expect(preferences.getViewColumns.calls.count()).toEqual(2);

  });

  it('should update totals on agent statistic updated', function(){

    $rootScope.$broadcast('AgentStatisticsUpdated');

    expect(updateFn).toHaveBeenCalledTimes(2);

  });

  it('should update view on agent config update', function(){
    $rootScope.$broadcast('AgentConfigUpdated');
    expect(updateFn).toHaveBeenCalledTimes(2);
  });

});
