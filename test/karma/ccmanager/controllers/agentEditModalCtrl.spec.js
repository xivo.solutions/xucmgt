import _ from 'lodash';

describe('agent edit modal controller', function() {
  var $scope;
  var $rootScope;
  var xucAgent;
  var xucQueue;
  var xucGroup;
  var xucMembership;
  var ctrl;
  var modalInstance;
  var NgTableParams;
  var $controller;
  var $q;


  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(angular.mock.inject(function(_XucAgent_, _XucQueue_, _XucGroup_, _$controller_, _$rootScope_, _$uibModal_, _NgTableParams_, _XucMembership_, _$q_, $httpBackend) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    xucQueue = _XucQueue_;
    xucAgent = _XucAgent_;
    xucGroup = _XucGroup_;
    xucMembership = _XucMembership_;
    spyOn(xucGroup,'reload');
    $controller = _$controller_;
    NgTableParams = _NgTableParams_;
    $q = _$q_;
    var loginPage = '<login-form on-login="onLogin()" host-and-port="hostAndPort" error-code="error" use-sso="useSso" title="Login"/>';

    $httpBackend.when('GET', '/ccmanager/login').respond(loginPage);

    modalInstance = _$uibModal_.open({
      templateUrl: 'assets/javascripts/ccmanager/controllers/agentEditContent.html'
    });


  }));

  function loadController(agent) {
    ctrl = $controller('agentEditModalCtrl', {
      '$scope' : $scope,
      '$uibModalInstance' : modalInstance,
      'agent' : agent,
      'NgTableParams' : NgTableParams,
      'xucQueue' : xucQueue,
      'xucAgent' : xucAgent,
      'xucGroup' : xucGroup
    });
  }

  it('should instanciate controller', function() {
    spyOn(xucMembership,'getUserDefaultMembership').and.callFake(function() {
      var p = $q.defer();
      p.resolve([]);
      return p.promise;
    });
    var agent = new MockAgentBuilder(10, "James", "Bond").build();
    loadController(agent);
    expect(ctrl).not.toBeUndefined();

    expect(xucGroup.reload).toHaveBeenCalled();
  });

  it('should load active queues', function() {
    var queues = [];
    queues[1] = {'id':1,'displayName':'First Queue', "number": 3001};
    queues[2] = {'id':2,'displayName':'Second Queue', "number": 3002};
    queues[5] = {'id':5,'displayName':'Third Queue', "number": 3005};

    spyOn(xucQueue,'getQueue').and.callFake(function(queueId){return queues[queueId];});

    spyOn(xucMembership,'getUserDefaultMembership').and.callFake(function() {
      var p = $q.defer();
      p.resolve([]);
      return p.promise;
    });
    var agent = new MockAgentBuilder(10, "James", "Bond").build();
    agent.queueMembers[1] = 7;
    agent.queueMembers[5] = 1;

    var expectedResult = [{
      "id": queues[1].id,
      "displayName": queues[1].displayName,
      "number": 3001,
      "penalty": 7,
      "default": null
    },{
      "id": queues[5].id,
      "displayName": queues[5].displayName,
      "number": 3005,
      "penalty": 1,
      "default": null
    }];

    loadController(agent);
    expect(ctrl).not.toBeUndefined();

    expect($scope.queues).toEqual(expectedResult);
  });

  it('should load active queues if config server is disabled', angular.mock.inject(function($httpBackend) {
    var queues = [];
    queues[1] = {'id':1,'displayName':'First Queue', "number": 3001};
    queues[2] = {'id':2,'displayName':'Second Queue', "number": 3002};
    queues[5] = {'id':5,'displayName':'Third Queue', "number": 3005};

    spyOn(xucQueue,'getQueue').and.callFake(function(queueId){return queues[queueId];});

    spyOn(xucMembership,'getUserDefaultMembership').and.callFake(function() {
      var p = $q.defer();
      p.reject("timeout");
      return p.promise;
    });
    var agent = new MockAgentBuilder(10, "James", "Bond").build();
    agent.queueMembers[1] = 7;
    agent.queueMembers[5] = 1;

    var expectedResult = [{
      "id": queues[1].id,
      "displayName": queues[1].displayName,
      "number": 3001,
      "penalty": 7,
      "default": null
    },{
      "id": queues[5].id,
      "displayName": queues[5].displayName,
      "number": 3005,
      "penalty": 1,
      "default": null
    }];

    $httpBackend.whenGET("agentEditContent.html").respond("");
    $httpBackend.expectGET("agentEditContent.html");

    loadController(agent);
    $rootScope.$apply();
    expect(ctrl).not.toBeUndefined();
    expect($scope.configManagerError).toEqual(true);
    expect($scope.queues).toEqual(expectedResult);
  }));

  it('should load default queues', angular.mock.inject(function($httpBackend) {
    var queues = [];
    queues[1] = {'id':1,'displayName':'First Queue', "number": 3001};
    queues[2] = {'id':2,'displayName':'Second Queue', "number": 3002};
    queues[5] = {'id':5,'displayName':'Third Queue', "number": 3005};

    var defaultQueues = [];
    defaultQueues.push({queueId:1, penalty:1});
    defaultQueues.push({queueId:2, penalty:8});

    spyOn(xucQueue,'getQueue').and.callFake(function(queueId){return queues[queueId];});

    spyOn(xucMembership,'getUserDefaultMembership').and.callFake(function() {
      var p = $q.defer();
      p.resolve(defaultQueues);
      return p.promise;
    });
    var agent = new MockAgentBuilder(10, "James", "Bond").build();
    agent.queueMembers[1] = 7;
    agent.queueMembers[5] = 1;

    var expectedResult = [{
      "id": queues[1].id,
      "displayName": queues[1].displayName,
      "number": 3001,
      "penalty": 7,
      "default": 1
    },{
      "id": queues[2].id,
      "displayName": queues[2].displayName,
      "number": 3002,
      "penalty": null,
      "default": 8
    },{
      "id": queues[5].id,
      "displayName": queues[5].displayName,
      "number": 3005,
      "penalty": 1,
      "default": null
    }];

    $httpBackend.whenGET("agentEditContent.html").respond("");
    $httpBackend.expectGET("agentEditContent.html");

    loadController(agent);
    $rootScope.$apply();
    //$httpBackend.flush();
    expect(ctrl).not.toBeUndefined();

    var result = _.sortBy($scope.queues, function(o) {return o.id;});
    expect(result).toEqual(expectedResult);
  }));

  it('should refresh group list on group loaded', function(){
    spyOn(xucMembership,'getUserDefaultMembership').and.callFake(function() {
      var p = $q.defer();
      p.resolve([]);
      return p.promise;
    });
    var agent = new MockAgentBuilder(10, "James", "Bond").build();
    loadController(agent);
    var g1 = new MockGroupBuilder(8,'group one').build();
    var g2 = new MockGroupBuilder(7,'group two').build();

    spyOn(xucGroup,'getGroups').and.returnValue([g1,g2]);

    $rootScope.$broadcast('GroupsLoaded');

    expect($scope.agentGroups).toEqual([g1, g2]);
  });

  it('should apply default config when asked', function(){
    var agent = new MockAgentBuilder(10, "James", "Bond").build();
    spyOn(xucMembership,'getUserDefaultMembership').and.callFake(function() {
      var p = $q.defer();
      p.resolve([]);
      return p.promise;
    });
    loadController(agent);
    spyOn($scope.agentQueues, 'reload');
    var initial = [{
      "id": 1,
      "displayName": "First",
      "number": 3001,
      "penalty": 7,
      "default": 1
    },{
      "id": 2,
      "displayName": "Second",
      "number": 3002,
      "penalty": null,
      "default": 8
    },{
      "id": 5,
      "displayName": "Fifth",
      "number": 3005,
      "penalty": 1,
      "default": null
    }];

    var expected = [{
      "id": 1,
      "displayName": "First",
      "number": 3001,
      "penalty": 1,
      "default": 1
    },{
      "id": 2,
      "displayName": "Second",
      "number": 3002,
      "penalty": 8,
      "default": 8
    },{
      "id": 5,
      "displayName": "Fifth",
      "number": 3005,
      "penalty": null,
      "default": null
    }];

    $scope.queues = initial;
    $scope.applyDefaultMembership();
    expect($scope.queues).toEqual(expected);
    expect($scope.agentQueues.reload).toHaveBeenCalled();
  });

  it('should apply current config as default when asked', function(){
    var agent = new MockAgentBuilder(10, "James", "Bond").build();
    spyOn(xucMembership,'getUserDefaultMembership').and.callFake(function() {
      var p = $q.defer();
      p.resolve([]);
      return p.promise;
    });
    loadController(agent);
    spyOn($scope.agentQueues, 'reload');
    var initial = [{
      "id": 1,
      "displayName": "First",
      "number": 3001,
      "penalty": 7,
      "default": 1
    },{
      "id": 2,
      "displayName": "Second",
      "number": 3002,
      "penalty": null,
      "default": 8
    },{
      "id": 5,
      "displayName": "Fifth",
      "number": 3005,
      "penalty": 1,
      "default": null
    }];

    var expected = [{
      "id": 1,
      "displayName": "First",
      "number": 3001,
      "penalty": 7,
      "default": 7
    },{
      "id": 2,
      "displayName": "Second",
      "number": 3002,
      "penalty": null,
      "default": null
    },{
      "id": 5,
      "displayName": "Fifth",
      "number": 3005,
      "penalty": 1,
      "default": 1
    }];

    $scope.queues = initial;
    $scope.applyCurrentMembershipAsDefault();
    expect($scope.queues).toEqual(expected);
    expect($scope.agentQueues.reload).toHaveBeenCalled();
  });

  it('should save the current queue membership and the default membership on Ok', function(){
    var agent = new MockAgentBuilder(10, "James", "Bond").build();
    spyOn(xucMembership,'getUserDefaultMembership').and.callFake(function() {
      var p = $q.defer();
      p.resolve([]);
      return p.promise;
    });
    loadController(agent);
    spyOn(xucAgent, 'updateQueues');
    spyOn(xucMembership, 'setUserDefaultMembership');
    spyOn(Cti, 'setAgentGroup');
    spyOn(modalInstance, 'close');

    var initial = [{
      "id": 1,
      "displayName": "First",
      "number": 3001,
      "penalty": 7,
      "default": 1
    },{
      "id": 2,
      "displayName": "Second",
      "number": 3002,
      "penalty": null,
      "default": 8
    },{
      "id": 5,
      "displayName": "Fifth",
      "number": 3005,
      "penalty": 1,
      "default": null
    }];

    var currentMembership = [
      {"id": 1, "penalty": 7},
      {"id": 5, "penalty": 1}
    ];

    var defaultMembership = [
      {"queueId": 1, "penalty": 1},
      {"queueId": 2, "penalty": 8}
    ];

    $scope.queues = initial;
    $scope.agent = {'id': 3, 'groupId': 7, 'userId': 1};
    $scope.ok();

    expect(xucAgent.updateQueues).toHaveBeenCalledWith(3, currentMembership);
    expect(xucMembership.setUserDefaultMembership).toHaveBeenCalledWith(1, defaultMembership);
    expect(Cti.setAgentGroup).toHaveBeenCalledWith(3, 7);
    expect(modalInstance.close).toHaveBeenCalled();
  });

  it('should list all deleted queues', function(){
    var agent = new MockAgentBuilder(10, "James", "Bond").build();
    spyOn(xucMembership,'getUserDefaultMembership').and.callFake(function() {
      var p = $q.defer();
      p.resolve([]);
      return p.promise;
    });
    loadController(agent);

    var queues = [{
      "id": 1,
      "displayName": "First",
      "number": 3001,
      "penalty": null,
      "default": 1
    },{
      "id": 2,
      "displayName": "Second",
      "number": 3002,
      "penalty": null,
      "default": null
    },{
      "id": 5,
      "displayName": "Fifth",
      "number": 3005,
      "penalty": 1,
      "default": null
    }];

    $scope.queues = queues;

    $scope.validatePenalty(queues[1]);
    expect($scope.removedQueues.length).toEqual(1);

    $scope.queues[0].default = null;

    $scope.validatePenalty(queues[0]);
    expect($scope.removedQueues.length).toEqual(2);

  });

});
