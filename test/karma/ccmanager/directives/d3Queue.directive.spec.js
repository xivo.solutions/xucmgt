'use strict';

describe('Directive: d3 queue', function () {
  var $compile, $rootScope, thresholds, $q;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  
  beforeEach(angular.mock.module('ccManager'));

  beforeEach(angular.mock.inject(function(_$compile_,_$rootScope_, Thresholds, $httpBackend, _$q_) {
    var loginPage = '<login-form on-login="onLogin()" host-and-port="hostAndPort" error-code="error" use-sso="useSso" title="Login"/>';

    $httpBackend.when('GET', '/ccmanager/login').respond(loginPage);
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    thresholds = Thresholds;
    $q = _$q_;
    spyOn(Thresholds, 'thresholdClass');
  }));

  it('should calculate class', function() {

    var metrics = $q.defer();
    metrics.resolve(["PercentageAnsweredBefore15","AvailableAgents","PercentageAbandonnedAfter15","WaitingCalls"]);

    $rootScope.queue = {name:"testQueue", WaitingCalls: 12};
    $rootScope.metrics = metrics.promise;

    var element = $compile('<d3-queue data="queue" get-metrics="metrics"></d3-queue>')($rootScope);
    expect(element).not.toBeUndefined();
    $rootScope.$digest();

    expect(thresholds.thresholdClass).toHaveBeenCalledWith('WaitingCalls',12);

  });

  it('Replaces the element with the appropriate content', function() {
    $rootScope.queue = {name:"testQueue"};
    var metrics = $q.defer();
    metrics.resolve(["PercentageAnsweredBefore15","AvailableAgents","PercentageAbandonnedAfter15","WaitingCalls"]);
    $rootScope.metrics = metrics.promise;

    var element = $compile('<d3-queue data="queue" get-metrics="metrics"></d3-queue>')($rootScope);

    expect(element).not.toBeUndefined();

    $rootScope.$digest();

    expect(element.find('svg').attr('height')).toBe('165');
    expect(element.find('svg').attr('width')).toBe('165');

  });

});
