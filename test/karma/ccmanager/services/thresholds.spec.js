'use strict';

describe('Service: ccmanager trhesholds', function () {
  var thresholds;
  var preferences;
  var $rootScope;

  beforeEach(angular.mock.module('ccManager'));

  beforeEach(angular.mock.inject(function(_Thresholds_, _Preferences_, _$rootScope_) {
    thresholds = _Thresholds_;
    preferences = _Preferences_;
    $rootScope = _$rootScope_;

  }));

  it('can instanciate thresholds service', function() {
    expect(thresholds).not.toBeUndefined();
  });

  it('can load threshold', function(){

    spyOn(preferences,'getThreshold').and.returnValue({low:2, high:5});

    thresholds.loadThreshold('LongestWaitTime');

    expect(preferences.getThreshold).toHaveBeenCalledWith('LongestWaitTime', thresholds.defaultThresholds['LongestWaitTime'].low, thresholds.defaultThresholds['LongestWaitTime'].high);

  });

  it('can save threshold', function(){

    spyOn(preferences,'saveThreshold');

    thresholds.saveThreshold('LongestWaitTime',50,120);

    expect(preferences.saveThreshold).toHaveBeenCalledWith('LongestWaitTime',50,120);

  });

  it('can load all thresholds', function(){
    var thrs = {};
    thrs['WaitingCalls'] = {low:7, high:11};
    thrs['LongestWaitTime'] = {low:30, high:60};


    spyOn(preferences,'getThresholds').and.returnValue(thrs);

    thresholds.loadThresholds();

    expect(preferences.getThresholds).toHaveBeenCalledWith(thresholds.defaultThresholds);
  });

  it('return all default counters', function() {
    expect(thresholds.getThresholdDefs()).toEqual(['WaitingCalls','LongestWaitTime']);
  });

  it('should load thresholds on init', function() {
    var thrs = {};
    thrs['WaitingCalls'] = {low:7, high:11};
    thrs['LongestWaitTime'] = {low:30, high:60};

    spyOn(preferences, 'getThresholds').and.returnValue(thrs);

    thresholds.init();

    expect(preferences.getThresholds).toHaveBeenCalledWith(thresholds.defaultThresholds);
  });

  it('should calculate display class for a counter', function() {
    var thr = {};
    thr['WaitingCalls'] = {low:7, high:11};

    spyOn(preferences, 'getThresholds').and.returnValue(thr);

    thresholds.init();

    var cl = thresholds.thresholdClass('WaitingCalls',0);

    expect(cl).toBe('lowThreshold');

    cl = thresholds.thresholdClass('WaitingCalls',1);

    expect(cl).toBe('lowThreshold');

    cl = thresholds.thresholdClass('WaitingCalls',7);

    expect(cl).toBe('middleThreshold');

    cl = thresholds.thresholdClass('WaitingCalls',11);

    expect(cl).toBe('highThreshold');

    cl = thresholds.thresholdClass('WaitingCalls',"-");

    expect(cl).toBe('');
  });

  it('should reload thresholds on preferences saved', function(){
    var thrs = {};
    thrs['WaitingCalls'] = {low:7, high:11};
    thrs['LongestWaitTime'] = {low:30, high:60};

    spyOn(preferences, 'getThresholds').and.returnValue(thrs);

    $rootScope.$broadcast(preferences.ThresholdsChanged);

    expect(preferences.getThresholds).toHaveBeenCalled();

    expect(thresholds.getCurrentThresholds()).toEqual(thrs);

  });

});
