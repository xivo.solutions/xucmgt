'use strict';

describe('Service: ccmanager metrics utils', function () {
  var MetricsUtils;
  
  beforeEach(angular.mock.module('ccManager'));
  
  beforeEach(angular.mock.inject(function(_MetricsUtils_) {
    MetricsUtils = _MetricsUtils_;
    
  }));
  
  it('can instanciate metrics utils service', function() {
    expect(MetricsUtils).not.toBeUndefined();
  });
  
  it('can get metrics text and format', function(){
    let infosRepTotal = MetricsUtils.getMetricInfos("TotalNumberCallsAnswered");
    expect(infosRepTotal).toEqual(["Rep T", jasmine.any(Function)]);
    
    let infosAvailAgents = MetricsUtils.getMetricInfos("AvailableAgents");
    expect(infosAvailAgents).toEqual(["Dispo", jasmine.any(Function)]);
    
    let infosPercentAbanTotal = MetricsUtils.getMetricInfos("PercentageAbandonnedTotal");
    expect(infosPercentAbanTotal).toEqual(["Aba T", jasmine.any(Function)]);
  });

  it('can get metrics with infos', function(){

    let simpleMetrics = [];
    let data = {};

    let metricsWithInfosEmpty = MetricsUtils.getMetricsWithInfos(simpleMetrics, data);

    expect(metricsWithInfosEmpty).toEqual({});

    simpleMetrics = ["PercentageAnsweredBefore15", "PercentageAnsweredTotal", "AvailableAgents", "WaitingCalls"];
    data = {
      PercentageAnsweredBefore15: 20,
      TotalNumberCallsAnswered: 96,
      TotalNumberCallsEntered: 100,
      AvailableAgents: 10000,
      WaitingCalls: 3,
      PercentageAnsweredTotal: 96
    };

    let metricsWithInfos = MetricsUtils.getMetricsWithInfos(simpleMetrics, data);
    expect(metricsWithInfos).toEqual({
      topLeft: {title: 'PercentageAnsweredBefore15', data: 20, text: 'Rep -15s', format: jasmine.any(Function)},
      topRight: {title: 'PercentageAnsweredTotal', data: 96, text: 'Rep T', format: jasmine.any(Function)},
      bottomLeft: {title: 'AvailableAgents', data: 10000, text: 'Dispo', format: jasmine.any(Function)},
      bottomRight: {title: 'WaitingCalls', data: 3, text: 'Att', format: jasmine.any(Function)}
    });

    simpleMetrics = ["PercentageAnsweredBefore15", "PercentageAnsweredTotal", "AvailableAgents", "WaitingCalls"];
    data = {
      PercentageAnsweredBefore15: 20,
      TotalNumberCallsAnswered: 96,
      AvailableAgents: 10000
    };

    let metricsWithMissingStat = MetricsUtils.getMetricsWithInfos(simpleMetrics, data);
    expect(metricsWithMissingStat).toEqual({
      topLeft: {title: 'PercentageAnsweredBefore15', data: 20, text: 'Rep -15s', format: jasmine.any(Function)},
      topRight: {title: 'PercentageAnsweredTotal', data: 0, text: 'Rep T', format: jasmine.any(Function)},
      bottomLeft: {title: 'AvailableAgents', data: 10000, text: 'Dispo', format: jasmine.any(Function)},
      bottomRight: {title: 'WaitingCalls', data: 0, text: 'Att', format: jasmine.any(Function)}
    });
  });
  
});
