import angular, { ILocationService } from "angular"
import { LoginHelper, AuthConfiguration } from "../../../app/assets/javascripts/xclogin/directives/loginForm.helper"
import { XucLink } from "../../../app/assets/javascripts/xccti/services/XucLink"
import { IRootScopeService, ILogService, ITimeoutService, IWindowService } from "angular"
import OIDCHelper from "../../../app/assets/javascripts/xccti/services/OIDCHelper.service"


describe('LoginFormHelper', () => {

  let loginHelper:        LoginHelper;
  let xucLink:            XucLink;
  let $rootScope:         IRootScopeService;
  let $timeout:           ITimeoutService;
  let $log:               ILogService;
  let $window:            IWindowService;
  let authConfig:         AuthConfiguration;
  let oidcHelper:         OIDCHelper;
  let $location:          ILocationService;

  let defaultHref = 'http://myxucmgt/'
  let locationValue = {
    origin: 'test',
    protocol: 'http:',
    href: defaultHref,
    replace: function(href: string) {
      this.href = href
    }
  }

  let windowProvider = {
    $get: function() {
      return {
        location: locationValue,
        navigator: {languages: 'en_EN'}}
    }
  }

  beforeEach(() => {
    angular.mock.module('html-templates')
    angular.mock.module('karma-backend')
    angular.mock.module('xcCti')
    angular.mock.module('xcHelper')
    angular.mock.module('xcLogin')
  
    angular.mock.module(($provide: any) => {
      $provide.provider('$window', windowProvider)
    })

    angular.mock.inject((_XucLink_: XucLink, _$rootScope_: IRootScopeService, _$timeout_: ITimeoutService,_$log_: ILogService, _$window_: IWindowService, _OIDCHelper_: OIDCHelper, _$location_: ILocationService) => {
      xucLink     = _XucLink_;
      $rootScope  = _$rootScope_;
      $timeout    = _$timeout_;
      $log        = _$log_;
      $window     = _$window_;
      oidcHelper  = _OIDCHelper_;
      $location   = _$location_;
      
      authConfig  = {
        useSso: false,
        casServerUrl: '',
        casLogoutEnable: false,
        openidServerUrl: '',
        openidClientId: '',
        requirePhoneNumber: false
      }
    })
  })

  it('should attempt autologin with token', async () => {
    spyOn(xucLink, 'getStoredCredentials').and.callFake(() => undefined)

    loginHelper = new LoginHelper(
      authConfig,
      xucLink,
      $timeout,
      $log,
      $window,
      oidcHelper,
      $location
    )

    let credentials = {
      username: undefined,
      phoneNumber: undefined,
      token: 'aaaa-bbbb-cccc-dddd'
    }

    let url = `http://myxucmgt/?token=${credentials.token}`
    locationValue.replace(url)
    $location.url(url)

    loginHelper.getXucCredentials()
      .then((res) => {
        expect(res).toEqual(credentials)
      })
      .catch((err) => {
        expect(err).toBeUndefined
      })
    
    locationValue.replace(defaultHref)
    $rootScope.$digest()
  })

  it('should recect an error if no credentials and no token is provided', () => {
    loginHelper = new LoginHelper(
      authConfig,
      xucLink,
      $timeout,
      $log,
      $window,
      oidcHelper,
      $location
    )

    spyOn(xucLink, 'getStoredCredentials').and.callFake(() => undefined);

    loginHelper.getXucCredentials()
      .then()
      .catch((err) => {
        expect(err).toEqual({ error: 'NoStoredCredentials', message: undefined })
      })

    $rootScope.$digest()
  })

  it('should get stored credentials', () => {
    loginHelper = new LoginHelper(
      authConfig,
      xucLink,
      $timeout,
      $log,
      $window,
      oidcHelper,
      $location
    )

    let storedCredentials = {
      username: 'jbond',
      phoneNumber: '1000',
      token: 'aaaa-bbbb-cccc-dddd',
      id_token: 'dddd-cccc-bbbb-aaaa'
    }

    jasmine.createSpy('getStoredCredentials')

    xucLink.getStoredCredentials = jasmine.createSpy().and.returnValue(storedCredentials)

    loginHelper.getXucCredentials()
      .then((res) => {
        expect(res).toEqual(storedCredentials)
      })
  })

  
})