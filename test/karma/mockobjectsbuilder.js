var MockAgentBuilder = function(id,firstName, lastName) {
  this.agent = {};
  this.agent.id = id;
  this.agent.userId = 1000 + id;
  this.agent.firstName = firstName;
  this.agent.lastName = lastName;
  this.agent.queueMembers = [];
  this.agent.defaultMembership = [];
  this.agent.state = '';
  this.state = {};
  this.state.agentId = id;
  this.agent.stats = {};

  this.inQueue = function(queueId, penalty) {
    this.agent.queueMembers[queueId] = penalty;
    return this;
  };

  this.inDefaultQueue = function(queueId, penalty) {
    this.agent.defaultMembership[queueId] = penalty;
    return this;
  };
  this.inGroup = function(groupId) {
    this.agent.groupId = groupId;
    return this;
  };
  this.build = function() {
    return this.agent;
  };
  this.load = function(xucAgent) {
    xucAgent.onAgentConfig(this.agent);
    if (typeof(this.state.name) != 'undefined')
      xucAgent.onAgentState(this.state);
    return xucAgent.getAgent(this.agent.id);
  };
  this.onState = function(agentState) {
    this.state.name = agentState;
    this.agent.state = agentState;
    return this;
  };
  this.onPhone = function(phoneNb) {
    this.state.phoneNb = phoneNb;
    return this;
  };
  this.withStat = function(statName, statValue) {
    this.agent.stats[statName] = statValue;
    return this;
  };
  return this;
};

var MockGroupBuilder = function(id, name) {
  this.group = {};
  this.group.id = id;
  this.group.name = name;
  this.group.displayName = name;

  this.build = function() {
    return this.group;
  };
  return this;
};

var queueId = 1;
var QueueBuilder = function(name, displayName) {
  this.queue = {};
  this.queue.name = name;
  this.queue.displayName = displayName;
  this.queue.id = queueId;
  queueId = queueId + 1;

  this.withDisplayName = function(displayName) {
    this.queue.displayName = displayName;
    return this;
  };
  this.build = function() {
    return this.queue;
  };
  return this;
};


var CallbackRequestBuilder = function(phoneNumber) {
  var period = {};
  period.periodStart = '00:00:00';
  period.periodEnd = '23:59:59';

  this.cb = {
    phoneNumber: phoneNumber,
    preferredPeriod: period,
    dueDate : '2015-06-08'
  };
  this.withAgentId = function(agentId) {
    this.cb.agentId=agentId;
    return this;
  };

  this.withQueue = function(queue) {
    this.cb.queue = queue
    return this;
  };

  this.withDueDate = function(dueDate) {
    this.cb.dueDate = dueDate
    return this;
  };

  this.build = function() {
    return this.cb;
  };

  return this;
};

var CallbackListBuilder = function(name, queueId, callbacks) {
  this.list = {
    name: name,
    queueId: queueId,
    callbacks: callbacks
  };

  this.withQueue = function(queue) {
    this.list.queue = queue;
    return this;
  };

  this.build = function() {
    return this.list;
  };

  return this;
};

var userId = 1;
var UserBuilder = function(name) {
  this.user = {};
  this.user.firstName = name;
  this.user.fullName = name;
  this.user.id = userId;
  userId = userId + 1;

  this.build = function() {
    return this.user;
  };
  return this;
};

var MockDataTransfer = function() {
  var data = {};
  return {
    getData: function(key) {
      return data[key];
    },
    setData: function(key, value) {
      data[key] = value;
    }
  };
};

var ContactBuilder = function(name, username, phoneNumber, isMeetingRoom = false, videoStatus = "Available", isPersonal = false) {

  var sampleContact = {
    "name": name,
    "subtitle1": "Sample 1",
    "subtitle2": "Sample 2",
    "picture": "",
    "isPersonal": isPersonal,
    "isFavorite": false,
    "isMeetingroom": isMeetingRoom,
    "canBeFavorite": true,
    "status": {
      "phone": 4,
      "video": videoStatus
    },
    "actions": {
      "Call": {
        "args": [
          phoneNumber
        ],
        "disable": false
      },
      "Video": {
        "args": [
          username
        ],
        "disable": false
      },
      "Chat": {
        "args": [
          username
        ],
        "disable": false
      },
      "Mail": {
        "args": ["noreply@avencall.com"],
        "disable": false
      },
      "ShareLink" : {
        "args": [
          "/invitation/video/eyJ0eXA"
        ]
      }
    },
    "sources": [
      {
        "name": "internal",
        "id": "1"
      }
    ],
    "details": [
      {
        "name": "Contacts",
        "fields": [
          {
            "name": "Téléphone interne",
            "data": phoneNumber,
            "dataType": "PhoneNumber"
          },
          {
            "name": "Téléphone pro",
            "data": "",
            "dataType": "PhoneNumber"
          },
          {
            "name": "Téléphone mobile",
            "data": "",
            "dataType": "PhoneNumber"
          },
          {
            "name": "Téléphone autre",
            "data": "2000",
            "dataType": "PhoneNumber"
          },
          {
            "name": "E-mail",
            "data": "noreply@avencall.com",
            "dataType": "Mail"
          },
          {
            "name": "Télécopie",
            "data": "3000",
            "dataType": "PhoneNumber"
          }
        ]
      },
      {
        "name": "Général",
        "fields": [
          {
            "name": "Fonction",
            "data": "",
            "dataType": "String"
          },
          {
            "name": "Service",
            "data": "",
            "dataType": "String"
          },
          {
            "name": "Manager",
            "data": "",
            "dataType": "String"
          },
          {
            "name": "Société",
            "data": "",
            "dataType": "String"
          },
          {
            "name": "Site web",
            "data": "",
            "dataType": "Url"
          }
        ]
      },
      {
        "name": "Lieu d'affectation",
        "fields": [
          {
            "name": "Bureau",
            "data": "",
            "dataType": "String"
          },
          {
            "name": "Adresse",
            "data": "",
            "dataType": "String"
          }
        ]
      }
    ]
  };
  return sampleContact;
};