import 'xccti/cti-webpack';
import 'xccti/services/XucDirectory.service';

describe('Content search tab controller', function() {
  var $rootScope;
  var $scope;
  var $translate;
  var $controller;
  var XucDirectory;
  var focus;
  var keyNavUtility;

  var searchContact = ContactBuilder("User 1", "user1", "1000");
  var searchBusyContact = ContactBuilder("User 1", "user1", "1000", false, "Busy");

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('Agent'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_$rootScope_, _$controller_, _$translate_, _XucDirectory_, _focus_, _keyNavUtility_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    XucDirectory = _XucDirectory_;
    $translate = _$translate_;
    $controller = _$controller_;
    focus = _focus_;
    keyNavUtility = _keyNavUtility_;
  }));

  function createController() {
    return $controller('ContentSearchController', {
      '$scope': $scope,
      '$translate': $translate,
      'XucDirectory': XucDirectory,
      'focus' : focus
    });
  }

  it('can instanciate controller', function() {
    var ctrl = createController();
    expect(ctrl).not.toBeUndefined();
  });

  it('should display results when notified', function() {
    spyOn(XucDirectory, 'getContactSheetResult').and.returnValue([searchContact]);
    var ctrl = createController();
    ctrl.onSearchResult();
    $scope.$digest();

    var expectedContact = ContactBuilder("User 1", "user1", "1000");
    expectedContact.hover = false;
    expectedContact.initials = "U 1";

    expect($scope.searchResult).toEqual([expectedContact]);
  });

  it('should get correct label, background color and text color for a user status', function() {
    var ctrl = createController();
    expect(ctrl.getPhoneStateLabel(searchContact.status)).toEqual('USER_STATUS_4');
    expect(ctrl.getPhoneStateBackColor(searchContact)).toEqual('user-status4');
  });

  it('should get correct label, background color and text color for a user in videoconference', function() {
    var ctrl = createController();
    expect(ctrl.getPhoneStateLabel(searchBusyContact.status)).toEqual('USER_VIDEOSTATUS_Busy');
    expect(ctrl.getPhoneStateBackColor(searchBusyContact)).toEqual('user-videostatus-Busy');
  });

  it('should test if a contact number is callable', function() {
    var ctrl = createController();
    expect(ctrl.isCallable(searchContact)).toBe(true);
  });
  
  it('should reset the index on new search', function () {

    spyOn(XucDirectory,'onSearchResult');
    spyOn(keyNavUtility, 'resetIndex');
    var ctrl = createController();
    ctrl.onSearchResult();
    $rootScope.$digest();
    
    expect(keyNavUtility.resetIndex).toHaveBeenCalled();
  });

});
