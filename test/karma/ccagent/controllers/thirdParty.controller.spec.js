describe('Third party integration controller', function() {
  var $rootScope;
  var $scope;
  var $controller;
  var $q;
  var configuration;
  var XucThirdParty;
  var externalEvent;
  var $window;
  var electronWrapper;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('Agent'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));

  beforeEach(angular.mock.inject(function(_$rootScope_, _$controller_, _$q_, _remoteConfiguration_, _XucThirdPartyService_, _ExternalEvent_, _$window_, _electronWrapper_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $controller = _$controller_;
    $q = _$q_;
    configuration = _remoteConfiguration_;
    XucThirdParty = _XucThirdPartyService_;
    externalEvent = _ExternalEvent_;
    $window = _$window_;
    electronWrapper = _electronWrapper_;
  }));

  function createController() {
    return $controller('ThirdPartyController', {
      '$scope': $scope,
      'configuration': configuration,
      'XucThirdParty': XucThirdParty,
      'externalEvent' : externalEvent,
      '$window': $window,
      'electronWrapper': electronWrapper
    });
  }

  it('can instanciate controller', function() {
    spyOn(XucThirdParty, 'setThirdPartyWs');

    var ctrl = createController();
    expect(ctrl).not.toBeUndefined();
    expect(XucThirdParty.setThirdPartyWs).not.toHaveBeenCalled();
  });

  it('subscribe to third party service if 3rd party url is defined', function() {
    spyOn(XucThirdParty, 'setThirdPartyWs');
    spyOn(XucThirdParty, 'addActionHandler');
    spyOn(XucThirdParty, 'addClearHandler');
    spyOn(externalEvent, 'registerThirdPartyCallback');

    var url = $q.defer();
    url.resolve('localhost');
    spyOn(configuration, 'get').and.returnValue(url.promise);

    createController();
    $scope.$apply();

    expect(XucThirdParty.setThirdPartyWs).toHaveBeenCalledWith('localhost');
    expect(externalEvent.registerThirdPartyCallback).toHaveBeenCalled();
    expect(XucThirdParty.addActionHandler).toHaveBeenCalled();
    expect(XucThirdParty.addClearHandler).toHaveBeenCalled();
  });

  it('set action if open action is received in event', function() {
    var ctrl = createController();
    const ev = {action: 'open'};

    ctrl.onThirdPartyAction({});
    expect($scope.action).toBeUndefined();
    ctrl.onThirdPartyAction(ev);
    expect($scope.action).toBe(ev);
  });

  it('open popup instead of embedding the page when configured to do so ', function() {
    spyOn($window, 'open');
    var ctrl = createController();
    const ev = {action: 'popup', url: 'http://localhost/23678'};

    ctrl.onThirdPartyAction(ev);
    expect($window.open).toHaveBeenCalledWith('http://localhost/23678', 'popup');
  });

  it('open multiple popup when configured to do so ', function() {
    let lastWindowName;
    spyOn($window, 'open').and.callFake((url, windowName) => {
      lastWindowName = windowName;
    });
    
    var ctrl = createController();
    const ev = {action: 'popup', url: 'http://localhost/23678', multitab: true};

    ctrl.onThirdPartyAction(ev);
    let firstWindowName = lastWindowName;
    expect($window.open).toHaveBeenCalled();

    ctrl.onThirdPartyAction(ev);
    let secondWindowName = lastWindowName;
    expect($window.open).toHaveBeenCalled();
    expect(firstWindowName).not.toEqual(secondWindowName);
  });

  it('run executable when asked to do so ', function() {
    
    spyOn(electronWrapper, 'runExecutable');
    
    var ctrl = createController();
    var path = 'executable';
    var args = ['tutu', 'titi'];
    const ev = {action: 'run', url: path, executableArgs: args};

    ctrl.onThirdPartyAction(ev);
    expect(electronWrapper.runExecutable).toHaveBeenCalledWith(path, args);

  });

  it('clear action if received in event(s)', function() {
    spyOn(XucThirdParty, 'clearAllActions');

    var ctrl = createController();
    const evMsg = {data: 'closeThirdParty'};

    ctrl.onMessageReceived(evMsg);
    expect(XucThirdParty.clearAllActions).toHaveBeenCalled();

    ctrl.onThirdPartyClearAction();
    expect($scope.action).toBe(null);
  });

  it('ask wrapper to go fullscreen on opening third party app', function() {
    spyOn(electronWrapper, 'setElectronFullscreen');
    var ctrl = createController();
    const ev = {action: 'open'};

    ctrl.onThirdPartyAction(ev);
    expect(electronWrapper.setElectronFullscreen).toHaveBeenCalled();
  });
});
