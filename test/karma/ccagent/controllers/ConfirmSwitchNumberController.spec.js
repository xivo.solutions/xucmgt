describe('Confirm agent switch number controller', function () {
  var $controller;
  var $window;
  var CtiProxy;
  var $state;
  var modalInstance;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));

  beforeEach(angular.mock.module('Agent'));

  beforeEach(angular.mock.inject(function (_$uibModal_, _$window_, _CtiProxy_, _$state_, _$controller_) {
    $window = _$window_;
    CtiProxy = _CtiProxy_;
    $state = _$state_;
    $controller = _$controller_;

    modalInstance = _$uibModal_.open({
      templateUrl: 'assets/javascripts/ccagent/controllers/confirmSwitchNumberModal.html'
    });
  }));

  function getController() {
    return $controller('ConfirmSwitchNumberController', {
      '$uibModalInstance': modalInstance,
      '$window': $window,
      'CtiProxy': CtiProxy,
      '$state': $state
    });
  }

  it('should instanciate controller', function () {
    const ctrl = getController();
    expect(ctrl).not.toBeUndefined();
  });

  it('should logout the agent, stop the webrtc and go to login page on modal confirmation', function () {
    spyOn($window.Cti, 'logoutAgent');
    spyOn(CtiProxy, 'stopUsingWebRtc');
    spyOn($state, 'go');
    const ctrl = getController();
    ctrl.validate();
    expect($window.Cti.logoutAgent).toHaveBeenCalled();
    expect(CtiProxy.stopUsingWebRtc).toHaveBeenCalled();
    expect($state.go).toHaveBeenCalled();
  });
});
