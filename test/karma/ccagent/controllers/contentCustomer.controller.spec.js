import 'xccti/cti-webpack';
import 'xccti/services/XucCallHistory.service';

describe('Content customer tab controller', function() {
  var $rootScope;
  var $scope;
  var $q;
  var $translate;
  var $controller;
  var XucCallHistory;
  var XucPhoneEventListener;
  var XucPhoneState;
  var XucSheet;
  var registeredInCallback = null;
  var registeredOffCallback = null;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('Agent'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_$rootScope_, _$controller_, _$translate_, _XucCallHistory_, _XucPhoneEventListener_, _$q_, _XucPhoneState_, _XucSheet_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    XucCallHistory = _XucCallHistory_;
    XucPhoneEventListener = _XucPhoneEventListener_;
    $translate = _$translate_;
    $controller = _$controller_;
    XucPhoneState = _XucPhoneState_;
    XucSheet = _XucSheet_;
    $q = _$q_;


    spyOn(XucPhoneEventListener, 'addRingingHandler').and.callFake(function(scope, callback) {
      registeredInCallback = callback;
    });

    spyOn(XucPhoneEventListener, 'addReleasedHandler').and.callFake(function(scope, callback) {
      registeredOffCallback = callback;
    });

  }));

  function createController() {
    return $controller('ContentCustomerController', {
      '$scope': $scope,
      '$translate': $translate,
      'XucCallHistory': XucCallHistory,
      'XucPhoneEventListener': XucPhoneEventListener,
      'XucSheet': XucSheet
    });
  }

  function triggerCall(call) {
    if(registeredInCallback != null) {
      registeredInCallback(call);
    }
    if(registeredOffCallback != null) {
      registeredOffCallback(call);
    }
  }

  it('can instanciate controller', function() {
    var ctrl = createController();
    expect(ctrl).not.toBeUndefined();
  });

  it('find customer call history of current call if call established', function() {
    spyOn(XucPhoneState, 'getCalls').and.returnValue([{state: 'Established', otherDN:"1000"}]);
    spyOn(XucCallHistory, 'findCustomerCallHistoryAsync').and.returnValue($q.resolve({}));
    var ctrl = createController();
    expect(ctrl).not.toBeUndefined();
    expect(XucCallHistory.findCustomerCallHistoryAsync).toHaveBeenCalled();
  });

  it('find customer call history of current call if call ringing', function() {
    spyOn(XucPhoneState, 'getCalls').and.returnValue([{state: 'Ringing', otherDN:"1000"}]);
    spyOn(XucCallHistory, 'findCustomerCallHistoryAsync').and.returnValue($q.resolve({}));
    var ctrl = createController();
    expect(ctrl).not.toBeUndefined();
    expect(XucCallHistory.findCustomerCallHistoryAsync).toHaveBeenCalled();
  });

  it('get css class of current call', function() {
    var ctrl = createController();
    expect(ctrl.getCallClass({status:'answered'})).toEqual('agent-answer');
    expect(ctrl.getCallClass({status:'ongoing'})).toEqual('waiting-agent-answer');
    expect(ctrl.getCallClass({status:'abandonned'})).toEqual('no-agent-answer');
  });

  it('returns true if call was answered', function() {
    var ctrl = createController();
    var call = {
      status: 'answered'
    };
    expect(ctrl.hasAnsweredCall(call)).toBeTruthy();    
  });

  it('get correct waitTime of call history', function() {
    var ctrl = createController();
    expect(ctrl.getCallWaitTime({status:'answered', waitTime: '00:00:01', duration: '00:00:11'})).toEqual('00:00:01');
    expect(ctrl.getCallWaitTime({status:'ongoing', waitTime: '00:00:01', duration: '00:00:11'})).toEqual('00:00:11');
    expect(ctrl.getCallWaitTime({status:'abandonned', waitTime: '00:00:01', duration: '00:00:11'})).toEqual('00:00:11');
  });

  it('triggers find customer call history when receive PHONEEVENT ringing', function() {
    createController();
    expect(XucPhoneEventListener.addRingingHandler).toHaveBeenCalled();
    spyOn(XucCallHistory, 'findCustomerCallHistoryAsync').and.returnValue($q.resolve({}));

    triggerCall({otherDN: "1000"});
    expect(XucCallHistory.findCustomerCallHistoryAsync).toHaveBeenCalled();
  });

  it('clean call history when receive PHONEEVENT released', function() {
    createController();
    expect(XucPhoneEventListener.addReleasedHandler).toHaveBeenCalled();
    $scope.history = {dummy : 'value'};
    $scope.attachedData = ['dummyvalue'];

    triggerCall({otherDN: "1000"});
    expect($scope.history).toEqual({});
    expect($scope.attachedData).toEqual([]);
  });

  it('display user data from call', function() {
    var ctrl = createController();
    $scope.attachedData = [];

    ctrl.onRinging({otherDN: "1000", userData:{USR_TITLE1:"1-Title",USR_DATA1:"1-hello",USR_TITLE2:"2-Title",USR_DATA2:"2-hello"}});

    expect($scope.attachedData).toEqual([['1-Title', '1-hello'], ['2-Title', '2-hello']]);
  });

  it('display user data in order from call', function() {
    var ctrl = createController();
    $scope.attachedData = [];

    ctrl.onRinging({otherDN: "1000", userData:{USR_TITLE2:"2-Title",USR_DATA2:"2-hello",USR_TITLE1:"1-Title",USR_DATA1:"1-hello"}});

    expect($scope.attachedData).toEqual([['1-Title', '1-hello'], ['2-Title', '2-hello']]);
  });

  it('display user data from call and sheet data concatenated', function() {
    var ctrl = createController();
    $scope.attachedData = [];
    spyOn(XucSheet, 'getAttachedData').and.returnValue({mySheetTitle: 'mySheetValue'});

    ctrl.onRinging({otherDN: "1000", userData:{USR_TITLE2:"2-Title",USR_DATA2:"2-hello",USR_TITLE1:"1-Title",USR_DATA1:"1-hello"}});

    expect($scope.attachedData).toEqual([['1-Title', '1-hello'], ['2-Title', '2-hello'], ['mySheetTitle', 'mySheetValue']]);
  });
});
