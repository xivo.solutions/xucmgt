import 'xccti/cti-webpack';

describe('Content agent list tab controller', function() {
  var $rootScope;
  var $scope;
  var $translate;
  var $controller;
  var XucUser;
  var XucAgent;
  var callContext;
  var $q;

  var agentData = {"agentId": 887, "groupId": 1, "phoneNb": 1000, "state": "AgentReady", "timeInState": 60};

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('Agent'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_$rootScope_, _$controller_, _$translate_, _XucUser_, _XucAgent_, _callContext_, _$q_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $translate = _$translate_;
    $controller = _$controller_;
    XucUser = _XucUser_;
    XucAgent = _XucAgent_;
    callContext = _callContext_;
    $q = _$q_;

    var agent = $q.defer();
    agent.resolve(agentData);

    spyOn(XucUser, "getUserAsync").and.callFake(() => {
      return agent.promise;
    });

    spyOn(XucAgent, "getAgentAsync").and.callFake(() => {
      return agent.promise;
    });
  }));

  function createController() {
    return $controller('ContentAgentsController', {
      '$scope': $scope,
      '$translate': $translate,
      'XucUser': XucUser,
      'XucAgent': XucAgent,
      'callContext': callContext
    });
  }

  it('can instanciate controller', function() {
    var ctrl = createController();
    expect(ctrl).not.toBeUndefined();
  });

  it('should load agent list without itself', function() {
    var agentList = [{'id':887, 'firstName' : 'James'},{'id':888, 'firstName' : 'Jack'}];
    spyOn(XucAgent, "getAgentsInGroup").and.returnValues(agentList);

    var ctrl = createController();
    spyOn(ctrl, "buildList").and.callThrough();

    $scope.$digest();
    expect(ctrl.buildList).toHaveBeenCalledWith(agentList, 887);
    expect($scope.agents).toEqual([{'id':888, 'firstName' : 'Jack'}]);
  });

  it('should filter agent list with/without logged out agents', function() {
    var agentList = [{'id':888, 'firstName' : 'Jack', state:'AgentLoggedIn'}, {'id':889, 'firstName' : 'Mike', state:'AgentLoggedOut'}];

    var ctrl = createController();
    expect(ctrl.buildList(agentList, 887)).toEqual([{'id':888, 'firstName' : 'Jack', state:'AgentLoggedIn'}]);

    $scope.displayLoggedAgents = false;
    expect(ctrl.buildList(agentList, 887)).toEqual([{'id':888, 'firstName' : 'Jack', state:'AgentLoggedIn'},{'id':889, 'firstName' : 'Mike', state:'AgentLoggedOut'}]);
  });

  it('should reload agent list on AgentStateUpdated', function() {
    spyOn(XucAgent, "getAgentsInGroup").and.callThrough();
    createController();
    $scope.$broadcast('AgentStateUpdated');
    $scope.$digest();
    expect(XucAgent.getAgentsInGroup.calls.count()).toEqual(2);
  });

  it('should reload agent list on AgentConfigUpdated', function() {
    spyOn(XucAgent, "getAgentsInGroup").and.callThrough();
    createController();
    $scope.$broadcast('AgentConfigUpdated');
    $scope.$digest();
    expect(XucAgent.getAgentsInGroup.calls.count()).toEqual(2);
  });

  it('should dial when clicking on agent', function() {
    spyOn(callContext, "normalizeDialOrAttTrans");
    var ctrl = createController();
    ctrl.dial(agentData);
    expect(callContext.normalizeDialOrAttTrans).toHaveBeenCalledWith(agentData.phoneNb);
  });

  it('should set agent info when hovering agent', function() {
    var ctrl = createController();
    ctrl.setHoverInfo(agentData);
    expect(agentData.hover).toEqual(true);
    expect($scope.agentInfo).toEqual({
      "state": agentData.state,
      "cause": agentData.status,
      "number": agentData.phoneNb,
      "since": agentData.timeInState,
    });
  });

  it('should set proper agent state colors when hovering', function() {
    var ctrl = createController();

    expect(ctrl.getAgentStateFillColor(agentData)).toEqual('state-agent-ready fill');
    expect(ctrl.getAgentStateBackColor(agentData)).toEqual('state-agent-ready');
  });

});
