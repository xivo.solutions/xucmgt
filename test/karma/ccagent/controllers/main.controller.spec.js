import 'ccagent/app';

describe("main controller", function() {

  let $controller;
  let $rootScope;
  let $scope;
  let $q;
  let CtiProxy;
  let XucDirectory;
  let XucQueueTotal;
  let externalView;
  let XucPhoneEventListener;
  let applicationConfiguration;
  let $state;
  let XucPhoneState;
  let XucLink;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('Agent'));

  beforeEach(angular.mock.inject(function (_$controller_, _$rootScope_, _$q_, _CtiProxy_, _XucDirectory_, _XucQueueTotal_, _remoteConfiguration_, _externalView_, _XucPhoneEventListener_, _applicationConfiguration_, _$state_, _XucPhoneState_, _XucLink_){
    $controller = _$controller_;
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $q = _$q_;
    CtiProxy = _CtiProxy_;
    XucDirectory = _XucDirectory_;
    XucQueueTotal = _XucQueueTotal_;
    externalView = _externalView_;
    XucPhoneEventListener = _XucPhoneEventListener_;
    applicationConfiguration = _applicationConfiguration_;
    $state = _$state_;
    XucPhoneState = _XucPhoneState_;
    XucLink = _XucLink_;
  }));

  it("can be instantiated", function() {
    let ctrl = $controller('MainController', {$scope: $scope});
    expect(ctrl).toBeDefined();
  });

  it("should toggle window on click", angular.mock.inject(function($state) {
    let _focus = jasmine.createSpy('focus');
    let $scope = $rootScope.$new();
    let ctrl = $controller('MainController', {$scope: $scope, $state: $state, focus: _focus});

    let tResult = $q.defer();
    tResult.resolve({name: 'content.test'});
    spyOn($state,'go').and.callFake(() => {
      return tResult.promise;
    });
    ctrl.show('test');
    $rootScope.$digest();
    expect($state.go).toHaveBeenCalledWith('content.test');
    expect(_focus).not.toHaveBeenCalled();
  }));

  it("should call focus on search element when going to search state", angular.mock.inject(function($state) {
    let _focus = jasmine.createSpy('focus');
    let $scope = $rootScope.$new();
    let ctrl = $controller('MainController', {$scope: $scope, $state: $state, focus: _focus});

    let tResult = $q.defer();
    tResult.resolve({name: 'content.search'});
    spyOn($state,'go').and.callFake(() => {
      return tResult.promise;
    });
    ctrl.show('search');
    $rootScope.$digest();
    expect($state.go).toHaveBeenCalledWith('content.search');
    expect(_focus).toHaveBeenCalledWith('search');
  }));

  it("should redirect to login page when link to xuc is closed", angular.mock.inject(function($state) {
    let $scope = $rootScope.$new();
    let ctrl = $controller('MainController', {$scope: $scope, $state: $state});
    spyOn(CtiProxy,'isUsingWebRtc').and.callFake(() => {return false;});
    spyOn(XucPhoneState,'getCalls').and.returnValue(() => {return 'returnValue';});
    expect(ctrl).toBeDefined();
    spyOn($state,'go');
    spyOn(XucLink,'logout');
    $rootScope.$broadcast('linkDisConnected');
    $rootScope.$digest();
    expect($state.go).toHaveBeenCalledWith('login', {'error': 'LinkClosed'});
  }));

  it('should trigger search if search icon is clicked', function(){
    let ctrl = $controller('MainController', {$scope: $scope});
    spyOn(CtiProxy,'dial');
    $scope.searchValue = "1000";

    ctrl.searchOrDial();
    expect(CtiProxy.dial).toHaveBeenCalledWith("1000", {});
  });

  it('should trigger search or call if enter is pressed', function(){
    let ctrl = $controller('MainController', {$scope: $scope});
    $scope.searchValue = "1000";
    spyOn(CtiProxy,'dial');

    ctrl.onKeyPress("13");
    expect(CtiProxy.dial).toHaveBeenCalledWith("1000", {});
  });

  it('should do nothing if another key is pressed', function(){
    let ctrl = $controller('MainController', {$scope: $scope});
    $scope.searchValue = "1000";
    spyOn(CtiProxy,'dial');

    ctrl.onKeyPress("10");
    expect(CtiProxy.dial.calls.any()).toEqual(false);
  });

  it('should search if not a phone number and if the input has more than two characters', function(){
    let ctrl = $controller('MainController', {$scope: $scope});
    $scope.searchValue = "user";
    spyOn(XucDirectory,'contactLookup');

    ctrl.searchOrDial();
    expect(XucDirectory.contactLookup).toHaveBeenCalledWith("user");
  });

  it('should not trigger the search with letters if the input has less than two characters', function(){
    let ctrl = $controller('MainController', {$scope: $scope});
    $scope.searchValue = "u";
    spyOn(XucDirectory,'contactLookup');

    ctrl.searchOrDial();
    expect(XucDirectory.contactLookup).not.toHaveBeenCalled();
  });

  it('enter key should not trigger the search if the input is empty', function(){
    let ctrl = $controller('MainController', {$scope: $scope});
    $scope.searchValue = "";
    spyOn(XucDirectory,'contactLookup');
    ctrl.onKeyPress("13");
    ctrl.searchOrDial();
    expect(XucDirectory.contactLookup).not.toHaveBeenCalled();
  });


  it('should ask for waitingCalls to be displayed as a badge in activity icon', function(){
    let ctrl = $controller('MainController', {$scope: $scope});
    spyOn(XucQueueTotal,'getCalcStats').and.returnValue({sum:{WaitingCalls:5}});

    expect(ctrl.getWaitingCalls()).toBe(5);
    expect($scope.waitingCalls).toBe(5);
  });

  it('clear input when user click on x', () => {
    let ctrl = $controller('MainController', {$scope: $scope});
    ctrl.emptyInput();
    expect($scope.searchValue).toBe("");
  });

  it("displays the external view button if set in server config", angular.mock.inject(() => {
    spyOn(externalView, 'isURLSet').and.returnValue(true);
    let ctrl = $controller("MainController", { $scope: $scope });
    $rootScope.$digest();
    expect(ctrl.showExternalViewButton).toBe(true);
  }));

  it("does not displays the external view button if set in server config", angular.mock.inject(() => {
    spyOn(externalView, 'isURLSet').and.returnValue(false);
    let ctrl = $controller("MainController", { $scope: $scope });
    $rootScope.$digest();
    expect(ctrl.showExternalViewButton).toBe(false);
  }));

  it('should, on switchboard, put the focus in the search bar and display the customer history tab when there is an incoming call', function(){
    spyOn(XucPhoneEventListener, 'addRingingHandler');
    spyOn(XucPhoneEventListener, 'addReleasedHandler');
    spyOn(applicationConfiguration, 'getCurrentAppConfig').and.returnValue({switchboard : true});

    $controller("MainController", { $scope: $scope, applicationConfiguration : applicationConfiguration });

    expect(XucPhoneEventListener.addRingingHandler).toHaveBeenCalled();
    expect(XucPhoneEventListener.addReleasedHandler).toHaveBeenCalled();
  });

  it('should focus when ringing', function(){
    let focus = jasmine.createSpy('focus');
    let tResult = $q.defer();

    tResult.resolve({name: 'content.customer.path'});
    spyOn($state,'go').and.callFake(() => {
      return tResult.promise;
    });
    let ctrl = $controller("MainController", {$scope : $scope, focus : focus, $state : $state});

    ctrl.onSwitchboardRinging();

    expect(focus).toHaveBeenCalled();
    expect($state.go).toHaveBeenCalledWith('content.customer.path');
  });

  it('should focus when the call is established', function(){
    let focus = jasmine.createSpy('focus');

    let ctrl = $controller("MainController", {$scope : $scope, focus : focus});

    ctrl.onSwitchboardEstablished();

    expect(focus).toHaveBeenCalled();
  });

  it('should focus when released', function(){
    let focus = jasmine.createSpy('focus');

    let ctrl = $controller("MainController", {$scope : $scope, focus : focus});

    ctrl.onSwitchboardRelease();

    expect(focus).toHaveBeenCalled();
  });

  it('should show the phone bar when ending a video call', function() {
    $controller('MainController', {$scope: $scope});
    $rootScope.$broadcast('EventCloseVideo');

    expect($scope.phoneBarIsVisible()).toEqual(true);
  });
  
});
