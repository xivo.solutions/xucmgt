import 'xccti/cti-webpack';

describe('Content activities tab controller', function() {
  var $rootScope;
  var $scope;
  var ctrl;
  var $q;
  var $httpBackend;
  var allQueues;
  var agentActivitiesHelper;
  var callContext;
  var applicationConfiguration;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('Agent'));

  beforeEach(angular.mock.inject(function(_$rootScope_, $controller, _XucQueue_, _$httpBackend_, _$q_, _agentActivitiesHelper_, 
    _callContext_, _applicationConfiguration_,  _userRights_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    callContext = _callContext_;
    agentActivitiesHelper = _agentActivitiesHelper_;
    applicationConfiguration = _applicationConfiguration_;


    // Create two queues with some fake information
    allQueues = [{
      'id': 1,
      'number': '3000',
      'displayName': 'My queue',
      'WaitingCalls': 1,
      'LongestWaitTime': 25,
      'LoggedAgents': 2,
      'AvailableAgents': 2,
      'EWT': 10
    }, {
      'id': 2,
      'number': '3001',
      'displayName': 'Another queue',
      'WaitingCalls': 1,
      'LongestWaitTime': 50,
      'LoggedAgents': 0,
      'AvailableAgents': 0
    },{
      'id': 3,
      'number': '3002',
      'displayName': 'More queue',
      'WaitingCalls': 1,
      'LongestWaitTime': 50,
      'LoggedAgents': 1,
      'AvailableAgents': 0
    },{
      'id': 4,
      'number': '3003',
      'displayName': 'Oh, still more queue',
      'WaitingCalls': 0,
      'LongestWaitTime': 0,
      'LoggedAgents': 0,
      'AvailableAgents': 0
    }];

    $httpBackend.whenGET("/ccagent/config/showQueueControls").respond({
      value: true
    });
    $httpBackend.whenGET("/ccagent/login").respond("");

    spyOn(applicationConfiguration, 'getCurrentAppConfig').and.returnValue({queueControl: true});

    spyOn(_userRights_, 'getDissuasionRight').and.callFake(() => {
      let result = $q.defer();
      result.resolve({
        value: true,
      });
      return result.promise;
    });

    ctrl = $controller('ContentActivitiesController', {
      '$scope': $scope,
      '$q': $q,
      'agentActivitiesHelper': agentActivitiesHelper
    });

  }));

  it('can instanciate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });

  it('loads queues', function() {
    $scope.displayMyQueues = false;
    var queues = $q.defer();
    queues.resolve(allQueues);

    spyOn(agentActivitiesHelper, "loadQueues").and.callFake(() => {
      return queues.promise;
    });
    spyOn(agentActivitiesHelper, "setQueueFilter");

    ctrl.loadQueues();
    $rootScope.$digest();
    expect(agentActivitiesHelper.setQueueFilter).toHaveBeenCalledWith(false);

    expect($scope.activities[0].id).toEqual(1);
    expect($scope.activities[1].id).toEqual(2);
  });


  it('subscribe to all queues when clicking on all checkbox', function() {
    $scope.displayMyQueues = true;

    var userQueues = $q.defer();
    userQueues.resolve([{
      'id' : 1,
      'number': '3000',
      'displayName': 'My queue',
      'associated': false,
      'WaitingCalls': 1,
      'LongestWaitTime':25,
      'AvailableAgents':1
    },
    {
      'id': 2,
      'number': '3001',
      'displayName': 'My other queue',
      'associated': false,
      'WaitingCalls': 1,
      'LongestWaitTime': 25,
      'AvailableAgents': 1
    }]);

    var queue = $q.defer();
    queue.resolve([]);

    spyOn(agentActivitiesHelper, "loadQueues").and.callFake(() => {
      return userQueues.promise;
    });

    ctrl.loadQueues();
    $rootScope.$digest();

    spyOn(agentActivitiesHelper, "enterOrLeaveQueue").and.returnValue($q.resolve());

    ctrl.enterAllQueues();

    expect(agentActivitiesHelper.enterOrLeaveQueue.calls.count()).toEqual(2);
  });

  it('removes queue from favorites', function() {
    spyOn(agentActivitiesHelper, "removeFavoriteQueue");
    spyOn(ctrl, "loadQueues");
    ctrl.removeFromFavorites(allQueues[0]);
    expect(agentActivitiesHelper.removeFavoriteQueue).toHaveBeenCalledWith(allQueues[0]);
    expect(ctrl.loadQueues).toHaveBeenCalled();
  });

  it('sorts activities by status', function() {
    expect(ctrl.sort.status('grey')).toEqual(0);
    expect(ctrl.sort.status('green')).toEqual(1);
    expect(ctrl.sort.status('orange')).toEqual(2);
    expect(ctrl.sort.status('red')).toEqual(3);
    expect(ctrl.sort.status('wrong')).toEqual(0);
  });

  it('gives current activity status', function() {
    expect(ctrl.getState(allQueues[0])).toEqual('green');
    expect(ctrl.getState(allQueues[1])).toEqual('red');
    expect(ctrl.getState(allQueues[2])).toEqual('orange');
    expect(ctrl.getState(allQueues[3])).toEqual('grey');
  });


  it('compute stats for the given activity selected (with EWT value 10)', function() {
    var stats = {
      number: '3000',
      calls: 1,
      eta: new Date(0, 0, 0, 0, 0, 10).getTime(),
      agents: 2
    };

    ctrl.setCounters(allQueues[0]);
    expect($scope.activityCounters).toEqual(stats);
  });

  it('compute stats for the given activity selected (with undefined EWT)', function() {
    var stats = {
      number: '3001',
      calls: 1,
      eta: new Date(0, 0, 0, 0, 0, 0).getTime(),
      agents: 0
    };

    ctrl.setCounters(allQueues[1]);
    expect($scope.activityCounters).toEqual(stats);
  });


  it('dial when clicking on queue phone number', function(){
    var activity = {number:'5646546'};
    spyOn(callContext,'normalizeDialOrAttTrans');

    ctrl.dial(activity);
    expect(callContext.normalizeDialOrAttTrans).toHaveBeenCalledWith('5646546');
  });

  it('set activity as hovered when selected with mouse pointer', function() {
    ctrl.setCounters(allQueues[0]);
    expect($scope.activityHovered).toBe(1);
    expect(ctrl.isHovered(allQueues[0])).toBe(true);
    expect(ctrl.isHovered(allQueues[1])).toBe(false);
  });

  it('reset activity hovered', function() {
    ctrl.setCounters(allQueues[1]);
    expect($scope.activityHovered).toBe(2);

    ctrl.clearHover();
    expect($scope.activityHovered).toBe(null);
    expect(ctrl.isHovered(allQueues[1])).toBe(false);
  });

  it('inits showDissuasion from the rights service', function() {
    $rootScope.$apply();
    expect($scope.showDissuasion).toBeTruthy();
  });

  it ('verifies that the stats popover opens when lauching the app', function() {

    let activity = [{name: 'queue1'}];
    $scope.activities = [activity];
    $scope.popoverStatsEnable['queue1'] = true;
    !$scope.dissuasionListVisible;
    ctrl.showQueues(activity);

    expect($scope.popoverStatsEnable['queue1']).toBeTruthy();
  });
  
  it ('verifies that the stats popover does not open when the dissuasion list is open', function() {

    let activity1 = {name: 'queue1'};
    let activity2 = {name: 'queue2'};

    $scope.activities = [activity1, activity2];
    $scope.popoverStatsEnable['queue1'] = true;
    ctrl.opendissuasiondropdown(activity1);

    expect($scope.popoverStatsEnable['queue1']).toBeFalsy();
    expect($scope.popoverStatsEnable['queue2']).toBeFalsy();
  });

  it ('verifies that the stats popover opens when the dissuasion list is closed', function() {

    let activity1 = {name: 'queue1'};
    let activity2 = {name: 'queue2'};

    $scope.activities = [activity1, activity2];
    $scope.popoverStatsEnable['queue1'] = true;
    ctrl.closedissuasiondropdown(activity1);

    expect($scope.popoverStatsEnable['queue1']).toBeTruthy();
    expect($scope.popoverStatsEnable['queue2']).toBeTruthy();
  });
});
