import angular from 'angular';
import _ from 'lodash';

describe('callLine directive', () => {
  var $compile;
  var $rootScope;
  var XucQueue;
  var XucPhoneState;
  var $q;
  var CtiProxy;
  var processVolume;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('Agent'));
  beforeEach(angular.mock.module('xcHelper'));

  beforeEach(angular.mock.inject((_$compile_, _$rootScope_, _XucQueue_, _$q_, _XucPhoneState_, _CtiProxy_, _processVolume_) =>{
    $compile = _$compile_;
    $rootScope = _$rootScope_;
    XucQueue = _XucQueue_;
    XucPhoneState = _XucPhoneState_;
    $q = _$q_;
    CtiProxy = _CtiProxy_;
    processVolume = _processVolume_;
  }));

  function createScopeAndVars (callState = 'Established') {
    var scope = $rootScope.$new();
    spyOn(XucQueue, 'getQueueByNameAsync').and.returnValue($q.resolve({name: 'cars', displayName: 'My Cars' }));


    scope.canRecord = false;

    scope.myCall = {
      otherDN: '1001',
      otherDName: 'James Bond',
      queueName: 'cars',
      startTime: Date.now(),
      state: callState,
      direction: 'incoming',
      acd: true
    };

    var element = $compile('<call-line call="myCall"></call-line>')(scope);
    $rootScope.$digest();
    return element.isolateScope();
  }

  it('should display queue real name', () => {
    var scope = $rootScope.$new();
    spyOn(XucQueue, 'getQueueByNameAsync').and.returnValue($q.resolve({name: 'cars', displayName: 'My Cars' }));
    

    scope.canRecord = false;
    
    scope.myCall = {
      otherDN: '1001',
      otherDName: 'James Bond',
      queueName: 'cars',
      startTime: Date.now(),
      state: 'Established',
      direction: 'incoming',
      acd: true
    };
    
    var element = $compile('<call-line call="myCall"></call-line>')(scope);
    
    $rootScope.$digest();
    var isolatedScope = element.isolateScope();   

    expect(isolatedScope.queueDisplayName).toBe('My Cars');
  });

  it('user can stay muted as long as the call is ongoing', function() {
    let isolatedScope = createScopeAndVars();

    $rootScope.$digest();
    isolatedScope.calls = [{call: "call 1"}];
    isolatedScope.isMuted = true;
    $rootScope.$digest();

    expect(isolatedScope.isMuted).toBeTruthy();
  });

  it('forwards sipCallId when apply action to ctiProxy when mute', function() {
    let isolatedScope = createScopeAndVars();
    spyOn(CtiProxy, 'toggleMicrophone');

    var call = {
      uniqueId: '1625046515.153',
      userData: {
        SIPCALLID: '123',
      }
    };

    expect(isolatedScope.isMuted).toBeFalsy();
    isolatedScope.toggleMicrophone(call);
    expect(CtiProxy.toggleMicrophone).toHaveBeenCalledWith(call.uniqueId);
    expect(isolatedScope.isMuted).toBeTruthy();
  });

  it('forwards sipCallId when apply action to ctiProxy when hold', function() {
    let isolatedScope = createScopeAndVars();
    spyOn(CtiProxy, 'hold');

    var calls = [
      {
        state: 'Established',
        uniqueId: '1625046515.153',
        userData: {
          SIPCALLID: '123',
        }
      }];
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
    spyOn(processVolume, 'monitorCall').and.callFake(()=>{});

    let action = _.find(isolatedScope.getCallActions(), (a) => a.label == 'hold');
    action.trigger(calls[0]);
    expect(CtiProxy.hold).toHaveBeenCalledWith(calls[0].uniqueId);
  });

  it('forwards uniqueId when apply action to ctiProxy when answer', function() {
    let isolatedScope = createScopeAndVars('Ringing');
    spyOn(CtiProxy, 'answer');

    var calls = [
      {
        state: 'Ringing',
        uniqueId: '1625046515.153',
        userData: {
          SIPCALLID: '123',
        }
      }];
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
    spyOn(processVolume, 'monitorCall').and.callFake(()=>{});

    let action = _.find(isolatedScope.getCallActions(), (a) => a.label == 'answer');
    action.trigger(calls[0]);
    expect(CtiProxy.answer).toHaveBeenCalledWith(calls[0].uniqueId);
  });

  it('forwards callId when apply action to ctiProxy when hangup', function() {
    let isolatedScope = createScopeAndVars();
    spyOn(CtiProxy, 'hangup');

    var calls = [
      {
        state: 'Established',
        uniqueId: '1625046515.153'
      }];
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    spyOn(XucPhoneState, 'getCalls').and.returnValue(calls);
    spyOn(processVolume, 'monitorCall').and.callFake(()=>{});

    let action = _.find(isolatedScope.getCallActions(), (a) => a.label == 'hangup');
    action.trigger(calls[0]);
    expect(CtiProxy.hangup).toHaveBeenCalledWith(calls[0].uniqueId);
  });

  it('should not display external numbers twice', function() {
    let isolatedScope = createScopeAndVars();
    
    var call = {
      otherDN: '1001',
      otherDName: '1001',
    };

    let displayPhoneNumber = isolatedScope.hasNameAndNumber(call);
    expect(displayPhoneNumber).toBe(false);
  });
});
