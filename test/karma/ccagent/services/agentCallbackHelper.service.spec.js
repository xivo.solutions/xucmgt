import moment from 'moment';

describe('Service: agent callback helper', function () {
  var agentCallbackHelper;
  var $rootScope;
  var $q;
  var xucAgentUser;
  var xucCallback;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('Agent'));

  beforeEach(angular.mock.inject(function(_$rootScope_, _agentCallbackHelper_, _$q_, _XucAgentUser_, _XucCallback_) {
    agentCallbackHelper = _agentCallbackHelper_;
    $rootScope = _$rootScope_;
    $q = _$q_;
    xucAgentUser = _XucAgentUser_;
    xucCallback = _XucCallback_;

    spyOn(Cti, 'getList');
  }));


  it('should load only callbacks from my queues', function() {
    var cb1 = CallbackRequestBuilder('1000').withQueue({id: 1}).build();
    var cb2 = CallbackRequestBuilder('1001').withQueue({id: 2}).build();
    var cb3 = CallbackRequestBuilder('1002').withQueue({id: 3}).build();

    var l1 = CallbackListBuilder('First List', 1, [cb1]).withQueue({id: 1}).build();
    var l2 = CallbackListBuilder('Second List', 2, [cb2]).withQueue({id: 2}).build();
    var l3 = CallbackListBuilder('Second List', 3, [cb3]).withQueue({id: 3}).build();

    spyOn(xucAgentUser, 'isMemberOfQueueAsync').and.callFake(function(qid) {
      var defer = $q.defer();
      defer.resolve(qid == 1 || qid == 3);
      return defer.promise;
    });

    expect(agentCallbackHelper.getCallbacks().length).toEqual(0);
    agentCallbackHelper.onCallbacksLoaded([l1, l2, l3]);
    $rootScope.$digest();

    expect(agentCallbackHelper.getCallbacks().length).toEqual(2);
  });

  it('should clear callbacks when no more callbacks are available', function() {

    const loadSomeCallbacks = () => {
      const cb1 = CallbackRequestBuilder('1000').withQueue({id: 1}).build();
      const l1 = CallbackListBuilder('First List', 1, [cb1]).withQueue({id: 1}).build();
      agentCallbackHelper.onCallbacksLoaded([l1]);
      $rootScope.$digest();
  
    };

    spyOn(xucAgentUser, 'isMemberOfQueueAsync').and.callFake(function(qid) {
      var defer = $q.defer();
      defer.resolve(qid == 1);
      return defer.promise;
    });

    loadSomeCallbacks();
    agentCallbackHelper.onCallbacksLoaded([]);
    $rootScope.$digest();

    expect(agentCallbackHelper.getCallbacks().length).toEqual(0);
  });

  
  it('should load callbacks from one queue with more than one list', function(){
    var cb1 = CallbackRequestBuilder('1000').withQueue({id: 1}).build();
    var cb2 = CallbackRequestBuilder('1001').withQueue({id: 1}).build();

    var l1 = CallbackListBuilder('First List', 1, [cb1]).withQueue({id: 1}).build();
    var l2 = CallbackListBuilder('Second List', 1, [cb2]).withQueue({id: 1}).build();

    spyOn(xucAgentUser, 'isMemberOfQueueAsync').and.callFake(function(qid) {
      var defer = $q.defer();
      defer.resolve(qid == 1);
      return defer.promise;
    });

    expect(agentCallbackHelper.getCallbacks().length).toEqual(0);
    agentCallbackHelper.onCallbacksLoaded([l1, l2]);
    $rootScope.$digest();

    expect(agentCallbackHelper.getCallbacks().length).toEqual(2);
  });

  it('should replace callbacks of a given queue when receiving an updated list', function(){
    var cb1 = CallbackRequestBuilder('1000').withQueue({id: 1}).build();
    var cb2 = CallbackRequestBuilder('1001').withQueue({id: 1}).build();

    var l1 = CallbackListBuilder('My List', 1, [cb1]).withQueue({id: 1}).build();
    var l2 = CallbackListBuilder('My List', 1, [cb2]).withQueue({id: 1}).build();

    spyOn(xucAgentUser, 'isMemberOfQueueAsync').and.callFake(function(qid) {
      var defer = $q.defer();
      defer.resolve(qid == 1);
      return defer.promise;
    });

    expect(agentCallbackHelper.getCallbacks().length).toEqual(0);
    agentCallbackHelper.onCallbacksLoaded([l1]);
    $rootScope.$digest();
    var res1 = agentCallbackHelper.getCallbacks();
    expect(res1.length).toEqual(1);
    expect(res1[0].phoneNumber).toEqual('1000');

    agentCallbackHelper.onCallbacksLoaded([l2]);
    $rootScope.$digest();
    var res2 = agentCallbackHelper.getCallbacks();
    expect(res2.length).toEqual(1);
    expect(res2[0].phoneNumber).toEqual('1001');
  });

  
  it('should not get already taken callbacks', function(){
    var cb1 = CallbackRequestBuilder('1000').withQueue({id: 1}).withAgentId(4).build();
    var cb2 = CallbackRequestBuilder('1001').withQueue({id: 1}).build();

    var l1 = CallbackListBuilder('First List', 1, [cb1,cb2]).withQueue({id: 1}).build();

    spyOn(xucAgentUser, 'isMemberOfQueueAsync').and.callFake(function() {
      var defer = $q.defer();
      defer.resolve(true);
      return defer.promise;
    });
    expect(agentCallbackHelper.getCallbacks().length).toEqual(0);
    agentCallbackHelper.onCallbacksLoaded([l1]);
    $rootScope.$digest();

    expect(agentCallbackHelper.getCallbacks().length).toEqual(1);
    expect(agentCallbackHelper.getCallbacks()[0]).toEqual(cb2);
  });

  it('should give correct callback color status icon', function(){
    spyOn(xucAgentUser, 'isMemberOfQueueAsync').and.callFake(function() {
      var defer = $q.defer();
      defer.resolve(true);
      return defer.promise;
    });

    var cbOk = CallbackRequestBuilder('1000').withQueue({id: 1}).withDueDate(moment().format("YYYY-MM-DD")).build();
    cbOk.dueStatus = xucCallback.getCallbackDueStatus(cbOk);
    var cbFuture = CallbackRequestBuilder('1002').withQueue({id: 1}).withDueDate(moment().add(1, "days").format("YYYY-MM-DD")).build();
    cbFuture.dueStatus = xucCallback.getCallbackDueStatus(cbFuture);
    var cbLate = CallbackRequestBuilder('1001').withQueue({id: 1}).withDueDate(moment().subtract(1, "days").format("YYYY-MM-DD")).build();
    cbLate.dueStatus = xucCallback.getCallbackDueStatus(cbLate);


    var l1 = CallbackListBuilder('Empty List', 1, []).withQueue({id: 1}).build();
    var l2 = CallbackListBuilder('Ok List', 2, [cbOk]).withQueue({id: 1}).build();
    var l3 = CallbackListBuilder('Futur & OK List', 3, [cbFuture, cbOk]).withQueue({id: 1}).build();
    var l4 = CallbackListBuilder('Red List', 4, [cbFuture, cbLate, cbOk]).withQueue({id: 1}).build();
    var l5 = CallbackListBuilder('Futur List', 3, [cbFuture]).withQueue({id: 1}).build();

    agentCallbackHelper.onCallbacksLoaded([l1]);
    $rootScope.$digest();
    expect(agentCallbackHelper.getStatus()).toEqual('');

    agentCallbackHelper.clearCallbacks();
    agentCallbackHelper.onCallbacksLoaded([l2]);
    $rootScope.$digest();
    expect(agentCallbackHelper.getStatus()).toEqual(xucCallback.DUE_STATUS_NOW);

    agentCallbackHelper.clearCallbacks();
    agentCallbackHelper.onCallbacksLoaded([l3]);
    $rootScope.$digest();
    expect(agentCallbackHelper.getStatus()).toEqual(xucCallback.DUE_STATUS_NOW);

    agentCallbackHelper.clearCallbacks();
    agentCallbackHelper.onCallbacksLoaded([l4]);
    $rootScope.$digest();
    expect(agentCallbackHelper.getStatus()).toEqual(xucCallback.DUE_STATUS_OVERDUE);

    
    agentCallbackHelper.clearCallbacks();
    agentCallbackHelper.onCallbacksLoaded([l5]);
    $rootScope.$digest();
    expect(agentCallbackHelper.getStatus()).toEqual(xucCallback.DUE_STATUS_LATER);
  });

  it('should remind which callback is currently started', function(){
    var cb = CallbackRequestBuilder('1000').withAgentId(4).withQueue({id: 1}).build();
    cb.uuid ="1234";

    spyOn(xucCallback, "startCallback");

    expect(agentCallbackHelper.isOngoingCallback(cb)).toBe(false);

    agentCallbackHelper.startCallbackCall(cb, 1000);
    $rootScope.$digest();
    expect(xucCallback.startCallback).toHaveBeenCalledWith(cb.uuid, 1000);
    expect(agentCallbackHelper.isOngoingCallback(cb)).toBe(true);
  });

  it('should release callback when cancel edition', function(){
    var cb = CallbackRequestBuilder('1000').withAgentId(4).withQueue({id: 1}).build();
    cb.uuid ="1234";

    spyOn(xucCallback, "releaseCallback");

    agentCallbackHelper.releaseCallback(cb);
    $rootScope.$digest();
    expect(xucCallback.releaseCallback).toHaveBeenCalledWith(cb.uuid);
    expect(agentCallbackHelper.isOngoingCallback(cb)).toBe(false);
  });

  it('should update ticket when closing callback', function(){
    var cb = CallbackRequestBuilder('1000').withAgentId(4).withQueue({id: 1}).build();
    cb.uuid ="1234";
    cb.ticket = agentCallbackHelper.createCallbackTicket();

    spyOn(xucCallback, "updateCallbackTicket");

    agentCallbackHelper.updateCallbackTicket(cb);
    $rootScope.$digest();
    expect(xucCallback.updateCallbackTicket).toHaveBeenCalledWith(cb.ticket);
    expect(agentCallbackHelper.isOngoingCallback(cb)).toBe(false);
  });

  it('should order callbacks in due order', () => {
    spyOn(xucAgentUser, 'isMemberOfQueueAsync').and.callFake(function() {
      var defer = $q.defer();
      defer.resolve(true);
      return defer.promise;
    });

    var cbOk = CallbackRequestBuilder('1000').withQueue({id: 1}).withDueDate(moment().format("YYYY-MM-DD")).build();
    cbOk.dueStatus = xucCallback.getCallbackDueStatus(cbOk);
    var cbFuture = CallbackRequestBuilder('1002').withQueue({id: 1}).withDueDate(moment().add(1, "days").format("YYYY-MM-DD")).build();
    cbFuture.dueStatus = xucCallback.getCallbackDueStatus(cbFuture);
    var cbLate = CallbackRequestBuilder('1001').withQueue({id: 1}).withDueDate(moment().subtract(1, "days").format("YYYY-MM-DD")).build();
    cbLate.dueStatus = xucCallback.getCallbackDueStatus(cbLate);

    var list = CallbackListBuilder('List with overdue', 4, [cbFuture, cbLate, cbOk]).withQueue({id: 1}).build();

    agentCallbackHelper.onCallbacksLoaded([list]);
    $rootScope.$digest();
    var callbacks = agentCallbackHelper.getCallbacks();
    expect(callbacks.length).toEqual(3);
    expect(callbacks[0].phoneNumber).toEqual('1001');
    expect(callbacks[1].phoneNumber).toEqual('1000');
    expect(callbacks[2].phoneNumber).toEqual('1002');

  });

});
