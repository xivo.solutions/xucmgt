describe('Conversation controller', function() {
  var $rootScope;
  var $scope;
  var ctrl;
  var $state;
  var $controller;

  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('ucAssistant'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_$rootScope_, _$controller_, _$translate_, _$state_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    $state =_$state_;
    $controller = _$controller_;
  }));

  function createController(){

    ctrl = $controller('ConversationController', {
      '$scope' :                  $scope,
      '$rootScope' :              $rootScope,
      '$state' :                  $state
    });
  }

  it('can instantiate controller', function(){
    createController();
    expect(ctrl).not.toBeUndefined();
  });

});
