describe("Menu controller", function () {
  var $rootScope;
  var $scope;
  var $state;
  var $uibModalStack;
  var $q;
  var $timeout;
  var ctrl;
  var personalContact;
  var mediaDevices;
  var localStorageService;
  var processVolume;
  var ctiProxy;
  var xucLink;
  var jitsiProxy;
  var $window;

  beforeEach(angular.mock.module("html-templates"));
  beforeEach(angular.mock.module("karma-backend"));
  beforeEach(angular.mock.module("ucAssistant"));

  beforeEach(
    angular.mock.inject(function (
      _$rootScope_,
      _$state_,
      _$uibModalStack_,
      $controller,
      $httpBackend,
      _personalContact_,
      _mediaDevices_,
      _localStorageService_,
      _processVolume_,
      _$q_,
      _$timeout_,
      _CtiProxy_,
      _XucLink_,
      _JitsiProxy_,
      _$window_
    ) {
      $rootScope = _$rootScope_;
      $scope = $rootScope.$new();
      $state = _$state_;
      $uibModalStack = _$uibModalStack_;
      $q = _$q_;
      $timeout = _$timeout_;
      personalContact = _personalContact_;
      mediaDevices = _mediaDevices_;
      localStorageService = _localStorageService_;
      processVolume = _processVolume_;
      ctiProxy = _CtiProxy_;
      xucLink = _XucLink_;
      jitsiProxy = _JitsiProxy_;
      $window = _$window_;
      $httpBackend
        .whenGET("/config/maxImportFileSize")
        .respond({ name: "maxImportFileSize", value: "20000000" });

      $window.externalConfig = {};

      ctrl = $controller("MenuController", {
        $scope: $scope,
        $state: $state,
        $uibModalStack: $uibModalStack,
        personalContact: personalContact,
        mediaDevices: mediaDevices,
        localStorageService: localStorageService,
        processVolume: processVolume,
      });
      spyOn(localStorageService, "set").and.callFake(() => {});
    })
  );

  it("can instantiate controller", function () {
    expect(ctrl).not.toBeUndefined();
  });

  it("dismiss modal if opened", function () {
    spyOn($state, "go");
    spyOn($uibModalStack, "dismissAll");

    ctrl.close("destination");
    expect($state.go).toHaveBeenCalledWith("destination");
    expect($uibModalStack.dismissAll).toHaveBeenCalled();
  });

  it("deletes all personal contacts", function () {
    var result = $q.defer();
    result.resolve({});
    spyOn(personalContact, "removeAll").and.callFake(() => {
      return result.promise;
    });
    spyOn($state, "go");

    ctrl.remove();
    $rootScope.$digest();
    expect(personalContact.removeAll).toHaveBeenCalled();
    expect($state.go).toHaveBeenCalledWith("interface.favorites");
  });

  it("export contacts", function () {
    spyOn(personalContact, "export");

    ctrl.export();
    $rootScope.$digest();
    expect(personalContact.export).toHaveBeenCalled();
  });

  it("imports contacts when file is selected", function () {
    var result = $q.defer();
    result.resolve({ data: "myResult" });
    spyOn(personalContact, "import").and.callFake(() => {
      return result.promise;
    });
    spyOn($timeout, "cancel");
    $scope.importFile = "test.csv";

    ctrl.import($scope.importFile);
    expect($scope.ajaxRequest.ongoing).toBe(true);
    $rootScope.$digest();
    expect(personalContact.import.calls.argsFor(0)[0]).toEqual("test.csv");
    expect($scope.imported).toBe("myResult");
    expect($scope.ajaxRequest.ongoing).toBe(false);
    expect($timeout.cancel).toHaveBeenCalled();
    expect($scope.importFile).toBeUndefined();
  });

  it("handle error on import contacts", function () {
    var responsePromise = $q.defer();
    var response = { data: { error: "NotHandledError" } };
    responsePromise.reject(response);
    spyOn(personalContact, "import").and.callFake(() => {
      return responsePromise.promise;
    });

    ctrl.import("test.csv");
    $rootScope.$digest();
    expect($scope.ajaxRequest["errorNotHandledError"]).toBe(true);
    expect($scope.ajaxRequest.ongoing).toBe(false);
  });

  it("on logout stop XucLink and WebRtc", function () {
    spyOn(xucLink, "logout");
    spyOn(ctiProxy, "isUsingWebRtc").and.returnValue(true);
    spyOn(ctiProxy, "stopUsingWebRtc");

    ctrl.logout();

    expect(xucLink.logout).toHaveBeenCalled();
    expect(ctiProxy.stopUsingWebRtc).toHaveBeenCalled();
  });

  it("on logout brings back the login page", function () {
    $window.externalConfig.useSso = false;
    spyOn(xucLink, "logout");
    spyOn($state, "go");
    spyOn($uibModalStack, "dismissAll");

    ctrl.logout();

    expect($uibModalStack.dismissAll).toHaveBeenCalled();
    expect($state.go).toHaveBeenCalledWith("login", { error: "Logout" });
  });

  it("on manual logout  with sso, clear credentials, stop xucLink and WebRtc", function () {
    $window.externalConfig.useSso = true;
    spyOn(xucLink, "clearCredentials");
    spyOn(xucLink, "logout");
    spyOn(ctiProxy, "isUsingWebRtc").and.returnValue(true);
    spyOn(ctiProxy, "stopUsingWebRtc");

    ctrl.logout();

    expect(xucLink.clearCredentials).toHaveBeenCalled();
    expect(xucLink.logout).toHaveBeenCalled();
    expect(ctiProxy.stopUsingWebRtc).toHaveBeenCalled();
  });

  it("on manual logout without sso, doesnt clear credentials, stop xucLink and WebRtc", function () {
    $window.externalConfig.useSso = false;
    spyOn(xucLink, "clearCredentials");
    spyOn(xucLink, "logout");
    spyOn(ctiProxy, "isUsingWebRtc").and.returnValue(true);
    spyOn(ctiProxy, "stopUsingWebRtc");

    ctrl.logout();

    expect(xucLink.clearCredentials).not.toHaveBeenCalled();
    expect(xucLink.logout).toHaveBeenCalled();
    expect(ctiProxy.stopUsingWebRtc).toHaveBeenCalled();
  });

  it("should stop the video if the user disconnects while a video is ongoing", function () {
    spyOn(xucLink, "logout");
    spyOn(ctiProxy, "stopUsingWebRtc");
    spyOn(xucLink, "clearCredentials");
    spyOn(Cti, "videoEvent");
    spyOn(xucLink, "whenLogged").and.callFake(() => {
      let deferred = $q.defer();
      deferred.resolve();
      return deferred.promise;
    });
    spyOn($rootScope, "$emit");

    spyOn(jitsiProxy, "videoIsOngoing").and.returnValue(true);
    spyOn(jitsiProxy, "dismissVideo");

    ctrl.logout();
    expect(jitsiProxy.dismissVideo).toHaveBeenCalled();
  });

});
