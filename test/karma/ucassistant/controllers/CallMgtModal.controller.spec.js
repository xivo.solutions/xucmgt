describe('CallMgtModal controller', function() {
  var $rootScope;
  var $scope;
  var XucUser;
  var XucUtils;
  var forward;
  var modalInstance;
  var CtiProxy;
  var $controller;
  var UserPreferenceService;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('ucAssistant'));

  beforeEach(angular.mock.inject(function (_$rootScope_, _$controller_, _$uibModal_, _XucUser_, _XucUtils_, _forward_, _CtiProxy_, _UserPreferenceService_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    XucUser = _XucUser_;
    XucUtils = _XucUtils_;
    forward = _forward_;
    CtiProxy = _CtiProxy_;
    $controller = _$controller_;
    UserPreferenceService = _UserPreferenceService_;

    modalInstance = {result: { then: jasmine.createSpy('modalInstance.result.then') }};
    spyOnProperty(UserPreferenceService, 'userPreferences', 'get').and.returnValue({'PREFERRED_DEVICE': '0'});
  }));

  function createController(isUsingWebRTC = true) {
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(isUsingWebRTC);

    return $controller('CallMgtModalController', {
      '$scope' :                  $scope,
      '$rootScope' :              $rootScope,
      '$uibModalInstance': modalInstance,
      'XucUser': XucUser,
      'XucUtils': XucUtils,
      'forward': forward,
      'CtiProxy': CtiProxy,
      'UserPreferenceService': UserPreferenceService
    });
  }

  it('can instantiate controller', function(){
    const ctrl = createController(true);
    expect(ctrl).not.toBeUndefined();
  });

  it('can set dnd mode', function(){
    const ctrl = createController(true);
    spyOn(forward, 'setDnd');
    ctrl.processDnd({});

    expect(forward.setDnd).toHaveBeenCalled();
  });

  it('can toggle switch based on a forwarding type', function() {
    const ctrl = createController(true);
    let currentUser = {
      'uncFwdEnabled': false,
      'naFwdEnabled': false
    };

    ctrl.toggle('uncFwd', currentUser);
    expect(currentUser.uncFwdEnabled).toEqual(true);
    ctrl.toggle('naFwd', currentUser);
    expect(currentUser.naFwdEnabled).toEqual(true);
    ctrl.toggle('uncFwd', currentUser);
    expect(currentUser.uncFwdEnabled).toEqual(false);
    ctrl.toggle('naFwd', currentUser);
    expect(currentUser.naFwdEnabled).toEqual(false);
  });

  it('toggle between phone and webrtc', function() {
    const ctrl = createController(true);
    spyOn(CtiProxy, 'toggleUniqueAccountDevice');

    $scope.$digest();
    ctrl.setUaDeviceSelected('phone');
    $scope.$digest();

    expect(CtiProxy.toggleUniqueAccountDevice).toHaveBeenCalledWith('phone');
  });

  it('toggle between webrtc and phone', function() {
    const ctrl = createController(false);
    spyOn(CtiProxy, 'toggleUniqueAccountDevice');

    $scope.$digest();
    ctrl.setUaDeviceSelected('webrtc');
    $scope.$digest();

    expect(CtiProxy.toggleUniqueAccountDevice).toHaveBeenCalledWith('webrtc');
  });

  it('disable the switch if the user is call', function() {
    const ctrl = createController(false);
    ctrl.calls = [];

    expect(ctrl.canSwitchDevice()).toBeTruthy();
  });

  it('enable the switch if the user is not in call', function() {
    const ctrl = createController(false);
    ctrl.calls = [{call: "call 1"}];

    expect(ctrl.canSwitchDevice()).toBeFalsy();
  });

  it('make sure both options are chosen if the user did not click on a preferred device', function() {
    spyOn(CtiProxy, 'isUsingMobileApp').and.returnValue(true);
    const ctrl = createController(false);
    
    expect(ctrl.userPreferenceModel.phone).toBeTruthy();
    expect(ctrl.userPreferenceModel.mobile).toBeTruthy();
  });
  
  it('checks if user is using mobile app', function() {
    spyOn(CtiProxy, 'isUsingMobileApp').and.returnValue(true);
    const ctrl = createController(false);
    $scope.$digest();
    expect(ctrl.isUsingMobileApp).toBe(true);
    expect(CtiProxy.isUsingMobileApp).toHaveBeenCalled();
  });
});
