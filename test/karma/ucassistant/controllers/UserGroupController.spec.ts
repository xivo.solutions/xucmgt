import angular, { IRootScopeService, IScope } from 'angular';
import { UserGroupController } from '../../../../app/assets/javascripts/ucassistant/controllers/UserGroup.controller';
import { UserGroupService } from '../../../../app/assets/javascripts/ucassistant/services/UserGroup.service';
import { StateService } from '@uirouter/angularjs';
import { MembershipStatus, UserGroup } from '../../../../app/assets/javascripts/xchelper/models/UserGroup.model';

describe('UserGroup controller', () => {
  var $rootScope: IRootScopeService;
  var $state: StateService;
  var $scope: IScope;
  var ctrl: UserGroupController;
  var UserGroupService: UserGroupService;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('ucAssistant'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(angular.mock.inject(function(_$rootScope_, $controller, _$translate_ , _$state_, _UserGroupService_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    $state = _$state_;
    UserGroupService = _UserGroupService_;

    ctrl = $controller('UserGroupController', {
      '$scope' :      $scope,
      '$translate':  _$translate_,
      '$state' : $state,
      'UserGroupService' : UserGroupService
    });
  }));

  var groupMembers: UserGroup[] = [
    {
      groupId: 1,
      groupName: "Support",
      number: "2222",
      membershipStatus: MembershipStatus.Available
    }
  ];

  beforeEach(() => {
    spyOnProperty(UserGroupService, 'userGroups', 'get').and.returnValue(groupMembers);
  });

  it('can instanciate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

  it('initializes groups on creation', function() {
    (ctrl as any).initializeGroups();
    expect(ctrl.groups).toEqual(groupMembers);
  });

  it('updates groups on userGroupUpdate event', () => {
    const userGroupsSpy = Object.getOwnPropertyDescriptor(UserGroupService, 'userGroups')!.get;

    $rootScope.$broadcast('userGroupUpdate');
    $rootScope.$digest();

    expect(userGroupsSpy).toHaveBeenCalled();
    expect(ctrl.groups).toEqual(groupMembers);
  });

  it('forward pause request to service', () => {
    spyOn(UserGroupService, 'pauseGroup');
    ctrl.pauseGroup(12);

    expect(UserGroupService.pauseGroup).toHaveBeenCalledWith(12);
  });

  it('forward unpause request to service', () => {
    spyOn(UserGroupService, 'unpauseGroup');
    ctrl.unpauseGroup(12);

    expect(UserGroupService.unpauseGroup).toHaveBeenCalledWith(12);
  });
});
