describe('CallMgtMenu controller', function() {
  var $rootScope;
  var $scope;
  var ctrl;
  var XucUser;
  var XucUtils;
  var $uibModal;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.module('xcHelper'));
  beforeEach(angular.mock.module('ucAssistant'));

  beforeEach(angular.mock.inject(function(_$rootScope_, $controller, _$uibModal_, _XucUser_, _XucUtils_) {
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    XucUser = _XucUser_;
    XucUtils = _XucUtils_;
    $uibModal = _$uibModal_;
    ctrl = $controller('CallMgtMenuController', {
      '$scope' :                  $scope,
      '$rootScope' :              $rootScope,
      'XucUser': XucUser,
      'XucUtils': XucUtils,
      '$uibModal': $uibModal
    });
  }));

  it('can instantiate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

  it('can open call management uibmodal', function(){
    spyOn($uibModal, 'open');
    ctrl.showForward();
    expect($uibModal.open).toHaveBeenCalled();
  });

});
