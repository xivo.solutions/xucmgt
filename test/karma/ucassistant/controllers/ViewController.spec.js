describe('View controller', function () {
  var $rootScope;
  var $scope;
  var $state;
  var XucPhoneEventListener;
  var XucUtils;
  var ctrl;
  var UserPreferenceService;

  beforeEach(angular.mock.module('ucAssistant'));
  beforeEach(angular.mock.module('xcCti'));

  beforeEach(function () {
    jasmine.addMatchers({
      toEqualData: customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function (_XucUtils_, _$rootScope_, $controller, _$state_, _XucPhoneEventListener_, _UserPreferenceService_) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $state = _$state_;
    XucPhoneEventListener = _XucPhoneEventListener_;
    XucUtils = _XucUtils_;
    UserPreferenceService = _UserPreferenceService_;

    spyOn(XucPhoneEventListener, 'addHandler').and.callFake(() => {});
    spyOn(UserPreferenceService, 'getMissedCalls');
    spyOn(UserPreferenceService, 'resetMissedCalls');


    ctrl = $controller('ViewController', {
      '$scope': $scope,
      '$rootScope': $rootScope,
      '$state': $state,
      'XucPhoneEventListener': XucPhoneEventListener,
      'XucUtils': XucUtils
    });
  }));

  it('can instantiate controller', function () {
    expect(ctrl).not.toBeUndefined();
  });

  it('triggers search if at least two letters are inputted', () => {
    spyOn($state, 'go');
    var input = 'steve';
    $scope.destination = input;
    ctrl.dialOrSearch();
    expect($state.go).toHaveBeenCalledWith('interface.search', Object({ showFavorites: false, search: 'steve' }));
  });

  it('does not trigger search if less than two letters are inputted', () => {
    spyOn($state, 'go');
    var input = 's';
    $scope.destination = input;
    ctrl.dialOrSearch();

    expect($state.go).not.toHaveBeenCalled();
  });

  it('does not trigger search when pressing enter key and search bar is empty', () => {
    spyOn($state, 'go');

    const escapeEvent = document.createEvent('CustomEvent');
    escapeEvent.which = 13;
    escapeEvent.initEvent('keypress', true, true);
    document.dispatchEvent(escapeEvent);
    ctrl.isEnterKey(escapeEvent);

    var emptyInput = "";
    $scope.destination = emptyInput; 
    ctrl.dialOrSearch();

    expect($state.go).not.toHaveBeenCalled();
  });

  it('check if user pressed enter', () => {
    spyOn(ctrl, 'dialOrSearch');
    const escapeEvent = document.createEvent('CustomEvent');
    escapeEvent.which = 13;
    escapeEvent.initEvent('keypress', true, true);
    document.dispatchEvent(escapeEvent);
    ctrl.isEnterKey(escapeEvent);
    expect(ctrl.dialOrSearch).toHaveBeenCalled();
  });

  it('change button icon to a phone if number are inputted', () => {
    spyOn(XucUtils, 'isaPhoneNb').and.returnValue(true);

    ctrl.changeIcon({});
    expect($scope.isPhone).toBe(true);
  });

  it('change button icon to a search icon if letters are inputted', () => {
    spyOn(XucUtils, 'isaPhoneNb').and.returnValue(false);

    ctrl.changeIcon();
    expect($scope.isPhone).toBe(false);
  });

  it('clear input when user click on x', () => {
    $scope.destination = 'oof';
    ctrl.emptyInput();
    expect($scope.destination).toBe("");
  });

  it('Set invalid input', () => {
    ctrl.setInputInvalid();
    expect(ctrl.tooltipVisible).toBeTrue();
    expect(ctrl.tooltip).toBe('UC_INVALID_SEARCH');
  });

  it('Set invalid input custom message', () => {
    ctrl.setInputInvalid('UC_INVALID_SOMETHING');
    expect(ctrl.tooltipVisible).toBeTrue();
    expect(ctrl.tooltip).toBe('UC_INVALID_SOMETHING');
  });

  it('Retrieve current number of missed Call when receiving a new missed call event', () => {
    expect(ctrl.missedCalls).toBe(0);
    $scope.$emit(UserPreferenceService.NEW_NB_MISSED_CALLS, 5);
    expect(ctrl.missedCalls).toBe(5);
  });
});
