describe('InitController', function() {
  var $rootScope;
  var $scope;
  var $state;
  var ctrl;
  var XucLink;
  var CtiProxy;
  var XucPhoneState;
  var errorModal;
  var externalEvent;
  var electronWrapper;
  var XucNotification;
  var $controller;
  var $window = {
    externalConfig: {
      useSso: true
    },
    location: {
      search: ""
    }
  };

  $window.externalConfig = { host: '192.168.0.3' };

  class jitsiConstructor {
    constructor(domain, options) {
      this.domain = domain;
      this.options = options,
      this.dispose = () => {delete this;},
      this.addListener = () => { };
    }
  }

  window.JitsiMeetExternalAPI = jitsiConstructor;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('ucAssistant'));
  beforeEach(angular.mock.module('xcHelper'));

  beforeEach(angular.mock.inject(function (_$rootScope_, _$state_, _XucLink_, _CtiProxy_, _XucPhoneState_, _errorModal_, _ExternalEvent_, _electronWrapper_, _XucNotification_, _$controller_) {
    window.externalConfig = { host: '192.168.0.3' };
    $rootScope =_$rootScope_;
    $scope = $rootScope.$new();
    $state = _$state_;
    XucLink = _XucLink_;
    CtiProxy = _CtiProxy_;
    XucPhoneState = _XucPhoneState_;
    errorModal = _errorModal_;
    externalEvent = _ExternalEvent_;
    electronWrapper = _electronWrapper_;
    XucNotification =_XucNotification_;
    $controller = _$controller_;
    ctrl = initCtrl($window);
  }));

  const initCtrl = (window) => {
    return $controller('InitController', {
      '$scope' :      $scope,
      'XucLink':      XucLink,
      'CtiProxy':     CtiProxy,
      '$state':       $state,
      '$window':      window,
      'XucPhoneState': XucPhoneState,
      'errorModal': errorModal,
      'externalEvent': externalEvent,
      'electronWrapper': electronWrapper,
      'XucNotification': XucNotification
    });
  };

  it('can instanciate controller', function() {
    expect(ctrl).not.toBeUndefined();
  });

  it('upon linkDisConnected stop XucLink and redirect to login page', function() {
    spyOn(XucLink, 'logout');
    spyOn($state, 'go');
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(false);

    $rootScope.$broadcast('linkDisConnected');
    $rootScope.$apply();

    expect(XucLink.logout).toHaveBeenCalled();
    expect($state.go).toHaveBeenCalledWith('login', {'error': 'LinkClosed'});
  });

  it('upon linkDisConnected stop webrtc if active and no call', function() {
    spyOn(XucLink, 'logout');
    spyOn($state, 'go');
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    spyOn(CtiProxy, 'stopUsingWebRtc');
    spyOn(XucPhoneState, 'getCalls').and.returnValue([]);

    $rootScope.$broadcast('linkDisConnected');
    $rootScope.$apply();

    expect(CtiProxy.stopUsingWebRtc).toHaveBeenCalled();
    expect(XucLink.logout).toHaveBeenCalled();
    expect($state.go).toHaveBeenCalledWith('login', {'error': 'LinkClosed'});
  });

  it('upon linkDisConnected when using webrtc, only display error if call is ongoing', function() {
    spyOn(XucLink, 'logout');
    spyOn($state, 'go');
    spyOn(CtiProxy, 'isUsingWebRtc').and.returnValue(true);
    spyOn(CtiProxy, 'stopUsingWebRtc');
    spyOn(errorModal, 'showErrorModal');
    var dummyCall = {srcNum: '1234'};
    spyOn(XucPhoneState, 'getCalls').and.returnValue([dummyCall]);

    $rootScope.$broadcast('linkDisConnected');
    $rootScope.$apply();

    expect(errorModal.showErrorModal).toHaveBeenCalledWith('CTI_SOCKET_CLOSED_WHILE_USING_WEBRTC');
    expect(CtiProxy.stopUsingWebRtc).not.toHaveBeenCalled();
    expect(XucLink.logout).not.toHaveBeenCalled();
    expect($state.go).not.toHaveBeenCalled();
  });

  it('on electron quit do stop WebRtc', function () {
    spyOn(CtiProxy, 'stopUsingWebRtc');
    spyOn(electronWrapper, 'forceQuit');
    spyOn(CtiProxy, 'hangup');
    spyOn(XucPhoneState, 'getCalls').and.returnValue([{ "uniqueId": "123.456" }, { "uniqueId": "456.789" }]);


    var fakeConfirmQuitEvent = {
      data: {type:'CONFIRM_QUIT_EVENT', value:1},
      origin: 'http://server',
      source: 'global',
      target: 'global'
    };
    externalEvent.handleEvent(fakeConfirmQuitEvent);
    $rootScope.$digest();
    expect(electronWrapper.forceQuit).toHaveBeenCalled();
    expect(CtiProxy.stopUsingWebRtc).toHaveBeenCalled();
    expect(CtiProxy.hangup).toHaveBeenCalledWith("123.456");
    expect(CtiProxy.hangup).toHaveBeenCalledWith("456.789");
  });

  it('should show the phone bar when ending a video call', function() {
    $rootScope.$broadcast('EventCloseVideo');

    expect($scope.phoneBarIsVisible()).toEqual(true);
  });
});
