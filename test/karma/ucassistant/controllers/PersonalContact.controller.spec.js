describe('PersonalContact controller', function () {
  var $rootScope;
  var $scope;
  var $state;
  var $q;
  var $uibModalStack;
  var $controller;
  var personalContact;
  var XucDirectory;
  var ctrl;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('ucAssistant'));

  beforeEach(angular.mock.inject(function (_$rootScope_, _$state_, _$uibModalStack_, _personalContact_, _$q_, _$controller_, _XucDirectory_, $httpBackend) {
    $rootScope = _$rootScope_;
    $scope = $rootScope.$new();
    $state = _$state_;
    $uibModalStack = _$uibModalStack_;
    $q = _$q_;
    $controller = _$controller_;
    personalContact = _personalContact_;
    XucDirectory = _XucDirectory_;

    $httpBackend.whenGET("/config/maxImportFileSize").respond({ "name": "maxImportFileSize", "value": "20000000" });
  }));

  const initCtrl = (contactId, contactData) => {
    ctrl = $controller('PersonalContactController', {
      '$scope': $scope,
      '$state': $state,
      '$uibModalStack': $uibModalStack,
      'personalContact': personalContact,
      'XucDirectory': XucDirectory,
      'contactId': contactId,
      'contactData': contactData
    });
  };

  it('can instantiate controller', function () {
    initCtrl();
    expect(ctrl).not.toBeUndefined();
  });

  it('creates a personal contact', function () {
    initCtrl();
    $scope.personalContactForm = {
      lastname: { $viewValue: 'Doe' },
      firstname: { $viewValue: 'John' },
      company: { $viewValue: 'Corp' },
      email: { $viewValue: 'j@corp' },
      number: { $viewValue: '1234' },
      mobile: { $viewValue: '4321' },
      fax: { $viewValue: '4444' },
    };

    var result = $q.defer();
    result.resolve({});
    spyOn(personalContact, 'add').and.callFake(() => {
      return result.promise;
    });
    spyOn($state, 'go');
    spyOn(Cti, 'addFavorite');

    ctrl.savePersonalContact();
    expect($scope.ajaxRequest.ongoing).toBe(true);
    expect(personalContact.add).toHaveBeenCalledWith(
      { "lastname": "Doe", "firstname": "John", "company": "Corp", "email": "j@corp", "number": "1234", "mobile": "4321", "fax": "4444" }
    );
    $rootScope.$digest();
    expect(Cti.addFavorite).not.toHaveBeenCalled();
    expect($state.go).toHaveBeenCalledWith('interface.favorites');
    expect($scope.ajaxRequest.ongoing).toBe(false);
  });

  it('creates a personal contact and sets it as favorite', function () {
    initCtrl();
    $scope.personalContactForm = {
      lastname: { $viewValue: 'Doe' },
      firstname: { $viewValue: 'John' },
    };

    var result = $q.defer();
    result.resolve({ data: { id: '1' } });
    spyOn(personalContact, 'add').and.callFake(() => {
      return result.promise;
    });
    spyOn($state, 'go');
    spyOn(Cti, 'addFavorite');
    $scope.favorite = true;

    ctrl.savePersonalContact();
    expect(personalContact.add).toHaveBeenCalledWith(
      { "lastname": "Doe", "firstname": "John" }
    );
    $rootScope.$digest();
    expect(Cti.addFavorite).toHaveBeenCalledWith('1', 'personal');
  });

  it('dismiss modal if opened', function () {
    initCtrl();
    spyOn($state, 'go');
    spyOn($uibModalStack, 'dismissAll');

    ctrl.close('destination');
    expect($state.go).toHaveBeenCalledWith('destination');
    expect($uibModalStack.dismissAll).toHaveBeenCalled();
  });

  it('dismiss modal if opened', function () {
    initCtrl();
    spyOn($state, 'go');
    spyOn($uibModalStack, 'dismissAll');

    ctrl.close('destination');
    expect($state.go).toHaveBeenCalledWith('destination');
    expect($uibModalStack.dismissAll).toHaveBeenCalled();
  });

  it('populates with personal contact info if contactId defined at controller instantiation', function () {
    let contact = {
      data: {
        lastname: 'Doe',
        firstname: 'John',
        number: '1234'
      }
    };

    var result = $q.defer();
    result.resolve(contact);
    spyOn(personalContact, 'get').and.callFake(() => {
      return result.promise;
    });
    spyOn(XucDirectory, 'getFavoriteContactSheet').and.returnValue([{ sources: [{ name: 'internal', id: '42' }, { name: 'personal', id: '1' }] }]);
    initCtrl('1');

    $rootScope.$digest();
    expect($scope.favorite).toBe(true);
    expect($scope.lastname).toBe('Doe');
    expect($scope.firstname).toBe('John');
    expect($scope.number).toBe('1234');
  });

  it('populates a new personal contact if contactData defined at controller instantiation', function () {
    let contactData = {
      lastname: 'Doe',
      firstname: 'John',
      number: '1234'
    };

    initCtrl(null, contactData);

    $rootScope.$digest();
    expect($scope.lastname).toBe('Doe');
    expect($scope.firstname).toBe('John');
    expect($scope.number).toBe('1234');
  });

  it('updates a personal contact number and favorite', function () {
    spyOn(XucDirectory, 'getFavoriteContactSheet').and.returnValue([{ sources: [{ name: 'internal', id: '42' }, { name: 'personal', id: '1' }] }]);
    initCtrl('1');

    $scope.personalContactForm = {
      lastname: { $viewValue: 'Doe' },
      firstname: { $viewValue: 'John' },
      number: { $viewValue: '4444' },
    };

    var result = $q.defer();
    result.resolve({ data: { id: '1' } });
    spyOn(personalContact, 'update').and.callFake(() => {
      return result.promise;
    });
    spyOn($state, 'go');
    spyOn(Cti, 'removeFavorite');

    ctrl.savePersonalContact();
    expect($scope.ajaxRequest.ongoing).toBe(true);
    expect(personalContact.update).toHaveBeenCalledWith(
      { "lastname": "Doe", "firstname": "John", "number": "4444" }, '1'
    );
    $rootScope.$digest();
    expect(Cti.removeFavorite).toHaveBeenCalledWith('1', 'personal');
    expect($state.go).toHaveBeenCalledWith('interface.favorites');
    expect($scope.ajaxRequest.ongoing).toBe(false);
  });

  it('deletes a personal contact', function () {
    initCtrl('1');

    var result = $q.defer();
    result.resolve({});
    spyOn(personalContact, 'remove').and.callFake(() => {
      return result.promise;
    });
    spyOn($state, 'go');

    ctrl.remove();
    $rootScope.$digest();
    expect(personalContact.remove).toHaveBeenCalledWith('1');
    expect($state.go).toHaveBeenCalledWith('interface.favorites');
  });
});