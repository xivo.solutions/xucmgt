import 'xccti/cti-webpack';

describe('Factory: Personal contacts', function () {
  var $q;
  var $httpBackend;
  var $window;
  var $rootScope;
  var personalContact;
  var fileReader;
  var token = "aaaa-bbbb-cccc-dddd-1234";
  var user = { username: "jbond", phoneNumber: "1001", token: token };
  var checkAuthHeader;

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('ucAssistant'));

  beforeEach(function () {
    jasmine.addMatchers({
      toEqualData: customMatchers.toEqualData
    });
  });

  beforeEach(angular.mock.inject(function (_personalContact_, _XucLink_, _$q_, _$httpBackend_, _$window_, _$rootScope_, _fileReader_) {
    personalContact = _personalContact_;
    $q = _$q_;
    $httpBackend = _$httpBackend_;
    $window = _$window_;
    $rootScope = _$rootScope_;
    fileReader = _fileReader_;

    spyOn(_XucLink_, 'whenLogged').and.returnValue($q.resolve(user));
    spyOn(_XucLink_, 'getServerUrl').and.returnValue("http://localhost");
    $httpBackend.whenGET("/config/maxImportFileSize").respond({ "name": "maxImportFileSize", "value": "20000000" });

    checkAuthHeader = (headers) => {
      return headers['Authorization'] === 'Bearer ' + token;
    };
  }));

  afterEach(function () {
    $httpBackend.verifyNoOutstandingExpectation();
    $httpBackend.verifyNoOutstandingRequest();
  });

  it('should list personal contacts', function () {
    const pc = { 'entries': { 'firstName': 'John', 'lastName': 'Doe' } };

    $httpBackend.expectGET('http://localhost/xuc/api/2.0/contact/personal', checkAuthHeader).respond(() => {
      return [200, pc];
    });
    let promise = personalContact.list();
    $httpBackend.flush();

    promise.then((result) => {
      expect(result.data).toEqual(pc);
    });
  });

  it('should add personal contact', function () {
    const pc = { 'firstName': 'John', 'lastName': 'Doe' };

    $httpBackend.expectPOST('http://localhost/xuc/api/2.0/contact/personal', pc, checkAuthHeader).respond(() => {
      return [200, {}];
    });
    let promise = personalContact.add(pc);
    $httpBackend.flush();

    promise.then((result) => {
      expect(result.data).toEqual({});
    });
  });

  it('should get personal contact', function () {
    const pc = { 'firstName': 'John', 'lastName': 'Doe' };

    $httpBackend.expectGET('http://localhost/xuc/api/2.0/contact/personal/1', checkAuthHeader).respond(() => {
      return [200, pc];
    });
    let promise = personalContact.get('1');
    $httpBackend.flush();

    promise.then((result) => {
      expect(result.data).toEqual(pc);
    });
  });

  it('should update personal contact', function () {
    const pc = { 'firstName': 'John', 'lastName': 'Doe' };

    $httpBackend.expectPUT('http://localhost/xuc/api/2.0/contact/personal/1', pc, checkAuthHeader).respond(() => {
      return [200, pc];
    });
    let promise = personalContact.update(pc, '1');
    $httpBackend.flush();

    promise.then((result) => {
      expect(result.data).toEqual(pc);
    });
  });

  it('should remove personal contact', function () {
    $httpBackend.expectDELETE('http://localhost/xuc/api/2.0/contact/personal/1', checkAuthHeader).respond(() => {
      return [204];
    });
    let promise = personalContact.remove('1');
    $httpBackend.flush();

    promise.then((result) => {
      expect(result.status).toEqual(204);
    });
  });

  it('should remove all personal contacts', function () {
    $httpBackend.expectDELETE('http://localhost/xuc/api/2.0/contact/personal', checkAuthHeader).respond(() => {
      return [204];
    });
    let promise = personalContact.removeAll();
    $httpBackend.flush();

    promise.then((result) => {
      expect(result.status).toEqual(204);
    });
  });

  it('should export all personal contacts', function () {
    spyOn($window, 'open');
    personalContact.export();
    $rootScope.$digest();

    expect($window.open).toHaveBeenCalledWith('http://localhost/xuc/api/2.0/contact/export/personal?token=' + token);
  });

  it('should import personal contacts', function () {
    let blob = new Blob([""], { type: 'text/csv' });
    blob["lastModifiedDate"] = "";
    blob["name"] = "filename.csv";
    let csvFile = blob;

    let filePromise = $q.defer();
    filePromise.resolve("csv");
    spyOn(fileReader, 'readAsText').and.callFake(() => { return filePromise.promise; });

    $httpBackend.expectPOST('http://localhost/xuc/api/2.0/contact/import/personal', "csv", checkAuthHeader).respond(() => {
      return [201];
    });
    let promise = personalContact.import(csvFile, $rootScope);
    $httpBackend.flush();

    promise.then((result) => {
      expect(result.status).toEqual(201);
    });
  });

  it('not allow invalid file type to be imported', function () {
    let blob = new Blob([""], { type: '' });
    blob["lastModifiedDate"] = "";
    blob["name"] = "filename.bad";
    let csvFile = blob;

    let promise = personalContact.import(csvFile, $rootScope);

    promise.then(() => { }, (result) => {
      expect(result.data).toEqual({ error: 'InvalidFileType' });
    });
  });

  it('not allow large file to be imported', function () {
    $httpBackend.whenGET("/config/maxImportFileSize").respond({ "name": "maxImportFileSize", "value": "1" });
    let blob = new Blob(["Im a too long data blob"], { type: '' });
    blob["lastModifiedDate"] = "";
    blob["name"] = "filename.csv";
    let csvFile = blob;

    let promise = personalContact.import(csvFile, $rootScope);

    promise.then(() => { }, (result) => {
      expect(result.data).toEqual({ error: 'FileTooLarge' });
    });
  });


});
