import angular, { IRootScopeService } from 'angular';
import { UserGroupService } from '../../../../app/assets/javascripts/ucassistant/services/UserGroup.service';
import { MembershipStatus } from '../../../../app/assets/javascripts/xchelper/models/UserGroup.model';

var Cti = (window as any).Cti;


describe('Service: User Group', function () {
  let userGroupService: UserGroupService;
  let remoteConfiguration: any;
  let $rootScope: IRootScopeService;

  beforeEach(angular.mock.module('ucAssistant'));
  beforeEach(angular.mock.module('xcCti'));
  beforeEach(angular.mock.inject(function (_UserGroupService_,_remoteConfiguration_, _$rootScope_) {
    userGroupService = _UserGroupService_;
    remoteConfiguration = _remoteConfiguration_;
    $rootScope = _$rootScope_;

    Cti.Topic(Cti.MessageType.USERGROUPS).clear();
    Cti.Topic(Cti.MessageType.USERGROUPUPDATE).clear();
    Cti.Topic(Cti.MessageType.FLASHTEXTEVENT).clear();
    spyOn(Cti, 'setHandler').and.callThrough();
    spyOn(Cti, 'sendCallback');
    spyOn(remoteConfiguration, 'isAgent').and.returnValue(false);
    spyOn($rootScope, '$broadcast');
    spyOn($rootScope, '$$phase');
  }));

  const initialState = () => {
    const eventData = {
      "msgType": "UserGroups",
      "ctiMessage": [
        {"groupId": 2, groupName: "Justice League", number: "2001", membershipStatus:  MembershipStatus.Available},
        {"groupId": 3, groupName: "X-Force", number: "2002", membershipStatus:  MembershipStatus.Available},
        {"groupId": 4, groupName: "Avengers", number: "2003", membershipStatus:  MembershipStatus.Available}
      ]
    };

    userGroupService.init();
    Cti.Topic(eventData.msgType).publish(eventData.ctiMessage);
    expect(userGroupService.userGroups).toEqual([
      {"groupId": 2, groupName: "Justice League", number: "2001", membershipStatus: MembershipStatus.Available},
      {"groupId": 3, groupName: "X-Force", number: "2002", membershipStatus: MembershipStatus.Available},
      {"groupId": 4, groupName: "Avengers", number: "2003", membershipStatus: MembershipStatus.Available}
    ]);
  };

  it("show pause actions is hide", () => {
    spyOn(remoteConfiguration, 'getBoolean').withArgs("ucShowGroupControl").and.returnValue(Promise.resolve(false));

    initialState();

    setTimeout(() => {
      expect(userGroupService.canShowPauseActions()).toBe(false);
    });
  });

  it("show group pause/unpause actions by default", () => {
    initialState();
    expect(userGroupService.canShowPauseActions()).toBe(true);
  });


  it('Send UserGroups from Service', () => {
    initialState();
  });

  it('Update userGroup from Service', () => {
    initialState();

    const eventDataUpdate = {
      "msgType": "UserGroupUpdate",
      "ctiMessage": {"groupId": 2, groupName: "League of legends", number: "2003", membershipStatus:  MembershipStatus.Available}
    };

    Cti.Topic(eventDataUpdate.msgType).publish(eventDataUpdate.ctiMessage);
    expect(userGroupService.userGroups).toEqual([
      {"groupId": 2, groupName: "League of legends", number: "2003", membershipStatus:  MembershipStatus.Available},
      {"groupId": 3, groupName: "X-Force", number: "2002", membershipStatus:  MembershipStatus.Available},
      {"groupId": 4, groupName: "Avengers", number: "2003", membershipStatus:  MembershipStatus.Available}
    ]);

    expect($rootScope.$broadcast).toHaveBeenCalledWith('userGroupUpdate');
  });

  it('Create group if user dont have any on update from service', () => {
    userGroupService.init();
    expect(userGroupService.userGroups).toBeUndefined;

    const eventDataUpdate = {
      "msgType": "UserGroupUpdate",
      "ctiMessage": {"groupId": 2, groupName: "League of legends", number: "2003", membershipStatus:  MembershipStatus.Available}
    };
    Cti.Topic(eventDataUpdate.msgType).publish(eventDataUpdate.ctiMessage);
    expect(userGroupService.userGroups).toEqual([
      {"groupId": 2, groupName: "League of legends", number: "2003", membershipStatus:  MembershipStatus.Available}
    ]);

    expect($rootScope.$broadcast).toHaveBeenCalledWith('userGroupUpdate');
  });

  it('Delete userGroup from Service', () => {
    initialState();

    const eventDataUpdate = {
      "msgType": "UserGroupUpdate",
      "ctiMessage": {"groupId": 2, groupName: "Justice League", number: "2003", membershipStatus: "Exited"}
    };

    Cti.Topic(eventDataUpdate.msgType).publish(eventDataUpdate.ctiMessage);
    expect(userGroupService.userGroups).toEqual([
      {"groupId": 3, groupName: "X-Force", number: "2002", membershipStatus:  MembershipStatus.Available},
      {"groupId": 4, groupName: "Avengers", number: "2003", membershipStatus:  MembershipStatus.Available}
    ]);

    expect($rootScope.$broadcast).toHaveBeenCalledWith('userGroupUpdate');
  });

  it('Send pause request from Service', () => {
    initialState();
    spyOn(Cti, 'pauseUserGroup');

    userGroupService.pauseGroup(12);
    expect(Cti.pauseUserGroup).toHaveBeenCalledWith(12);
  });


  it('Send unpause request from Service', () => {
    initialState();
    spyOn(Cti, 'unpauseUserGroup');

    userGroupService.unpauseGroup(12);
    expect(Cti.unpauseUserGroup).toHaveBeenCalledWith(12);
  });

  it('call group', () => {
    initialState();
    spyOn(Cti, 'dial');

    userGroupService.callGroup(12);
    expect(Cti.dial).toHaveBeenCalledWith(12);
  });
});
