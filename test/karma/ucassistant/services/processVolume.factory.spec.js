import 'xccti/cti-webpack';

describe('Factory: Process volume', function () {
  var processVolume;
  var $rootScope;
  var $interval;
  var $timeout;
  var xcWebrtcMock;

  class VolumeAnalyserMock {
    constructor(volume){
      this.volume = volume;
    }

    getVolumeData () {
      return this.volume;
    }
  }

  class MonitoredCallMock {
    constructor(sipCallId, volume){
      this.inputVolumeAnalyser = new VolumeAnalyserMock(volume);
      this.outputVolumeAnalyser = new VolumeAnalyserMock(volume);
      this.sipCallId = sipCallId;
    }

    startInputAnalyser() { }
    startOutputAnalyser() { }

  }

  var oldXcWebrtc;

  var createXcWebrtcMock = (volume) => {
    let xcWebrtcMethods = {
      getAudioContext: (()=>{return volume;})(),
      setLocalStreamHandler: () => {},
      setRemoteStreamHandler: () => {}
    };

    xcWebrtcMock = jasmine.createSpyObj('xc_webrtc', xcWebrtcMethods);
    xcWebrtcMock.MessageType = xc_webrtc.MessageType;
    oldXcWebrtc = xc_webrtc;
    xc_webrtc = xcWebrtcMock;
  };

  beforeEach(angular.mock.module('html-templates'));
  beforeEach(angular.mock.module('karma-backend'));
  beforeEach(angular.mock.module('ucAssistant'));

  beforeEach(angular.mock.inject(function(_processVolume_, _$rootScope_, _$interval_, _$timeout_) {
    processVolume = _processVolume_;
    $rootScope = _$rootScope_;
    $interval = _$interval_;
    $timeout = _$timeout_;
  }));

  afterEach(function() {
    xc_webrtc = oldXcWebrtc;
  });

  it('send the correct event if the microphone is not working for 5 seconds', function() {
    createXcWebrtcMock(0);
    let outputVolumeData;
    let inputActive;

    $rootScope.$on(processVolume.EVENT_OUTPUT_VOLUME,(event, data)=>{
      outputVolumeData = data;
    });

    $rootScope.$on(processVolume.EVENT_INPUT_ACTIVE,(event, data)=>{
      inputActive = data;
    });

    processVolume.monitorCall('qwerty123', MonitoredCallMock);
    $interval.flush(processVolume.PROCESS_AUDIO_INTERVAL_MS + 1);
    
    $rootScope.$digest();

    expect(outputVolumeData).toEqual(jasmine.objectContaining({sipCallId: 'qwerty123', volume: 0}));
    $timeout.flush(processVolume.MICROPHONE_ACTIVITY_TIMEOUT_MS + 1);
    expect(inputActive).toBeFalsy();
  });

  it('send the correct event if the microphone is working', function() {
    createXcWebrtcMock(50);
    let outputVolumeData;
    let audioInputData;

    $rootScope.$on(processVolume.EVENT_OUTPUT_VOLUME,(event, data)=>{
      outputVolumeData = data;
    });

    $rootScope.$on(processVolume.EVENT_INPUT_ACTIVE,(event, data)=>{
      audioInputData = data;
    });

    processVolume.monitorCall('qwerty123', MonitoredCallMock);
    $interval.flush(processVolume.PROCESS_AUDIO_INTERVAL_MS + 1);

    $rootScope.$digest();

    expect(outputVolumeData).toEqual(jasmine.objectContaining({sipCallId: 'qwerty123', volume: 50}));
    expect(audioInputData).toBeTruthy();
  });

});
