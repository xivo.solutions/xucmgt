const { CallHistoryModel, CallStatus } = require("xchelper/models/CallHistory.model");

describe('Service: Call History', function () {
  var callHistoryService;

  beforeEach(angular.mock.module('ucAssistant'));
  
  beforeEach(angular.mock.inject(function (_CallHistoryService_) {
    callHistoryService = _CallHistoryService_;
  }));


  const genCallHistoryModel = (_callStatus, _number, _firstName, _lastName, _phoneStatus, _videoStatus, _userName, _initials, _start, _duration) => {
    if (_callStatus == CallStatus.Emitted) {
      return {
        status: _callStatus,
        dstNum: _number,
        dstFirstName: _firstName,
        dstLastName: _lastName,
        dstPhoneStatus: _phoneStatus,
        dstVideoStatus: _videoStatus,
        dstUsername: _userName,
        initials: _initials,
        start : _start,
        duration: _duration
      };
    } else {
      return {
        status: _callStatus,
        srcNum: _number,
        srcFirstName: _firstName,
        srcLastName: _lastName,
        srcPhoneStatus: _phoneStatus,
        srcVideoStatus: _videoStatus,
        srcUsername: _userName,
        initials: _initials,
        start : _start,
        duration: _duration
      };
    }
  };

  const haveSameData = function (obj1, obj2) {
    const values2 = Object.values(obj2);

    if(obj1.length != obj2.length) return false;

    Object.values(obj1).forEach(
      (calls, index) => {
        if(calls != values2[index]) return false;
      }
    );

    return true;
  };

  it('should handle initials properly', function () {
    const date = new Date();
    const dateStr = date.toISOString().slice(0, 10);
    const mapCallsWithInitials = [
      new CallHistoryModel(),
    ];

    const mapCallsWithoutInitials = [
      new CallHistoryModel(),
    ];
    
    // // to handle initials with firstname and lastname
    mapCallsWithInitials[0].srcFirstName = 'John';
    mapCallsWithInitials[0].srcLastName = 'Doe'; 
    mapCallsWithInitials[0].start = date;

    expect(callHistoryService.MapCallHistoryModelToCallHistoryViewModel(mapCallsWithInitials)[dateStr][0][0].initials).toBe('J D');

    // to handle initials without firstname and lastname
    mapCallsWithoutInitials[0].start = date;
    
    expect(callHistoryService.MapCallHistoryModelToCallHistoryViewModel(mapCallsWithoutInitials)[dateStr][0][0].initials).toBe('');

  });

  it('should return the correct call information', function() {
    const date = new Date();
    const dateStr = date.toISOString().slice(0, 10);
    const mapCalls = [
      genCallHistoryModel("answered", 1000, "John", "Doe", 8, "Available", "jdoe", "J D", date, "1234"),
      genCallHistoryModel("missed", 1000, "James", "Bond", 8, "Available", "jbond", "J B", date, "2345"),
      genCallHistoryModel("emmited", 1000, "Bruce", "Willis", 8, "Available", "Bwillis", "B W", date, "3456"),
      genCallHistoryModel("ongoing", 1000, "John", "Abruzzi", 8, "Available", "Jabruzzi", "J A", date, "4567"),
    ];

    const callsReturned = callHistoryService.MapCallHistoryModelToCallHistoryViewModel(mapCalls)[dateStr];
    const keys          = Object.keys(callsReturned);

    mapCalls.forEach((call, index) => {
      expect(haveSameData(call, callsReturned[keys[index]][0])).toBe(true);
    });
    
  });

});