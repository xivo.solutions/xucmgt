var webpackConfig = require('./webpack.config.js');
const path = require('path');
const os = require('os');

webpackConfig.mode = "development";
webpackConfig.module.rules.splice(1, 2);
webpackConfig.output.path = path.join(os.tmpdir(), '_karma_webpack_'),

module.exports = function (config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine', 'webpack'],
    reporters: ['progress'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['ChromeHeadless'],
    singleRun: false,
    autoWatchBatchDelay: 300,

    files: [
      './node_modules/jquery/dist/jquery.js',
      './node_modules/angular/angular.js',
      './node_modules/angular-mocks/angular-mocks.js',
      './test/karma/jasminecustommatchers.js',
      './test/karma/mockobjectsbuilder.js',
      './app/assets/javascripts/xccti/js/cti.js',
      './app/assets/javascripts/xccti/js/callback.js',
      './app/assets/javascripts/xccti/js/membership.js',
      './public/javascripts/SIPml-api.js',
      './app/assets/javascripts/xccti/js/xc_webrtc.js',
      './test/karma/bootstrap.js',
      './app/assets/javascripts/ccagent/app.js',
      './app/assets/javascripts/ccmanager/app.js',
      './app/assets/javascripts/ucassistant/app.js',
      { pattern: './public/**/*', watched: false, served: true, included: false },
      './app/assets/javascripts/**/*.html',
      { pattern: './test/karma/**/*.spec.js', watched: false },
      { pattern: './test/karma/**/*.spec.ts', watched: false },
    ],

    proxies: {
      "/assets/audio/pratix/": "/base/public/audio/pratix/",
      "/assets/i18n/": "/base/public/i18n/",
      "/assets/javascripts/ccagent/": "/base/app/assets/javascripts/ccagent/",
      "/assets/images/": "/base/public/images/"
    },

    preprocessors: {
      './app/assets/javascripts/**/*.html': ['ng-html2js'],
      './test/karma/**/*.spec.js': ['webpack'],
      './app/assets/javascripts/ccagent/app.js': ['webpack'],
      './app/assets/javascripts/ccmanager/app.js': ['webpack'],
      './app/assets/javascripts/ucassistant/app.js': ['webpack'],
      './test/karma/**/*.spec.ts': ['webpack'],
    },

    webpack: webpackConfig,

    webpackMiddleware: {
      logLevel: 'INFO'
    },

    ngHtml2JsPreprocessor: {
      stripPrefix: 'app/',
      moduleName: 'html-templates'
    },

  });
};
