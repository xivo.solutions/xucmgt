$(window).load(function() {
  const openLink = (protocol) => {
    window.open(protocol + ":" + $("#"+protocol).val());
  };

  const dialCti = (cti) => {
    let msg = JSON.parse('{"type":"PHONE_EVENT", "value":"'+$("#"+cti).val()+'"}');
    window.parent.postMessage(msg, '*');
  };

  $("#callto").val("*55");
  $("#tel").val("*55");
  $("#cti").val("*55");

  $("#extviewdial").on('click', function(e) {
    dialCti('cti');
  });

  $("#extviewcallto").on('click', function(e) {
    openLink('callto');
  });

  $("#extviewtel").on('click', function(e) {
    openLink('tel');
  });
});