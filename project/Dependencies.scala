import play.sbt.PlayImport.{ehcache, guice}
import sbt._

object Version {
  val htmlcompressor = "1.5.2"
  val scalatestplay  = "7.0.0"
  val mockito        = "5.11.0"
}

object Library {
  val htmlcompressor =
    "com.googlecode.htmlcompressor" % "htmlcompressor" % Version.htmlcompressor
  val scalatestplay =
    "org.scalatestplus.play" %% "scalatestplus-play" % Version.scalatestplay
  val mockito = "org.mockito" % "mockito-core" % Version.mockito
}

object Dependencies {

  import Library._

  val scalaVersion = "3.4.1"

  val resolutionRepos = Seq(
    "Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository",
    "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/"
  )

  val runDep: Seq[ModuleID] = run(
    htmlcompressor,
    guice,
    ehcache
  )

  val testDep: Seq[ModuleID] = test(
    scalatestplay,
    mockito
  )

  def run(deps: ModuleID*): Seq[ModuleID]  = deps
  def test(deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")

}
