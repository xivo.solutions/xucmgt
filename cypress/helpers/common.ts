import { Form } from "cypress-helper";

export class Common {
  userToken: string = "";

  login(username: string, password: string): void {
    cy.intercept("POST", "/xuc/api/2.0/auth/login").as("postLogin"); // and assign an alias
    const loginForm: Form = {
      fields: {
        username: { selector: "#input_0", value: username },
        password: { selector: "#input_1", value: password },
      },
      submitButton: "#loginbutton",
    };
    cy.fillForm(loginForm);
    cy.wait("@postLogin", { timeout: 3000 });
  }

  agentLogin(username: string, password: string, phoneNumber: number): void {
    const agentLoginForm: Form = {
      fields: {
        username: { selector: "#input_0", value: username },
        password: { selector: "#input_1", value: password },
        phoneNumber: { selector: "#input_2", value: phoneNumber },
      },
      submitButton: "#loginbutton",
    };
    cy.fillForm(agentLoginForm);
    cy.get('.phone', { timeout: 30000 });
  }

  initialSate() {
    cy.visit("/");

    this.login("apeuh", "1234");

    cy.url().should("include", "/ucassistant/favorites");
  }

  logOut() {
    cy.get("a.menu").click();
    cy.get("i.fa-logout-button").click();
  }

  expectPlayingAudio = (expectedValue: boolean) => {
    cy.get("audio,video").should((els: any) => {
      let audible = false;
      els.each((i: any, el: any) => {
        console.log(el);
        console.log(el.duration, el.paused, el.muted);
        if (el.duration > 0 && !el.paused && !el.muted) {
          audible = true;
        }

        // expect(el.duration > 0 && !el.paused && !el.muted).to.eq(false)
      });
      expect(audible).to.eq(expectedValue);
    });
  };

  call = (number: number | string) => {
    cy.search("#search", `${number}`);
    cy.get('[volume="inputVolume"]', { timeout: 30000 });
  };

  agentLogout = () => {
    cy.get('i.fa-chevron-down[ng-class="getAgentStateClassName(agentState)"]').click();
    cy.get('a[ng-click="switchState(item)"] i.fa-power-off').click();
  }
}

export const common = new Common();
