class ConfigMgtAPi {
  private apiUrl = Cypress.env("CONFIG_API_URL");
  private apiKey = Cypress.env("CONFIG_API_KEY");

  private request(method: string, endpoint: string, body?: any) {
    return cy.request({
      method,
      url: `${this.apiUrl}${endpoint}`,
      headers: {
        'accept': 'application/json',
        'X-Auth-Token': this.apiKey,
        'Content-Type': 'application/json'
      },
      body
    });
  }

  createGroup(group: Group) {
    return this.request('POST', '/groups', group)
      .then(response => {
        expect(response.status).to.eq(201);
        return response.body;
      });
  }

  getGroup(groupId: number) {
    return this.request('GET', `/groups/${groupId}`)
      .then(response => {
        expect(response.status).to.eq(200);
        return response.body;
      });
  }

  getAllGroups() {
    return this.request('GET', '/groups').then(response => {
      expect(response.status).to.eq(200);
      return response.body;
    });
  }

  updateGroup(group: Group, groupId: number) {
    return this.request('PUT', `/groups/${groupId}`, group)
      .then(response => {
        expect(response.status).to.eq(200);
      });
  }

  deleteGroup(groupId: number) {
    return this.request('DELETE', `/groups/${groupId}`)
      .then(response => {
        expect(response.status).to.eq(204);
      });
  }

  deleteGroupByName(groupName: string) {
    return this.getAllGroups().then(groups => {
      const group = groups.find(g => g.name === groupName);
      if (group) {
        return this.deleteGroup(group.id);
      }
    });
  }

  addUserInGroup(userId: number, groupId: number) {
    return this.request('POST', `/groups/${groupId}/members/${userId}`)
      .then(response => {
        expect(response.status).to.eq(201);
      });
  }

  removeUserInGroup(userId: number, groupId: number) {
    return this.request('DELETE', `/groups/${groupId}/members/${userId}`)
      .then(response => {
        expect(response.status).to.eq(204);
      });
  }

  getUserByUsername(username: string) {
    return this.request('GET', `/users/username/${username}`)
      .then(response => {
        expect(response.status).to.eq(200);
        return response.body;
      })
  }
}

export interface Group {
  id?: number,
  name?: string,
  number?: string
  context?: string,
  mds?: string
}

export const configMgtAPi = new ConfigMgtAPi();
