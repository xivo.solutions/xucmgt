import { common } from "@helpers/common";

const verifyAgentNumber = (number: string) => {
  cy.wait(2000);
  cy.url().should("include", "/main");
  cy.get(".phone").should("contain", number);
}
const agentSwitchInitialState = () => {
  cy.visit("/ccagent");

  common.agentLogin("pamploa", "1234", 1904);
  verifyAgentNumber("1904");
}

const switchModalShouldExist = () => {
  cy.get(".modal-dialog").should("exist");
  cy.get("#modal-footer-switch-number").should("exist");
  cy.get("#modal-footer-switch-number > button.btn-warning")
    .should("exist")
    .invoke("attr", "ng-click")
    .should("contain", "modalCtrl.cancel()");
  cy.get("#modal-footer-switch-number > button.btn-primary")
    .should("exist")
    .invoke("attr", "ng-click")
    .should("contain", "modalCtrl.validate()");
  cy.wait(1000);
};

const switchModalShouldNotExist = () => {
  cy.get(".modal-dialog").should("not.exist");
  cy.get("#modal-footer-switch-number").should("not.exist");
  cy.wait(1000);
};

const confirmSwitch = () => {
  cy.get("#modal-footer-switch-number > button.btn-primary").click();
  cy.wait(2000);
};

const cancelSwitch = () => {
  cy.get("#modal-footer-switch-number > button.btn-warning").click();
  cy.wait(2000);
};

describe("Agent switch phone number", () => {
  afterEach(() => {
    common.agentLogout();
  })

  it("should open agent switch number modal", () => {
    agentSwitchInitialState()

    cy.get(".switch-phone").click();
    switchModalShouldExist();

    cancelSwitch();
    switchModalShouldNotExist();
  });

  it("should switch number properly", () => {
    agentSwitchInitialState()

    cy.get(".switch-phone").click();
    switchModalShouldExist();
    confirmSwitch();

    cy.url().should("include", "ccagent#!/login?error=SwitchNumber");
    cy.get('input[ng-model="phoneNumber"][name="phoneNumber"][type="text"]')
      .clear()
      .type("1908");

    cy.get("#loginbutton").click();
    verifyAgentNumber("1908");
  })
});
