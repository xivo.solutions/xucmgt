import { common } from "@helpers/common";

describe("Auto Login process Test", () => {
  it("Should Login in CCAgent", () => {
    cy.visit("/ccagent");
    common.agentLogin("pamploa", "1234", 1904);
    cy.url().should("include", "/main");
    cy.reload();
    cy.url().should("include", "/main");
  });
});
