import { common } from "@helpers/common";

const initialSate = () => {
  cy.visit("/ccagent");
  common.agentLogin("pamploa", "1234", 1904);
};

describe("Menu navigation links should be present", () => {
  it("History", () => {
    initialSate();

    cy.get(".links .history-menu").then(($el) => {
      const afterElementBefore = window.getComputedStyle($el[0], "::after");
      const contentBefore = afterElementBefore.getPropertyValue("content");

      expect(contentBefore).to.not.equal("none");
      const urlBefore = contentBefore.replace(/^["'](.*)["']$/, "$1");
      expect(urlBefore).to.contain(
        "/assets/images/ccagent/menu/history_default.svg"
      );

      cy.wrap($el).click();
      cy.wait(2000);

      cy.wrap($el).then(($clickedEl) => {
        const afterElementAfter = window.getComputedStyle(
          $clickedEl[0],
          "::after"
        );
        const contentAfter = afterElementAfter.getPropertyValue("content");

        expect(contentAfter).to.not.equal("none");
        const urlAfter = contentAfter.replace(/^["'](.*)["']$/, "$1");

        expect(urlAfter).to.not.equal(urlBefore);

        expect(urlAfter).to.contain(
          "/assets/images/ccagent/menu/history_selected.svg"
        );
      });
    });
  });

  it("Activity", () => {
    initialSate();

    cy.get(".links .history-menu").click();
    cy.wait(2000);

    cy.get(".links .activities-menu").then(($el) => {
      const afterElementBefore = window.getComputedStyle($el[0], "::after");
      const contentBefore = afterElementBefore.getPropertyValue("content");

      expect(contentBefore).to.not.equal("none");
      const urlBefore = contentBefore.replace(/^["'](.*)["']$/, "$1");
      expect(urlBefore).to.contain(
        "/assets/images/ccagent/menu/activities_default.svg"
      );

      cy.wrap($el).click();
      cy.wait(2000);

      cy.wrap($el).then(($clickedEl) => {
        const afterElementAfter = window.getComputedStyle(
          $clickedEl[0],
          "::after"
        );
        const contentAfter = afterElementAfter.getPropertyValue("content");

        expect(contentAfter).to.not.equal("none");
        const urlAfter = contentAfter.replace(/^["'](.*)["']$/, "$1");

        expect(urlAfter).to.not.equal(urlBefore);

        expect(urlAfter).to.contain(
          "/assets/images/ccagent/menu/activities_selected.svg"
        );
      });
    });
  });

  it("Agents", () => {
    initialSate();

    cy.get(".links .history-menu").click();
    cy.wait(2000);

    cy.get(".links .agents-menu").then(($el) => {
      const afterElementBefore = window.getComputedStyle($el[0], "::after");
      const contentBefore = afterElementBefore.getPropertyValue("content");

      expect(contentBefore).to.not.equal("none");
      const urlBefore = contentBefore.replace(/^["'](.*)["']$/, "$1");
      expect(urlBefore).to.contain(
        "/assets/images/ccagent/menu/agents_default.svg"
      );

      cy.wrap($el).click();
      cy.wait(2000);

      cy.wrap($el).then(($clickedEl) => {
        const afterElementAfter = window.getComputedStyle(
          $clickedEl[0],
          "::after"
        );
        const contentAfter = afterElementAfter.getPropertyValue("content");

        expect(contentAfter).to.not.equal("none");
        const urlAfter = contentAfter.replace(/^["'](.*)["']$/, "$1");

        expect(urlAfter).to.not.equal(urlBefore);

        expect(urlAfter).to.contain(
          "/assets/images/ccagent/menu/agents_selected.svg"
        );
      });
    });
  });

  it("Callbacks", () => {
    initialSate();

    cy.get(".links .history-menu").click();
    cy.wait(2000);

    cy.get(".links .callbacks-menu").then(($el) => {
      const afterElementBefore = window.getComputedStyle($el[0], "::after");
      const contentBefore = afterElementBefore.getPropertyValue("content");

      expect(contentBefore).to.not.equal("none");
      const urlBefore = contentBefore.replace(/^["'](.*)["']$/, "$1");
      expect(urlBefore).to.contain(
        "/assets/images/ccagent/menu/callbacks_default.svg"
      );

      cy.wrap($el).click();
      cy.wait(2000);

      cy.wrap($el).then(($clickedEl) => {
        const afterElementAfter = window.getComputedStyle(
          $clickedEl[0],
          "::after"
        );
        const contentAfter = afterElementAfter.getPropertyValue("content");

        expect(contentAfter).to.not.equal("none");
        const urlAfter = contentAfter.replace(/^["'](.*)["']$/, "$1");

        expect(urlAfter).to.not.equal(urlBefore);

        expect(urlAfter).to.contain(
          "/assets/images/ccagent/menu/callbacks_selected.svg"
        );
      });
    });
  });

  it("Customer", () => {
    initialSate();

    cy.get(".links .history-menu").click();
    cy.wait(2000);

    cy.get(".links .customer-menu").then(($el) => {
      const afterElementBefore = window.getComputedStyle($el[0], "::after");
      const contentBefore = afterElementBefore.getPropertyValue("content");

      expect(contentBefore).to.not.equal("none");
      const urlBefore = contentBefore.replace(/^["'](.*)["']$/, "$1");
      expect(urlBefore).to.contain(
        "/assets/images/ccagent/menu/customer_default.svg"
      );

      cy.wrap($el).click();
      cy.wait(2000);

      cy.wrap($el).then(($clickedEl) => {
        const afterElementAfter = window.getComputedStyle(
          $clickedEl[0],
          "::after"
        );
        const contentAfter = afterElementAfter.getPropertyValue("content");

        expect(contentAfter).to.not.equal("none");
        const urlAfter = contentAfter.replace(/^["'](.*)["']$/, "$1");

        expect(urlAfter).to.not.equal(urlBefore);

        expect(urlAfter).to.contain(
          "/assets/images/ccagent/menu/customer_selected.svg"
        );
      });
    });
  });
});
