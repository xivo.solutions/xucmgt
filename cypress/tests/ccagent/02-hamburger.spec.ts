import { common } from "@helpers/common";

const initialSate = () => {
  cy.visit("/ccagent");
  common.agentLogin("pamploa", "1234", 1904);
};

describe("Hamburger menu should work", () => {
  it("switch from one menu to another and displays the correct content", () => {
    initialSate();

    cy.get(".config-menu > i").then(($el) => {

      cy.wrap($el).click();
      cy.get('#keybindings-header').should('have.class', 'selected');
      cy.get('#audio-header').should('not.have.class', 'selected');
      cy.get('.keybindings-options').should('not.have.class', 'ng-hide');
      cy.get('.audio-options').should('have.class', 'ng-hide');
      
      cy.get('#audio-header').click();
      cy.get('#audio-header').should('have.class', 'selected');
      cy.get('#keybindings-header').should('not.have.class', 'selected');
      cy.get('.keybindings-options').should('have.class', 'ng-hide');
      cy.get('.audio-options').should('not.have.class', 'ng-hide');
    });
  });
});
