import { Form } from "cypress-helper";
import { common } from "@helpers/common";

describe("Call History Test", () => {
  it("outgoing call should be listed in call history", () => {
    common.initialSate();
    common.call("*55");
    cy.expectPlayingAudio(true);
    cy.get(".call-hangup").click();
    cy.get(".xivo-icone-menu-historique").click();
    cy.get(
      "div.history-container:nth-child(1) .history-detail:nth-child(1) .history-phone span"
    ).should("have.attr", "title", "*55");
    cy.get("div.history-container:nth-child(1) .history-detail:first-child")
      .find("img")
      .should(
        "have.attr",
        "src",
        "/assets/images/ucassistant/call_statut_outgoing.svg"
    );
  });

  it("incoming call should be listed in call history", () => {
    cy.visit("");
    common.login("ccah", "1234");
    common.call(1908);
    cy.get('.call-row-wrapper', { timeout: 10000 }).should('not.exist');

    common.logOut();

    const loginForm: Form = {
      fields: {
        username: { selector: "#input_3", value: "apeuh" },
        password: { selector: "#input_4", value: "1234" },
      },
      submitButton: "#loginbutton",
    };
    cy.fillForm(loginForm);

    cy.get(".badge").should("contain", 1);
    cy.get(".xivo-icone-menu-historique").click();
    cy.get(
      "div.history-container:nth-child(1) .history-detail:nth-child(1) .history-phone span"
    ).should("have.attr", "title", "Cindy Cah");
    cy.get("div.history-container:nth-child(1) .history-detail:first-child")
      .find("img")
      .should(
        "have.attr",
        "src",
        "/assets/images/ucassistant/call_statut_missed.svg"
      );
  });
});
