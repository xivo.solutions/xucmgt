import { common } from "@helpers/common";

describe("Login process Test", () => {
  it("Should Login!", () => {
    common.initialSate();
  });
  it("Should Not Login, Wrong Passwod!", () => {
    cy.visit("/");
    common.login("apeuh", "titi");

    cy.get("#loginError").should("exist");
    cy.url().should("not.include", "/ucassistant/favorites");
  });
  it("Should Not Login, Wrong Login!", () => {
    cy.visit("/");
    common.login("TITI", "1234");

    cy.get("#loginError").should("exist");
    cy.url().should("not.include", "/ucassistant/favorites");
  });
  it("Should Not Login, No Login Password!", () => {
    cy.visit("/");
    cy.get("#loginbutton").click();

    cy.get("#loginError").should("exist");
    cy.url().should("not.include", "/ucassistant/favorites");
  });
});
