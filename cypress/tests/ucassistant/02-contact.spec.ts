import { common } from "@helpers/common";

describe("Contact interactions Test", () => {
  beforeEach(() => {
    common.initialSate();
  });

  it("Should search for a user!", () => {
    cy.search("#search", "user");

    cy.get(".user-line").should("have.length", 4);
    cy.get(".user-infos > .name-service-post-section > .name-surname")
      .first()
      .should("contain.text", "User 1");
    cy.get(".user-infos > .name-service-post-section > .name-surname")
      .last()
      .should("contain.text", "User 4");
  });

  it("Should Toggle the contact Detail", () => {
    cy.search("#search", "user");

    cy.get(".user-line").should("exist");
    cy.get(".user-line").first().click();
    cy.get(".contact-sheet-modal").first().should("exist");
    cy.wait(2000);
    cy.get(".fa.fa-times").first().click();
    cy.get(".contact-sheet-modal").should("not.exist");
  });

  it("should redirect to chat page", () => {
    let helloWorld = "hello world!";
    cy.search("#search", "user");
    setTimeout(() => {}, 4000);

    cy.get(".user-line").last().find(".comments.button").last().should("exist");
    cy.get(".comments.button").last().should("exist");
    cy.get(".user-infos > .name-service-post-section > .name-surname")
      .last()
      .should("contain.text", "User 4");
    cy.get(".user-line").last().find(".comments.button").last().click();

    cy.url().should("include", "/ucassistant/conversation");
    cy.get(".messages-header").should("contain.text", "User 4");

    cy.get(".chat-text-send").should("not.be.visible");
    cy.get(".chat-text").type(helloWorld).should("have.value", helloWorld);

    cy.get(".party-messages").then(($elemtents) => {
      console.log($elemtents.children().length);
      console.log($elemtents.children());
      if ($elemtents.children().length === 0) {
        cy.get(".party-message.me").should("not.exist");
        cy.get(".chat-text-send").click();
        cy.get(".party-message.me").should("have.length", 1);
      } else {
        cy.get(".party-message.me").then(($elems) => {
          let messegeNumber = $elems.length;
          cy.get(".chat-text-send").click();
          cy.get(".party-message.me").should("have.length", 1 + messegeNumber);
        });
      }
    });
  });

  it("should create and delete a personal contact", () => {
    cy.get("#menu").click();
    cy.get(".menu-item").should("exist");
    cy.get('li.uib-tab.nav-item[index="1"]').contains('Contacts').click()
    cy.get('[ui-sref="interface.personalContact"]').click()
    cy.get(".btn.btn-primary.pull-right.ng-binding")
      .should("exist")
      .should("be.disabled");
    cy.get("#input_3").should("exist");
    cy.get("#input_3").type("bilbo").should("have.value", "bilbo");
    cy.get(".btn.btn-primary.pull-right.ng-binding")
      .should("exist")
      .should("be.disabled");
    cy.get("#input_4").should("exist");
    cy.get("#input_4").type("4242").should("have.value", "4242");
    cy.get(".btn.btn-primary.pull-right.ng-binding")
      .should("exist")
      .should("not.be.disabled")
      .click();

    cy.search("#search", "bilbo");
    cy.get(".user-line").should("have.length", 1);
    cy.get(".xivo-modifier").click();
    cy.get(".favorites").click();
    cy.get(".btn.btn-primary.pull-right.ng-binding")
      .should("exist")
      .should("not.be.disabled")
      .click();

    cy.contains("bilbo").should("exist");
    cy.search("#search", "bilbo");
    cy.get(".user-line").should("have.length", 1);
    cy.get(".xivo-modifier").click();
    cy.get(".delete").click();
    cy.get('[ng-click="ctrl.remove()"]').click();
  });

  it("should make a call", () => {
    cy.search("#search", "user");
    cy.get(".user-line").last().find(".call.button").last().should("exist");
    cy.get(".call.button").last().should("exist");
    cy.get(".user-line")
      .get(".user-infos > .name-service-post-section > .name-surname")
      .last()
      .should("contain.text", "User 4");
    cy.get(".table-row-call.tr-call-hoverable").should("not.exist");
    cy.get(".user-line").last().find(".call.button").last().click();
    cy.wait(200);
    cy.get(".table-row-call.tr-call-hoverable").should("exist");
    cy.wait(500);
    cy.get(".call-hangup").click();
    cy.get(".table-row-call.tr-call-hoverable").should("not.exist");
    cy.get(".user-line").last().click();
    cy.get(".contact-sheet-modal").should("exist");
    cy.get("#call-contact-button").click();
    cy.get(".contact-sheet-modal").should("not.exist");
    cy.wait(200);
    cy.get(".table-row-call.tr-call-hoverable").should("exist");
    cy.wait(500);
    cy.get(".call-hangup").click();
    cy.get(".table-row-call.tr-call-hoverable").should("not.exist");
    cy.get(".user-line")
      .last()
      .find("#chat-contact-button")
      .last()
      .should("exist");
    cy.get(".user-line").last().find("#chat-contact-button").last().click();
    cy.get('[ng-click="callRemoteParty()"]').should("exist").click();
    cy.wait(200);
    cy.get(".table-row-call.tr-call-hoverable").should("exist");
    cy.wait(500);
    cy.get(".call-hangup").click();
    cy.get(".table-row-call.tr-call-hoverable").should("not.exist");
  });
});
