import { common } from "@helpers/common";

describe('Menu interactions Tests', () => {
    it('Can Select a Ringtone', () => {
        common.initialSate();
        cy.get('#menu').click()

        cy.url().should('include', '/ucassistant/menu');
        cy.get('.active > .nav-link').should('contain.text', 'Audio')

        cy.get('.menu-item[ui-sref="interface.ringtoneSelection"]').click()
        cy.url().should('include', '/ucassistant/menu/ringtone-selection');
        cy.get('.modal-dialog .modal-content .modal-header span').should('contain.text', 'Sonnerie')

        cy.get('#KeysofMoon-TheSuccess').find('.ringtone-listener-button').first().click()
        cy.get('#KeysofMoon-TheSuccess').find('.xivo.xivo-play').should('not.exist')
        cy.get('#KeysofMoon-TheSuccess').find('.xivo.xivo-pause-appel').should('exist')

        cy.get('#ringtone-listener').should('exist');
        common.expectPlayingAudio(true);

        cy.get('#KeysofMoon-TheSuccess').find('.ringtone-listener-button').first().click()
        cy.get('#KeysofMoon-TheSuccess').find('.xivo.xivo-pause-appel').should('not.exist')
        cy.get('#KeysofMoon-TheSuccess').find('.xivo.xivo-play').should('exist')
        common.expectPlayingAudio(false)

        cy.get('#KeysofMoon-TheSuccess').click()
        cy.get('#KeysofMoon-TheSuccess > .ringtone-selected-bloc > i').should('exist')

        cy.get('#Sonneriepardéfaut > .ringtone-selected-bloc > i').should('not.exist')

        cy.get('.close[ui-sref="interface.menu"]').click()
        cy.url().should('include', '/ucassistant/menu');


        cy.get('.menu-item[ui-sref="interface.ringtoneSelection"]').first().click()
        cy.url().should('include', '/ucassistant/menu/ringtone-selection');
        cy.get('.modal-dialog .modal-content .modal-header span').should('contain.text', 'Sonnerie')
        cy.get('#KeysofMoon-TheSuccess > .ringtone-selected-bloc > i').should('exist')
    })
    it('Can change config tab', () => {
        common.initialSate();
        cy.get('#menu').click()

        cy.url().should('include', '/ucassistant/menu');
        cy.get('.active > .nav-link').should('contain.text', 'Audio')
        cy.get('li.uib-tab.nav-item[index="1"]').contains('Contacts').click()
        cy.get('.active > .nav-link').should('contain.text', 'Contacts')
    })

})

