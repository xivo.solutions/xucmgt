import { common } from "@helpers/common";
import { configMgtAPi, Group } from "@helpers/configMgtApi";

const groupName = "cypress_group";
const updatedGroupName = "new_cypress_group";

beforeEach(() => {
  Cypress.config('defaultCommandTimeout', 20000);
})

after(() => {
  configMgtAPi.deleteGroupByName(groupName);
  configMgtAPi.deleteGroupByName(updatedGroupName);
});


describe("Show groups the user belong to", () => {
  it("user without groups", () => {
    cy.visit("/");
    common.login("ajavel", "1234");
    cy.get(
      "[ng-class=\"{menuselected : isActive('interface.groups')}\"] > .btn"
    ).should("not.exist");
  });

  it("user with groups", () => {
    cy.visit("/");
    common.login("slebrekin", "1234");
    let groupsMenuBtn =
      "[ng-class=\"{menuselected : isActive('interface.groups')}\"] > .btn";
    cy.get(groupsMenuBtn).should("exist").click();

    let userGroups = [
      {"name": "sales_team", "number": "2200"},
      {"name": "support_team", "number": "2100"},
      {"name": "admin_team", "number": "2300"},
    ];

    cy.get('.group-line > .group-infos').then($elements => {
      const groups = $elements.toArray().map(el => ({
        name: el.querySelector('.name-surname').textContent.trim(),
        number: el.querySelector('.business-unit').textContent.trim()
      }));

      expect(groups).to.deep.include.members(userGroups);
    });
  });
});

describe("UserGroups live update", () => {
  it("Update sidebar with group icon ", () => {
    cy.visit("/");
    common.login("ajavel", "1234");

    configMgtAPi.getUserByUsername("ajavel").then(user => {
      const group1: Group  = {
        number: "2111",
        name: groupName,
        mds: "default",
        context: "default"
      }

      configMgtAPi.createGroup(group1).then(group => {
        // no group
        let groupsMenuBtn =
          "[ng-class=\"{menuselected : isActive('interface.groups')}\"] > .btn";
        cy.get(
          groupsMenuBtn).should("not.exist");
        // Created
        configMgtAPi.addUserInGroup(user.id, group.id);

        cy.get(groupsMenuBtn).should("exist").click();
        // Updated
        const updatedGroup: Group = {
          name: updatedGroupName,
          number: "2222"
        }
        configMgtAPi.updateGroup(updatedGroup, group.id)

        cy.get('.group-line > .group-infos').each(($el, index) => {
          cy.wrap($el).find('.name-surname').should('contain', updatedGroup.name);
          cy.wrap($el).find('.business-unit').should('contain', updatedGroup.number);
        });

        // Deleted when user removed from the group
        configMgtAPi.removeUserInGroup(user.id, group.id);

        cy.get(groupsMenuBtn).should("not.exist");

        // Deleted when the group is deleted
        configMgtAPi.addUserInGroup(user.id, group.id);
        cy.get(groupsMenuBtn).should("exist").click();
        configMgtAPi.deleteGroup(group.id);
        cy.get(groupsMenuBtn).should("not.exist");
      });
    });
  });
});

describe("UserGroups pause actions", () => {

  let groupsMenuBtn =
  "[ng-class=\"{menuselected : isActive('interface.groups')}\"] > .btn";
  let pauseActionBtn = ".group-line > .call-buttons > .group-pause-action";
  let unpauseActionBtn = ".group-line > .call-buttons > .group-unpause-action";
  let groupStatus = ".group-infos > .avatar-status";

  it("should show pause buttons according to config", () => {
    cy.interceptAndModifyRequest('GET', '/config/ucShowGroupControl', 200, { value: "false" }, 'ucShowGroupControl');
    cy.visit("/");
    common.login("slebrekin", "1234");

    cy.get(groupsMenuBtn).should("exist").click();
    cy.get(pauseActionBtn).should("not.exist");
  });

  it("shoud not show pause buttons according to config", () => {
    cy.interceptAndModifyRequest('GET', '/config/ucShowGroupControl', 200, { value: "true" }, 'ucShowGroupControl');
    cy.visit("/");
    common.login("slebrekin", "1234");

    cy.get(groupsMenuBtn).should("exist").click();
    cy.get(pauseActionBtn).should("exist");
  });

  it("should pause / unpause in group properly", () => {
    cy.interceptAndModifyRequest('GET', '/config/ucShowGroupControl', 200, { value: "true" }, 'ucShowGroupControl');
    cy.visit("/");
    common.login("ddrate", "1234");

    cy.get(groupsMenuBtn).should("exist").click();
    cy.get(groupStatus).then($el => {
      cy.log("unpause if group is paused");
      if ($el.attr("title") === "Unavailable") {
        cy.get(unpauseActionBtn).should("exist").click();
        cy.log("Wait 2s to prevent debounce to cancel the next click")
        cy.wait(2000);
      }
    });

    cy.get(unpauseActionBtn).should("not.exist");
    cy.get(groupStatus).should("have.attr", "title", "Available");
    cy.get(pauseActionBtn).should("exist").click();

    cy.log("Wait 2s to prevent debounce to cancel the next click")
    cy.wait(2000);
    cy.get(pauseActionBtn).should("not.exist");
    cy.get(groupStatus).should("have.attr", "title", "Unavailable");
    cy.get(unpauseActionBtn).should("exist").click();

    cy.get(pauseActionBtn).should("exist");
    cy.get(groupStatus).should("have.attr", "title", "Available");
  });
});
