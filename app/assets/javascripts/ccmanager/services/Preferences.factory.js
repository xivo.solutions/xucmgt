import _ from 'lodash';

export default function Preferences(localStorageService, $rootScope, XucQueue, XucQueueRecording, $log) {

  var prefKey = 'preferences.';
  var QueueSelectedKey = 'queueSelected';
  var QueueViewColumnsKey = 'queueViewColumns';
  var ThresholdsKey = 'thresholds';
  var IntroHelperKey = 'introHelper';
  var _ThresholdsChanged = prefKey + ThresholdsKey;
  var _QueueViewColumnsChanged = prefKey + QueueViewColumnsKey;

  var queueIds = [];
  var introHelpers = {};
  var viewOptions = [];
  var recordingOptions = [];
  var _CheckboxPersistedTypeOption = 'checkboxPersisted';
  var _CheckboxFreeTypeOption = 'checkboxFree';

  var _init = function() {
    queueIds = localStorageService.get(QueueSelectedKey) || [];
    introHelpers = localStorageService.get(IntroHelperKey) || {};
  };

  var _reset = function() {
    queueIds = [];
  };

  var _getSelectedQueues = function() {
    queueIds = localStorageService.get(QueueSelectedKey) || [];
    return queueIds;
  };

  var _setQueueVisibility = function(queueName,visibility) {
    var queueId=-1;
    _.each(XucQueue.getQueues(),function(queue) {
      if(queue.name.toLowerCase()==queueName.toLowerCase()){
        queueId = queue.id;
      }
    });

    if(visibility){
      if (queueIds.indexOf(queueId) ==-1) {
        queueIds.push(queueId);
      }
    }else{
      if (queueIds.indexOf(queueId) >-1) {
        queueIds.splice(queueIds.indexOf(queueId), 1);
      }
    }
    localStorageService.set(QueueSelectedKey, queueIds);
  };

  var _setQueuesVisibility = function(queueNames,visibility) {
    _reset();
    _.each(queueNames.split(','),function(v){
      _setQueueVisibility(v,visibility);
    });
  };

  var _toggleQueueSelection = function(queueId) {
    if (queueIds.indexOf(queueId) >= 0) {
      queueIds.splice(queueIds.indexOf(queueId), 1);
    } else {
      queueIds.push(queueId);
    }
    localStorageService.set(QueueSelectedKey, queueIds);
  };

  var _isIntroHelperShown = function(introId) {
    if(typeof(introHelpers[introId]) !== "undefined") {
      return introHelpers[introId];
    } else {
      return false;
    }
  };

  var _setIntroHelperShown = function(introId) {
    introHelpers[introId] = true;
    localStorageService.set(IntroHelperKey, introHelpers);
  };

  var _isQueueSelected = function(queueId) {
    return queueIds.indexOf(queueId) >= 0;
  };

  var _noQueueSelected = function() {
    return queueIds.length === 0;
  };

  var _getViewColumns = function(availColumns, key, firstCols) {
    var updatedCols = [];
    var aCols = angular.copy(availColumns);
    var savedCols = localStorageService.get(key) || [];

    angular.forEach(savedCols, function(sCol) {
      angular.forEach(aCols, function(aCol, key) {
        if (aCol.id === sCol.id) {
          aCol.show = sCol.show;
          updatedCols.push(aCols.splice(key, 1)[0]);
        }
      });
    });
    var calculatedCols = updatedCols.concat(aCols);
    if(firstCols) {
      var _removeFirsts = function(col) {
        _.remove(calculatedCols, function(cCol){
          return cCol.id === col.id;
        });
      };
      _.map(firstCols, _removeFirsts);
      calculatedCols = firstCols.concat(calculatedCols);
    }
    return calculatedCols;
  };

  var _saveViewColumns = function(cols, key) {
    localStorageService.set(key, cols);
  };

  var _moveCol = function(fromCol, targetCol, tableCol, colPrefKey) {
    var fromIdx = -1;
    var targetIdx = -1;

    var _moveToLeft = function(fromIdx, column, targetCol) {
      var colsToSave = [];
      angular.forEach(tableCol, function(col) {
        if (col.id === targetCol) {
          colsToSave.push(tableCol[fromIdx]);
        }
        if (col.id !== column) {
          colsToSave.push(col);
        }
      });
      _saveViewColumns(colsToSave, colPrefKey);
    };
    var _moveToRight = function(fromIdx, column, targetCol) {
      var colsToSave = [];
      angular.forEach(tableCol, function(col) {
        if (col.id !== column) {
          colsToSave.push(col);
        }
        if (col.id === targetCol) {
          colsToSave.push(tableCol[fromIdx]);
        }
      });
      _saveViewColumns(colsToSave, colPrefKey);
    };
    angular.forEach(tableCol, function(col, key) {
      if (col.id === fromCol)
        fromIdx = key;
      if (col.id === targetCol)
        targetIdx = key;
    });
    if (targetIdx >= 0 && fromIdx >= 0) {
      if (fromIdx > targetIdx)
        _moveToLeft(fromIdx, fromCol, targetCol);
      else
        _moveToRight(fromIdx, fromCol, targetCol);
    }
  };
  var _getOption = function(key, defaultValue) {
    var savedOption = localStorageService.get(key) || defaultValue;
    return savedOption;
  };
  var _getBoolOption = function (key, defaultValue) {
    var boolValue = localStorageService.get(key);
    if (_.isNil(boolValue)) return defaultValue;
    return boolValue == 'true' || boolValue ? true : false;
  };

  var _setOption = function(key, value) {
    localStorageService.set(key, value);
  };

  $rootScope.$on('LocalStorageModule.notification.setitem', function(event, args) {
    $log.debug('broadcast', args.key);
    $rootScope.$broadcast('preferences.' + args.key);
  });

  var _saveThreshold = function(counter, low, high) {
    var trh = localStorageService.get(ThresholdsKey) || {};
    trh[counter] = {low:low, high:high};
    _setOption(ThresholdsKey,trh);
  };

  var _getThreshold = function(counter, low, high) {
    var thr = {};
    thr[counter] = {low:low, high:high};
    var s =  _getOption(ThresholdsKey, thr);
    return s[counter] || thr[counter];
  };

  var _getThresholds = function(defaultThrs) {
    var savedThrs =  _getOption(ThresholdsKey, defaultThrs);

    for (var key in savedThrs) {
      defaultThrs[key] = savedThrs[key];
    }
    return defaultThrs;
  };

  var _addOption = function(scope, key, type, defaultValue, intermediateValue, optionList) {
    let val = (type == _CheckboxPersistedTypeOption) ? _getBoolOption(key, defaultValue) : defaultValue;

    var option = {
      "key": key,
      "type": type,
      "value": val,
      "intermediate": intermediateValue
    };
    _.remove(optionList, function(o) { return o.key === key; });
    optionList.push(option);
    return option;
  };

  var _addViewOption = function(scope, key, type, defaultValue) {
    return _addOption(scope, key, type, defaultValue, false, viewOptions);
  };

  var _addRecordingOption = function(scope, key, type, value) {
    let expectedValues = [];

    switch(value) {
    case XucQueueRecording.ALL_RECORDED: expectedValues = [true, false];
      break;
    case XucQueueRecording.NONE_RECORDED: expectedValues = [false, false];
      break;
    case XucQueueRecording.SOME_RECORDED: expectedValues = [false, true];
      break;
    }

    return _addOption(scope, key, type, expectedValues[0], expectedValues[1], recordingOptions);
  };

  var _setRecordingOptionValue = function(option, value) {
    if (!option) return;

    option.intermediate = false;

    switch(value) {
    case XucQueueRecording.ALL_RECORDED: option.value = true;
      break;
    case XucQueueRecording.NONE_RECORDED: option.value = false;
      break;
    case XucQueueRecording.SOME_RECORDED: option.intermediate = true;
      break;
    }
  };

  var _clearOptions = function(){
    if (viewOptions.length > 0) viewOptions.length = 0;
    if (recordingOptions.length > 0) recordingOptions.length = 0;
  };

  var _getViewOptions = function() {
    return viewOptions;
  };

  var _getRecordingOptions = function() {
    return recordingOptions;
  };

  return {
    QueueViewColumnsKey : QueueViewColumnsKey,
    ThresholdsChanged : _ThresholdsChanged,
    QueueViewColumnsChanged : _QueueViewColumnsChanged,
    init    : _init,
    reset   : _reset,
    getSelectedQueues       : _getSelectedQueues,
    toggleQueueSelection    : _toggleQueueSelection,
    setQueueVisibility      : _setQueueVisibility,
    setQueuesVisibility     : _setQueuesVisibility,
    isQueueSelected : _isQueueSelected,
    noQueueSelected : _noQueueSelected,
    saveViewColumns : _saveViewColumns,
    getViewColumns : _getViewColumns,
    getOption : _getOption,
    getBoolOption : _getBoolOption,
    setOption : _setOption,
    moveCol : _moveCol,
    saveThreshold: _saveThreshold,
    getThreshold: _getThreshold,
    getThresholds: _getThresholds,
    isIntroHelperShown: _isIntroHelperShown,
    setIntroHelperShown: _setIntroHelperShown,
    addViewOption: _addViewOption,
    addRecordingOption: _addRecordingOption,
    setRecordingOptionValue: _setRecordingOptionValue,
    clearOptions: _clearOptions,
    getViewOptions: _getViewOptions,
    getRecordingOptions: _getRecordingOptions,
    CheckboxPersistedTypeOption: _CheckboxPersistedTypeOption,
    CheckboxFreeTypeOption: _CheckboxFreeTypeOption
  };
}