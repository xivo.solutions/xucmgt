(function(){
  'use strict';
  
  angular
    .module('ccManager')
    .factory('MetricsUtils',MetricsUtils);
  
  MetricsUtils.$inject= ["$filter"];
  
  function MetricsUtils($filter) {

    const schemaOrdering = ["topLeft", "topRight", "bottomLeft", "bottomRight"];
    
    const getMetricsWithInfos = (simpleMetrics, data) => {
      let metricsWithInfos = {};
      if (simpleMetrics && simpleMetrics.length > 0) {
        simpleMetrics.forEach((metric, index) => processMetric(metric, index, metricsWithInfos, data));
      }

      return metricsWithInfos;
    };

    const processMetric = (metric, index, metricsWithInfos, data) => {
      let [metricText, metricFormat] = getMetricInfos(metric);
      metricsWithInfos[schemaOrdering[index]] = {
        title: metric,
        data: data[metric] | "-",
        text: metricText,
        format: metricFormat
      };
    };

    const getMetricInfos = (metric) => {
      switch(metric) {

      // Provided by xuc
      case "TotalNumberCallsEntered":
        return ["Appels T", noFormat];
      case "TotalNumberCallsAnswered":
        return ["Rep T", noFormat];
      case "TotalNumberCallsAbandonned":
        return ["Aba T", noFormat];
      case "TotalNumberCallsClosed":
        return ["Fer T", noFormat];
      case "TotalNumberCallsTimeout":
        return ["Exp T", noFormat];
  
      case "PercentageAbandonnedTotal":
        return ["Aba T", percentFormatter];  
      case "TotalNumberCallsAbandonnedAfter15":
        return ["Aba +15s", noFormat];
      case "PercentageAbandonnedAfter15":
        return ["Aba +15s", percentFormatter];

      case "PercentageAnsweredTotal":
        return ["Rep T", percentFormatter];
      case "TotalNumberCallsAnsweredBefore15":
        return ["Rep -15s", noFormat];
      case "PercentageAnsweredBefore15":
        return ["Rep -15s", percentFormatter];

      case "AvailableAgents":
        return ["Dispo", noFormat];
      case "WaitingCalls":
        return  ["Att", noFormat];
      case "TalkingAgents":
        return ["En appel", noFormat];
      case "LongestWaitTime":
        return ["Att Max", noFormat];
      case "EWT":
        return ["Att Est", noFormat];


      default:
        return [];
      }
    };

    const percentFormatter = function(counter) {
      if (counter) {
        return $filter('number')(counter,1) + " %";
      }
      else {
        return "- %";
      }
    };
    const noFormat = function(counter){return (counter || "0");};
    
    return {
      getMetricsWithInfos: getMetricsWithInfos,
      getMetricInfos: getMetricInfos,
      processMetric: processMetric
    };
  }
  
})();
