import $ from 'jquery';

(function(){
  'use strict';

  angular
    .module('ccManager')
    .factory('Thresholds',Thresholds);

  Thresholds.$inject= ['Preferences', '$log', '$rootScope'];

  function Thresholds(preferences, $log, $rootScope) {
    var _defaultThresholds = {};
    _defaultThresholds.WaitingCalls = {low:2, high:5};
    _defaultThresholds.LongestWaitTime = {low:30, high:60};

    var _currentThresholds = {};

    var _getCurrentTresholds = function() {
      return _currentThresholds;
    };

    var _init = function() {
      _currentThresholds = preferences.getThresholds(_defaultThresholds);
    };

    var _loadThreshold = function(counter) {
      return preferences.getThreshold(counter, _defaultThresholds[counter].low, _defaultThresholds[counter].high);
    };

    var _saveThreshold = function(counter, low, high) {
      preferences.saveThreshold(counter, low, high);
    };

    var _loadThresholds = function() {
      return preferences.getThresholds(_defaultThresholds);
    };

    var _getThresholdDefs = function() {
      var defs = [];
      for (var counter in _defaultThresholds) {
        if (Object.prototype.hasOwnProperty.call(_defaultThresholds, counter)) {
          defs.push(counter);
        }
      }
      return defs;
    };

    var _thresholdClass = function(field, value) {
      if (_currentThresholds[field] && $.isNumeric(value)) {
        if (value < _currentThresholds[field].low) {
          return "lowThreshold";
        }
        else if(value < _currentThresholds[field].high)  return "middleThreshold";
        else return "highThreshold";
      }
      else return('');
    };

    $rootScope.$on(preferences.ThresholdsChanged, function() {
      $log.debug('Thresholds: ' + preferences.ThresholdsChanged);
      _currentThresholds = _loadThresholds();
    });

    return {
      init: _init,
      defaultThresholds: _defaultThresholds,
      getThresholdDefs: _getThresholdDefs,
      loadThreshold : _loadThreshold,
      saveThreshold : _saveThreshold,
      loadThresholds : _loadThresholds,
      thresholdClass : _thresholdClass,
      getCurrentThresholds : _getCurrentTresholds
    };
  }

})();
