export default class ccViewGlobalController {

  constructor($scope, $http, $log, Preferences, XucQueueRecording) {
    this.preferences = Preferences;
    this.$scope = $scope;

    $scope.option = {
      compactView: this.preferences.addViewOption($scope,"VIEW_OPTION_COMPACT", this.preferences.CheckboxPersistedTypeOption, false),
      showAll: this.preferences.addViewOption($scope,"VIEW_OPTION_SHOWALL", this.preferences.CheckboxPersistedTypeOption, true),
    };
    XucQueueRecording.getState().then(response => {
      $scope.option.recordingQueue = this.preferences.addRecordingOption($scope,"SWITCH_QUEUES_RECORDING",
        this.preferences.CheckboxFreeTypeOption, response);
    }, (error) => {
      XucQueueRecording.handleError(error);
    });

    XucQueueRecording.subscribeToQueueConfig(this.$scope, this.onQueueConfigUpdate.bind(this));

    $scope.hasError = () => {
      return XucQueueRecording.hasConfigManagerError();
    };

    $scope.$watch('option.showAll.value', function(newVal){
      $scope.toggleShowAllAgents(newVal);
    });
  }

  onQueueConfigUpdate(value) {
    this.preferences.setRecordingOptionValue(this.$scope.option.recordingQueue, value);
  }
}
