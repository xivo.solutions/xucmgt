import _ from 'lodash';

(function() {
  'use strict';

  angular.module('ccManager').controller('agentsUpdateController', agentsUpdateController);

  agentsUpdateController.$inject = ['$scope', '$log', '$uibModal', '$controller', 'XucQueue'];

  function agentsUpdateController($scope, $log, $uibModal, $controller, XucQueue) {
    angular.extend(agentsUpdateController, $controller('agentSelectController', {$scope: $scope}));

    function _getSelectedAgents() {
      var sAgents  = _($scope.displayedData).filter(function(ag) {return ag.selected; }).value();
      return sAgents;
    }

    function _getQueuesFromSelectedAgents() {
      var sAgents  = _getSelectedAgents();
      var queues = [];
      _.forEach(sAgents, function(a) {
        _.forEach(a.queueMembers, function (p, qid) {
          if (!_.isUndefined(p)) {
            var existing = _.find(queues, {'id': qid});
            if(_.isUndefined(existing)) {
              var q = _.clone(XucQueue.getQueue(qid));
              q.penalty = p;
              queues.push(q);
            } else {
              if(existing.penalty > p) {
                _.remove(queues, {'id': qid});
                existing.penalty = p;
                queues.push(existing);
              }
            }
          }
        });
      });
      return queues;
    }

    $scope.updateAgents = function() {
      $uibModal.open({
        templateUrl: 'agentsUpdateModalContent.html',
        controller: 'agentsUpdateModalController',
        resolve: {
          selectedAgents: _getSelectedAgents
        }
      }).result.catch(angular.noop);
    };

    $scope.createBaseConfiguration = function() {
      $uibModal.open({
        templateUrl: 'assets/javascripts/ccmanager/controllers/agentsCreateDefaultConfiguration.html',
        controller: 'agentsCreateDefaultConfigurationController',
        resolve: {
          selectedAgents: _getSelectedAgents,
          selectedQueues: function() { return []; }
        }
      });
    };

    $scope.createBaseFromCurrent = function() {
      $uibModal.open({
        templateUrl: 'assets/javascripts/ccmanager/controllers/agentsCreateDefaultConfiguration.html',
        controller: 'agentsCreateDefaultConfigurationController',
        resolve: {
          selectedAgents: _getSelectedAgents,
          selectedQueues: _getQueuesFromSelectedAgents
        }
      });
    };
  }

  angular.module('ccManager').controller('agentsUpdateModalController', agentsUpdateModalController);
  agentsUpdateModalController.$inject = ['$scope', '$uibModalInstance', 'XucQueue', 'AuQueueTable', 'XucAgent', 'selectedAgents', 'XucMembership'];

  function agentsUpdateModalController($scope, $uibModalInstance, xucQueue, auQueueTable, xucAgent, selectedAgents, xucMembership) {

    $scope.selectedAgents = selectedAgents;

    $scope.newQueue = {
      queue: null,
      penalty: 0
    };

    $scope.queuesToAdd = [];
    $scope.queuesToRemove = [];
    $scope.auQueues = [];

    $scope.initModal = function() {
      _.map(xucQueue.getQueues(), function(queue){
        $scope.auQueues.push(queue);
      });
    };

    $scope.queuesToAddTable = auQueueTable.builTable($scope.queuesToAdd, { 'queue.displayName' : 'asc'});
    $scope.queuesToRemoveTable = auQueueTable.builTable($scope.queuesToRemove, { 'displayName' : 'asc'});

    $scope.addQueue = function() {
      $scope.queuesToAdd.push({queue:$scope.newQueue.queue,penalty: $scope.newQueue.penalty});
      _onQueueAdded($scope.newQueue.queue);
      $scope.queuesToAddTable.reload();
    };

    $scope.removeQueueToAdd = function(queueDef) {
      _.remove($scope.queuesToAdd, function (queueWithPenalty) {
        return queueWithPenalty.queue.id === queueDef.queue.id;
      });
      $scope.auQueues.push(queueDef.queue);
      $scope.queuesToAddTable.reload();
    };

    $scope.removeQueueToRemove = function(queue) {
      _.remove($scope.queuesToRemove, function (q) {
        return q.id === queue.id;
      });
      $scope.queuesToRemoveTable.reload();
      $scope.auQueues.push(queue);
    };

    $scope.addQueueToRemove = function() {
      $scope.queuesToRemove.push($scope.newQueue.queue);
      _onQueueAdded($scope.newQueue.queue);
      $scope.queuesToRemoveTable.reload();
    };

    $scope.cancel = function () {
      $scope.queuesToAdd = [];
      $scope.queuesToRemove = [];
      $uibModalInstance.dismiss('cancel');
    };

    $scope.ok = function () {
      xucAgent.addAgentsToQueues($scope.selectedAgents, $scope.queuesToAdd);
      xucAgent.removeAgentsFromQueues($scope.selectedAgents, $scope.queuesToRemove);
      $scope.queuesToAdd = [];
      $scope.queuesToRemove = [];
      $uibModalInstance.dismiss('ok');
    };

    $scope.applyUsersDefaultMembership = function() {
      var userIds = _.map($scope.selectedAgents, 'userId');
      xucMembership.applyUsersDefaultMembership(userIds);
      $uibModalInstance.dismiss('ok');
    };

    $uibModalInstance.opened.then($scope.initModal);

    $scope.validatePenalty = function(queue) {
      if (typeof queue.penalty === 'undefined') {
        queue.penalty = 0;
      }
    };

    var _onQueueAdded = function() {
      _.remove($scope.auQueues, function (queue) {
        return queue.id === $scope.newQueue.queue.id;
      });
      $scope.newQueue.penalty = 0;
      $scope.newQueue.queue = null;
    };
  }


  angular
    .module('ccManager')
    .factory('AuQueueTable',AuQueueTable);

  AuQueueTable.$inject= ['NgTableParams', 'XucTableHelper'];

  function AuQueueTable(NgTableParams, XucTableHelper){

    var _buildTable = function(data, sortOrder) {
      return new NgTableParams({
        page: 1,
        count: 5,
        sorting: sortOrder
      },XucTableHelper.createSettings(data));
    };

    return {
      builTable : _buildTable
    };
  }
})();
