(function() {
  'use strict';
  angular.module('ccManager').controller('LoginController', ['$window', '$scope', '$state', '$stateParams',
    function($window, $scope, $state, $stateParams) {

      $scope.error = $stateParams.error;

      $scope.onLogin = function() {
        $state.go('interface');
      };
      $scope.hostAndPort = $window.externalConfig.internalHostAndPort;
      $scope.useSso = $window.externalConfig.useSso;
      $scope.casServerUrl = $window.externalConfig.casServerUrl;
      $scope.casLogoutEnable = $window.externalConfig.casLogoutEnable;
      $scope.openidServerUrl = $window.externalConfig.openidServerUrl;
      $scope.openidClientId = $window.externalConfig.openidClientId;
      $scope.openidLogoutEnable = $window.externalConfig.openidLogoutEnable;
    }
  ]);
})();
