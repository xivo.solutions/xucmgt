import _ from 'lodash';

(function(){
  'use strict';

  angular
    .module('ccManager')
    .controller('queueSelectCtrl',queueSelectCtrl);

  queueSelectCtrl.$inject= ['$scope','XucQueue','Preferences', '$translate'];

  function queueSelectCtrl($scope, XucQueue, preferences, $translate) {
    $scope.multiLang = {
      selectAll       : $translate.instant('MS_CHECKALL'),
      selectNone      : $translate.instant('MS_UNCHECKALL'),
      reset           : $translate.instant('BTN_CANCEL'),
      search          : $translate.instant('MS_SEARCHPLACEHOLDER'),
      nothingSelected : $translate.instant('MS_NOSELECTION')
    };

    $scope.queues = [];
    $scope.helperVisible = !preferences.isIntroHelperShown('queueselection');

    var _loadQueues = function() {

      var sortedQueues = _.sortBy(XucQueue.getQueues(),  function(o) { return o.number; });

      $scope.queues = _.map(sortedQueues, function(q) {
        q.ticked = preferences.isQueueSelected(q.id);
        return q;
      });

    };

    _loadQueues();

    $scope.toggleQueue = function(q) {
      preferences.toggleQueueSelection(q.id);
    };

    $scope.updateSelection = function() {

      _.forEach($scope.queues, function(q) {
        if(q.ticked != preferences.isQueueSelected(q.id)) {
          preferences.toggleQueueSelection(q.id);
        }
      });
    };

    var noQueueLoaded = function() {
      return $scope.queues.length === 0;
    };

    $scope.hideHelper = function() {
      $scope.helperVisible = false;
      preferences.setIntroHelperShown('queueselection');
    };

    $scope.$on('QueuesLoaded', function() {
      _loadQueues();
      $scope.status.disabled = noQueueLoaded();
    });

    $scope.status = {
      disabled: (noQueueLoaded())
    };
  }
})();
