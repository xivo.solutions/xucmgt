import $ from 'jquery';

(function(){
  'use strict';

  angular
    .module('ccManager')
    .controller('ccCallbackController',ccCallbackController);

  ccCallbackController.$inject= ['$scope','XucCallback', 'Preferences', '$log', '$window', '$filter', "$location", "$anchorScroll", "XucLink"];

  function ccCallbackController($scope, xucCallback, Preferences, $log, $window, $filter, $location, $anchorScroll, XucLink) {
    $anchorScroll.yOffset = 70;
    $scope.callbackLists = [];

    $scope.getServerBaseUrl = function(hostAndPort) {
      return $filter('prepareServerUrl')($window.location.protocol, hostAndPort, 'http');
    };

    $scope.refreshCallbacks = function() {
      $log.debug('refreshCallbacks');
      delete $scope.oldestCallback;
      $scope.callbacksCount = 0;
      $scope.callbackLists = xucCallback.getCallbackLists().filter((list) => {
        return Preferences.isQueueSelected(list.queueId);
      });
      $scope.callbackLists.forEach((list) => {
        list.callbacks.forEach((c) => {
          $scope.callbacksCount++;
          if (!$scope.oldestCallback || c.dueDate < $scope.oldestCallback.dueDate) {
            $scope.oldestCallback = c;
          }
        });
      });
    };

    $scope.refreshCallbacksAndApply = function() {
      $scope.refreshCallbacks();
    };


    $scope._getCharset = function(){
      let charset = 'utf-8';
      if(window.navigator.platform === 'Win32') {
        charset = 'iso-8859-1';
      }
      return charset;
    };

    $scope.uploadCallBack = function (listUuid) {
      XucLink.whenLogged().then((user) => {
        $.ajax({
          type: "POST",
          contentType: "text/plain;charset=" + $scope._getCharset(),
          data: $('#import-' + listUuid)[0].files[0],
          url: $scope.getServerBaseUrl($window.externalConfig.internalHostAndPort) + '/xuc/api/1.0/callback_lists/' + listUuid + '/callback_requests/csv',
          headers: {'Authorization': 'Bearer ' + user.token},
          timeout: 5000,
          processData: false,
          error: function (e) {
            console.log("Error uploading call back list !!!!!!!");
            console.log(e);
          },
          success: function () {
            xucCallback.refreshCallbacks();
          }
        });
      }
      );
    };

    $scope.exportCallBack = function(listUuid) {
      XucLink.whenLogged().then((user) => {
        $.ajax({
          type: "GET",
          url: $scope.getServerBaseUrl($window.externalConfig.internalHostAndPort) + '/xuc/api/1.0/callback_lists/' + listUuid + '/callback_tickets/csv',
          headers: { 'Authorization': 'Bearer ' + user.token },
          error: function(e) {
            console.log("Error exporting call back list !!!!!!!");
            console.log(e);
          },
          // eslint-disable-next-line no-unused-vars
          success: function(data, textStatus, jqXHR) {
            console.log("Callback export successful");

            const url = window.URL.createObjectURL(new Blob([data], { type: 'text/csv' }));
            const a = document.createElement('a');
            a.href = url;
            a.download = listUuid + '.csv';

            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);

            xucCallback.refreshCallbacks();
          }
        });
      });
    };


    $scope.gotoAnchor = function(id) {
      $anchorScroll('import-'+id);
    };

    $scope.$on('CallbacksLoaded', $scope.refreshCallbacksAndApply);
    $scope.$on('CallbackTaken', $scope.refreshCallbacksAndApply);
    $scope.$on('CallbackReleased', $scope.refreshCallbacksAndApply);
    $scope.$on('CallbackClotured', $scope.refreshCallbacksAndApply);
    $scope.$on('preferences.queueSelected', $scope.refreshCallbacksAndApply);

    xucCallback.refreshCallbacks();
  }

})();
