(function(){
  'use strict';

  angular
    .module('ccManager')
    .controller('ccChooseAgentCtrl',ccChooseAgentCtrl);

  ccChooseAgentCtrl.$inject= ['$scope','XucAgent','$uibModal'];

  function ccChooseAgentCtrl($scope, XucAgent, $uibModal) {

    var ModalInstanceCtrl = function ($scope, $uibModalInstance, agents) {

      $scope.agents = agents;
      $scope.orderList = "lastName";
      $scope.selected = {
        agent: $scope.agents[0]
      };

      $scope.ok = function () {
        $uibModalInstance.close($scope.selected.agent);
      };
      $scope.selected = function (agent) {
        $uibModalInstance.close(agent);
      };

      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
    };

    $scope.agents = XucAgent.getAgents();
    $scope.addAgent = function(queueId,penalty) {
      $scope.agents = XucAgent.getAgentsNotInQueue(queueId);
      var modalInstance = $uibModal.open({
        templateUrl: 'myModalContent.html',
        controller: ModalInstanceCtrl,
        size: 'sm',
        resolve: {
          agents: function () {
            return $scope.agents;
          }
        }
      });
      modalInstance.result.then(function (agent) {
        Cti.setAgentQueue(agent.id,queueId,penalty);
      },
      function () {
      }
      );
    };
  }
})();
