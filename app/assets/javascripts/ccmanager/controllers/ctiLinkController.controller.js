import moment from 'moment';

(function() {
  'use strict';

  angular.module('ccManager').controller('ctiLinkController', ctiLinkController);

  ctiLinkController.$inject = [ '$rootScope', '$scope', '$timeout', 'XucAgent', 'XucQueue', 'XucGroup'];
  function ctiLinkController($rootScope, $scope, $timeout, xucAgent, xucQueue, xucGroup) {
    var clockFrequency = 5;

    var startAllServices = function() {
      xucGroup.start();
      xucQueue.start();
      xucAgent.start();
    };

    // timeout
    var updateClock = function() {
      $scope.clock = moment().format('H:mm:ss');
      $timeout(updateClock, clockFrequency * 1000);
      if (!$rootScope.$$phase) $scope.$digest();
    };
    $timeout(updateClock, 1000, false);

    startAllServices();
  }
})();
