export default class ccChangeOptionController {

  constructor($scope, Preferences, XucQueueRecording, remoteConfiguration) {

    remoteConfiguration.getBooleanOrElse('showRecordingSwitch', true).then((resp) => {
      $scope.isRecordingSwitch = resp;
    });

    $scope.getViewOptions = function() {
      return Preferences.getViewOptions();
    };

    $scope.getRecordingOptions = function() {
      return Preferences.getRecordingOptions();
    };

    $scope.onSwitchViewOption = function(option) {
      Preferences.setOption(option.key, option.value);
    };

    $scope.onSwitchRecordingOption = function(option) {
      XucQueueRecording.setState(option.value ? "on" : "off");
    };

    $scope.hasRecordingQueues = () => {
      return XucQueueRecording.getRecordingQueues().length;
    };
  }
}
