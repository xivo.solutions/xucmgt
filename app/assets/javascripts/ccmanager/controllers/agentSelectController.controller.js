import _ from 'lodash';

(function() {
  'use strict';

  angular.module('ccManager').controller('agentSelectController', agentSelectController);

  agentSelectController.$inject = ['$scope'];

  function agentSelectController($scope) {

    $scope.agentSelectorClass = function() {
      var nbSelected = _.filter($scope.displayedData, function(o) { return o.selected; }).length;
      if (nbSelected === 0) return 'fa-square-o';
      if (nbSelected === $scope.displayedData.length) return 'fa-check-square-o';
      if (nbSelected <= $scope.displayedData.length) return 'fa-minus-square-o';
    };

    $scope.toggleSelectAgents = function() {
      var nbSelected = _.filter($scope.displayedData, function(o) { return o.selected; }).length;
      if (nbSelected === 0) $scope.displayedData = _($scope.displayedData).map(function(ag) { ag.selected=true; return ag; }).value();
      else $scope.displayedData = _($scope.displayedData).map(function(ag) { ag.selected=false; return ag; }).value();
    };

    $scope.noAgentSelected = function() {
      return (_.filter($scope.displayedData, function(o) { return o.selected; }).length === 0);
    };
  }
})();
