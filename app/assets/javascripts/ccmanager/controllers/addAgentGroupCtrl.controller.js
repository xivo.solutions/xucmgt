(function(){
  'use strict';

  angular
    .module('ccManager')
    .controller('addAgentGroupCtrl',addAgentGroupCtrl);

  addAgentGroupCtrl.$inject= ['$scope','XucGroup','$uibModal','NgTableParams','XucTableHelper'];

  function addAgentGroupCtrl($scope, xucGroup, $uibModal, NgTableParams, XucTableHelper) {

    var ModalInstanceCtrl = function ($scope, $uibModalInstance,
      NgTableParameters, xucGroup, queue, penalty) {

      $scope.groups = xucGroup.getAvailableGroups(queue.id, penalty);
      $scope.queue = queue;
      $scope.penalty = penalty;

      $scope.addGroup = function(group, queueId, penalty) {
        xucGroup.addAgentsNotInQueueFromGroupTo(group.id,queueId, penalty);
        $uibModalInstance.dismiss('done');
      };

      $scope.agentGroups = new NgTableParameters(
        {
          page: 1,
          count: XucTableHelper.POPUP_TABLE_ROWS,
          sorting: {
            name: 'asc'
          }
        },
        XucTableHelper.createSettings($scope.groups)
      );

      $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
      };
    };

    $scope.addAgentGroup = function(queue, penalty) {
      $scope.queue = queue;
      $scope.penalty = penalty;

      var modalInstance = $uibModal.open({
        templateUrl: 'addAgentGroupContent.html',
        controller: ModalInstanceCtrl,
        resolve: {
          NgTableParameters :function () {
            return NgTableParams;
          },
          xucGroup: function() {
            return xucGroup;
          },
          queue: function() {
            return $scope.queue;
          },
          penalty: function() {
            return $scope.penalty;
          }
        }
      });

      modalInstance.result.then(function () {
      }, function () {
      });
    };
  }
})();
