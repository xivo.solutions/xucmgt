(function() {
  'use strict';
  angular.module('ccManager', [
    'xcLogin',
    'xcCti',
    'xcHelper',
    'ngCookies',
    'ui.bootstrap',
    'ui.slider',
    'ui.router',
    'ui.indeterminate',
    'LocalStorageModule',
    'ngTable',
    'pascalprecht.translate',
    'angularjs-dropdown-multiselect',
    'isteven-multi-select',
    'ngMaterial',
  ]);
})();
