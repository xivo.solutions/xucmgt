export default function run(Preferences, Thresholds, XucMembership, $rootScope, $state, XucLink, $log, $transitions, XucAgent){
  /* eslint-disable no-unused-vars */
  var xucAgent = XucAgent;
  /* eslint-enable no-unused-vars */

  $rootScope.$state = $state;
  Preferences.init();
  Thresholds.init();

  $transitions.onStart({
    to: function(state) {
      return state.data !== null && state.data.requireLogin === true;
    }},function(trans) {
    if(!XucLink.isLogged()) {
      $log.info("Login required");
      return trans.router.stateService.target('login');
    }
  });
}
