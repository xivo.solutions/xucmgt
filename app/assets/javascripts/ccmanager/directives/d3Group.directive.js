import d3 from 'd3';

(function(){
  'use strict';

  angular
    .module('ccManager')
    .directive('d3Group',d3Group);

  d3Group.$inject= [];

  function d3Group(){
    return {
      restrict : 'E',
      scope : {
        data: '='
      },
      link : function(scope, element) {
        scope.data =scope.data||{};
        var heightSvg = 50;
        var widthSvg = 180;
        var padding = 1;
        var roundCorner = 5;
        var fontSize = 15;
        var svgContainer = d3.select(element[0]).append("svg").attr("height", heightSvg+5).attr("width", widthSvg+5);

        var drawBox = function(className, offset ) {
          svgContainer.append("rect").attr("x",padding+offset).attr("y",3*padding+offset).attr("rx",roundCorner).attr("ry",roundCorner)
            .attr("width", widthSvg-(2*padding)).attr("height", heightSvg-(6*padding))
            .attr("class", className);
        };
        var labelPos = {};
        labelPos.top = (2*padding) + fontSize*1.1;
        labelPos.middle = (2*padding) + fontSize*2.5;
        labelPos.bottom = (2*padding) + fontSize*3.5;
        var groupStyle = "font-size:"+fontSize+"px;";

        var drawLabel = function(text, ypos) {
          svgContainer.append("text").attr("style", groupStyle)
            .append("tspan").attr("x", widthSvg/2).attr("y",ypos)
            .attr("class","AgentGroupText").text(text);
        };
        drawBox("AgentGroupBoxShadow", 3 );
        drawBox("AgentGroupBox", 0 );
        drawLabel(scope.data.nbOfAgents,labelPos.top);
        drawLabel(scope.data.name,labelPos.middle);
      }
    };
  }
})();
