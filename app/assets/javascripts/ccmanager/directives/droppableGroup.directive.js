(function(){
  'use strict';

  angular
    .module('ccManager')
    .directive('droppableGroup',droppableGroup);

  function droppableGroup(dragDrop){
    return {
      replace:true,
      link: function(scope, element) {
        var el = element[0];
        el.addEventListener(
          'dragover',
          function(e) {
            e.dataTransfer.dropEffect = 'move';
            if (e.preventDefault) e.preventDefault();
            this.style.backgroundColor = "Orange";
            return false;
          },
          false
        );
        el.addEventListener(
          'dragleave',
          function(e) {
            e.dataTransfer.dropEffect = 'move';
            if (e.preventDefault) e.preventDefault();
            this.style.backgroundColor = "";
            return false;
          },
          false
        );
        el.addEventListener(
          'drop',
          function(e) {
            dragDrop.onDropEvent(e);
            this.style.backgroundColor = "";

            let data = dragDrop.getDataTransfer(e);
            scope.agentInGroup(Number(data.groupId),
              data.queueId,
              data.penalty,
              scope.queue.id,
              scope.groupOfGroups.penalty,
              data.clone
            );
            return false;
          },
          false
        );
      }
    };
  }
})();
