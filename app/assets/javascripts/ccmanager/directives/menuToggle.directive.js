/* menuToggle button directive using bootstrap glyphicons */
(function() {
  'use strict';

  angular
    .module('ccManager')
    .directive('menuToggle',menuToggle)
    .directive('menuToggleAnim',menuToggleAnim);

  function menuToggle() {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        state: '=',
        label: '@',
      },
      template: "<div class=\"menu-toggle\" ng-click=\"$event.stopPropagation()\">" +
                    "<div ng-click=\"toggleState()\" >" +
                        "<span class=\"glyphicon\" ng-class=\"state ? '' : 'glyphicon-menu-hamburger'\">" +
                            "<span class=\"menu-toggle-label\">{{label}}</span>" +
                    "</div>" +
                "</div>",
      link: function($scope) {
        $scope.toggleState = function() {
          $scope.state = !$scope.state;
          // To be removed once no more new !
          $scope.$parent.hideHelper();
          return $scope.state;
        };
      }
    };
  }


  function menuToggleAnim($animate, $timeout) {
    return {
      restrict: 'A',
      scope: {
        'state': '=',
      },
      link: function($scope, $element) {
        $scope.$watch('state', function(show, oldShow) {
          if (show === oldShow) return;
          if (show) {
            $animate.addClass($element, 'sidenav-open').then(function() {
              // this is hackish, we should use ngAnimate instead that handle promises,
              // unfortunately current hack of ngTable breaks compatibility
              $timeout(function() {
                $scope.$root.$broadcast('menu-toggled');
              },300);
            });
          }
          else{
            $animate.removeClass($element, 'sidenav-open').then(function() {
              $timeout(function() {
                $scope.$root.$broadcast('menu-toggled');
              },300);
            });
          }
        });
      }
    };
  }

})();
