var ccManager = angular.module('ccManager');
ccManager.directive('multiSelect', function(){
  return {
    restrict: 'E',
    template: '<div ng-if="mstranslation"><div ng-dropdown-multiselect="" options="options" selected-model="modelWrapper" translation-texts="mstranslation"></div></div>',
    scope: {
      selectedModel: "=",
      options: "="
    },
    controller: function($scope, $translate){
      $scope.modelWrapper = [];
      $translate([
        'MS_CHECKALL',
        'MS_UNCHECKALL',
        'MS_SELECTIONCOUNT',
        'MS_SELECTIONOF',
        'MS_SEARCHPLACEHOLDER',
        'MS_BUTTONDEFAULTTEXT',
        'MS_DYNAMICBUTTONTEXTSUFFIX',
      ]).then (tr => $scope.mstranslation = {
        checkAll : tr.MS_CHECKALL,
        uncheckAll : tr.MS_UNCHECKALL,
        selectionCount : tr.MS_SELECTIONCOUNT,
        selectionOf : tr.MS_SELECTIONOF,
        searchPlaceholder : tr.MS_SEARCHPLACEHOLDER,
        buttonDefaultText : tr.MS_BUTTONDEFAULTTEXT,
        dynamicButtonTextSuffix: tr.MS_DYNAMICBUTTONTEXTSUFFIX
      });

      $scope.$watchCollection('modelWrapper', (newValue) => {
        $scope.selectedModel = newValue;
      });
    }

  };
});
