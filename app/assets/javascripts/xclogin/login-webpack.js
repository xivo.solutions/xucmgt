import 'angular';
import 'angular-translate';
import 'angular-ui-bootstrap';

import './login.module.js';
import './controllers/fatalError.controller.js';
import './directives/loginForm.directive';
import './services/userRights.factory';

