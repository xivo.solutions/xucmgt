import 'jquery';
import 'jquery-ui';
import 'angular';
import 'angular-cookies';
import 'angular-local-storage';
import 'angular-translate';
import 'angular-translate-loader-partial';
import 'angular-ui-bootstrap';
import 'angular-ui-slider';
import '@uirouter/angularjs';
import moment from 'moment';

window.moment = moment;

/* Old libraries */
import './xccti/cti-webpack';
import './xchelper/helper.module';
import './xclogin/login-webpack';
import './xcchat/chat.module';
