
export enum UserPreferenceKey {
    PreferredDevice = "PREFERRED_DEVICE",
    MobileAppInfo = "MOBILE_APP_INFO",
    NbMissedCall = "NB_MISSED_CALL"
}

export enum UserPreferenceValueType {
    string = "String",
    boolean = "Boolean"
}

export enum UserPreferenceBoolean {
    True = "true",
    False = "false"
}

export enum UserPreferencePreferredDevice {
    WebApp = 'WebApp',
    MobileApp = 'MobileApp',
    WebAppAndMobileApp = 'WebAppAndMobileApp'
}

export type UserPreference = {
    [key in UserPreferenceKey]: {
        value: any;
        value_type: UserPreferenceValueType;
    };
};