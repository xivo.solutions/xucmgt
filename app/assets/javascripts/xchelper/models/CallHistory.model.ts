export enum CallStatus {
    Emitted = "emitted",
    Answered = "answered",
    Missed = "missed",
    Ongoing = "ongoing",
    Abandoned = "abandoned",
    DivertCaRatio = "divert_ca_ratio",
    DivertWaitTime = "divert_waittime",
    Closed = "closed",
    Full = "full",
    JoinEmpty = "joinempty",
    LeaveEmpty = "leaveempty",
    Timeout = "timeout",
    ExitWithKey = "exit_with_key",
}

export enum VideoEvents {
    Available = "Available",
    Busy = "Busy"
}

export enum PhoneHintStatus {
    ONHOLD = 16,
    RINGING = 8,
    INDISPONIBLE = 4,
    BUSY_AND_RINGING = 9,
    AVAILABLE = 0,
    CALLING = 1,
    BUSY = 2,
    DEACTIVATED = -1,
    UNEXISTING = -2,
    ERROR = -99
}

export interface CallDetail {
    callStatus: CallStatus;
    duration: string;
    day: Date;
}

export class CallHistoryModel {
    start?: Date = undefined;
    status?: CallStatus = undefined;
    dstVideoStatus?: VideoEvents = undefined;
    dstPhoneStatus?: PhoneHintStatus = undefined;
    srcVideoStatus?: VideoEvents = undefined;
    srcPhoneStatus?: PhoneHintStatus = undefined;
    duration: string = "";
    isMeetingRoom: boolean = false;
    callsDetail: Array<CallDetail> = [];
    initials: string = "";
    srcFirstName: string = "";
    srcLastName: string = "";
    dstFirstName: string = "";
    dstLastName: string = "";
    dstUsername: string = "";
    srcUsername: string = "";
    dstNum: number = 0;
    srcNum: number = 0;

    constructor() {
    }
}

export interface CallHistoryViewModel {
    videoStatus?: VideoEvents;
    phoneStatus?: PhoneHintStatus;
    callStatus?: CallStatus;
    initials: string;
    day?: Date;
    firstName: string;
    lastName: string;
    userName: string;
    number: number;
    duration: string;
}

export interface CallHistoryGroupViewModel {
    [key: string] : Array<CallHistorySubGroupViewModel>;
}

export interface CallHistorySubGroupViewModel {
    [key: string] : Array<CallHistoryViewModel> | CallHistoryViewModel
}