import {ApplicationConfiguration} from "../../xccti/services/applicationConfiguration.provider";

export class HeadsetsProvider {
  constructor(private configProvider: ApplicationConfiguration){}
  
  getHeadsetModelByVendorID (vendorId: number): number | undefined {
    return this.configProvider
      .getCurrentAppConfig()
      .headsetSupportedVendors
      .find(v => v == vendorId)
  }
  
  getSupportedVendors() {
    return this.configProvider.getCurrentAppConfig().headsetSupportedVendors
  }
}