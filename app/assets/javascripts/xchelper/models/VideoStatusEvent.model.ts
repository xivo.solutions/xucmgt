import { VideoEvents } from "xchelper/models/CallHistory.model";

export interface VideoStatusEvent {
  fromUser: string,
  status: VideoEvents
}
