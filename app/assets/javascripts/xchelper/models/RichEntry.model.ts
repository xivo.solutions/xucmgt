export interface RichEntry {
  contact_id?: string;
  entry: Array<string | Boolean>; //see fields, defaultHeaders from RichResult.scala
  favorite?: boolean;
  personal: boolean;
  source?: string; //should be list, but we don't wanna break legacy calls
  status: number;
  username?: string;
  videoStatus?: string;
}

export interface SearchContactResult {
  headers: Array<string>;
  entries: Array<RichEntry>;
}
