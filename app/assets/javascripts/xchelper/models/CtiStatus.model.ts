export enum CtiStatusEnum {
  AgentReady = 0,
  AgentOnPause = 1,
  AgentLoggedOut = 2,
  AgentOnWrapup = 3,
  AgentOnCall = 4,
  AgentDialing = 5,
  AgentRinging = 6,
  AgentLogin = 7
}



export interface CtiStatus {
  name: string;
  display_name: string;
  status: CtiStatusEnum;
}

export interface AgentState {
  name: string;
  userStatus: {
    name: string,
    longName: string
  };
  agentId: number,
  phoneNb: string
  queues: any
  since: number
}
