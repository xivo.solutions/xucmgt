import {PhoneHintStatus, VideoEvents} from './CallHistory.model';

export enum ContactSheetActionEnum {
    Call='Call', Video='Video', Chat='Chat', Mail='Mail', ShareLink='ShareLink', VideoInvite = 'VideoInvite', AudioInvite = 'AudioInvite'
}
export enum ContactSheetDataType {
    PhoneNumber= 'PhoneNumber', Mail= 'Mail',String='String', Url='Url'
}

export interface ContactSheetDetailFields {
    name: string,
    data: string,
    dataType: ContactSheetDataType
}

export interface ContactSheetDetailCategorie {
    name: string,
    fields: Array<ContactSheetDetailFields>
}

export interface ContactSheetSources {
    name: string,
    id: string
}

export interface ContactSheetAction {
    disable: boolean,
    args: Array<string>
}

export interface ContactSheetStatus {
    phone: PhoneHintStatus,
    video: VideoEvents
}
export interface ContactSheetModel {
    name: string,
    subtitle1: string,
    subtitle2: string,
    picture: string,
    isPersonal: boolean,
    isFavorite: boolean,
    isMeetingroom:  boolean,
    canBeFavorite:  boolean,
    status: ContactSheetStatus,
    actions: { [key: string]: ContactSheetAction },
    sources: Array<ContactSheetSources>,
    details: Array<ContactSheetDetailCategorie>,
    initials?: String 
}
