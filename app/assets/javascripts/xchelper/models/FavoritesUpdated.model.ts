export interface FavoritesUpdated {
    action: string, 
    contact_id: string, 
    source: string
}
