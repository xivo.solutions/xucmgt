export enum MembershipStatus {
  Available = 'Available',
  Exited = 'Exited',
  Paused = 'Paused'
}

export interface UserGroup {
  groupId: number;
  groupName: string;
  number: string;
  membershipStatus: MembershipStatus
}
