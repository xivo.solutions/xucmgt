export interface PhoneHintEvent {
  number: string,
  status: number
}
