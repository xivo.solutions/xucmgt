export default function audioConfiguration() {

  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/xchelper/directives/audioConfiguration.html',
    controller: ($scope, mediaDevices, webRtcAudio, xcHelperPreferences, AudioDevicesMgrService, $rootScope, XucPhoneState, WebHIDHeadsetService) => {

      $scope.ringtoneDevices = [];
      $scope.audioDevices = [];
      $scope.microphoneDevices = [];
      $scope.headsetName = WebHIDHeadsetService.getCurrentDeviceName();
            
      $rootScope.$on(WebHIDHeadsetService.HEADSET_SELECTED, (_, device) => {
        console.debug(`Received ${device} in ContentSettingsController`);
        $scope.headsetName = device;
        if (!$rootScope.$$phase) $scope.$apply();
      });

      $rootScope.$on(AudioDevicesMgrService.AUDIO_DEVICE_PICKED_EVT, (_e, device) => {
        $scope.selectedAudioDevice = device.deviceId;
      });

      $rootScope.$on(AudioDevicesMgrService.MIC_DEVICE_PICKED_EVT, (_e, device) => {
        $scope.selectedMicrophoneDevice = device.deviceId;
      });

      $rootScope.$on(AudioDevicesMgrService.AUDIO_DEVICE_UPDATE_EVT, (_e, devices) => {
        $scope.audioDevices = devices;
      });

      $rootScope.$on(AudioDevicesMgrService.MIC_DEVICE_UPDATE_EVT, (_e, devices) => {
        $scope.microphoneDevices = devices;
      });

      AudioDevicesMgrService.initAudioDevices();

      $scope.changeAudioOutputDevice = (deviceId) => {
        AudioDevicesMgrService.changeAudioOutputDevice(deviceId);
      };

      $scope.changeAudioInputDevice = (deviceId) => {
        AudioDevicesMgrService.changeAudioInputDevice(deviceId);
      };

      $scope.getAudioInputStatus = () => {
        return XucPhoneState.getDeviceConference().calls.length > 0 ? 'disabled' : '';
      };
    
      function _getOutputDevices () {
        mediaDevices.getAudioOutput().then((d) => {
          $scope.ringtoneDevices = d;
        });
      }

      _getOutputDevices();

      $scope.ringingDeviceId = xcHelperPreferences.getRingingDeviceId();

      $scope.changeRingingDevice = ($model) => {
        webRtcAudio.changeRingingDevice($model).then(() => {
          xcHelperPreferences.setRingingDeviceId($model);
        }, () => {
          angular.noop();
        });
      };
    }
  };
}
