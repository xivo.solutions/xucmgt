import {IDirective, IScope} from 'angular';
import {ContactSheetActionEnum, ContactSheetModel} from '../models/contact-sheet.model';
import ng from 'angular';

interface ActionProperties {
    class: string;
    id: string;
    icon: string;
    title: string;
    onClick: (contact: ContactSheetModel, $event: Event) => any;
    displayCondition: (contact: ContactSheetModel) => boolean;
    disableCondition: (contact: ContactSheetModel) => boolean;
}

export interface ActionsProperties {
    [key: string]: ActionProperties;
}

interface customScope extends ng.IScope {
    contact: ContactSheetModel;
    ordering: Array<string>;
    actionsProperties: ActionsProperties;
    isInModal: boolean;
    closeModal: () => void;
    handleClick: (actionsProperty: ActionProperties, action: ContactSheetActionEnum, contact: ContactSheetModel, $event: Event) => void;
}

export default class contactButtons implements IDirective {

    customScope?: customScope = undefined
    scope = {
        contact: '=',
        actionsProperties: '=',
        ordering: '='
    };
    restrict = 'E';
    templateUrl = 'assets/javascripts/xchelper/directives/contactButtons.html';

    constructor() {}

    handleClick(actionsProperty: ActionProperties, action: ContactSheetActionEnum, contact: ContactSheetModel, $event: Event) {
        if (!contact.actions[action].disable) {
            actionsProperty.onClick(contact, $event)
        } else {
            $event.stopPropagation()
        }
    }

    link(scope: IScope) {

        this.customScope = scope as customScope;

        if (this.customScope.contact.actions[ContactSheetActionEnum.Video]) {
            this.customScope.contact.actions[ContactSheetActionEnum.VideoInvite] = {
                args: ['VideoInvite'],
                disable: false
            };
        }
        if (this.customScope.contact.actions[ContactSheetActionEnum.Call]) {
            this.customScope.contact.actions[ContactSheetActionEnum.AudioInvite] = {
                args: ['AudioInvite'],
                disable: false
            };
        }
        this.customScope.handleClick = this.handleClick;
    }

}
