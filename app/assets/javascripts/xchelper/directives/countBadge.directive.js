export default function countBadge() {
  return {
    restrict: 'E',
    replace: true,
    scope: {
      value: '='
    },
    template: "<span class=\"badge count-badge\">{{value || 0}}</span>"
  };
}