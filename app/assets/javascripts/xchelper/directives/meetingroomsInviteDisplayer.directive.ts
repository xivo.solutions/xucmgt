import IAngularStatic, { IAngularEvent } from "angular"
import { IDirective, IScope, IRootScopeService } from "angular"
import MeetingroomsInviteManager from "xchelper/services/meetingroomsInviteManager.service"

export default class MeetingroomsInviteDisplayer implements IDirective {
  
  static $inject = ["$rootScope", "MeetingroomsInviteManager", "toast", "$translate"]
  public restrict: string
  public isolatedScope: {
    ongoingInvitations: number
  }
  public templateUrl: string
  
  constructor (
    private $rootScope: IRootScopeService,
    private MeetingroomsInviteManager: MeetingroomsInviteManager,
    private toast: any,
    private $translate: any) {
      this.isolatedScope = {
        ongoingInvitations: 0
      }
      this.restrict = 'E'
      this.templateUrl = 'assets/javascripts/xchelper/directives/meetingroomsInviteDisplayer.html'
    }
    
    link(scope: IScope) {
      
      this.isolatedScope = IAngularStatic.extend(scope, this.isolatedScope)
      
      const onNewInvitationCount = (e: IAngularEvent, count: number) => {
        this.isolatedScope.ongoingInvitations = count
        if (!this.$rootScope.$$phase) scope.$apply()
      }
      
      const onDisplayInvitationToast = (e: IAngularEvent, message: string, displayName: string) => {
        displayToast(message, displayName)
        if (!this.$rootScope.$$phase) scope.$apply()
      }
      
      const displayToast = (message: string, displayName: string) => {
        this.toast({
          duration: 3000,
          message: `${displayName} ${this.$translate.instant(message)}`,
          className: 'basic-toast',
          position: "center",
          container: '.toast-container'
        });
      }

      const resetIsolatedScope = () => {
        this.isolatedScope = {
          ongoingInvitations: 0
        }
      }
      
      this.$rootScope.$on(this.MeetingroomsInviteManager.DISPLAY_INVITATION_TOAST_EVENT, onDisplayInvitationToast.bind(this))
      this.$rootScope.$on(this.MeetingroomsInviteManager.NEW_INVITATION_COUNT_EVENT, onNewInvitationCount.bind(this))
      scope.$on('$destroy', resetIsolatedScope).bind(this);
    }
  }