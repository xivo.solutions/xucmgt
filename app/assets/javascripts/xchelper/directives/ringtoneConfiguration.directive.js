export default function ringtoneConfiguration(webRtcAudio, $translate) {
  
  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/xchelper/directives/ringtoneConfiguration.html',
    controller: ($scope) => {
      
      $scope.ringtones = [
        {id: 1, name: $translate.instant('DEFAULT_RINGTONE_NAME'), file: 'incoming_call.mp3'},
        {id: 2, name: 'The Return', file: 'the_return.mp3'},
        {id: 3, name: 'Keys of Moon - The Success', file: 'keys_of_moon_the_success.mp3'},
        {id: 4, name: 'Another Happy Ukulele Song', file: 'another_happy_ukulele_song.mp3'},
        {id: 5, name: 'Cordless Phone Ring', file: 'cbakos_cordless_phone_ring.wav'},
        {id: 6, name: 'Fekete Rigo', file: 'sandor_molnar_fekete_rigo.mp3'},
        {id: 7, name: 'Pop Ringtone', file: 'imthemap_pop_ringtone.wav'},
        {id: 8, name: 'British Telephone', file: 'inchadney_british_telephone.wav'},
        {id: 9, name: 'Adventure', file: 'adventure.mp3'},
        {id: 10, name: 'Anthem of the united sloths', file: 'anthem_of_the_united_sloths.mp3'},
        {id: 11, name: 'Armageddon', file: 'armageddon.mp3'},
        {id: 12, name: 'Banjo fever', file: 'banjo_fever.mp3'},
        {id: 13, name: 'Chronos', file: 'chronos.mp3'},
        {id: 14, name: 'Coconut caprice', file: 'coconut_caprice.mp3'},
        {id: 15, name: 'Regrock', file: 'regrock.mp3'},
        {id: 16, name: 'Sunny rasta', file: 'sunny_rasta.mp3'}

      ];

      let ringtone = JSON.parse(window.localStorage.getItem('user_ringtone'));
      $scope.selectedRingtone = ringtone ? ringtone.id : $scope.ringtones[0].id;

      $scope.changeRingtone = (ringtoneId) => {
        $scope.selectedRingtone = ringtoneId;
        let ringtone = $scope.ringtones.find(r => r.id == ringtoneId);
        window.localStorage.setItem('user_ringtone', JSON.stringify(ringtone));
        webRtcAudio.changeRingtone(ringtone);
      };
    }
  };
}