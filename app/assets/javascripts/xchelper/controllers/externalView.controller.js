export default class ExternalViewController {

  constructor(externalView) {
    this.externalViewUrl = externalView.getURL();
  }
}
