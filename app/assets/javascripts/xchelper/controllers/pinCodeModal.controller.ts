import { IHttpPromise, IHttpResponse, IScope } from "angular"
import { XucCredentials } from "xccti/services/XucLink"
import JitsiProxy from "xchelper/services/jitsiProxy.service"

export default class PinCodeModal {
  public showError: boolean
  public pinCode: string = ''

  constructor(
    private $uibModalInstance: ng.ui.bootstrap.IModalInstanceService,
    private JitsiProxy: JitsiProxy,
    private token: string,
    private userData: XucCredentials
  ) {
    this.showError = false
  }

  onKeyPress = (keyCode: number) => {
    if (keyCode == 13) {
      this.onSubmitPinModal()
    }
  }

  cancel = () => {
    this.$uibModalInstance.dismiss('cancel')
  }

  onSubmitPinModal = () => {
    this.JitsiProxy.validateToken(this.token, this.userData, this.pinCode).then(
      (result: any) => {
        this.$uibModalInstance.close(result.data.token)
      },
      () => (this.showError = true)
    )
  }
}
