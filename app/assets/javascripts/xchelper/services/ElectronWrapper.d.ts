type ElectronSize = {
  width: number, height: number, minimalist: boolean
}

export interface ElectronWrapper {

  ELECTRON_AGENT_DEFAULT_WIDTH: number
  ELECTRON_AGENT_DEFAULT_HEIGHT: number
  ELECTRON_AGENT_MINI_WIDTH: number
  ELECTRON_AGENT_MINI_HEIGHT: number
  ELECTRON_AGENT_THIRD_PARTY_MIN_WIDTH: number
  ELECTRON_AGENT_QUIT_OPTIONS: { type: string, buttons: Array<string>, defaultId: number, message: string, detail: string }
  
  isElectron(): boolean
  forceQuit(): void
  setElectronConfig(options: {
    width?: number,
    height?: number,
    title?: string,
    minimalist?: boolean,
    confirmQuit?: {
      reset?: boolean,
      type?: string,
      buttons?: string[],
      defaultId?: number,
      message?: string,
      detail?: string
    }
  }): void
  setFocus(): boolean
  setTrayIcon(icon: string): void
  runExecutable(path: string, args: Array<string>): void
  toggleWindow(toggledSize: ElectronSize, defaultSize: ElectronSize): void
  isWindowToggled(): boolean
  startJitsi(roomuuid: string, roomdisplayname: string, token: string, user: string | undefined): void
  muteJitsi(): void
  setElectronFullscreen(): void
}
