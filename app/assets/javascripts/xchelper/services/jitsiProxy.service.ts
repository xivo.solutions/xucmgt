import { IScope, ILogService, IHttpService, IAngularEvent, IHttpPromise } from "angular"
import { JitsiMeetExternalAPI, RichWindow } from "RichWindow"
import { XucLink, XucCredentials } from "xccti/services/XucLink"
import { XucUserService } from "xccti/services/XucUser"
import XucVideoEventManager from "xccti/services/XucVideoEventManager.service"
import PinCodeModal from "xchelper/controllers/pinCodeModal.controller"
import { ElectronWrapper } from "./ElectronWrapper"
import MeetingroomsInviteManager from "./meetingroomsInviteManager.service"
import { Buffer } from 'buffer'

class JitsiOptions {
  constructor(
    public roomName: string,
    public parentNode: Element | null,
    public userInfo: { displayName: string | undefined },
    public jwt: string | null,
    public configOverwrite: { [key: string]: string | boolean | { [key: string]: boolean } },
    public onload: () => any
  ) { }
}

type jwtToken = string
type roomType = 'personal' | 'static' | 'temporary'

export class Token {
  constructor(
    public raw: jwtToken,
    public content: TokenContent
  ) { }
}

export class TokenContent {
  constructor(
    public iss: string,
    public room: string,
    public sub: string,
    public aud: string,
    public roomuuid: string,
    public roomdisplayname: string,
    public roomnumber: string,
    public requirepin: boolean,
    public expiry: string  | undefined,
    public timestamp: string  | undefined
  ) { }
}

export default class JitsiProxy {

  public ERR_VIDEO_ALREADY_STARTED: string = "ERR_VIDEO_ALREADY_STARTED"
  public ERR_CANNOT_START_MEETINGROOM: string = "ERR_CANNOT_START_MEETINGROOM"
  public JITSI_IFRAME_LOADED: string = "JITSI_IFRAME_LOADED"
  public VIDEO_NEEDS_PIN_CODE: string = "pinCode"
  public VIDEO_START_EVENT = "EventStartVideo"
  public VIDEO_START_ERROR_EVENT = "EventStartVideoError"
  public VIDEO_SHOW_PHONEBAR_EVENT = "EventShowPhoneBar"
  public VIDEO_END_EVENT = "EventCloseVideo"
  public VIDEO_KICKED_EVENT = "EventKicked"
  public VIDEO_INVITATION_ACCEPT = "EventVideoInvitationAccept"
  public VIDEO_INVITATION_REFUSE = "EventVideoInvitationRefuse"
  public VIDEO_INVITATION_TIMEOUT = "EventVideoInvitationTimeout"
  public INFO_KICKED_BY = "INFO_KICKED_BY"
  public timeout: number = 5000
  public showPhoneBar: boolean = true
  public userName: string | undefined
  public jitsiApiObj: JitsiMeetExternalAPI | undefined
  public electronJitsiRunning: boolean = false

  private storedToken: Token | undefined
  public jitsiAvailable: boolean = false
  
  static $inject = [
    "$window", "electronWrapper", "XucUser", "$rootScope",
    "XucLink", "$log", "$http", "$uibModal",
    "MeetingroomsInviteManager", "XucVideoEventManager",
    "toast", "$translate"
  ]

  constructor(
    private $window: RichWindow,
    private electronWrapper: ElectronWrapper,
    private XucUser: XucUserService,
    private $rootScope: IScope,
    private XucLink: XucLink,
    private $log: ILogService,
    private $http: IHttpService,
    private $uibModal: ng.ui.bootstrap.IModalService,
    private MeetingroomsInviteManager: MeetingroomsInviteManager,
    private XucVideoEventManager: XucVideoEventManager,
    private toast: any,
    private $translate: any
  ) { this.init() }

  public startVideo = async (
    roomId: string,
    roomType: roomType,
  ) => {
    return this.initJitsi(
      this.getMeetingRoomToken(roomId, roomType, this.getToken, this.parseToken),
      false    
    )
  }

  public joinOngoingVideo = async (
    token: Token,    
  ) => {
    this.initJitsi(
      Promise.resolve(token),
      true
    )
  }

  public videoIsOngoing = (): boolean => {
    return (this.jitsiApiObj != undefined || this.electronJitsiRunning)
  }

  public validateToken = (token: jwtToken, userData: XucCredentials, pin: string | undefined): IHttpPromise<any> => {
    return this.$http.get<any>(
      this.getUrlValidateToken(token, pin),
      { headers: { 'Authorization': `Bearer ${userData.token}` }, timeout: this.timeout }
    )
  }

  public inviteToMeetingRoom = (username: string, displayName: string, token: Token | undefined = this.storedToken) : void => {
    if (token) this.MeetingroomsInviteManager.sendInvitation(username, displayName, token.raw)
  }

  public startPointToPointVideoCall = async (username: string, displayName: string) : Promise<void> => {
    let userData = await this.XucLink.whenLogged()
    let rawToken = await this.getToken(displayName, userData, 'temporary')
    let token = this.parseToken(rawToken.data.token)
    this.joinOngoingVideo(token)
    this.inviteToMeetingRoom(username, displayName, token)
  }

  public acceptInvitation = (inviteSequence: number, username: string, token: jwtToken) : void => {
      this.$rootScope.$emit(this.VIDEO_INVITATION_ACCEPT)
      this.MeetingroomsInviteManager.acceptInvitation(inviteSequence, username, token)        
      this.joinOngoingVideo(this.parseToken(token))
  }

  public rejectInvitation = (inviteSequence: number, username: string) : void => {
    this.$rootScope.$emit(this.VIDEO_INVITATION_REFUSE)
    this.MeetingroomsInviteManager.rejectInvitation(inviteSequence, username)  
  }

  public togglePhoneBar = () => {
    this.showPhoneBar = !this.showPhoneBar
    if (this.showPhoneBar) this.$rootScope.$emit(this.VIDEO_SHOW_PHONEBAR_EVENT)
  }

  public getShowPhoneBar = () => {
    return this.showPhoneBar
  }

  public dismissVideo = () => {
    if (this.jitsiApiObj) this.jitsiApiObj.dispose()
    this.jitsiApiObj = undefined
    this.$rootScope.$emit(this.VIDEO_END_EVENT)
    this.$window.Cti.videoEvent(this.XucVideoEventManager.EVENT_VIDEO_END)
    this.storedToken = undefined
  }

  public subscribeToStartVideo = (scope: IScope, callback: (event: IAngularEvent, ...args: any[]) => any) => {
    var handler = this.$rootScope.$on(this.VIDEO_START_EVENT, (callback))
    scope.$on('$destroy', handler)
  }

  public subscribeToStartVideoError = (scope: IScope, callback: (event: IAngularEvent, ...args: any[]) => any) => {
    var handler = this.$rootScope.$on(this.VIDEO_START_ERROR_EVENT, callback)
    scope.$on('$destroy', handler)
  }

  public subscribeToCloseVideo = (scope: IScope, callback: (event: IAngularEvent, ...args: any[]) => any) => {
    var handler = this.$rootScope.$on(this.VIDEO_END_EVENT, callback)
    scope.$on('$destroy', handler)
  }

  public subscribeToShowPhoneBar = (scope: IScope, callback: (event: IAngularEvent, ...args: any[]) => any) => {
    var handler = this.$rootScope.$on(this.VIDEO_SHOW_PHONEBAR_EVENT, callback)
    scope.$on('$destroy', handler)
  }

  public muteMicrophoneInJitsi = () => {
    if (this.electronJitsiRunning) {
      this.electronWrapper.muteJitsi()
      return Promise.resolve(true)
    } else {
      return this.jitsiApiObj?.isAudioMuted().then(muted => {
        if (!muted) this.jitsiApiObj?.executeCommand('toggleAudio')
      })
    }  
  }

  private parseToken = (token: string): Token => {
    try {
      const jsonVal = JSON.parse(Buffer.from(token.split('.')[1], 'base64').toString('utf8'))
      return new Token(token, new TokenContent(
        jsonVal.iss,
        jsonVal.room,
        jsonVal.sub,
        jsonVal.aud,
        jsonVal.roomuuid,
        jsonVal.roomdisplayname,
        jsonVal.roomnumber,
        jsonVal.requirepin,
        jsonVal.expiry,
        jsonVal.timestamp
      ))
    } catch (e) {
      throw `Cannot decode token ${token} for meetingRoom : ${e}`
    }
  }


  private checkPinCode = (token: Token, tokenDecoder: Function) : Promise<Token> => {
    return new Promise( async (resolve) => {
      let userData = await this.XucLink.whenLogged()
      if (!token.content.requirepin) {
        resolve(token)
      } else {
        let tokenPinValidated = await this.pinCodeModal(token.raw, userData).result        
        resolve(tokenDecoder(tokenPinValidated))
      }    
    })
  }

  private onIframeLoaded = () => {
    this.$rootScope.$emit(this.JITSI_IFRAME_LOADED)
  }
  
  private initJitsi = async (
    tokenGetter: Promise<Token>,
    isInvite: boolean,
    tradeTokenForCookie: Function = this.tradeTokenForCookie,
    parseToken: (token: jwtToken) => Token = this.parseToken
  ) => {
    return new Promise(async (resolve, reject) => {
      if (this.videoIsOngoing()) {
        this.$rootScope.$emit(this.VIDEO_START_ERROR_EVENT, this.ERR_VIDEO_ALREADY_STARTED)
        reject(this.ERR_VIDEO_ALREADY_STARTED)
      } else {
        try {
          let token = await tokenGetter     
          await tradeTokenForCookie(token)
          this.storedToken = (isInvite) ? token : await this.checkPinCode(token, parseToken)        
          let roomNumber = (this.storedToken.content.roomnumber) ? " (**" + this.storedToken.content.roomnumber + ")" : ""                  
          resolve(
            this.startJitsi(
              this.storedToken.content.roomuuid,
              this.storedToken.content.roomdisplayname + roomNumber,
              token.raw
            )          
          )
        } catch (error) {          
          this.$log.debug(`Cannot start meetingroom`, error)
          reject(this.ERR_CANNOT_START_MEETINGROOM)
          this.$rootScope.$emit(this.VIDEO_START_ERROR_EVENT, this.ERR_CANNOT_START_MEETINGROOM)
        }
      }
    })
  }

  public onKickedOut = (event: any) => {
    if (event.kicked.local) {
      let kickerName = (event.kicker.displayName) ? event.kicker.displayName : this.jitsiApiObj?.getDisplayName(event.kicker.id);
      this.toast({
        duration: 3000,
        message: `${this.$translate.instant(this.INFO_KICKED_BY)} ${kickerName}`,
        className: 'basic-toast',
        position: "center",
        container: '.toast-container'
      })
      this.dismissVideo()
    }
  }

  private startJitsi = (roomuuid: string, roomdisplayname: string, token: jwtToken) => {
    if (this.electronWrapper.isElectron()){
      this.electronWrapper.startJitsi(roomuuid, roomdisplayname, token, this.userName)
    } else {
      this.jitsiApiObj = new this.$window.JitsiMeetExternalAPI(
        this.getVideoDomain(),
        new JitsiOptions(roomuuid, document.querySelector('#meet'), { displayName: this.userName }, token, { subject: roomdisplayname, prejoinConfig: { enabled :false }}, this.onIframeLoaded)
      )
      this.jitsiApiObj.addListener('readyToClose', this.dismissVideo)
      this.jitsiApiObj.addListener('participantKickedOut', this.onKickedOut)
      this.$rootScope.$emit(this.VIDEO_START_EVENT)
      this.$window.Cti.videoEvent(this.XucVideoEventManager.EVENT_VIDEO_START)
      return this.jitsiApiObj
    }
  }

  private pinCodeModal = (token: jwtToken, userData: XucCredentials) => {
    return this.$uibModal.open({
      templateUrl: 'assets/javascripts/xchelper/controllers/pinCodeModal.html',
      controller: PinCodeModal,
      controllerAs: 'ctrl',
      windowClass: 'callMgtModal',
      resolve: {
        token: () => {
          return token
        },
        userData: () => {
          return userData
        }
      }
    })
  }

  public electronAdvancedVideoEventDecoder = (electronEvent: any) => {
    try {
      electronEvent = JSON.parse(electronEvent);
    } catch(e) {console.debug(`Could not parse videoEvent ${electronEvent}`)}
    switch (electronEvent.event) {
      case this.VIDEO_KICKED_EVENT:
        this.onKickedOut(electronEvent.eventFromJitsi)
        break
    }
  }

  private onElectronJitsiEvent = (event: IAngularEvent, electronEvent: string | {event: string, data: any}) => {
    switch (electronEvent) {
      case this.VIDEO_START_EVENT: 
        this.$rootScope.$emit(this.VIDEO_START_EVENT)
        this.$window.Cti.videoEvent(this.XucVideoEventManager.EVENT_VIDEO_START)
        document.getElementById("jitsi-video")?.classList.add("foreground")
        this.electronJitsiRunning = true
        break
      case this.VIDEO_END_EVENT: 
        this.$rootScope.$emit(this.VIDEO_END_EVENT)
        this.$window.Cti.videoEvent(this.XucVideoEventManager.EVENT_VIDEO_END)
        document.getElementById("jitsi-video")?.classList.remove("foreground")
        this.electronJitsiRunning = false
        break
      default:
        this.electronAdvancedVideoEventDecoder(electronEvent)
        break
    }
  }

  private isApiReachable = () => {
    this.$http.get("/video/external_api.js").then( () => {
      this.jitsiAvailable = true
    }).catch( () => {
      this.$log.warn("Jitsi not reachable : cannot instatiate meetingrooms")
    })
  }

  private getVideoDomain = () => {
    return this.$window.externalConfig.host + '/video'
  }

  private getToken = (roomId: string, userData: XucCredentials, roomType: roomType): IHttpPromise<any> => {
    return this.$http.get<any>(
      this.getUrlForMeetingroom(roomId, roomType),
      { headers: { 'Authorization': `Bearer ${userData.token}` }, timeout: this.timeout }
    )
  }

  private baseUrl = () => {
    return this.XucLink.getServerUrl('http') + `/xuc/api/2.0/config/meetingrooms`
  }

  private getUrlForMeetingroom = (roomId: string, roomType: roomType) => {
    let baseUrl = this.baseUrl()
    return `${baseUrl}/${roomType}/token/${roomId}`
  }

  private getUrlValidateToken = (token: jwtToken, pin: string | undefined) => {
    let url = `${this.XucLink.getServerUrl('http')}/meetingrooms/token/validate/${token}`
    if (pin !== undefined) {
      url += `?pin=${pin}`
    }
    return url
  }

  private getMeetingRoomToken = async (
    roomId: string,
    roomType: roomType,
    tokenGetter: (roomName: string, userData: XucCredentials, roomType: roomType) => IHttpPromise<XucCredentials> = this.getToken,
    tokenDecoder: Function
  ): Promise<Token> => {
    let userData = await this.XucLink.whenLogged()
    let token = await tokenGetter(roomId, userData, roomType)
    return tokenDecoder(token.data.token)
  }

  private tradeTokenForCookie = async (
    token: Token,
    tokenValidator: (token: jwtToken, userData: XucCredentials, pin: string | undefined) => IHttpPromise<void> = this.validateToken
  ): Promise<void> => {
    let userData = await this.XucLink.whenLogged()
    await tokenValidator(token.raw, userData, undefined)
  }

  private init = () => {
    this.$log.info("Starting jitsiProxy service")
    this.isApiReachable()
    this.XucUser.getUserAsync().then((user) => {
      this.userName = user.fullName
    })
    this.$rootScope.$on('JITSI_EVENT', this.onElectronJitsiEvent)
    this.XucLink.whenLoggedOut().then(this.uninit)
    this.$rootScope.$on('linkDisConnected', () => {
      if (this.videoIsOngoing()) this.dismissVideo()
    })
  }

  private uninit = () => {
    this.$log.info("Unloading jitsiProxy service")
    if (this.jitsiApiObj) this.dismissVideo()
    this.showPhoneBar = true
    this.userName = undefined
    this.XucLink.whenLogged().then(this.init)
  }
}
