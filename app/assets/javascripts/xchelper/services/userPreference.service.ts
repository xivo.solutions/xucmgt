import {RichWindow} from "../../RichWindow";
import {XucLink} from "xccti/services/XucLink"
import {isInteger, isNil} from "lodash";
import {
  UserPreference,
  UserPreferenceBoolean,
  UserPreferenceKey,
  UserPreferencePreferredDevice,
  UserPreferenceValueType
} from "../models/userPreference.model";
import {ILogService, IScope} from "angular";

export default class UserPreferenceService {
  private _userPreferences?: UserPreference = undefined;
  private opened: boolean = false;
  public modalInstance: any = undefined;
  public timeout: number = 15000;
  public timer: any = undefined;
  public NEW_NB_MISSED_CALLS: string = "newNbMissedCalls";

  get userPreferences() {
    return this._userPreferences;
  }

  static $inject = ["$window", "$uibModal", "$log", "remoteConfiguration", "XucLink", "$rootScope"]

  constructor(
    private $window: RichWindow,
    private $uibModal: ng.ui.bootstrap.IModalService,
    private $log: ILogService,
    private remoteConfiguration: any,
    private XucLink: XucLink,
    private $rootScope: IScope
  ) {
  }

  init() {
    this.$log.info("Starting UserPreference service")
    this.XucLink.whenLoggedOut().then(this.unInit)
    this.$window.Cti?.setHandler(this.$window.Cti.MessageType.USERPREFERENCE, this.onGetUserPreference.bind(this))
  }

  private unInit = () => {
    this.$log.info("Unloading UserPreference service")
    this._userPreferences = undefined
  }

  /**
   * Method Called when the WS get a USERPREFERENCE MessageType
   * @param userPreference
   */
  onGetUserPreference(userPreference: UserPreference) {
    this._userPreferences = userPreference;
    if (!isNil(this._userPreferences) &&
      !isNil(this._userPreferences[UserPreferenceKey.NbMissedCall]) &&
      !isNaN(parseInt(this._userPreferences[UserPreferenceKey.NbMissedCall].value))) {
      this.$rootScope.$broadcast(this.NEW_NB_MISSED_CALLS, parseInt(this._userPreferences[UserPreferenceKey.NbMissedCall].value));
    }
    if (this.opened) return;
    if (
      !isNil(this._userPreferences) &&
      !isNil(this._userPreferences[UserPreferenceKey.MobileAppInfo]) &&
      this._userPreferences[UserPreferenceKey.MobileAppInfo].value === UserPreferenceBoolean.True &&
      !this.remoteConfiguration?.isAgent() &&
      !this.remoteConfiguration.hasSwitchBoard()
    ) {
      this.modalInstance = this.$uibModal.open({
        templateUrl: 'assets/javascripts/xchelper/controllers/notificationModal.controller.html',
        size: 'sm',
        controller: 'NotificationModalController',
        resolve: {}
      })
      this.opened = true
      this.modalInstance.result.catch(
        () => {
          if (this.timer) clearTimeout(this.timer)
          this.$log.debug("modal closed")
          this.modalInstance = undefined
          this.onChangeMobileAppInfo(UserPreferenceBoolean.False)
          this.opened = false
        }
      )
    }
  }

  onCleanUserPreference() {
    this._userPreferences = undefined;
  }

  onChangeUserPreference(key: UserPreferenceKey, value: any, value_type: UserPreferenceValueType) {
    this.$window.Cti?.setUserPreference(key, value, value_type);
  }

  onChangePreferredDevice(value: UserPreferencePreferredDevice) {
    this.$window.Cti?.setUserPreference(UserPreferenceKey.PreferredDevice, value, UserPreferenceValueType.string);
  }

  onChangeMobileAppInfo(value: UserPreferenceBoolean) {
    this.$window.Cti?.setUserPreference(UserPreferenceKey.MobileAppInfo, value, UserPreferenceValueType.boolean);
  }

  resetMissedCalls() {
    this.$window.Cti?.setUserPreference(UserPreferenceKey.NbMissedCall, "0", UserPreferenceValueType.string);
  }

  getPreferredDevice() {
    return (this.userPreferences && this.userPreferences[UserPreferenceKey.PreferredDevice]) ?
      this.userPreferences[UserPreferenceKey.PreferredDevice].value :
      UserPreferencePreferredDevice.WebAppAndMobileApp;
  }

  getMissedCalls() {
    return (this.userPreferences &&
      this.userPreferences[UserPreferenceKey.NbMissedCall] &&
      !isNaN(parseInt(this.userPreferences[UserPreferenceKey.NbMissedCall].value)))
      ? parseInt(this.userPreferences[UserPreferenceKey.NbMissedCall].value)
      : 0;
  }
}
