import { IRootScopeService } from 'angular';
import {ElectronWrapper}  from 'xchelper/services/ElectronWrapper';

export class BadgeManagerService {

    static $inject = ["$rootScope", "electronWrapper", "XucChat", "UserPreferenceService"];

    constructor(
        private $rootScope: IRootScopeService,
        private electronWrapper: ElectronWrapper,
        private XucChat: any,
        private UserPreferenceService: any
    ) {
    }

    hasMissedMessage = false;
    hasMissedCall = false;

    updateBadge() {
        if (this.hasMissedMessage || this.hasMissedCall) {
            this.electronWrapper.setTrayIcon('missed');
        } else {
            this.electronWrapper.setTrayIcon('default');
        }
    };

    onMissedMessage(e: any, counter: number) {
        this.hasMissedMessage = counter > 0;
        this.updateBadge();
    };


    onMissedCall(e: any, counter: number) {
        this.hasMissedCall = counter > 0;
        this.updateBadge();
    };

    init() {
        this.$rootScope.$on(this.XucChat.CHAT_UNREAD_MESSAGE, this.onMissedMessage.bind(this));
        this.$rootScope.$on(this.UserPreferenceService.NEW_NB_MISSED_CALLS, this.onMissedCall.bind(this));
    };  
}