import { ILogService, IRootScopeService, IWindowService } from 'angular';
import { RichWindow } from 'RichWindow';

type Device = {
  deviceId: string
  kind: string
}

export class AudioDevicesMgrService {
  
  static $inject = ["$rootScope", "localStorageService", "$window", "mediaDevices", "processVolume", "$log", "webRtcAudio"];
  private audioDevices: Array<Device> = []
  private microphoneDevices: Array<Device> = []
  
  public AUDIO_DEVICE_UPDATE_EVT = "AudioDevicesUpdateEvent"
  public MIC_DEVICE_UPDATE_EVT = "MicrophoneDevicesUpdateEvent"

  public AUDIO_DEVICE_PICKED_EVT = "AudioDevicePickedEvent"
  public MIC_DEVICE_PICKED_EVT = "MicrophoneDevicePickedEvent"
  public RINGING_DEVICE_PICKED_EVT = "RingingDevicePickedEvent"
  
  constructor(
    private $rootScope: IRootScopeService,
    private localStorageService: any,
    private $window: RichWindow,
    private mediaDevices: any,
    private processVolume: any,
    private $log: ILogService,
    private webRtcAudio: any
  ) {}
  
  changeAudioOutputDevice(deviceId: string) {
    this.$window.xc_webrtc.applyAudioOutputToElements(deviceId);
    this.localStorageService.set("AudioOutputDeviceId", deviceId);
  }

  changeAudioInputDevice(deviceId: string) {
    this.$window.SIPml.setGlobalMicrophoneDeviceId(deviceId);
    this.$window.xc_webrtc
      .swapCurrentMicrophone(deviceId)
      .then((newStream: MediaStream) => {
        this.processVolume.onMicChange(newStream);
      })
      .catch((err: Error) => {
        this.$log.debug(err);
      });
    this.localStorageService.set("AudioInputDeviceId", deviceId);
  }
  
  changeRingingDevice(deviceId: string) {
    this.webRtcAudio.changeRingingDevice(deviceId);
    this.localStorageService.set("ringingDeviceId", deviceId);
  }

  initAudioDevices() {
    this.mediaDevices
      .getAudioDevices()
      .then(this.initPreviousAudioDevices.bind(this))
      .catch((err: Error) => {
        this.$log.error("Error when getting devices", err);
      });
  }

  initPreviousAudioDevices(devices: Array<Device>) {
    this.sortDevices(devices);
    this.initOutputs();
    this.initInputs();
    this.initRinging();
  }

  sortDevices(devices: Array<Device>) {
    this.audioDevices = this.filterDevicesByKind(devices, "audiooutput");
    this.$rootScope.$broadcast(this.AUDIO_DEVICE_UPDATE_EVT, this.audioDevices);
    
    this.microphoneDevices = this.filterDevicesByKind(devices, "audioinput");
    this.$rootScope.$broadcast(this.MIC_DEVICE_UPDATE_EVT, this.microphoneDevices);
  }

  initRinging() {
    let selectedRingingDevice = this.selectDevice(
      this.audioDevices,
      "ringingDeviceId"
    );

    this.$rootScope.$broadcast(this.RINGING_DEVICE_PICKED_EVT, selectedRingingDevice)
  }

  initOutputs() {
    let selectedAudioDevice = this.selectDevice(
      this.audioDevices,
      "AudioOutputDeviceId"
    );
    this.$rootScope.$broadcast(this.AUDIO_DEVICE_PICKED_EVT, selectedAudioDevice)
  }

  initInputs() {
    let selectedMicrophoneDevice = this.selectDevice(
      this.microphoneDevices,
      "AudioInputDeviceId"
    );
    this.$rootScope.$broadcast(this.MIC_DEVICE_PICKED_EVT, selectedMicrophoneDevice)
  }

  selectDevice(devices: Array<Device>, storageKey: string) {
    const storedId = this.localStorageService.get(storageKey);
    return (
      devices.find((device) => device.deviceId == storedId) ||
      devices.find((device) => device.deviceId == "default")
    );
  }

  filterDevicesByKind(devices: Array<Device>, kind: string) {
    return devices.filter((device) => device.kind === kind);
  }
}