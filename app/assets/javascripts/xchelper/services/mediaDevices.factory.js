export default function mediaDevices($window) {
  var deferred;

  function _getAudioOutput() {
    return _getDevices(["audiooutput"]);
  }

  function _getAudioDevices() {
    return _getDevices(["audioinput", "audiooutput"]);
  }

  function _getDevices(filter) {
    return new Promise((res, rej) => {
      $window.navigator.mediaDevices.enumerateDevices().then((result) => {
        res(
          result.filter((item) => {
            return filter.includes(item.kind);
          })
        );
      }).catch(err => {
        console.error(err);
        rej(err);
      });
    });
  }

  return {
    getAudioOutput: _getAudioOutput,
    getAudioDevices: _getAudioDevices,
  };
}
