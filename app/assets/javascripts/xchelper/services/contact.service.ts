import _ from 'lodash';
import { ContactSheetActionEnum, ContactSheetDetailFields, ContactSheetModel, ContactSheetSources } from '../models/contact-sheet.model';
import {ActionsProperties} from '../directives/contactButtons.directive';
import {ApplicationConfiguration, AppType} from '../../xccti/services/applicationConfiguration.provider';
import {isNil} from 'lodash';

export default class ContactService {

    appConfig: any;
    static $inject = ['$window', 'callContext', 'JitsiProxy', 'XucPhoneState', 'CtiProxy', '$log', 'applicationConfiguration', '$translate', 'XucUtils', '$state', 'toast' ]
    constructor(
                private $window: Window,
                private callContext: any,
                private  JitsiProxy: any,
                private  XucPhoneState: any,
                private  CtiProxy: any,
                private  $log: any,
                private  applicationConfiguration: ApplicationConfiguration,
                private $translate: any,
                private XucUtils: any,
                private $state: any,
                private toast: any) {
        this.appConfig = this.applicationConfiguration.getCurrentAppConfig();
    }

    audioInviteContext(contact: ContactSheetModel) {
        return (!isNil(contact.actions[ContactSheetActionEnum.AudioInvite]) && this.userWithInviteToConferenceIcon(contact) || this.meetingRoomWithInviteToConferenceIcon(contact))
    }

    execContextualPhoneAction(contact: ContactSheetModel, nb: string, $event: Event) {
        if (this.audioInviteContext(contact)) {
            this.inviteToConference(contact, $event, nb)
        } else this.makeACall(contact, $event, nb)
    }

    getContextualPhoneClass(contact: ContactSheetModel) {
        if (this.audioInviteContext(contact)) {
            return "xivo-include-call-audioconf"
        } else return "xivo-telephone"
    }

    canShareLink(contact: ContactSheetModel): boolean {
        return contact.isMeetingroom && (contact.actions[ContactSheetActionEnum.ShareLink] !== undefined);
    }

    getFieldDataFormatted(contact: ContactSheetModel, field : ContactSheetDetailFields): string {
        switch (field.dataType) {
            case 'PhoneNumber':
                return contact.isMeetingroom ? '**' + field.data : this.XucUtils.prettyPhoneNb(field.data);
            default:
                return field.data;
        }
    }

    getPhoneStateBackColor(contact: any): string {
        if (contact.status.video === "Busy")
            return "user-videostatus-" + contact.status.video;
        if (contact.isMeetingroom) return "user-meetingroom";
        return "user-status" + contact.status.phone + (contact.keydown ? " lighter" : "");
    }

    getPhoneStateLabel(contact: ContactSheetModel): string {
        if (contact.status.video === 'Busy')
            return "USER_VIDEOSTATUS_" + contact.status.video;
        return "USER_STATUS_" + contact.status.phone;
    }

    navigateToContactEditor(contact: ContactSheetModel): void {
        if (contact.isMeetingroom) {
            const maybeMeetingRoomId = contact.sources.find((source: ContactSheetSources) => {
                return source.name === 'xivo_meetingroom'
            })?.id
            if (maybeMeetingRoomId) {
                this.$state.go("interface.meetingRooms", { id: maybeMeetingRoomId })
            } else {
                console.error("Cannot find meetingRoom id from the contact sources", contact)
            }
        } else {
            const maybeContactId = contact.sources.find((source: ContactSheetSources) => {
                return source.name === 'personal'
            })?.id
            if (maybeContactId) {
                this.$state.go("interface.personalContact", { id: maybeContactId })
            } else {
                console.error("Cannot find personal contact id from the contact sources", contact)
            }
        }
    }

    isJitsiAvailable(): boolean {
        return this.JitsiProxy.jitsiAvailable;
    }

    generateMeetingShareLink(contact: ContactSheetModel): string {
        let args = contact.actions[ContactSheetActionEnum.ShareLink].args;
        return `${this.$window.location.origin}${args ? args[0] : ''}`;
    }

    inviteToConference(contact: ContactSheetModel, $event: Event, nb: string = contact.actions[ContactSheetActionEnum.Call].args[0]): void  {
        $event.stopPropagation();
        let conference = this.XucPhoneState.getConference();
        if (conference !== undefined) {
            this.CtiProxy.conferenceInvite(conference.conferenceNumber, nb, 'User', true);
        }
    }

    userWithInviteToConferenceIcon(contact: ContactSheetModel): boolean {
        return this.canInviteToConference() && this.isCallable(contact) && !contact.isMeetingroom;
    }

    meetingRoomWithInviteToConferenceIcon(contact: ContactSheetModel): boolean {
        return this.canInviteToConference() && contact.isMeetingroom && this.isJitsiAvailable();
    }

    canInviteToMeetingRoom(contact: ContactSheetModel): boolean {
            return this.JitsiProxy.videoIsOngoing() &&
            !contact.isMeetingroom &&
            !contact.isPersonal &&
            contact.actions[ContactSheetActionEnum.Video];
    }

    canInviteToConference(): boolean {
        return this.XucPhoneState.getConferenceOrganizer();
    }

    isCallable(contact: ContactSheetModel): boolean {
        return contact.actions[ContactSheetActionEnum.Call] != undefined
            || contact.actions[ContactSheetActionEnum.Video] != undefined
            || this.callContext.isMeetingRoom(contact);
    }

    inviteToMeetingRoom(contact: ContactSheetModel): void {
        this.JitsiProxy.inviteToMeetingRoom(contact.actions[ContactSheetActionEnum.Video].args[0], contact.name);
    }

    userWithCallIcon(contact: ContactSheetModel): boolean {
        return !this.canInviteToConference()
            && this.isCallable(contact)
            && !contact.isMeetingroom
            && !this.hasJustEmail(contact);
    }

    meetingRoomWithCallIcon(contact: ContactSheetModel): boolean {
        return !this.canInviteToConference()
            && contact.isMeetingroom
            && this.isJitsiAvailable();
    }

    hasJustEmail(contact: ContactSheetModel): boolean {
        const emailAction = contact.actions[ContactSheetActionEnum.Mail];
        const otherAction = Object
            .entries(contact.actions)
            .filter(([key, value]) => key != "Email" && value)
        if (!otherAction
            && emailAction
            && emailAction.args.length != 0
            && emailAction.args[0] != ""){
            return true
        } else return false
    }

    getMailToLink(contact: ContactSheetModel): string {
        return this.generateMailToLink(contact);
    }


    getFieldMailToLink(contact: ContactSheetModel, field: ContactSheetDetailFields): string {
        return this.generateMailToLink(contact, field.data);
    }

    getFieldAction(field: ContactSheetDetailFields, contact: ContactSheetModel, $event: Event) : void | string {
        $event.stopPropagation();
        switch (field.dataType) {
            case 'PhoneNumber':
                return this.makeACall(contact, $event, field.data);
            case 'Mail':
                return this.getMailToLink(contact);
            default:
                return;
        }
    }

    startPointToPointVideoCall(contact: ContactSheetModel, $event: Event): void {
        $event.stopPropagation();
        this.JitsiProxy.startPointToPointVideoCall(contact.actions[ContactSheetActionEnum.Video].args[0], contact.name);
    }


    dial(number?: string): void {
        this.callContext.normalizeDialOrAttTrans(number);
    }

    makeACall(contact: ContactSheetModel, $event: Event, phone?: string): void {
        $event.stopPropagation();
        let args =  contact.actions[ContactSheetActionEnum.Call].args;
        let number = args ? args[0] : '';
        if (contact.isMeetingroom) {
            return this.dial('**' + number);
        }
        return this.dial(phone || number);
    }

    makeAVideoCall(contact: ContactSheetModel, $event: Event): void {
        $event.stopPropagation();
        if (contact.isMeetingroom) {
            return this.join(contact);
        }
        return this.startPointToPointVideoCall(contact, $event);
    }

    _isPersonal(meetingRoomContact: ContactSheetModel): string {
        return meetingRoomContact.isPersonal ? 'personal' : 'static';
    }

    join(contact: ContactSheetModel, $event?: Event): void {
        $event?.stopPropagation();
        if(contact.isMeetingroom) {
            this.JitsiProxy.startVideo(contact.actions[ContactSheetActionEnum.Video].args[0], this._isPersonal(contact)).then(() =>
                this.XucPhoneState.getCallsNotOnHold().forEach((call: any) => this.CtiProxy.hold(call.uniqueId))
            ).catch( (error: any) => this.$log.error("Cannot start meeting room", error));
        }
    }

    generateMailToLink(contact: ContactSheetModel, fieldEmail?: string): string {
        const template = this.appConfig.mailTemplate;
        const fullName = contact.name;
        const email = fieldEmail === undefined ? (contact.actions[ContactSheetActionEnum.Mail] && contact.actions[ContactSheetActionEnum.Mail].args && contact.actions[ContactSheetActionEnum.Mail].args[0] ? contact.actions[ContactSheetActionEnum.Mail].args[0] : '') : fieldEmail;
        const calls  = this.XucPhoneState.getCalls();

        const callerNum = _.get(calls.find((call: any) => call.otherDN !== 'anonymous'), 'otherDN');
        const subject = this.replaceVariables(template.subject, callerNum, fullName);
        const body = this.replaceVariables(template.body, callerNum, fullName);

        return encodeURI(`mailTo:${email}?subject=${subject}&body=${body}`);
    }

    replaceVariables(text: string, callerNum: string, fullName: string): string {
        const replace = (text: string, pattern: string, value: string, fallback: string) => {
            let v = value ? value : `###${this.$translate.instant(fallback)}###`;
            let r = new RegExp(pattern,"g");
            return text.replace(r, v);
        };

        text = replace(text, '{callernum}', callerNum, 'PLACEHOLDER_NUMBER');
        text = replace(text, '{dstname}', fullName, 'PLACEHOLDER_NAME');
        return text;
    }

    getTranslationType = (type: string) => {
        switch (type) {
        case 'meetingroom':
          return 'MEETINGROOM_COPIED';
        case 'phone':
          return 'PHONE_COPIED';
        case 'email':
          return 'EMAIL_COPIED';
        }
      };

    clipboardPopup(field: string, $event: Event) {
        $event.stopPropagation();
        this.toast({
            duration: 3000,
            message: this.$translate.instant(this.getTranslationType(field)),
            className: 'alert-copied',
            position: 'center',
            container: '.toast-container'
        });
    }


    getActionsProperties(closeContactSheetCallback?: Function): ActionsProperties {
        const actions: ActionsProperties = {
            'Edit': {
                class: 'edit',
                id: 'edit-contact-button',
                icon: `<span class='nd-action-button'><i class='xivo-modifier'></i></span>`,
                title: 'EDIT',
                onClick: (contact: ContactSheetModel, $event: Event) => {
                    $event.stopPropagation();
                    this.navigateToContactEditor(contact);
                },
                displayCondition: (contact: ContactSheetModel) => contact.isPersonal && this.appConfig.appType == AppType.UCAssistant,
                disableCondition: (contact: ContactSheetModel) => contact.actions['Edit'].disable
            },
            'Call': {
                class: 'call',
                id: 'call-contact-button',
                icon: `<span class="nd-action-button"><i class='xivo-telephone'></i></span>`,
                title: 'PHONE_CALL',
                onClick: (contact: ContactSheetModel, $event: Event) => {
                    $event.stopPropagation();
                    this.makeACall(contact, $event);
                    closeContactSheetCallback?.()
                },
                displayCondition: (contact: ContactSheetModel) => !isNil(contact.actions[ContactSheetActionEnum.Call]) && this.userWithCallIcon(contact) || this.meetingRoomWithCallIcon(contact) || !isNil(contact.actions[ContactSheetActionEnum.AudioInvite]) && this.userWithInviteToConferenceIcon(contact) || this.meetingRoomWithInviteToConferenceIcon(contact),
                disableCondition: (contact: ContactSheetModel) => contact.actions['Call'].disable
            },
            'Video': {
                class: 'video-call',
                id: 'video-contact-button',
                icon: `<span class="nd-action-button"><i class="xivo-appel-visio"></i></span>`,
                title: 'P2P_VIDEO_CALL',
                onClick: (contact: ContactSheetModel, $event: Event) => {
                    $event.stopPropagation();
                    contact.isMeetingroom ? this.join(contact, $event) : this.startPointToPointVideoCall(contact, $event);
                    closeContactSheetCallback?.()
                },
                displayCondition: (contact: ContactSheetModel) => !isNil(contact.actions[ContactSheetActionEnum.Video]) && this.isJitsiAvailable() && !this.canInviteToMeetingRoom(contact),
                disableCondition: (contact: ContactSheetModel) => contact.actions['Video'].disable
            },
            'Chat': {
                class: 'comments',
                id: 'chat-contact-button',
                icon: `<span class="nd-action-button"><i class="xivo-message"></i></span>`,
                title: 'CHAT',
                onClick: (contact: ContactSheetModel, $event: Event) => {
                    $event.stopPropagation();
                    let username = contact.actions.Chat ? contact.actions.Chat.args[0] : '';
                    this.$state.go(this.appConfig.routing + '.conversation', {remoteParty: username});
                    closeContactSheetCallback?.()
                },
                displayCondition: (contact: ContactSheetModel) => !isNil(contact.actions[ContactSheetActionEnum.Chat]),
                disableCondition: (contact: ContactSheetModel) => contact.actions['Chat'].disable || !this.appConfig.chat
            },
            'Mail': {
                class: '',
                id: 'mail-contact-button',
                icon: `<span class="nd-action-button"><i class='fas fa-envelope'></i></span>`,
                title: 'SEND_EMAIL',
                onClick: (contact: ContactSheetModel, $event: Event) => {
                    $event.stopPropagation(); window.location.href = this.generateMailToLink(contact);
                },
                displayCondition: (contact: ContactSheetModel) => !isNil(contact.actions[ContactSheetActionEnum.Mail]),
                disableCondition: (contact: ContactSheetModel) => contact.actions['Mail'].disable
            },
            'ShareLink': {
                class: 'share',
                id: 'sharelink-contact-button',
                icon: `<span class='nd-action-button'><i class='fas fa-share-alt'></i></span>`,
                title: 'SHARE_LINK',
                onClick: (contact: ContactSheetModel, $event: Event) => {
                    $event.stopPropagation(); navigator.clipboard.writeText(this.generateMeetingShareLink(contact));
                    this.clipboardPopup('meetingroom', $event);
                },
                displayCondition: (contact: ContactSheetModel) => !isNil(contact.actions[ContactSheetActionEnum.ShareLink]),
                disableCondition: (contact: ContactSheetModel) => contact.actions['ShareLink'].disable
            },
            'VideoInvite': {
                class: 'video-call',
                id: 'videoinvite-contact-button',
                icon: `
                <span class='nd-action-button'>
                <div class="fa-users-plus-wrapper">
                    <span><i class="fa fa-users fa-users-plus-0"></i></span>
                    <i class="fa fa-plus fa-users-plus-1" aria-hidden="true"></i>
                </div>
                </span>
                `,
                title: 'INVITE_TO_MEETING_ROOM',
                onClick: (contact: ContactSheetModel, $event: Event) => {
                    $event.stopPropagation();
                    this.inviteToMeetingRoom(contact);
                    closeContactSheetCallback?.()
                },
                displayCondition: (contact: ContactSheetModel) => !isNil(contact.actions[ContactSheetActionEnum.VideoInvite]) && this.isJitsiAvailable() && this.canInviteToMeetingRoom(contact),
                disableCondition: (contact: ContactSheetModel) => contact.actions['VideoInvite'].disable
            },
            'AudioInvite': {
                class: 'audio-invite',
                id: 'audioinvite-contact-button',
                icon: `
                <span class='nd-action-button invite-to-conference-icon'>
                <span class="xivo-phone-plus-wrapper">
                    <span><i class="xivo-telephone"></i></span>
                    <i class="fa fa-plus xivo-phone-plus" aria-hidden="true"></i>
                    </i>
                </span>
                </span>`,
                title: 'INVITE_TO_CONFERENCE',
                onClick: (contact: ContactSheetModel, $event: Event) => {
                    $event.stopPropagation();
                    this.inviteToConference(contact, $event);
                    closeContactSheetCallback?.()
                },
                displayCondition: (contact: ContactSheetModel) => !isNil(contact.actions[ContactSheetActionEnum.AudioInvite]) && this.userWithInviteToConferenceIcon(contact) || this.meetingRoomWithInviteToConferenceIcon(contact),
                disableCondition: (contact: ContactSheetModel) => contact.actions['AudioInvite'].disable
            }
        }

        return actions
    }

    setInitials(contact: ContactSheetModel): String|undefined {
        if (!contact.name) return;

        contact.initials =  (contact.name.match(/\b\w/g) || []).slice(0, 4).join(' ');
        return contact.initials
      };
}
