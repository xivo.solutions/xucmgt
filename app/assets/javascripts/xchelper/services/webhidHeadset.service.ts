import {ILogService, IScope} from "angular";
import {RichWindow} from "../../RichWindow";
import { HeadsetsProvider } from "xchelper/models/WebHIDHeadsets.model";
import { ApplicationConfiguration } from "xccti/services/applicationConfiguration.provider";

interface HIDDevice {
  productName: string;
  vendorId: number;
  productId: number;
  usagePage: number;
  usage: number;
  interface: number;
  collections: HIDCollectionInfo[];
  open(): Promise<void>;
  close(): Promise<void>;
  addEventListener(evt: string, cb: Function): undefined
  receiveFeatureReport(id: number): any
}

interface HIDCollectionInfo {
  usage: number;
  usagePage: number;
  inputReports: HIDReportInfo[];
}

interface HIDReportInfo {
  reportId: number;
  items: HIDReportItem[];
}

interface HIDReportItem {
  usages: Array<number>;
  usagePage: number;
}

interface HID {
  requestDevice(options: { filters: Array<Object> }): Promise<HIDDevice[]>;
  getDevices(): Promise<HIDDevice[]>;
}

interface HIDInputReportEvent extends Event {
  data: {buffer?: Array<any>, getUint8: (n: number) => number}
  device: HIDDevice
  reportId: number
}

interface NavigatorWithHid extends Navigator {
  hid: HID
}

export default class WebHIDHeadsetService {
  
  static $inject = ["$window", "$log", "$rootScope", "XucPhoneEventListener", "CtiProxy", "electronWrapper", "applicationConfiguration", "localStorageService"]
  private device: HIDDevice | undefined
  private ringing = false;
  private established = false;
  private headsetProvider: HeadsetsProvider;
  private headsetModel: number | undefined;
  private headsetName: string | undefined;

  public HEADSET_SELECTED = "WEBHID_DEVICE_SELECTED"
  
  constructor(
    private $window: RichWindow,
    private $log: ILogService,
    private $rootScope: IScope,
    private XucPhoneEventListener: any,
    private CtiProxy: any,
    private electronWrapper: any,
    private applicationConfiguration: ApplicationConfiguration,
    private localStorageService: any
  ) {
    this.$log.info("Starting webHID headset service");
    this.headsetProvider = new HeadsetsProvider(this.applicationConfiguration);
    this.XucPhoneEventListener.addRingingHandler($rootScope, () => { this.ringing = true });
    this.XucPhoneEventListener.addEstablishedHandler($rootScope, () => { this.ringing = false; this.established = true });
    this.XucPhoneEventListener.addReleasedHandler($rootScope, () => { this.ringing = false; this.established = false });
    this.$rootScope.$on("WEBHID_INPUT", this.processElectronHIDInput.bind(this))
  }

  async init() {
    let filters = this.headsetProvider.getSupportedVendors();
    this.electronWrapper.isElectron() ? this.electronHandler(filters) : this.webHandler(filters);
  }

  private setInputListener(device: HIDDevice) {
    device.addEventListener('inputreport', (e: HIDInputReportEvent) => {
      const { data, reportId } = e;
      let eventCode
      if (data.buffer) {
        let uia = new Uint8Array(data.buffer)
        eventCode = uia[0]
      } else eventCode = data.getUint8(0)
      if (eventCode == 0) return
      this.processEvent(eventCode, reportId)
    })
  }

  private electronHandler(filters: Array<Object>) {
    this.electronWrapper.requestWebHID(filters);
  }

  private processElectronHIDInput(_: any, data: {vendorId: number, code: number, reportId: number, report: HIDReportInfo}) {
    console.debug("Received external HID input", data)
    this.headsetModel = this.headsetProvider.getHeadsetModelByVendorID(data.vendorId)
    if (this.headsetModel) {
      this.processEvent(data.code, data.reportId, data.report)
    } else console.warn(`This device is not supported for webHID`)
  }

  private async webHandler(filters: Array<Object>) {
    filters = filters.map(filter => { return {vendorId: filter}}) 
    let [device] = await (this.$window.navigator as NavigatorWithHid).hid.requestDevice({filters})
    if (device) {
      this.headsetModel = this.headsetProvider.getHeadsetModelByVendorID(device.vendorId)
      if (this.headsetModel) {
        await this.openDevice(device)
        this.localStorageService.set("HID_DEVICE", {vendorId: device.vendorId, productId: device.productId})
      } else console.warn(`This device is not supported for webHID: ${device.productName}`)
    }
  }

  public async openDevice(device: HIDDevice) {
    if (device) {
      try {
        await device.open()
        this.headsetModel = device.vendorId
        this.device = device;
        this.headsetName = this.device.productName;
        this.$rootScope.$broadcast(this.HEADSET_SELECTED, this.headsetName);
        this.setInputListener(this.device);
      } catch (e) {
        console.warn(`Could not open WebHID device ${device.productName}`)
        console.debug("Reason :", e)
      }
    }
  }

  public getDeviceFromBrowser(device: {vendorId: number, productId: number}) {
    return this.$window.navigator.hid.getDevices().then((devices: Array<HIDDevice>) => {
      return devices.find((d: HIDDevice) => d.vendorId == device.vendorId && d.productId == device.productId)
    })
  }

  public getCurrentDeviceName = () => {
    return this.headsetName
  }

  public setCurrentDeviceName = (name: string) => {
    return this.headsetName = name;
  }

  private processEvent = (eventCode: number, reportId: number, electronReport?: HIDReportInfo) => {
    console.debug(`Web HID event ${eventCode} on report ${reportId} for vendor ${this.headsetModel}`)
    let report = electronReport || this.getWebReport(reportId)
    let matchingAction = this.getMatchingAction(eventCode, report)
    if (matchingAction) this.executeMatchingAction(matchingAction)
  }

  private getWebReport = (reportId: number): HIDReportInfo | undefined => {
    let report = this.device?.collections
      .flatMap(col => col.inputReports)
      .find(report => report.reportId === reportId)

    if (report && report.items?.length > 1) {
      report.items[0].usages = report.items.reduce((acc: Array<number>, item: HIDReportItem) => acc.concat(item.usages), []);
      report.items = [report.items[0]]
    }

    return report
  }

  private getMatchingAction = (eventCode: number, report: HIDReportInfo | undefined): number | undefined => {
    let truthyBitPosition = this.firstTruthyBitPos(eventCode)

    for (const item of report?.items || []) {
      for (let i = 0; i < (item.usages?.length || 0); i++) {
        if (i === truthyBitPosition) {
          return item.usages[i];
        }
      }
    }
  }

  /*
    Return -1 if the parameter is undefined or zero
    Check if the last bit is falsy ((nb & 1) === 0)
    If it's falsy then shift all bits to the right by one step and re-do
    If it's truthy then return it's position  
  */
  private firstTruthyBitPos = (nb: number) => {
    if (!nb || nb == 0) return -1;
    let position = 0;
    while ((nb & 1) === 0) { 
        nb >>= 1; 
        position++; 
    }
    return position;
  }

  // For more infos on the code below, see HID Usage Tables standard
  private executeMatchingAction = (encodedAction: number) => {
    let [page, action] = this.toHIDCode(encodedAction)
    console.debug("Received action", `${page}:${action}`)
    switch (page) {
      case "B": // Telephony page
        switch (action) {
          case "20": // Hook switch
            this.hookSwitch()
            break
        }
        break
      case "C": // Consumer page
        switch (action) {
          case "B0": // Play
          case "B1": // Pause
          case "CD": // Toggle Play/Pause
            this.hookSwitch()
            break
        }
        break
    }
  }

  private toHIDCode = (action: number): Array<string> => {
    let decoded = action
      .toString(16)
      .toUpperCase()

    let usage = decoded.slice(-2)
    let page = decoded.slice(0, -4)

      // Page and usage
      return [page, usage]
  }

  private hookSwitch = () => {
    if (this.ringing) this.answer()
    else if (this.established) this.hangup()
  }

  private answer = () => {
    this.CtiProxy.answer()
  }

  private hangup = () => {
    this.CtiProxy.hangup()
  }
}
