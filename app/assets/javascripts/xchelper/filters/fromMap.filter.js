export default function fromMap() {
  return function(input) {
    var out = {};
    input.forEach((v, k) => out[k] = v);
    return out;
  };
}
