export default function trusted($sce) {
  return function(url) {
    return $sce.trustAsResourceUrl(url);
  };
}