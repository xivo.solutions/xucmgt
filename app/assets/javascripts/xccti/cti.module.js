import angular from 'angular';
import 'moment-duration-format';
import 'angularjs-toast/angularjs-toast.css';
import AgentStates from './services/AgentStates.service';
import XucUser from './services/XucUser.service';
import XucQueue from './services/XucQueue.service';
import XucQueueRecording from './services/XucQueueRecording.service';
import XucAgent from './services/XucAgent.service';
import XucAgentUser from './services/XucAgentUser.service';
import XucGroup from './services/XucGroup.service';
import XucQueueGroup from './services/XucQueueGroup.service';
import XucConfRoom from './services/XucConfRoom.service';
import XucVoiceMail from './services/XucVoiceMail.service';
import XucAgentTotal from './services/XucAgentTotal.service';
import XucQueueTotal from './services/XucQueueTotal.service';
import XucDirectory from './services/XucDirectory.service';
import XucCallback from './services/XucCallback.service';
import XucPhoneState from './services/XucPhoneState.service';
import XucThirdPartyLoginUrl from './services/XucThirdPartyLoginUrl.service';
import XucLink from './services/XucLink.service';
import XucUtils from './services/XucUtils.service';
import XucTableHelper from './services/XucTableHelper.service';
import XucMembership from './services/XucMembership.service';
import XucPhoneEventListener from './services/XucPhoneEventListener.service';
import XucVideoEventManager from './services/XucVideoEventManager.service';
import XucSwitchboard from './services/XucSwitchboard.service';
import XucPhoneHintService from './services/XucPhoneHintService.service';
import XucNotification from './services/XucNotification.service';
import XucCallHistory from './services/XucCallHistory.service';
import XucSheet from './services/XucSheet.factory';
import XucChat from './services/XucChat.service';
import XucFailedDestination from './services/XucFailedDestination.factory';
import remoteConfiguration from './services/remoteConfiguration.provider.js';
import applicationConfiguration from './services/applicationConfiguration.provider';
import OIDCHelper from './services/OIDCHelper.service';

import { queueTime, totalTime, timeInState, booleanText, dateTime, dashWhenEmpty, prettyPhoneNb, prepareServerUrl, userDisplayName }
  from './filters/Xuc.filters';
import XucThirdPartyService from "xccti/services/XucThirdParty.service";
import CtiStatusService from "xccti/services/CtiStatus.service";

var ctiModule = angular.module('xcCti', ['pascalprecht.translate', 'angular-web-notification', 'ngSanitize', 'ngAnimate', 'angularjsToast']);

ctiModule.config(function ($translateProvider) {
  $translateProvider.translations('fr', {
    AgentReady: 'Disponible',
    AgentOnPause: 'En pause',
    AgentLoggedOut: 'Déconnecté',
    AgentOnCall: 'Appel en cours',
    AgentOnIncomingCall: 'Appel Entrant',
    AgentOnOutgoingCall: 'Appel Sortant',
    AgentOnWrapup: 'Post Appel',
    AgentOnAcdCall: 'Appel ACD en cours',
    AgentDialing: 'Numérotation',
    AgentRinging: 'Sonnerie',
    Fax: 'Fax',
    Email: 'Réponse par mail',
    NoAnswer: 'Non réponse',
    Answered: 'Répondu',
    Callback: 'A rappeler',
    IncomingNotificationTitle: 'Appel Entrant',
    IncomingVideoNotificationTitle: 'Invitation à une vidéoconférence',
    ReceivedMessage: 'Message reçu',
    OnHoldNotificationTitle: 'Appel en attente depuis',
    DayAbbrv: 'j',
    ChatError: 'Erreur avec le serveur de Chat. Merci de contacter votre administrateur',
    ChatOffline: 'Message envoyé mais l\'utilisateur est hors-ligne',
    webRTCAudioDisabled: 'Le périphérique audio n\'est pas accessible'
  });
  $translateProvider.translations('en', {
    AgentReady: 'Ready',
    AgentOnPause: 'Paused',
    AgentLoggedOut: 'Logged Out',
    AgentOnCall: 'On Call',
    AgentOnIncomingCall: 'Incoming Call',
    AgentOnOutgoingCall: 'Outgoing Call',
    AgentOnWrapup: 'Wrapup',
    AgentOnAcdCall: 'ACD Call',
    AgentDialing: 'Dialing',
    AgentRinging: 'Ringing',
    Fax: 'Fax',
    Email: 'Handled by mail',
    NoAnswer: 'No answer',
    Answered: 'Answered',
    Callback: 'To reschedule',
    IncomingNotificationTitle: 'Incoming Call',
    IncomingVideoNotificationTitle: 'Meetingroom invitation',
    ReceivedMesage: 'Message received',
    OnHoldNotificationTitle: 'Call on-hold since',
    DayAbbrv: 'd',
    ChatError: 'Error with chat server. Please contact your administrator',
    ChatOffline: 'Message delivered to offline user',
    webRTCAudioDisabled: 'The audio device cannot be accessed'
  });
});

ctiModule.factory('AgentStates', AgentStates);
ctiModule.factory('XucUser', XucUser);
ctiModule.factory('XucQueue', XucQueue);
ctiModule.factory('XucQueueRecording', XucQueueRecording);
ctiModule.factory('XucAgent', XucAgent);
ctiModule.factory('XucGroup', XucGroup);
ctiModule.factory('XucQueueGroup', XucQueueGroup);
ctiModule.factory('XucConfRoom', XucConfRoom);
ctiModule.factory('XucVoiceMail', XucVoiceMail);
ctiModule.factory('XucAgentTotal', XucAgentTotal);
ctiModule.factory('XucQueueTotal', XucQueueTotal);
ctiModule.factory('XucDirectory', XucDirectory);
ctiModule.factory('XucCallback', XucCallback);
ctiModule.factory('XucPhoneState', XucPhoneState);
ctiModule.factory('XucThirdPartyLoginUrl', XucThirdPartyLoginUrl);
ctiModule.factory('XucLink', XucLink);
ctiModule.factory('XucUtils', XucUtils);
ctiModule.factory('XucTableHelper', XucTableHelper);
ctiModule.factory('XucMembership', XucMembership);
ctiModule.factory('XucPhoneEventListener', XucPhoneEventListener);
ctiModule.factory('XucVideoEventManager', XucVideoEventManager);
ctiModule.factory('XucSwitchboard', XucSwitchboard);
ctiModule.factory('XucPhoneHintService', XucPhoneHintService);
ctiModule.factory('XucNotification', XucNotification);
ctiModule.factory('XucAgentUser', XucAgentUser);
ctiModule.factory('XucCallHistory', XucCallHistory);
ctiModule.factory('XucSheet', XucSheet);
ctiModule.factory('XucChat', XucChat);
ctiModule.factory('XucFailedDestination', XucFailedDestination);

ctiModule.filter('queueTime', queueTime);
ctiModule.filter('totalTime', totalTime);
ctiModule.filter('timeInState', timeInState);
ctiModule.filter('booleanText', booleanText);
ctiModule.filter('dateTime', dateTime);
ctiModule.filter('dashWhenEmpty', dashWhenEmpty);
ctiModule.filter('prettyPhoneNb', prettyPhoneNb);
ctiModule.filter('prepareServerUrl', prepareServerUrl);
ctiModule.filter('userDisplayName', userDisplayName);

ctiModule.provider('remoteConfiguration', remoteConfiguration);
ctiModule.provider('applicationConfiguration', applicationConfiguration);
ctiModule.service('XucThirdPartyService', XucThirdPartyService);
ctiModule.service('CtiStatusService', CtiStatusService);
ctiModule.service('OIDCHelper', OIDCHelper);
