export interface XucPhoneState {
    STATE_DIALING: string
    STATE_RINGING: string
    STATE_ESTABLISHED: string
    STATE_AVAILABLE: string
    STATE_ONHOLD: string
    getState: () => string
    getCalls: () => any[]
    getCallsOnHold : () => any[]
    isInCall: () => boolean
    getDeviceConference: () => { calls: any[], active: boolean }
    dial: (number: string) => void
    isPhoneAvailable: () => boolean
    isPhoneOffHook: (status: string) => boolean
    isPhoneRinging: (status: string) => boolean
    isConferenceAndIsMuted: (uniqueId: number) => boolean
    getConference: () => any
    addStateHandler: (f: Function) => any
    getConferenceOrganizer: () => boolean
}