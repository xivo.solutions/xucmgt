import {CtiStatus, CtiStatusEnum} from "../../xchelper/models/CtiStatus.model";
import {CtiProxy} from "./ctiProxy";
import {IRootScopeService} from "angular";
import {RichWindow} from "../../RichWindow";

export default class CtiStatusService {
    static $inject = ["$rootScope", "$window", "CtiProxy"];

    currentStatus: string = '';
    _ctiStatuses : CtiStatus[] = [];

    constructor(private $rootScope: IRootScopeService, private $window: RichWindow, private CtiProxy: CtiProxy) {
        this.$window?.Cti.setHandler(this.$window?.Cti.MessageType.CTISTATUSES, this.onCtiStatuses.bind(this));

    }

    set ctiStatuses(_ctiStatuses: CtiStatus[]) {
        this._ctiStatuses = _ctiStatuses;
    }

    get ctiStatuses(): CtiStatus[] {
        return this._ctiStatuses;
    }

    onCtiStatuses(_ctiStatuses: CtiStatus[]) {
        this.ctiStatuses = _ctiStatuses.sort((a, b) => a.status > b.status ? 1 : b.status > a.status ? -1 : 0);
        this.$rootScope.$broadcast('AgentUserStatusesLoaded');
    }

    mapAgentStateToCtiStatus(agentState: string): CtiStatusEnum {
        switch (agentState) {
            case 'AgentReady':
                return CtiStatusEnum.AgentReady;
            case 'AgentOnPause':
                return CtiStatusEnum.AgentOnPause;
            case 'AgentLoggedOut':
                return CtiStatusEnum.AgentLoggedOut;
            case 'AgentOnWrapup':
                return CtiStatusEnum.AgentOnWrapup;
            case 'AgentOnCall' :
                return CtiStatusEnum.AgentOnCall;
            case 'AgentDialing' :
                return CtiStatusEnum.AgentDialing;
            case 'AgentRinging' :
                return CtiStatusEnum.AgentRinging;
            case 'AgentLogin':
                return CtiStatusEnum.AgentLogin;
            default:
                return CtiStatusEnum.AgentReady;
        }
    }

    mapCtiStatusToAgentState(ctiStatus: CtiStatus): string {
        return CtiStatusEnum[ctiStatus.status];
    }

    switchAgentStateCtiStatus(status: CtiStatusEnum, currentStatusName: string) {
        switch (status) {
            case CtiStatusEnum.AgentReady:
                this.$window?.Cti.unpauseAgent();
                break;
            case CtiStatusEnum.AgentOnPause:
                if(currentStatusName != "" && currentStatusName != null)
                  this.$window?.Cti.pauseAgent(undefined, currentStatusName);
                else this.$window?.Cti.pauseAgent();
                break
            case CtiStatusEnum.AgentLoggedOut:
                this.CtiProxy.stopUsingWebRtc();
                this.$window?.Cti.logoutAgent();
                break
            default:
                break;
        }
        this.currentStatus = currentStatusName;
    }

    getPossibleCtiStatus(status: CtiStatusEnum) {
            switch (status) {
                case CtiStatusEnum.AgentOnPause:
                    return this.ctiStatuses.filter((elem: CtiStatus) => elem.name !== this.currentStatus)
                case CtiStatusEnum.AgentOnWrapup:
                    return this.ctiStatuses;
                case CtiStatusEnum.AgentReady:
                case CtiStatusEnum.AgentOnCall:
                case CtiStatusEnum.AgentDialing:
                case CtiStatusEnum.AgentRinging:
                case CtiStatusEnum.AgentLogin:
                    return this.ctiStatuses.filter((elem: CtiStatus) => elem.status !== CtiStatusEnum.AgentReady)
                case CtiStatusEnum.AgentLoggedOut:
                    return this.ctiStatuses.filter((elem: CtiStatus) => elem.status === CtiStatusEnum.AgentLoggedOut)
            }
    }

}