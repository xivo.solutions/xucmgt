export enum XucAuthErrorCode {
  NoStoredCredentials = "NoStoredCredentials",
  LinkClosed = "LinkClosed",
  NoResponse = "NoResponse",
  ThirdPartyNoResponse = "3rdPartyNoResponse",
  LoggedInOnAnotherPhone = "LoggedInOnAnotherPhone",
  EmptyLogin = "EmptyLogin",
  UserNotFound = "UserNotFound",
  InvalidCredentials = "InvalidCredentials",
  InvalidToken = "InvalidToken",
  InvalidJson = "InvalidJson",
  BearerNotFound = "BearerNotFound",
  AuthorizationHeaderNotFound = "AuthorizationHeaderNotFound",
  TokenExpired = "TokenExpired",
  UnhandledError = "UnhandledError",
  SsoAuthenticationFailed = "SsoAuthenticationFailed",
  CasServerUrlNotSet = "CasServerUrlNotSet",
  CasServerInvalidResponse = "CasServerInvalidResponse",
  CasServerInvalidParameter = "CasServerInvalidParameter",
  CasServerInvalidRequest = "CasServerInvalidRequest",
  CasServerInvalidTicketSpec = "CasServerInvalidTicketSpec",
  CasServerUnauthorizedServiceProxy = "CasServerUnauthorizedServiceProxy",
  CasServerInvalidProxyCallback = "CasServerInvalidProxyCallback",
  CasServerInvalidTicket = "CasServerInvalidTicket",
  CasServerInvalidService = "CasServerInvalidService",
  CasServerInternalError = "CasServerInternalError",
  OidcNotEnabled = "OidcNotEnabled",
  OidcInvalidParameter = "OidcInvalidParameter",
  OidcAuthenticationFailed = "OidcAuthenticationFailed",
  InvalidConfiguration = "InvalidConfiguration",
  Logout = "Logout",
  RequirePhoneNumber = "RequirePhoneNumber",
  NotAnAgent = "NotAnAgent",
  SwitchNumber = "SwitchNumber",
  PhoneNumberUnknown = "PhoneNumberUnknown"
}

export interface XucAuthError {
  readonly error: XucAuthErrorCode
  readonly message?: string
}
