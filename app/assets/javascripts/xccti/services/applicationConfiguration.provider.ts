import { ILogService } from 'angular';
import _ from 'lodash';

export enum AppType {
  CCAgent = 'ccAgent',
  Switchboard = 'switchboard',
  UCAssistant = 'ucAssistant'
}

type Headset = {
  "vendorId": number
}

export class AppConfig {
  constructor(
    public appType: AppType,
    public liveStats = false,
    public keyBindings = new Set(),
    public queueControl = false,
    public chat = false,
    public routing = '',
    public mailTemplate = {
      "subject": "",
      "body": ""
    },
    public headsetSupportedVendors: Array<number> = []
  ) {}
}

export interface ApplicationConfiguration {
  getCurrentAppConfig: () => AppConfig
}

export default class applicationConfiguration {

  public defaultConfig: AppConfig
  public ccAgentConfig: AppConfig
  public switchBoardConfig: AppConfig

  constructor(){
    this.defaultConfig = new AppConfig(AppType.UCAssistant)
    this.defaultConfig.keyBindings = new Set(["F10"])
    this.defaultConfig.chat = true
    this.defaultConfig.routing = "interface"

    this.ccAgentConfig = new AppConfig(AppType.CCAgent)
    this.ccAgentConfig.liveStats = true
    this.ccAgentConfig.keyBindings = new Set(["F3", "F4", "F7", "F10"])
    this.ccAgentConfig.chat = false
    this.ccAgentConfig.routing = "content"

    this.switchBoardConfig = new AppConfig(AppType.Switchboard)
    this.switchBoardConfig.keyBindings = new Set(["F3", "F4", "F7", "F8", "F9", "F10"])
    this.switchBoardConfig.chat = true
    this.switchBoardConfig.routing = "content"
  }

  $get = (remoteConfiguration: any, $log: ILogService) => {

    if (remoteConfiguration.isAgent()){
      remoteConfiguration.getBoolean("showQueueControls").then((value: boolean) =>
        this.ccAgentConfig.queueControl = value
      );
    }

    remoteConfiguration.getBoolean("disableChat").then((value: boolean) => {
      if (value) {
        this.defaultConfig.chat = false;
        this.switchBoardConfig.chat = false;
      }
    });

    remoteConfiguration.get('email', false).then((template: {subject: string, body: string}) => {
      if (_.has(template, ['subject']) &&
          _.has(template, ['body'])) {
        this.defaultConfig.mailTemplate =
        this.switchBoardConfig.mailTemplate =
        this.ccAgentConfig.mailTemplate = template;
      } else {
        $log.warn('email template configured is not containing subject or email field, default empty template will be used');
      }
    });

    remoteConfiguration.get('headsets', false).then((headsets: Array<{vendorId: number}>) => {
      $log.info("Got headsets configuration", headsets)
      if (headsets.length > 0) {
        let vendors = headsets.map(h => {return h.vendorId})
        this.defaultConfig.headsetSupportedVendors =
        this.switchBoardConfig.headsetSupportedVendors =
        this.ccAgentConfig.headsetSupportedVendors = vendors;
      }
    });

    const _getCurrentAppConfig = (): AppConfig => {
      if (remoteConfiguration.hasSwitchBoard()) {
        return this.switchBoardConfig;
      }
      if (remoteConfiguration.isAgent()) {
        return this.ccAgentConfig;
      }
      return this.defaultConfig;
    };

    return {
      getCurrentAppConfig: _getCurrentAppConfig
    };
  };

}
