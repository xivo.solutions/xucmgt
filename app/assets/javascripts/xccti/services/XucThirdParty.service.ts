import angular, {IHttpService, ILogService, IQService, IRootScopeService} from "angular";
import {RichWindow} from "../../RichWindow";
import {isNil} from "lodash";

type ThirdPartyAction = {
    action: string,
    url: string,
    event: string,
    autopause: boolean,
    triggered: boolean,
    autopauseReason: any
}

export default class XucThirdPartyService {
    static $inject = ["$rootScope", "$http", "$q", "$window", "XucPhoneEventListener", "XucUser", "XucQueue", "XucAgent", "XucLink", "$log"];

    _actionEventName = 'XucThirdParty.Action';
    _clearEventName = 'XucThirdParty.Clear';
    _thirdPartyWs: string | undefined;
    _thirdPartyActions: Array<ThirdPartyAction> = []
    _wasEstablished = false;
    _$log: ILogService;

    constructor(
        private $rootScope: IRootScopeService,
        private $http: IHttpService,
        private $q: IQService,
        private $window: RichWindow,
        private XucPhoneEventListener: any,
        private XucUser: any,
        private XucQueue: any,
        private XucAgent: any,
        private XucLink: any,
        private $log: any,) {
        this._$log = $log;
        this._$log.debug('XucThirdParty service initialization');


        this.XucPhoneEventListener.addRingingHandler(this.$rootScope, this._onRinging.bind(this));
        this.XucPhoneEventListener.addEstablishedHandler(this.$rootScope, this._onEstablished.bind(this));
        this.XucPhoneEventListener.addReleasedHandler(this.$rootScope, this._onReleased.bind(this));

    }

    setThirdPartyWs(url: string) {
        this._thirdPartyWs = url;
    };

    /**
     * Subscribe to Third party Action events. The callback will be called
     * when a third party action must be done
     * @param scope The scope containing the callback
     * @param callback The callback function called with the action required as argument.
     * @private
     */

    addActionHandler(scope: any, callback: any) {
        let handler = this.$rootScope.$on(this._actionEventName, function (event, action) {
            callback(action);
        });
        scope.$on('$destroy', handler);
    };

    addClearHandler(scope: any, callback: any) {
        let handler = this.$rootScope.$on(this._clearEventName, callback);
        scope.$on('$destroy', handler);
    };

    clearAllActions() {
        for (let action of this._thirdPartyActions) {
            if (action.autopause) {
                this.XucAgent.unpause();
                break;
            }
        }
        this.$rootScope.$emit(this._clearEventName);
        this._thirdPartyActions = [];
    };


    private _callThirdPartyWs(data: any) {
        if (!this._thirdPartyWs) {
            let defer = this.$q.defer();
            defer.reject("XucThirdParty not initialized with WS Url");
            return defer.promise;
        }

        return this.$http.post(this._thirdPartyWs, JSON.stringify(data))
            .then((response: any) => {
                this._$log.debug(`Got WS Response ${JSON.stringify(response.data)}`);
                this._thirdPartyActions.push(response.data);
            }, () => {
                this._$log.error(`Error while fetching response from ${this._thirdPartyWs}`);
            });
    };

    private _checkAndFireEvent(phoneEvent: any) {
        if (this._thirdPartyActions.length > 0) {
            if (phoneEvent === "EventEstablished") {
                this._wasEstablished = true;
            }

            this._thirdPartyActions.forEach(action => {
                if (phoneEvent === action.event && !action.triggered) {
                    if (phoneEvent !== "EventReleased" || (phoneEvent === "EventReleased" && this._wasEstablished)) {
                        this._$log.debug("Firing ThirdParty action");
                        if (action.autopause) {
                            if (angular.isDefined(action.autopauseReason)) {
                                this.XucAgent.pauseWithReason(action.autopauseReason);
                            } else {
                                this.XucAgent.pause();
                            }
                        }
                        this.$rootScope.$emit(this._actionEventName, angular.copy(action));
                        action.triggered = true;
                    }
                }
            })
        }
    };

    private _onRinging(event: any) {
        this._$log.debug(`_onRinging ${JSON.stringify(event)}`);
        let promises = [this.XucUser.getUserAsync(), this.XucLink.whenLogged()];
        if (typeof (event.queueName) !== "undefined") {
            promises.push(this.XucQueue.getQueueByNameAsync(event.queueName));
        }
        this._wasEstablished = false;

        this.$q.all(promises).then((results) => {
            let user = results[0];
            let queue;
            let token = results[1].token;
            if (results.length > 2) {
                queue = results[2];
            }

            let data = {
                user: {
                    userId: user.userId,
                    agentId: user.agentId,
                    firstName: user.firstName,
                    lastName: user.lastName,
                    fullName: user.fullName,
                    token: token
                },
                callee: event.DN,
                caller: event.otherDN,
                userData: event.userData,
                callDataCallId: event.linkedId,
                queue: typeof (queue) !== "undefined" ? {
                    id: queue.id,
                    name: queue.name,
                    displayName: queue.displayName,
                    number: queue.number
                } : undefined
            };
            if (typeof (queue) === "undefined") {
                delete data['queue'];
            }
            this._callThirdPartyWs(data)
                .then(() => {
                    this._checkAndFireEvent(this.XucPhoneEventListener.EVENT_RINGING);
                })
                .catch(() => {
                    this._$log.debug("Service seems disabled or not available");
                });
        });

    };

    _onEstablished(event: any) {
        this._$log.debug("_onEstablished");
        this._checkAndFireEvent(event.eventType);
    };

    _sendCountDownEvent() {
        if (!isNil(document.getElementById('thirdpartyframe'))) {
            this._thirdPartyActions.forEach(action => {
                (document.getElementById('thirdpartyframe') as any).contentWindow.postMessage('xivo-StartCountDown', action ? action.url : "");
            })
        }
    };


    _onReleased(event: any) {
        this._$log.debug("_onReleased");
        this._checkAndFireEvent(event.eventType);
        this._sendCountDownEvent();
        this._thirdPartyActions = this._thirdPartyActions.filter(action => action.triggered);
    };

}