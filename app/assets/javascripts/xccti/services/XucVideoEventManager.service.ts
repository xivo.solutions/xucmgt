import { ILogService, IRootScopeService, IScope } from "angular"
import { RichWindow } from "RichWindow"
import { XucLink } from "xccti/services/XucLink"

type MeetingRoomInvite = {
  requestId : number
  token: string
  displayName: string
  username: string
}

type VideoEvent = {
  fromUser: string
  status: string
}

export default class XucVideoEventManager {
  public baseEventName = 'XucVideoEventManager'
  public EVENT_VIDEO_START = "videoStart"
  public EVENT_VIDEO_END = "videoEnd"
  public EVENT_USER_BUSY = "Busy"
  public EVENT_USER_AVAILABLE = "Available"
  public EVENT_INVITE = "MeetingRoomInvite"
  public EVENT_INVITE_ACK = "MeetingRoomInviteAck"
  public EVENT_INVITE_RESPONSE = "MeetingRoomInviteResponse"

  static $inject = ["$log", "$rootScope", "XucLink", "$window"]

  constructor ( private $log: ILogService,
    private $rootScope: IRootScopeService,
    private XucLink: XucLink,
    private $window: RichWindow) {
       this.XucLink.whenLogged().then(this.init)
     }

  private onVideoInviteEvent = (inviteEvent: MeetingRoomInvite) : void => {
    this.$rootScope.$emit(this.baseEventName + this.EVENT_INVITE, inviteEvent)
  }

  private onVideoEvent = (videoEvent: VideoEvent) : void => {
    this.$rootScope.$emit(this.baseEventName + videoEvent.status, videoEvent)
  }

  private subscribe = (scope: IScope, callback: Function, targetEvent: string) : void => {
    let unregister = this.$rootScope.$on(this.baseEventName + targetEvent, (_, event) => {
      this.$rootScope.$applyAsync(() => {
        callback(event)
      })
    })
    scope.$on('$destroy', unregister)
  }

  private init = () : void => {
    this.$log.info("Starting XucVideoEventManager service")
    this.$window.Cti.setHandler(this.$window.Cti.MessageType.VIDEOEVENT, this.onVideoEvent)
    this.$window.Cti.setHandler(this.$window.Cti.MessageType.MEETINGROOMINVITE, this.onVideoInviteEvent)
    this.XucLink.whenLoggedOut().then(this.unInit)
  }

  private unInit = () : void => {
    this.$log.info("Unloading XucVideoEventManager service")
    this.XucLink.whenLogged().then(this.init)
  }

  public subscribeToVideoStatusEvent = (scope: IScope, callback: Function) : void => {
    this.subscribe(scope, callback, this.EVENT_USER_BUSY)
    this.subscribe(scope, callback, this.EVENT_USER_AVAILABLE) 
  }

  public subscribeToVideoInviteEvent = (scope: IScope, callback: Function) : void => { 
    this.subscribe(scope, callback, this.EVENT_INVITE) 
  }

}
