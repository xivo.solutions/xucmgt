import angular, { ILogService, IRootScopeService } from "angular";
import { XucLink } from "./XucLink";
import { ContactSheetModel } from "xchelper/models/contact-sheet.model"
import { PhoneHintEvent } from "xchelper/models/PhoneHintEvent.model";
import { VideoStatusEvent } from "xchelper/models/VideoStatusEvent.model";
import { RichEntry, SearchContactResult  } from "xchelper/models/RichEntry.model";
import { FavoritesUpdated } from "xchelper/models/FavoritesUpdated.model";

type XucDirectoryEvent = PhoneHintEvent | VideoStatusEvent;
enum XucDirectoryEventType {
  Phone = "phone",
  Video = "video"
}

export default function XucDirectory($rootScope: IRootScopeService, XucPhoneHintService: any, XucLink: XucLink, $log: ILogService, $q: any, XucVideoEventManager: any, Broadcaster:any) {
  var searchResult: RichEntry[]  = [];
  var contactSheets: ContactSheetModel[] = [];
  var favoriteContactSheets: ContactSheetModel[] = [];
  var favorites: RichEntry[] = [];
  var headers: any[] = [];
  var searchTerm: any = null;
  var isSearching = false;
  var displayNameRequests = new Map();
  var displayNamesCache = new Map();
  var Cti = (window as any).Cti;

  const _directoryLookup = function(term: any) {
    searchTerm = term ? term : '';
    isSearching = true;
    Cti.directoryLookUp(searchTerm);
  };

  const _contactLookup = function(term: any) {
    searchTerm = term ? term : '';
    isSearching = true;
    Cti.searchContacts(searchTerm);
  };

  const _getSearchTerm = function() {
    return searchTerm;
  };

  var _isSearching = function() {
    return isSearching;
  };

  const _onSearchResult = function(result: SearchContactResult) {
    searchResult = angular.copy(result.entries);
    isSearching = false;
    _setHeaders(result);
    $rootScope.$broadcast('searchResultUpdated');
  };

  const _onContactSheet = function(result: {sheets: any}) {
    contactSheets = angular.copy(result.sheets);
    isSearching = false;
    $rootScope.$broadcast('contactSheetUpdated');
  };

  const _onFavoriteContactSheet = function(result: {sheets: any}) {
    favoriteContactSheets = angular.copy(result.sheets);
    isSearching = false;
    $rootScope.$broadcast('favoriteContactSheetUpdated');
  };

  const _onFavorites = function(newFavorites: {entries: any, headers: any}) {
    searchResult = [];
    favorites = angular.copy(newFavorites.entries);
    _setHeaders(newFavorites);
    $rootScope.$broadcast('favoritesUpdated');
  };

  const _onUserDisplayNameResult = function(result: {displayName: any, userName: any}) {
    if (result.displayName) {
      if (displayNameRequests.has(result.userName)) {
        displayNamesCache.set(result.userName, result.displayName);
        displayNameRequests.get(result.userName).resolve(result.displayName);
        displayNameRequests.delete(result.userName);
      }
      else {
        $log.error("Unknown result received, no such username in the map", result.userName);
      }
    }
    else {
      $log.error("Error when retrieving display name ", result);
      displayNameRequests.forEach((values) => {
        values.reject('Failure');
      });
      displayNameRequests.clear();
    }
  };

  const _setHeaders = function(result: {entries: any, headers: any}) {
    headers = angular.copy(result.headers);
    if(headers.indexOf("Favoris") != -1) {
      headers.splice(-3, 2);
    }
  };

  const _onFavoriteUpdated = function(update: FavoritesUpdated) {
    $log.debug(`Initiating Favorite Update ${update}`);
    if (update.contact_id.trim() == "") return;
    if (update.action === "Removed" || update.action === "Added") {
      contactSheets.forEach((elem) => {
        if(elem.sources.find((source) =>
          source.name === update.source && source.id === update.contact_id
        )){
          elem.isFavorite = ! elem.isFavorite;
        }
      });
    }
    else {
      $log.log("SERVER ERROR: Favorite update failed: ", update);
    }
    _updateFavorites(update);
    $rootScope.$broadcast('searchResultUpdated');
  };

  const _updateFavorites = function(update: FavoritesUpdated) {
    if (update.action === "Removed") {
      var result: any = _find(favoriteContactSheets, update);
      result.elem.isFavorite = false;
      if (_find(contactSheets, update) === undefined) contactSheets.push(result.elem);
      if (result.index >= 0) favoriteContactSheets.splice(result.index, 1);
    }
    else if (update.action == "Added") {
      var newFavorite = _find(contactSheets, update);
      if (newFavorite) {
        newFavorite.elem.isFavorite = true;
        favoriteContactSheets.push(newFavorite.elem);
      }
    }
    $rootScope.$broadcast('favoritesUpdated');
    $rootScope.$broadcast('favoriteContactSheetUpdated');
  };

  var _find = function(contacts: ContactSheetModel[], update: FavoritesUpdated): {'elem': ContactSheetModel, 'index': number} | undefined {
    var result;
    contacts.forEach(function(elem, i: number) {
      if(elem.sources.find((source) =>
        source.name === update.source && source.id === update.contact_id
      )){
        result = {'elem': elem, 'index': i};
      }
    });
    return result;
  };

  const _getSearchResult = function() {
    return searchResult;
  };

  const _getContactSheetResult = function() {
    return contactSheets;
  };

  const _getFavoriteContactSheet = function() {
    return favoriteContactSheets;
  };

  const _getFavorites = function() {
    return favorites;
  };

  const _clearResults = function() {
    favorites = [];
    searchResult = [];
    searchTerm = null;
    isSearching = false;
    $rootScope.$broadcast('searchResultUpdated');
  };

  const _getHeaders = function() {
    return headers;
  };

  const isNotEmpty = function(array: any[]) {return array.length > 0};

  /** Update the status of a contact in a list (searchResult or favorites) **/

  const _updateStatus = function(event: XucDirectoryEvent, eventType: XucDirectoryEventType) {
    let source: RichEntry[] = [];
    let sourceEvent = "";
    let matchFound = false;

    if (isNotEmpty(searchResult)) {
      source = searchResult;
      sourceEvent = 'searchResultUpdated';
    }
    else if(isNotEmpty(favorites)) {
      source = favorites;
      sourceEvent = 'favoritesUpdated';
    } else return;

    switch(eventType){
      case XucDirectoryEventType.Video:{
        let videoEvent: VideoStatusEvent = event as VideoStatusEvent;
        angular.forEach(source, function(richEntry: RichEntry) {
          if (angular.isDefined(richEntry) &&
              angular.isArray(richEntry.entry) &&
              richEntry.username === videoEvent.fromUser)
          {
            richEntry.videoStatus = videoEvent.status;
            matchFound = true;
          }
        });
        break;
      }
      case XucDirectoryEventType.Phone:{
        let phoneEvent: PhoneHintEvent = event as PhoneHintEvent;
        angular.forEach(source, function(richEntry: RichEntry) {
          if (angular.isDefined(richEntry) &&
              angular.isArray(richEntry.entry) &&
              richEntry.entry[1] === phoneEvent.number) //the second field is the xivo number
          {
            richEntry.status = phoneEvent.status;
            matchFound = true;
          }
        });
        break;
      }
    }


    if (matchFound) {
      Broadcaster.send(sourceEvent);
    }
  };

  /** Update the status of a contact in a list (contactSheets or favoriteContactSheets) **/

  const _updateContactStatus = (list:ContactSheetModel[],
    event:XucDirectoryEvent,
    eventType:XucDirectoryEventType,
    sourceEvent:string) => {
    switch(eventType){
      case XucDirectoryEventType.Phone: {
        let phoneEvent: PhoneHintEvent = event as PhoneHintEvent;
        list.forEach(contact => {
          if (contact.actions.Call &&
            isNotEmpty(contact.actions.Call.args) &&
              contact.actions.Call.args[0] === phoneEvent.number) {
            contact.status.phone = phoneEvent.status;
          }
        });
        break;
      }
      case XucDirectoryEventType.Video: {
        let videoEvent: VideoStatusEvent = event as VideoStatusEvent;
        list.forEach(contact => {
          if (contact.actions.Video &&
            isNotEmpty(contact.actions.Video.args) &&
              contact.actions.Video.args[0] === videoEvent.fromUser) {
            contact.status.video = videoEvent.status;
          }
        });
        break;
      }
    }
    Broadcaster.send(sourceEvent);
  };


  const _onEvent = function(event: XucDirectoryEvent,
    eventType: XucDirectoryEventType,
    updateContactStatusFunc: (list: ContactSheetModel[], event: XucDirectoryEvent, eventType: XucDirectoryEventType, sourceEvent: string) => void,
    updateStatusFunc: (event: XucDirectoryEvent, eventType: XucDirectoryEventType) => void
  ) {
    if (isNotEmpty(contactSheets)) {
      updateContactStatusFunc(contactSheets, event, eventType, 'contactSheetUpdated');
    }
    if(isNotEmpty(favoriteContactSheets)) {
      updateContactStatusFunc(favoriteContactSheets, event, eventType, 'favoriteContactSheetUpdated');
    } else {
      updateStatusFunc(event,eventType);
    }
  };

  const _onPhoneHint = function(phoneHint: PhoneHintEvent) {
    _onEvent(phoneHint, XucDirectoryEventType.Phone, _updateContactStatus, _updateStatus);
  };

  const _onVideoEvent = function(videoEvent: VideoStatusEvent) {
    _onEvent(videoEvent, XucDirectoryEventType.Video, _updateContactStatus, _updateStatus);
  };

  const _getUserDisplayName = (user: any) => {
    var deferred = $q.defer();
    if (displayNamesCache.has(user)) {
      deferred.resolve(displayNamesCache.get(user));
    }
    else
    {
      displayNameRequests.set(user, deferred);
      Cti.displayNameLookup(user);
    }
    return deferred.promise;
  };

  XucPhoneHintService.addEventListener($rootScope, _onPhoneHint);
  XucVideoEventManager.subscribeToVideoStatusEvent($rootScope,_onVideoEvent);

  const _init = () => {
    $log.info("Starting XucDirectory service");
    Cti.setHandler(Cti.MessageType.DIRECTORYRESULT, _onSearchResult);
    Cti.setHandler(Cti.MessageType.CONTACTSHEET, _onContactSheet);
    Cti.setHandler(Cti.MessageType.FAVORITECONTACTSHEET, _onFavoriteContactSheet);
    Cti.setHandler(Cti.MessageType.FAVORITES, _onFavorites);
    Cti.setHandler(Cti.MessageType.FAVORITEUPDATED, _onFavoriteUpdated);
    Cti.setHandler(Cti.MessageType.USERDISPLAYNAME, _onUserDisplayNameResult);
    XucLink.whenLoggedOut().then(_unInit);
  };

  const _unInit = () => {
    $log.info("Unloading XucDirectory service");
    displayNameRequests.clear();
    displayNamesCache.clear();
    XucLink.whenLogged().then(_init);
  };

  XucLink.whenLogged().then(_init);

  return {
    directoryLookup : _directoryLookup, // deprecated
    contactLookup : _contactLookup,
    onSearchResult : _onSearchResult,
    onContactSheet : _onContactSheet,
    onFavoriteContactSheet : _onFavoriteContactSheet,
    onFavorites : _onFavorites,
    onFavoriteUpdated : _onFavoriteUpdated,
    getSearchResult : _getSearchResult, // deprecated
    getContactSheetResult : _getContactSheetResult,
    getFavorites : _getFavorites,
    clearResults: _clearResults,
    getHeaders : _getHeaders,
    getSearchTerm : _getSearchTerm,
    getFavoriteContactSheet : _getFavoriteContactSheet,
    isSearching : _isSearching,
    getUserDisplayName: _getUserDisplayName,
  };
}
