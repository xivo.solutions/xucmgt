import {RichWindow} from "RichWindow"

type OIDCServerConfigurationResponse = {
  data: {
    authorization_endpoint: String
    end_session_endpoint: String
  }
}

type EncodableURIComponent = string | number | boolean

export default class OIDCHelper {
  static $inject = ["$window"]
  private openidConfigPath = "/.well-known/openid-configuration"
  
  constructor(
    private $window: RichWindow
  ) {}
    
  public buildServiceUrl() {
    let baseUrl = `${this.$window.location.protocol}//${this.$window.location.host}${this.$window.location.pathname}${this.$window.location.hash}`
    return baseUrl.replace("#!/login", "").split("?")[0]
  }

  private setState(phoneNumber: String = "") {
    return (phoneNumber.length > 0) ? `&state=${phoneNumber}` : ''
  }

  public buildConfigUrl(serverUrl: String) { return serverUrl + this.openidConfigPath }

  public buildLoginUrl(OIDCServerConfiguration: OIDCServerConfigurationResponse, phoneNumber: string, clientId: EncodableURIComponent) {
    return `${OIDCServerConfiguration.data.authorization_endpoint}?` +
    'client_id=' + encodeURIComponent(clientId) +
    '&redirect_uri=' + encodeURIComponent(this.buildServiceUrl()) +
    '&response_type=token+id_token' +
    '&response_mode=fragment' +
    '&scope=openid' +
    '&nonce=nonce' + 
    this.setState(phoneNumber)
  }

  private buildLogoutParameter(clientId: EncodableURIComponent, idToken?: String) {
    return idToken ? `&id_token_hint=${idToken}` : `&client_id=${encodeURIComponent(clientId)}`
  }

  public buildLogoutUrl(OIDCServerConfiguration: OIDCServerConfigurationResponse, clientId: EncodableURIComponent, idToken: String) {
    return `${OIDCServerConfiguration.data.end_session_endpoint}` +
      "?post_logout_redirect_uri=" +
      encodeURIComponent(this.buildServiceUrl()) +
      this.buildLogoutParameter(clientId, idToken)
  }

  public getPhoneNbFromState(url: String) {
    let params = Object.fromEntries(new URLSearchParams(url.split('?')[1]));
    return params?.state
  }
    
}