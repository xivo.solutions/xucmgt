import _ from "lodash";

export default function XucLink($rootScope, $q, $log, $window, $http, $filter, $timeout, OIDCHelper) {
  var _hostAndPort = "";
  var _homeUrl = "/";
  var _redirectToHomeUrl = false;
  var _user = {};
  var _logged = false;
  var _token = null;
  var _logonDeferred = $q.defer();
  var _linkClosedDeferred = $q.defer();
  var _profileRightDeferred = $q.defer();
  var _timeout = 5000;
  var broadcastingLinkDisconnected = false;
  var _tokenRenewPromise;

  const obj = $window;
  const path = 'setElectronConfig';
  const isElectron = _.isFunction(_.get(obj, path));
  const _isElectron = () => {
    return isElectron;
  };

  const _getSoftwareType = () => {
    return _isElectron() ? "electron" : "webBrowser";
  };

  const _getApplicationType = () => {
    const validTypes = ['ccagent', 'ccmanager', 'switchboard'];
    const url = new URL($window.location.href);
    return validTypes.find(type => url.pathname.includes(type)) || 'uc';
  };

  var _isJson = function (value) {
    try
    {
      if (typeof value === 'object')
      {
        JSON.parse(JSON.stringify(value));
      }
      else
      {
        JSON.parse(value);
      }
      return true;
    }
    catch (e)
    {
      return false;
    }
  };

  var _parseXHRError = function(response) {
    if(_isJson(response.data) && response.data) {
      return response.data;
    } else {
      return {error: 'NoResponse', message:'No response from server'};
    }
  };

  var _handleThirdPartyError = function(response) {
    let result;
    if(response.data) {
      result = response.data;
    } else {
      result = {error: '3rdPartyNoResponse', message:'No response from server'};
    }
    return $q.reject(result);
  };

  var _parseUrlParameters = function (url) {
    return Object.fromEntries(new URLSearchParams(url.split('?')[1]));
  };

  var _loggedOnHandler = function() {
    _logged = true;
    _logonDeferred.resolve(_user);
    $rootScope.$broadcast('ctiLoggedOn', _user);
  };

  var _onRightProfile = function(event) {
    _profileRightDeferred.resolve(event);
  };

  var _whenLogged = function() { return _logonDeferred.promise;};
  var _whenLoggedOut = function() { return _linkClosedDeferred.promise;};

  var _linkStatusHandler = function(event) {
    $log.info("link status : " + event.status);
    if (event.status !== 'opened') {
      if(_logged) {
        _logged = false;
        _logonDeferred = $q.defer();
      }
      _linkClosedDeferred.resolve();
      _linkClosedDeferred = $q.defer();
      _profileRightDeferred = $q.defer();
      if (!broadcastingLinkDisconnected){
        $rootScope.$broadcast('linkDisConnected');
        broadcastingLinkDisconnected = true;
      }
      if(_redirectToHomeUrl) {
        $window.location.replace(_homeUrl+"?error='unable to connect to xuc server'");
      }
    } else {
      broadcastingLinkDisconnected = false;
    }
  };

  var _setHostAndPort = function(hostAndPort) {
    _hostAndPort = hostAndPort;
  };
  var _setHomeUrl = function(homeUrl) {
    _homeUrl = homeUrl;
  };
  var _setRedirectToHomeUrl = function(value) {
    _redirectToHomeUrl = value;
  };
  var _getServerUrl = function(protocol) {
    return $filter('prepareServerUrl')($window.location.protocol, _hostAndPort, protocol);
  };

  var _getStoredCredentials = function() {
    if(typeof(Storage) !== "undefined" && angular.isString(localStorage.getItem("credentials"))) {
      try {
        return JSON.parse(localStorage.getItem("credentials"));
      } catch(e) {
        $log.error("Unable to parse stored credentials");
        return null;
      }
    } else {
      return null;
    }
  };

  var _storeCredentials = function(credentials) {
    $log.debug("storeCredentials");
    if(typeof(Storage) !== "undefined") {
      localStorage.setItem("credentials",JSON.stringify(credentials));
    }
  };

  var _clearCredentials = function() {
    _cleanupLocalStorage();
  };

  var _checkXucIsUp = function() {
    $log.debug('checkXucIsUp, xucserver host and port: ', _hostAndPort);

    return $http.get(_getServerUrl('http') + "/xuc/api/2.0/auth/check", {timeout:_timeout})
      .catch(function(response) {
        var data = _parseXHRError(response);
        if(data.error == "NoResponse") {
          return $q.reject(data);
        } else {
          return $q.resolve();
        }
      });
  };

  var _checkCredentials = function(credentials) {
    $log.debug('checkCredentials, xucserver host and port: ', _hostAndPort);

    return $http.get(_getServerUrl('http') + "/xuc/api/2.0/auth/check", {headers: { 'Authorization': 'Bearer ' + credentials.token }, timeout:_timeout})
      .then(function(response) {
        let newCredentials = response.data;
        newCredentials.phoneNumber = credentials.phoneNumber;
        return newCredentials;
      }, function(response) {
        var data = _parseXHRError(response);
        $log.warn("Error while checking credentials: " + data);
        if(data.error !== "NoResponse") {
          _cleanupLocalStorage();
        }
        return $q.reject(data);
      });
  };

  var _getSsoCredentials = function() {
    $log.debug("getSsoCredentials");
    return $http.get(_getServerUrl('http') + "/xuc/api/2.0/auth/sso?" + "softwareType=" + _getSoftwareType() + "&applicationType=" + _getApplicationType(), {withCredentials: true, timeout:_timeout})
      .then(function(response) {
        return response.data;
      }, function(response) {
        var data = _parseXHRError(response);
        $log.warn("Error during SSO authentication: " + JSON.stringify(data));
        return $q.reject(data);
      });
  };

  var _getCasToken = function(casServerUrl, phoneNumber) {
    var ticket = _parseUrlParameters($window.location.search).ticket;

    var service = escape($window.location.href.match(/(^[^#?]*)/)[0] + "?phoneNumber=" + phoneNumber);
    var loginUrl = casServerUrl + "/login?service=" + service;

    if(angular.isDefined(ticket)) {
      return $q.resolve(ticket);
    } else {
      $window.location.href = loginUrl;
    }
  };

  var _tradeCasToken = function(casServerUrl, ticket, phoneNumber) {
    var service = escape($window.location.href.match(/(^[^#?]*)/)[0] + "?phoneNumber=" + phoneNumber);
    var loginUrl = casServerUrl + "/login?service=" + service;

    var parameters = "service=" + service + "&ticket=" + ticket + "&softwareType=" + _getSoftwareType() + "&applicationType=" + _getApplicationType();
    return $http.get(_getServerUrl('http') + "/xuc/api/2.0/auth/cas?" + parameters, {timeout:_timeout})
      .then(function(response) {
        return response.data;
      }, function(response) {
        var data = _parseXHRError(response);
        $log.warn("Error during CAS authentication: " + JSON.stringify(data));
        if(data.error === "CasServerInvalidTicket") {
          $window.location.href = loginUrl;
          return $q.defer().promise;
        } else {
          return $q.reject(data);
        }
      });
  };

  var _logoutFromCas = function(casServerUrl) {
    var service = escape($window.location.href.match(/(^[^#?]*)/)[0]);
    var logoutUrl = casServerUrl + "/logout?service=" + service;
    $window.location.href = logoutUrl;
  };

  const _getOpenidToken = function(openIdServerUrl, clientId, phoneNumber) {
    let accessToken = _parseUrlParameters($window.location.href).access_token;
    let idToken = _parseUrlParameters($window.location.href).id_token;
    if (angular.isDefined(accessToken)) {
      idToken ? _setIdToken(idToken) : undefined;
      return $q.resolve(accessToken);
    } else {
      return $http.get(OIDCHelper.buildConfigUrl(openIdServerUrl), {timeout: _timeout}).then(OIDCServerConfiguration => {
        let loginUrl = OIDCHelper.buildLoginUrl(OIDCServerConfiguration, phoneNumber, clientId);
        $window.location.href = loginUrl;
        return $q.defer().promise;
      }, _handleThirdPartyError);
    }
  };

  var _tradeOpenidToken = (openidToken) => {
    return $http.get(_getServerUrl('http') + "/xuc/api/2.0/auth/oidc?token=" + openidToken + "&softwareType=" + _getSoftwareType() + "&applicationType=" + _getApplicationType(), {timeout:_timeout})
      .then(function(response) {
        return response.data;
      }).catch(err =>{
        $log.warn("Error during OpenID authentication", err);
        var data = _parseXHRError(err);
        return $q.reject(data);
      });
  };

  var _logoutFromOpenid = (openIdServerUrl, clientId, phoneNumber) => {
    if (!phoneNumber) phoneNumber = "";
    return $http.get(OIDCHelper.buildConfigUrl(openIdServerUrl), { timeout: _timeout }).then(OIDCServerConfiguration => {
      let idToken = _getIdToken();
      let logoutUrl = OIDCHelper.buildLogoutUrl(OIDCServerConfiguration, clientId, idToken);
      $window.location.href = logoutUrl;
      return $q.defer().promise;
    }, _handleThirdPartyError);
  };

  var _loginWithCredentials = function(credentials) {
    $log.debug("loginWithCredentials");
    _user.username = credentials.login;
    _user.phoneNumber = credentials.phoneNumber;
    _user.token = credentials.token;
    var wsurl = _getServerUrl('ws') + "/xuc/api/2.0/cti?token=" + credentials.token;
    _scheduleTokenRenewal(credentials);
    return Cti.WebSocket.init(wsurl, credentials.login, credentials.phoneNumber);
  };

  var _checkAndloginWithCredentials = function(credentials) {
    $log.debug("checkAndloginWithCredentials");
    return _checkCredentials(credentials).then(function(newCredentials) {
      _storeCredentials(newCredentials);
      return _loginWithCredentials(newCredentials);
    });
  };

  var _login = function(username, password, phoneNumber) {
    $log.debug("login");
    return $http.post(_getServerUrl('http') + "/xuc/api/2.0/auth/login", {"login": username, "password": password, "softwareType": _getSoftwareType(), "applicationType": _getApplicationType()}, {timeout:_timeout})
      .then(function(response) {
        var data = response.data;
        data.phoneNumber = phoneNumber;
        _storeCredentials(data);
        return _loginWithCredentials(data);
      }, function(response) {
        var data = _parseXHRError(response);
        $log.warn("Error while logging in: " + JSON.stringify(data));
        return $q.reject(data);
      });
  };

  var _initCti = function(username, password, phoneNumber) {
    _user.username = username;
    _user.password = password;
    _user.phoneNumber = phoneNumber;
    var wsurl = '';
    if(_token)
      wsurl = _getServerUrl('ws') + "/xuc/ctichannel/tokenAuthentication?token=" + _token;
    else
      wsurl = _getServerUrl('ws') + "/xuc/ctichannel?username=" + username + "&amp;agentNumber=" +
      phoneNumber + "&amp;password=" + encodeURIComponent(password);

    if(typeof(Storage) !== "undefined" && angular.isString(phoneNumber) && phoneNumber.length > 0 && phoneNumber !== "0") {
      localStorage.agentNumber = phoneNumber;
    }

    Cti.WebSocket.init(wsurl, username, phoneNumber);
  };

  var _cleanupLocalStorage = function() {
    $log.info("cleanupLocalStorage");
    if(typeof(Storage) !== "undefined") {
      localStorage.removeItem("xucToken");
      localStorage.removeItem("agentNumber");
      localStorage.removeItem("credentials");
      _token = null;
    }
  };

  var _logout = function() {
    Cti.close();
    $rootScope.$broadcast("ctiLoggedOut", _user);
    $timeout.cancel(_tokenRenewPromise);
    if (_redirectToHomeUrl) {
      $log.info("redirecting to ",_homeUrl);
      $window.location.replace(_homeUrl);
    }
  };

  var _isLogged = function() {
    return _logged;
  };

  var _isRightAllowed = function(profileRight, module) {
    return profileRight.rights.indexOf(module) > -1;
  };

  var _scheduleTokenRenewal = function (credentials) {
    if (credentials.TTL) {
      let halfLifeTime = credentials.TTL / 2;
      $log.debug(`User credentials will be renewed in ${halfLifeTime} seconds`);
      _tokenRenewPromise = $timeout(() => {
        $log.debug("Renew user credentials");
        _checkCredentials(credentials).then((newCredentials) => {
          _updateUserToken(newCredentials);
          _scheduleTokenRenewal(newCredentials, newCredentials.TTL);
        });
      }, halfLifeTime * 1000);
    }
  };

  var _updateUserToken = function(credentials) {
    _user.token = credentials.token;
    _storeCredentials(credentials);
  };

  var _getTokenRenewPromise = function() {
    return _tokenRenewPromise || $q.reject('There is no token renew schedule yet');
  };

  var _getPhoneNumber = function() { return _user.phoneNumber; };
  var _getXucToken = function () { return _user.token; };
  var _getIdToken = function () { return _user.id_token; };
  var _setIdToken = function (idToken) {_user.id_token = idToken;};
  var _getXucUser = () => { return _user; };

  var _getProfileRightAsync = function() { return _profileRightDeferred.promise; };

  var _getLoginTimeoutMs = function() { return _timeout; };
  var _setLoginTimeoutMs = function(newTimeout) { _timeout = newTimeout; };

  Cti.setHandler(Cti.MessageType.LOGGEDON, _loggedOnHandler);
  Cti.setHandler(Cti.MessageType.LINKSTATUSUPDATE, _linkStatusHandler);
  Cti.setHandler(Cti.MessageType.RIGHTPROFILE, _onRightProfile);

  return {
    login: _login,
    checkAndloginWithCredentials: _checkAndloginWithCredentials,
    getStoredCredentials: _getStoredCredentials,
    checkCredentials: _checkCredentials,
    checkXucIsUp: _checkXucIsUp,
    storeCredentials: _storeCredentials,
    clearCredentials: _clearCredentials,
    getSsoCredentials: _getSsoCredentials,
    getCasToken: _getCasToken,
    tradeCasToken: _tradeCasToken,
    getOpenidToken: _getOpenidToken,
    tradeOpenidToken: _tradeOpenidToken,
    getServerUrl: _getServerUrl,
    setHostAndPort : _setHostAndPort,
    setHomeUrl : _setHomeUrl,
    setRedirectToHomeUrl: _setRedirectToHomeUrl,
    initCti : _initCti,
    logout : _logout,
    logoutFromCas: _logoutFromCas,
    isLogged : _isLogged,
    whenLogged: _whenLogged,
    whenLoggedOut: _whenLoggedOut,
    getProfileRightAsync: _getProfileRightAsync,
    isRightAllowed: _isRightAllowed,
    getPhoneNumber: _getPhoneNumber,
    getXucToken: _getXucToken,
    getIdToken: _getIdToken,
    setIdToken: _setIdToken,
    parseUrlParameters: _parseUrlParameters,
    logoutFromOpenid: _logoutFromOpenid,
    getXucUser: _getXucUser,
    getTokenRenewPromise: _getTokenRenewPromise,
    getLoginTimeoutMs: _getLoginTimeoutMs,
    setLoginTimeoutMs: _setLoginTimeoutMs
  };
}
