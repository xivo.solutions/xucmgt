import angular from 'angular';
import angularanimate from 'angular-animate';
import LocalStorage from 'angular-local-storage';
import angulartranslate from 'angular-translate';
import uibootstrap from 'angular-ui-bootstrap';
import uirouter from '@uirouter/angularjs';
import 'angular-ui-carousel';
import angulartable from 'angular-smart-table';

import 'nvd3';
import 'angular-nvd3';

import 'xccti/cti-webpack';
import 'xchelper/helper.module';
import 'xclogin/login-webpack';
import 'xcchat/chat.module';
import 'angular-material';
import 'angular-material/angular-material.min.css';

/* Agent Module dependencies */
import config from './agent.config';
import run from './agent.run';
import LoginController from './controllers/login.controller';
import MainController from './controllers/main.controller';
import ThirdPartyController from './controllers/ThirdParty.controller';
import switchBoard from './directives/switchBoard.directive';
import ContentActivitiesController from './controllers/contentActivities.controller';
import ContentAgentsController from './controllers/contentAgents.controller';
import ContentCallbacksController from './controllers/contentCallbacks.controller';
import ContentCustomerController from './controllers/contentCustomer.controller';
import ContentHistoryController from './controllers/contentHistory.controller';
import ContentSearchController from './controllers/contentSearch.controller';
import ContentSettingsController from './controllers/contentSettings.controller';
import agentState from './directives/agentState.directive';
import statsPanel from './directives/statsPanel.directive';
import callControl from './directives/callControl.directive';
import callLine from './directives/callLine.directive';
import callConference from './directives/callConference.directive';
import agentPreferences from './services/agentPreferences.factory';
import agentCallbackHelper from './services/agentCallbackHelper.service';
import agentActivitiesHelper from './services/agentActivitiesHelper.service';
import ContentConversationController from './controllers/contentConversation.controller';
import ContentConversationHistoryController from './controllers/contentConversationHistory.controller';
import ContactSheetController from "xchelper/controllers/contact-sheet.controller";
import ContactService from "xchelper/services/contact.service";
import contactButtons from 'xchelper/directives/contactButtons.directive';
import ConfirmSwitchNumberController from 'ccagent/controllers/ConfirmSwitchNumberController';

angular.module('Agent', ['xcCti', 'xcLogin', 'xcHelper', angulartranslate, uibootstrap, LocalStorage, uirouter,
  angulartable, angularanimate, 'ui.carousel', 'nvd3', 'xcChat', 'ngMaterial'])
  .config(config)
  .controller('LoginController', LoginController)
  .controller('MainController', MainController)
  .controller('ThirdPartyController', ThirdPartyController)
  .controller('ContentActivitiesController', ContentActivitiesController)
  .controller('ContentAgentsController', ContentAgentsController)
  .controller('ContentCallbacksController', ContentCallbacksController)
  .controller('ContentCustomerController', ContentCustomerController)
  .controller('ContentHistoryController', ContentHistoryController)
  .controller('ContentSearchController', ContentSearchController)
  .controller('ContentSettingsController', ContentSettingsController)
  .controller('ContentConversationController', ContentConversationController)
  .controller('ContentConversationHistoryController', ContentConversationHistoryController)
  .controller('contactSheetController', ContactSheetController)
  .controller('ConfirmSwitchNumberController', ConfirmSwitchNumberController)
  .service('agentPreferences', agentPreferences)
  .service('agentCallbackHelper', agentCallbackHelper)
  .service('agentActivitiesHelper', agentActivitiesHelper)
  .service('ContactService', ContactService)
  .directive('agentState', agentState)
  .directive('statsPanel', statsPanel)
  .directive('callControl', callControl)
  .directive('callLine', callLine)
  .directive('switchBoard', switchBoard)
  .directive('callConference', callConference)
  .directive('contactButtons', contactButtons)

  .run(run);
