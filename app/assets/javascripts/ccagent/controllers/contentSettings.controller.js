import { AppType } from "xccti/services/applicationConfiguration.provider";

export default class ContentSettingsController {

  constructor($scope, applicationConfiguration, CtiProxy, WebHIDHeadsetService) {
    this.$scope = $scope;
    this.applicationConfiguration = applicationConfiguration;
    this.CtiProxy = CtiProxy;
    this.webHIDHeadsetService = WebHIDHeadsetService;
    this.selectedOption = "keybindings";
  }

  displayOptions(name) {
    document.querySelectorAll("div.option-header").forEach(elem => { elem.classList.remove("selected");});
    document.getElementById(`${name}-header`).classList.add("selected");
    this.selectedOption = name;
  }

  initWebHID() {
    this.webHIDHeadsetService.init();
  }

  displayAudioSettings() {
    return this.CtiProxy.isUsingWebRtc();
  }

  isSwitchboard() {
    return this.applicationConfiguration.getCurrentAppConfig().appType == AppType.Switchboard;
  }

}
