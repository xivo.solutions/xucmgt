import _ from 'lodash';

export default class ContentAgentsController {

  constructor($scope, agentPreferences, XucUser, XucAgent, callContext) {
    this.$scope = $scope;
    this.agentPreferences = agentPreferences;
    this.XucUser = XucUser;
    this.XucAgent = XucAgent;
    this.callContext = callContext;

    this.$scope.agents = [];
    this.$scope.displayLoggedAgents = true;

    this.$scope.$on('AgentStateUpdated', this.loadAgents.bind(this));
    this.$scope.$on('AgentConfigUpdated', this.loadAgents.bind(this));
    this.loadAgents.call(this);
  }

  loadAgents() {
    this.XucUser.getUserAsync().then(user => {
      if (user.agentId) {
        this.XucAgent.getAgentAsync(user.agentId).then(agent => {
          this.$scope.agents = this.buildList(this.XucAgent.getAgentsInGroup(agent.groupId), user.agentId);
        });
      }
    });
  }

  buildList(agents, currentId) {
    let agentFiltered = agents.filter(agent => {
      return agent.id != currentId;
    });
    if (this.$scope.displayLoggedAgents) {
      agentFiltered = agentFiltered.filter(agent => {
        return agent.state !== 'AgentLoggedOut';
      });
    }
    return agentFiltered;
  }

  getAgentStateColor(agent) {
    return 'state-' + _.kebabCase(agent.state);
  }

  getAgentStateFillColor(agent) {
    return this.getAgentStateColor(agent) + ' fill';
  }

  getAgentStateBackColor(agent) {
    return this.getAgentStateColor(agent);
  }

  setHoverInfo(agent) {
    agent.hover = true;
    this.$scope.agentInfo = {
      state: agent.state,
      cause: agent.status,
      number: agent.phoneNb,
      since: agent.timeInState
    };
  }

  dial(agent) {
    this.callContext.normalizeDialOrAttTrans(agent.phoneNb);
  }

}