import { XucAuthErrorCode } from "xccti/services/XucLinkError";

export default class ConfirmSwitchNumberController {

  constructor($uibModalInstance, $window, CtiProxy, $state) {
    this.$uibModalInstance = $uibModalInstance;
    this.$window = $window;
    this.CtiProxy = CtiProxy;
    this.$state = $state;
    this.$uibModalInstance.result.then(() => { }, () => {
      this.close();
    });
  }

  cancel() {
    this.close();
  }

  validate() {
    this.$window.Cti.logoutAgent();
    this.CtiProxy.stopUsingWebRtc();
    this.$state.go("login", {
      error: XucAuthErrorCode.SwitchNumber
    });
    this.close();
  }

  close() {
    this.$uibModalInstance.close();
  }
}