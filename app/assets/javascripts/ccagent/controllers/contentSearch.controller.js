import _ from 'lodash';

export default class ContentSearchController {

  constructor($scope, XucDirectory, focus, keyNavUtility, callContext, $rootScope, ContactService) {
    this.$scope = $scope;
    this.$rootScope = $rootScope;
    this.XucDirectory = XucDirectory;
    this.$scope.searchResult = [];
    this.focus = focus;
    this.keyNavUtility = keyNavUtility;
    this.callContext = callContext;
    this.focus('search');
    this.$scope.$on('contactSheetUpdated', this.onSearchResult.bind(this));
    this.contactService = ContactService;
  }

  onSearchResult() {
    this.keyNavUtility.resetIndex();
    this.$scope.searchResult = _.each(this.XucDirectory.getContactSheetResult(), (contact) => {
      contact.hover = false;
      this.contactService.setInitials(contact);
      return contact;
    });
    _.remove(this.$scope.searchResult, (contact) => {
      return !this.isCallable(contact);
    });
    this.$scope.headers = this.XucDirectory.getHeaders().slice(0, 5);
    this.$scope.searchTerm = this.XucDirectory.getSearchTerm() ? this.XucDirectory.getSearchTerm() : '*';
    if(!this.$rootScope.$$phase) this.$scope.$apply();
  }

  getPhoneStateLabel(status) {
    if (status.video == 'Busy') return "USER_VIDEOSTATUS_" + status.video;
    return "USER_STATUS_" + status.phone;
  }

  isMeetingRoom(contact) {
    return this.callContext.isMeetingRoom(contact);
  }

  getPhoneStateBackColor(contact) {
    let lighter = contact.keydown ? " lighter" : "";
    if (contact.status.video == "Busy") return "user-videostatus-" + contact.status.video;
    return "user-status" + contact.status.phone + lighter;
  }

  getContactLabelColor(contact) {
    if (contact.keydown) return 'text-white';
  }

  isCallable(contact) {
    return !contact.actions.Call.disabled;
  }

  isEmpty(field) {
    if (typeof field != 'undefined') {
      if (field.length > 0) {
        return true;
      }
    }
    return false;
  }

  isSearching() {
    return this.XucDirectory.isSearching();
  }

  setHover(contact) {
    contact.hover = true;
  }

  resetHover(contact) {
    contact.hover = false;
  }

  openDropdown(contactIndex) {
    this.keyNavUtility.stop(true);
    this.$scope.searchResult[contactIndex].keydown = true;
  }
}