import _ from 'lodash';

export default function agentActivitiesHelper(XucQueue, XucAgentUser, XucQueueTotal, agentPreferences, agentCallbackHelper, $log, $q) {
  this.$q = $q;
  this.$log = $log;
  this.XucQueue = XucQueue;
  this.XucAgentUser = XucAgentUser;
  this.XucQueueTotal = XucQueueTotal;
  this.agentPreferences = agentPreferences;
  this.agentCallbackHelper = agentCallbackHelper;

  this.queuesFiltered = true;
  this.defaultPenalty = 0;

  var _init = (scope) => {
    _loadQueues();
    this.XucQueue.subscribeToStatistics(scope, _loadQueues);
  };

  var _getQueueFilter = () => {
    return this.queuesFiltered;
  };

  var _setQueueFilter = (isFiltered) => {
    this.queuesFiltered = isFiltered;
  };

  var _loadQueues = () => {
    return this.queuesFiltered ? _loadAgentQueues() : _loadAllQueues();
  };

  var _loadAllQueues = () => {
    return this.XucQueue.getQueuesAsync().then(activities => {
      this.XucQueueTotal.calculate(activities);
      return activities;
    });
  };

  var _loadAgentQueues = () => {
    return this.XucAgentUser.getQueuesAsync().then(activities => {
      var favoriteQueuesAsync = [];
      angular.forEach(this.agentPreferences.getFavoriteQueues(), favQueueId => {
        if (_.findIndex(activities, (activity) => {
          return activity.id == favQueueId;
        }) == -1) {
          favoriteQueuesAsync.push(this.XucQueue.getQueueAsync(favQueueId).then(activity => {
            if (typeof(activity) !== "undefined") {
              activities.push(activity);
            }
          }));
        }
      });

      return this.$q.all(favoriteQueuesAsync).then(() => {
        return activities;
      });
    }).then(activities => {
      this.XucQueueTotal.calculate(activities);
      return activities;
    });
  };

  var _enterOrLeaveQueue = (queue) => {
    if (queue.associated) {
      return this.XucAgentUser.leaveQueueAsync(queue.id).then(() => {
        return this.agentCallbackHelper.loadAllCallbacks();
      });
    } else {
      return this.XucAgentUser.joinQueueAsync(queue.id, this.defaultPenalty).then(() => {
        this.agentPreferences.addFavoriteQueue(queue.id);
        return this.agentCallbackHelper.loadAllCallbacks();
      });
    }
  };

  var _isMemberOfQueue = (queue) => {
    return this.XucAgentUser.isMemberOfQueueAsync(queue.id);
  };

  var _addFavoriteQueue = (queue) => {
    this.agentPreferences.addFavoriteQueue(queue.id);
  };

  var _removeFavoriteQueue = (queue) => {
    this.agentPreferences.removeFavoriteQueue(queue.id);
  };

  return {
    init: _init,
    getQueueFilter: _getQueueFilter,
    setQueueFilter: _setQueueFilter,
    loadQueues: _loadQueues,
    enterOrLeaveQueue:  _enterOrLeaveQueue,
    isMemberOfQueue: _isMemberOfQueue,
    addFavoriteQueue: _addFavoriteQueue,
    removeFavoriteQueue: _removeFavoriteQueue
  };
}
