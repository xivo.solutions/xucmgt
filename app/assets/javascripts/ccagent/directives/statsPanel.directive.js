import _ from 'lodash';

export default function statsPanel($translate, $log, XucAgent, $filter) {

  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/ccagent/directives/statsPanel.html',
    scope: {
      agentId: "@"
    },
    link: (scope, elem, attr) => {
      scope.stats = [10];

      scope.updateAgentData = function () {
        if (attr.agentId) {
          XucAgent.getAgentAsync(parseInt(attr.agentId)).then((agent) => {
            scope.stats.length = 0;
            scope.stats[0] = {
              id: "PausedTime",
              value: $filter('date')(new Date(0, 0, 0).setSeconds(agent.stats.PausedTime || 0), "HH:mm:ss"),
              rawValue: agent.stats.PausedTime || 0,
              class: "stat-item-button"
            };
            scope.stats[1] = {
              id: "InbAcdCallTime",
              value: $filter('date')(new Date(0, 0, 0).setSeconds(agent.stats.InbAcdCallTime || 0), "HH:mm:ss"),
              rawValue: agent.stats.InbAcdCallTime || 0,
              class: "stat-item-button"
            };
            scope.stats[2] = {
              id: "InbAverAcdCallTime",
              value: $filter('date')(new Date(0, 0, 0).setSeconds(agent.stats.InbAverAcdCallTime || 0), "HH:mm:ss"),
              rawValue: agent.stats.InbAverAcdCallTime || 0
            };
            scope.stats[3] = {
              id: "OutCallTime",
              value: $filter('date')(new Date(0, 0, 0).setSeconds(agent.stats.OutCallTime || 0), "HH:mm:ss"),
              rawValue: agent.stats.OutCallTime || 0,
              class: "stat-item-button"
            };
            scope.stats[4] = {
              id: "WrapupTime",
              value: $filter('date')(new Date(0, 0, 0).setSeconds(agent.stats.WrapupTime || 0), "HH:mm:ss"),
              rawValue: agent.stats.WrapupTime || 0,
              class: "stat-item-button"
            };
            scope.stats[5] = {
              id: "InbAcdCalls",
              value: agent.stats.InbAcdCalls || 0,
              class: "stat-item-button"
            };
            scope.stats[6] = {
              id: "InbAnsAcdCalls",
              value: agent.stats.InbAnsAcdCalls || 0,
              class: "stat-item-button"
            };
            scope.stats[7] = {
              id: "InbUnansAcdCalls",
              value: agent.stats.InbUnansAcdCalls || 0,
              class: "stat-item-button"
            };
            scope.stats[8] = {
              id: "OutCalls",
              value: agent.stats.OutCalls || 0,
              class: "stat-item-button"
            };
            scope.stats[9] = {
              id: "InbPercUnansAcdCalls",
              value: $filter('number')(agent.stats.InbPercUnansAcdCalls || 0, 0) + ' %',
              rawValue: $filter('number')(agent.stats.InbPercUnansAcdCalls || 0, 0)
            };
            scope.stats[10] = {
              id: "ReadyTime",
              value: $filter('date')(new Date(0, 0, 0).setSeconds(agent.stats.ReadyTime || 0), "HH:mm:ss"),
              rawValue: agent.stats.ReadyTime || 0,
              class: "stat-item-button"
            };
            scope.stats.map((s) => {
              s.label = $translate.instant('AGT_STAT_' + s.id.toUpperCase());
              return s;
            });

            scope.refreshChart();
          });
        }
      };

      scope.refreshChart = () =>{
        if (scope.currentChart) {
          let item = _.filter(config, { id: scope.currentChart});
          scope.chart = [];
          _.forEach(_.get(item,['0','statsIndexList']), (value) => {
            scope.chart.push(scope.stats[value]);
          });
        }
      };

      scope.toggleChart = (itemId) => {
        const _enableCharts = (bar, donut) => {
          scope.displayChartDonut=donut;
          scope.displayChartBar=bar;
        };

        let previousChart = scope.currentChart;
        scope.currentChart = itemId;
        scope.chart = [];

        if (!itemId || itemId == previousChart){
          delete scope.currentChart;
          _enableCharts(false, false);
          return;
        }

        scope.refreshChart();

        switch(itemId) {
        case 'PausedTime':
        case 'InbAcdCallTime':
        case 'OutCallTime':
        case 'WrapupTime':
        case 'ReadyTime':
          _enableCharts(true, false);
          break;
        case 'InbAcdCalls':
        case 'InbAnsAcdCalls':
        case 'InbUnansAcdCalls':
        case 'OutCalls':
          _enableCharts(false, true);
          break; 
        default:
          _enableCharts(false, false);
        }
      };

      scope.$on('AgentStatisticsUpdated', function () {
        scope.updateAgentData();
      });

      scope.updateAgentData();

      const barChartCfg = [10, 0, 1, 3, 4];
      const donutChartCfg = [6, 7, 8];

      var config = [
        { id: "PausedTime", statsIndexList: barChartCfg},
        { id: "InbAcdCallTime", statsIndexList: barChartCfg },
        { id: "OutCallTime", statsIndexList: barChartCfg },
        { id: "WrapupTime", statsIndexList: barChartCfg },
        { id: "ReadyTime", statsIndexList: barChartCfg },
        { id: "InbAcdCalls", statsIndexList: donutChartCfg },
        { id: "InbAnsAcdCalls", statsIndexList: donutChartCfg },
        { id: "OutCalls", statsIndexList: donutChartCfg },
        { id: "InbUnansAcdCalls", statsIndexList: donutChartCfg }
      ];
      
    }

  };
}
