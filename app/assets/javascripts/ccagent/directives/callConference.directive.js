
export default function callConference($translate, $log, CtiProxy, XucPhoneState, XucAgentUser, callControlActions) {

  return {
    require: '^^callControl',
    restrict: 'E',
    replace: true,
    templateUrl: 'assets/javascripts/ccagent/directives/callConference.html',
    scope: {
      call: '=*',
      showRecordingControls: '='
    },
    controller: ($scope) => {
      $scope.conference = XucPhoneState.getDeviceConference();
      $scope.listened = false;
      $scope.recording = false;

      $scope.getCallActions = () => {
        return callControlActions.getDeviceConferenceActions(
          XucPhoneState.STATE_ESTABLISHED,
          $scope.recording,
          $scope.canRecord && $scope.showRecordingControls
        );
      };

      $scope.isMonitored = () => {
        XucAgentUser.isMonitored();
      };

      $scope.canMonitor = () => {
        XucAgentUser.canMonitor();
      };

      $scope.hasNameAndNumber = (call) => {
        return call.otherDName && call.otherDName !== call.otherDN;
      };      

      XucAgentUser.subscribeToListenEvent($scope, (event) => {
        $scope.listened = event.started;
      });

      XucAgentUser.subscribeToAgentState($scope, (state) => {
        $scope.recording = state.monitorState == "ACTIVE";
        $scope.canRecord = state.monitorState == "ACTIVE" || state.monitorState == "PAUSED";
      });
    }
  };
}
