import _ from 'lodash';

export default function callLine($translate, $log, CtiProxy, XucPhoneState, XucAgentUser, XucQueue, callControlActions, applicationConfiguration, audioQualityStatistics, XucPhoneEventListener) {

  return {
    require: '^^callControl',
    restrict: 'E',
    replace: true,
    templateUrl: 'assets/javascripts/ccagent/directives/callLine.html',
    scope: {
      call: '=*',
      showRecordingControls: '='
    },
    controller: ($scope) => {
      $scope.listened = false;
      $scope.recording = false;
      $scope.isMuted = false;
      $scope.audioQuality = undefined;
      $scope.queueDisplayName = '';

      if(typeof($scope.call.queueName) !== "undefined") {
        XucQueue.getQueueByNameAsync($scope.call.queueName).then((q) => {
          $scope.queueDisplayName = q.displayName;
        });
      }

      $scope.getClass = () => {
        var direction = _.toLower($scope.call.direction);
        var acd = $scope.call.acd?'acd':'nonacd';
        var state = _.toLower($scope.call.state);

        return 'call-' + direction +
          ' call-' + acd +
          ' call-' + state +
          ' call-' + direction + '-' + acd + '-' + state;
      };

      $scope.getIconClass = () => {
        switch($scope.call.state) {
        case 'OnHold':
          return 'fa-pause';
        case 'Established':
          return 'fa-play';
        default:
          return 'fa-spinner fa-pulse fa-fw';
        }
      };

      $scope.audioQualityClass = () => {
        if ($scope.audioQuality == audioQualityStatistics.AUDIO_QUALITY_GOOD) return 'good-quality';
        else if ($scope.audioQuality == audioQualityStatistics.AUDIO_QUALITY_MEDIUM) return 'medium-quality';
        else if ($scope.audioQuality == audioQualityStatistics.AUDIO_QUALITY_BAD) return 'bad-quality';
        else return '';
      };
      
      $scope.$on(audioQualityStatistics.AUDIO_QUALITY_EVENT, (event, data)=>{
        $scope.audioQuality = data.quality;
      });

      $scope.isWebRtcActive = function() {
        return CtiProxy.isUsingWebRtc();
      };

      $scope.toggleMicrophone = function(call) {
        CtiProxy.toggleMicrophone(call.uniqueId);
        $scope.isMuted = !$scope.isMuted;
      };

      $scope.$on(callControlActions.MICROPHONE_MUTED_ACTION, () =>{
        $scope.isMuted = true;
      });

      $scope.$on(callControlActions.MICROPHONE_UNMUTED_ACTION, () =>{
        $scope.isMuted = false;
      });

      $scope.getCallActions = () => {
        return callControlActions.getLineActions(
          $scope.call.state,
          $scope.isMuted,
          $scope.recording,
          $scope.canRecord && $scope.showRecordingControls
        );
      };

      $scope.isMonitored = () => {
        XucAgentUser.isMonitored();
      };

      $scope.canMonitor = () => {
        XucAgentUser.canMonitor();
      };

      XucAgentUser.subscribeToListenEvent($scope, (event) => {
        $scope.listened = event.started;
      });

      XucAgentUser.subscribeToAgentState($scope, (state) => {
        $scope.recording = state.monitorState == "ACTIVE";
        if($scope.showRecordingControls) {
          $scope.canRecord = state.monitorState == "ACTIVE" || state.monitorState == "PAUSED";
        }
      });

      XucPhoneEventListener.addEstablishedHandler($scope, () => { $scope.isMuted = false; });

      $scope.hasNameAndNumber = (call) => {
        return call.otherDName && call.otherDName !== call.otherDN; 
      };

    }
  };
}
