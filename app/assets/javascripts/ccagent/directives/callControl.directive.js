export default function callControl(XucPhoneState, CtiProxy, remoteConfiguration, XucUtils, applicationConfiguration, XucSwitchboard, $rootScope, incomingCall, processVolume, audioQualityStatistics, XucPhoneEventListener, $timeout, JitsiProxy) {

  return {
    restrict: 'E',
    templateUrl: 'assets/javascripts/ccagent/directives/callControl.html',
    controller: ($scope) => {
      const keyBindings = applicationConfiguration.getCurrentAppConfig().keyBindings;
      $scope.microphoneIsBroken = false;
      $scope.disableMicrophoneBrokenMessage = false;
      $scope.calls = XucPhoneState.getCalls();
      $scope.audioQuality = undefined;
      $scope.statistics = undefined;
      $scope.callPlaceholderDisplayed = false;
      $scope.placeholderTimeout = undefined;
      $scope.dynamicTranslation = {phoneNb: undefined};

      $scope.audioQualityTimeoutMS = 15000;
      $scope.audioQualityTimeout = undefined;
      $scope.displayAudioQuality = false;
      $scope.callInitError = false;

      JitsiProxy.subscribeToStartVideoError($scope, (event, error) => {
        $scope.videoInitError = error;
      });

      $scope.$watchCollection("calls", function(newVal) {
        if (newVal === undefined || newVal.length === 0) {
          $scope.disableMicrophoneBrokenMessage = false;
          $scope.microphoneIsBroken = false;
          $scope.removeAudioQualityError();
          $scope.clearAudioQualityTimeout();
        }
        $scope.removeCallPlaceholder();
        $scope.cancelPlaceholderTimeout();
      });
      
      const callControlBindings = (event) => {
        if (keyBindings.has(event.key)) {
          event.preventDefault();
          switch (event.key) {
          case 'F3':
            $scope.triggerAction('answer');
            break;
          case 'F4':
            $scope.triggerAction('hangup');
            break;
          case 'F7':
            CtiProxy.completeTransfer();
            break;
          case 'F8':
            tryDirectTransfer(XucUtils.reshapeInput(angular.element('#search').val()));
            break;
          case 'F9':
            CtiProxy.directTransfer(XucSwitchboard.getHoldQueue().number);
            break;
          case 'F10':
            document.getElementById("search").focus();
            break;
          }
        }
      };

      $scope.removeCallPlaceholder = (showError = false) => {
        $scope.callInitError = showError;
        $scope.callPlaceholderDisplayed = false;
        $scope.cancelPlaceholderTimeout();
      };
  
      $scope.displayCallPlaceholder = () => {
        $scope.callPlaceholderDisplayed = true;
        $scope.placeholderTimeout = $timeout($scope.removeCallPlaceholder.bind(null, true), 5000);
      };
  
      $scope.cancelPlaceholderTimeout = () => {
        if ($scope.placeholderTimeout != undefined) {
          $timeout.cancel($scope.placeholderTimeout);
          $scope.placeholderTimeout = undefined;
        }
      };

      $scope.closeCallInitError = () => {
        $scope.callInitError = false;
      };

      $scope.closeVideoInitError = () => {
        $scope.videoInitError = undefined;
      };

      $scope.displayCallInitError = () => {
        return $scope.callInitError;
      };

      $scope.$on('dialingNumber', (e, data) => {
        $scope.closeCallInitError();
        $scope.dynamicTranslation.phoneNb = data;
      });
  
      $rootScope.$on('EventShowCallPlaceHolder', $scope.displayCallPlaceholder);  

      $scope.startAudioQualityMonitoring = (call) => {
        audioQualityStatistics.startAudioQualityMonitoring(call.userData.SIPCALLID);
      };
  
      $scope.stopAudioQualityMonitoring = (call) => {
        audioQualityStatistics.stopAudioQualityMonitoring(call.userData.SIPCALLID);
        $scope.audioQuality = undefined;
      };
      
      $scope.isWebRtcActive = function() {
        return CtiProxy.isUsingWebRtc();
      };

      if ($scope.isWebRtcActive()) {
        XucPhoneEventListener.addEstablishedHandler($scope, $scope.startAudioQualityMonitoring);
        XucPhoneEventListener.addReleasedHandler($scope, $scope.stopAudioQualityMonitoring);

        
        $scope.$on(audioQualityStatistics.AUDIO_QUALITY_EVENT, (event, data) => {
          if ($scope.audioQuality == audioQualityStatistics.AUDIO_QUALITY_MEDIUM || $scope.audioQuality == audioQualityStatistics.AUDIO_QUALITY_BAD) {
            if (data.quality == audioQualityStatistics.AUDIO_QUALITY_GOOD && !$scope.audioQualityTimeout) {
              $scope.audioQualityTimeout = $timeout($scope.removeAudioQualityError, $scope.audioQualityTimeoutMS);
            }
          }
    
          if (data.quality == audioQualityStatistics.AUDIO_QUALITY_MEDIUM || data.quality == audioQualityStatistics.AUDIO_QUALITY_BAD) {
            $scope.enableAudioQualityError();
            $scope.clearAudioQualityTimeout();
          }
          $scope.audioQuality = data.quality;
          $scope.statistics = data.statistics;
        });
      }

      $scope.displayAudioQualityIssue = () => {
        return $scope.displayAudioQuality;
      };

      $scope.removeAudioQualityError = () => {
        $scope.displayAudioQuality = false;
        if ($scope.audioQualityTimeout) $scope.clearAudioQualityTimeout();
      };
  
      $scope.clearAudioQualityTimeout = () => {
        if ($scope.audioQualityTimeout) {
          $timeout.cancel($scope.audioQualityTimeout);
          $scope.audioQualityTimeout = undefined;
        }
      };
  
      $scope.enableAudioQualityError = () => {
        $scope.displayAudioQuality = true;
      };

      document.addEventListener("keydown", callControlBindings);
      $scope.$on('$destroy', function() {
        document.removeEventListener("keydown", callControlBindings);
      });

      $scope.calls = XucPhoneState.getCalls();
      $scope.conference = XucPhoneState.getDeviceConference();

      $scope.showRecordingControls = false;

      remoteConfiguration.getBoolean("showRecordingControls").then(value => $scope.showRecordingControls = value);

      const tryDirectTransfer = (searchInput) => {
        if (searchInput.type == 'phone') {
          CtiProxy.directTransfer(XucUtils.normalizePhoneNb(searchInput.value));
        }
      };

      $scope.isRinging = () => {
        return $scope.calls.some(c => c.state == XucPhoneState.STATE_RINGING);
      };

      $scope.isEstablished = () => {
        return $scope.calls.length > 0 && !$scope.isRinging();
      };

      $scope.triggerAction = (preciseAction = undefined) => {
        if ($scope.isRinging() && preciseAction != 'hangup') {
          CtiProxy.answer();
        }
        if ($scope.isEstablished() && preciseAction != 'answer') {
          CtiProxy.hangup();
        }
      };

      $rootScope.$on(processVolume.EVENT_INPUT_ACTIVE, (event, data) => {
        $scope.microphoneIsBroken = !data;
        if (data == true) $scope.disableMicrophoneBrokenMessage = true;
      });

      $scope.displayMicrophoneError = () => {
        if ($scope.disableMicrophoneBrokenMessage) return false;
        return $scope.microphoneIsBroken;
      };

      $scope.disableMicrophoneError = () => {
        $scope.disableMicrophoneBrokenMessage = true;
      };

    }
  };
}
