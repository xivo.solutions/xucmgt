import {
    CallHistoryGroupViewModel,
    CallHistoryModel,
    CallHistoryViewModel,
    CallStatus
} from "../../xchelper/models/CallHistory.model";
import moment from "moment";
import {isNil} from "lodash";

export class CallHistoryService {

    constructor(private $filter: any) {
    }

    MapCallHistoryModelToCallHistoryViewModel(history: Array<CallHistoryModel>): CallHistoryGroupViewModel {
        let newHistory: Array<CallHistoryViewModel> = [];

        history.forEach((item: CallHistoryModel) => {
            let newCall: CallHistoryViewModel = {
                callStatus: item.status,
                number: item.status === CallStatus.Emitted ? item.dstNum : item.srcNum,
                firstName: item.status === CallStatus.Emitted ? item.dstFirstName : item.srcFirstName,
                lastName: item.status === CallStatus.Emitted ? item.dstLastName : item.srcLastName,
                phoneStatus: item.status === CallStatus.Emitted ? item.dstPhoneStatus : item.srcPhoneStatus,
                videoStatus: item.status === CallStatus.Emitted ? item.dstVideoStatus : item.srcVideoStatus,
                userName: item.status === CallStatus.Emitted ? item.dstUsername : item.srcUsername,
                initials: "",
                day : item.start,
                duration: item.duration
            };
            if (newCall.firstName !== null && newCall.firstName !== '') newCall.initials += newCall.firstName.charAt(0)
            if (newCall.initials !== '') newCall.initials += ' '
            if (newCall.lastName !== null && newCall.lastName !== '') newCall.initials += newCall.lastName.charAt(0)
            newHistory.push(newCall);
        });

        let filteredCallHistory: any = {} // See CallHistoryGroupViewModel in CallHistory.model.ts
        newHistory.filter(elem => elem.number !== null).forEach((call) => {
            if (!isNil(call.day)) {
                const date = this.$filter('date')(new Date(call.day), "yyyy-MM-dd")
                let dateIndex = date.toString()
                if (!filteredCallHistory[dateIndex]) {
                    filteredCallHistory[dateIndex] = {}
                }
                let userIndex = call.userName ? call.userName : call.number
                if (!filteredCallHistory[dateIndex][userIndex]) {
                    filteredCallHistory[dateIndex][userIndex] = []
                }
                filteredCallHistory[dateIndex][userIndex].push(call)
            }
        })
        return filteredCallHistory as CallHistoryGroupViewModel
    }
}