import {
  MembershipStatus,
  UserGroup,
} from "../../xchelper/models/UserGroup.model";
import { RichWindow } from "../../RichWindow";
import { XucLink } from "xccti/services/XucLink";
import { ILogService, IRootScopeService } from "angular";

export class UserGroupService {
  private _userGroups?: Array<UserGroup> = undefined;
  private _showPauseActions: Boolean = true;

  get userGroups(): Array<UserGroup> | undefined {
    return this._userGroups;
  }

  static $inject = ["$window", "$log", "XucLink", "$rootScope", "remoteConfiguration"];

  constructor(
    private $window: RichWindow,
    private $log: ILogService,
    private XucLink: XucLink,
    private $rootScope: IRootScopeService,
    private remoteConfiguration: any
  ) {}

  forceScopeDigest() {
    if(!this.$rootScope.$$phase) this.$rootScope.$apply();
  }

  init() {
    this.$log.info("Starting UserGroup service");
    this.XucLink.whenLoggedOut().then(this.unInit);
    this.$window.Cti?.setHandler(
      this.$window.Cti.MessageType.USERGROUPS,
      this.onGetUserGroups.bind(this)
    );
    this.$window.Cti?.setHandler(
      this.$window.Cti.MessageType.USERGROUPUPDATE,
      this.onGetUserGroupUpdate.bind(this)
    );
  }

  private unInit = (): void => {
    this.$log.info("Unloading UserGroup service");
    this._userGroups = undefined;
  };

  canShowPauseActions = (): Boolean => {
    return this._showPauseActions;
  }

  /**
   * Method Called when the WS get a USERGROUPS MessageType
   * @param userGroups
   */
  onGetUserGroups(userGroups: Array<UserGroup>): void {
    this._userGroups = userGroups;
    this.remoteConfiguration.getBoolean("ucShowGroupControl").then((value: boolean) =>
      this._showPauseActions = value
    );
    this.forceScopeDigest();
  }

  handleNewGroup = (newGroup: UserGroup): boolean => {
    if (
      !this._userGroups ||
      (!this._userGroups?.some((group) => group.groupId === newGroup.groupId) &&
      newGroup.membershipStatus !== MembershipStatus.Exited)
    ) {
      this._userGroups = [...(this._userGroups || []), newGroup];
      return true;
    }
    return false;
  }

  handleExistingGroup = (groupUpdate: UserGroup): void => {
   this._userGroups = this._userGroups
    ?.filter(
      (group) =>
        group.groupId !== groupUpdate.groupId ||
        groupUpdate.membershipStatus !== MembershipStatus.Exited
    )
    .map((group) =>
      group.groupId === groupUpdate.groupId
        ? { ...group, ...groupUpdate }
        : group
    );
  }

  onGetUserGroupUpdate(userGroup: UserGroup): void {
    const isNew = this.handleNewGroup(userGroup);
    if (!isNew) {
      this.handleExistingGroup(userGroup);
    }
    this.$rootScope.$broadcast('userGroupUpdate');
    this.forceScopeDigest();
  }

  callGroup(groupNumber: number): void {
    this.$window?.Cti.dial(groupNumber);
  }

  pauseGroup(groupId: number): void {
    this.$window?.Cti.pauseUserGroup(groupId)
  }

  unpauseGroup(groupId: number): void {
    this.$window?.Cti.unpauseUserGroup(groupId)
  }
}
