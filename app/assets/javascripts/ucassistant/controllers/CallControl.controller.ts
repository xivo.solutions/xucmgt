import { IRootScopeService, IScope, ITimeoutService, IPromise } from 'angular'
import _ from 'lodash'
import {ApplicationConfiguration, AppConfig} from 'xccti/services/applicationConfiguration.provider'
import { CtiProxy } from 'xccti/services/ctiProxy'
import { XucPhoneState } from 'xccti/services/XucPhoneState'
import JitsiProxy from 'xchelper/services/jitsiProxy.service'
import processVolume from 'xchelper/services/processVolume.factory'

type Conference = {
  conferenceNumber: number
  currentUserRole: string
  participants: Array<Participant>
}

type Call = {
  userData: any
  uniqueId: number
  conference: Conference
  state: string
}

type Participant = {
  index: number
  kicked: boolean
  isMe: boolean
}

type AudioStatistics = {
  uploadStatistics: {
    packetsLostPercentage: number
    jitter: number
    roundTripTime: number
  }
  downloadStatistics: {
    packetsLostPercentage: number
    jitter: number
  }
}

export default class CallControlController {
  
  public audioQualityTimeoutMS: number
  public audioQualityTimeout:  IPromise<void> | undefined
  public displayAudioQuality: boolean
  public appConfig: AppConfig
  public calls: Array<Call>
  public conference: {calls: Array<Call>, active: boolean}
  public isMuted: boolean
  public microphoneIsBroken: boolean
  public disableMicrophoneBrokenMessage: boolean
  public audioQuality: string | undefined
  public statistics: AudioStatistics | undefined
  public callPlaceholderDisplayed: boolean
  public placeholderTimeout: IPromise<void> | undefined
  public callInitError: boolean
  public dynamicTranslation: {phoneNb: number | undefined} = {phoneNb: undefined}
  public videoInitError: string | undefined
  
  static $inject = ["$scope", "$timeout", "XucPhoneState", "applicationConfiguration",
  "CtiProxy", "keyboard", "processVolume", "$rootScope", "audioQualityStatistics",
  "XucPhoneEventListener", "JitsiProxy", "incomingCall", "onHold"]
  
  constructor(private $scope: IScope,
    private $timeout: ITimeoutService,
    private XucPhoneState: XucPhoneState, 
    private applicationConfiguration: ApplicationConfiguration,
    private CtiProxy: CtiProxy,
    private keyboard: any,
    private processVolume: processVolume,
    private $rootScope: IRootScopeService,
    private audioQualityStatistics: any,
    private XucPhoneEventListener: any,
    private JitsiProxy: JitsiProxy,
    private incomingCall: any,
    private onHold: any) {
      
      this.audioQualityTimeoutMS = 15000
      this.displayAudioQuality = false
      this.appConfig = this.applicationConfiguration.getCurrentAppConfig()
      this.calls = XucPhoneState.getCalls()
      this.conference = XucPhoneState.getDeviceConference()
      this.isMuted = false
      this.microphoneIsBroken = false
      this.disableMicrophoneBrokenMessage = false
      this.callPlaceholderDisplayed = false
      this.callInitError = false
      
      this.JitsiProxy.subscribeToStartVideoError(this.$scope, (event, error) => {
        this.videoInitError = error
      })
      
      this.$scope.$watchCollection(() => {return this.calls}, (newVal: any) => {
        if (newVal === undefined || newVal.length === 0) {
          this.isMuted = false
          this.disableMicrophoneBrokenMessage = false
          this.microphoneIsBroken = false
          this.removeAudioQualityError()
          this.clearAudioQualityTimeout()
        }
        this.removeCallPlaceholder()
        this.cancelPlaceholderTimeout()
      })
      
      this.$rootScope.$on('EventShowCallPlaceHolder', this.displayCallPlaceholder)
      
      if (this.isWebRtcActive()) {
        this.XucPhoneEventListener.addEstablishedHandler(this.$scope, this.startAudioQualityMonitoring)
        this.XucPhoneEventListener.addReleasedHandler(this.$scope, this.stopAudioQualityMonitoring)
        
        this.$scope.$on(audioQualityStatistics.AUDIO_QUALITY_EVENT, (event, data: {quality: string, statistics: AudioStatistics}) => {
          if (this.audioQuality == audioQualityStatistics.AUDIO_QUALITY_MEDIUM || this.audioQuality == audioQualityStatistics.AUDIO_QUALITY_BAD) {
            if (data.quality == audioQualityStatistics.AUDIO_QUALITY_GOOD && !this.audioQualityTimeout) {
              this.audioQualityTimeout = $timeout(this.removeAudioQualityError, this.audioQualityTimeoutMS)
            }
          }
          
          if (data.quality == audioQualityStatistics.AUDIO_QUALITY_MEDIUM || data.quality == audioQualityStatistics.AUDIO_QUALITY_BAD) {
            this.enableAudioQualityError()
            this.clearAudioQualityTimeout()
          }
          
          this.audioQuality = data.quality
          this.statistics = data.statistics
        })
      }
      
      this.$scope.$on('dialingNumber', (e, data) => {
        this.closeCallInitError()
        this.dynamicTranslation.phoneNb = data
      })
      
      this.$rootScope.$on(this.processVolume.EVENT_INPUT_ACTIVE, (event, data) => {
        this.microphoneIsBroken = !data
        if (data == true) this.disableMicrophoneBrokenMessage = true
      })
    }
    
    startAudioQualityMonitoring = (call: Call): void => {
      this.audioQualityStatistics.startAudioQualityMonitoring(call.userData.SIPCALLID)
    }
    
    stopAudioQualityMonitoring = (call: Call): void => {
      if (call) this.audioQualityStatistics.stopAudioQualityMonitoring(call.userData.SIPCALLID)
      this.audioQuality = undefined
    }
    
    isWebRtcActive = (): boolean => {
      return this.CtiProxy.isUsingWebRtc()
    }

    
    removeCallPlaceholder = (showError = false): void => {
      this.callInitError = showError
      this.callPlaceholderDisplayed = false
      this.cancelPlaceholderTimeout()
    }
    
    displayCallPlaceholder = (): void => {
      this.callPlaceholderDisplayed = true
      this.placeholderTimeout = this.$timeout(this.removeCallPlaceholder.bind(null, true), 5000)
    }
    
    cancelPlaceholderTimeout = (): void => {
      if (this.placeholderTimeout != undefined) {
        this.$timeout.cancel(this.placeholderTimeout)
        this.placeholderTimeout = undefined
      }
    }
    
    removeAudioQualityError = (): void => {
      this.displayAudioQuality = false
    }
    
    clearAudioQualityTimeout = (): void => {
      if (this.audioQualityTimeout) {
        this.$timeout.cancel(this.audioQualityTimeout)
        this.audioQualityTimeout = undefined
      }
    }
    
    enableAudioQualityError = (): void => {
      this.displayAudioQuality = true
    }
    
    audioQualityClass = (): string => {
      if (this.audioQuality == this.audioQualityStatistics.AUDIO_QUALITY_GOOD) return 'good-quality'
      else if (this.audioQuality == this.audioQualityStatistics.AUDIO_QUALITY_MEDIUM) return 'medium-quality'
      else if (this.audioQuality == this.audioQualityStatistics.AUDIO_QUALITY_BAD) return 'bad-quality'
      else return ''
    }
    
    displayAudioQualityIssue = (): boolean => {
      return this.displayAudioQuality
    }
    
    hold = (uniqueId: number): void => {
      this.isMuted = false
      this.CtiProxy.hold(uniqueId)
    }
    
    isSecondCallAndIsYealink = (): boolean => {
      return this.calls.length > 1 && this.CtiProxy.getDeviceVendor() === 'Yealink'
    }
    
    hangup = (uniqueId: number): void => {
      this.CtiProxy.hangup(uniqueId)
    }
    
    answer = (uniqueId: number): void => {
      if (this.JitsiProxy.videoIsOngoing()) this.JitsiProxy.muteMicrophoneInJitsi()
      this.CtiProxy.answer(uniqueId)
    }
    
    isConferenceCall = (call: Call): boolean => {
      return call.conference !== undefined;
    }

    hasConferenceCall = (): boolean => {
      return this.calls.find(call => this.isConferenceCall(call)) !== undefined;
    }

    hasCall = (): boolean => {
      return this.calls.find(call => !this.isConferenceCall(call)) !== undefined;
    }

    makeConference = (): void => {
      this.CtiProxy.conference()
    }

    displayMakeConferenceButton = (): boolean => {
      return this.calls.length > 1 && this.isConferenceCapable() && this.calls.every(call => !this.isConferenceCall(call))
    }

    closeCallInitError = (): void => {
      this.callInitError = false
    }
    
    closeVideoInitError = (): void => {
      this.videoInitError = undefined
    }
    
    displayCallInitError = (): boolean => {
      return this.callInitError
    }
    
    transfer = (): void => {
      this.CtiProxy.completeTransfer()
    }
    
    toggleMicrophone = (call: Call): void => {
      if (call.conference) {
        this.isMuted ? this.conferenceUnmuteMe(call.conference.conferenceNumber) : this.conferenceMuteMe(call.conference.conferenceNumber)
      } else {
        this.CtiProxy.toggleMicrophone(call.uniqueId)
        this.isMuted = !this.isMuted
      }
    }

    includeCallToConference = () : void => {
      this.CtiProxy.includeToConference()
    }

    displayIncludeCallToConferenceButton = (): boolean => {
      return this.calls.length > 1 && this.hasConferenceCall() && this.hasCall()
    }

    displayTransferButton = () : boolean => {
      return (this.calls.length === 2 && this.calls.every(call => !this.isConferenceCall(call)))
    }

    conferenceMuteMe = (conferenceNumber: number): void => {
      this.CtiProxy.conferenceMuteMe(conferenceNumber)
      this.isMuted = true
    }
  
    conferenceUnmuteMe = (conferenceNumber: number): void => {
      this.CtiProxy.conferenceUnmuteMe(conferenceNumber)
      this.isMuted = false
    }
    
    conferenceMuteAll = (conferenceNumber: number): void => {
      this.CtiProxy.conferenceMuteAll(conferenceNumber)
    }
    
    conferenceUnmuteAll = (conferenceNumber: number): void => {
      this.CtiProxy.conferenceUnmuteAll(conferenceNumber)
    }
    
    conferenceMute = (conferenceNumber: number, index: number): void => {
      this.CtiProxy.conferenceMute(conferenceNumber, index)
    }
    
    conferenceUnmute = (conferenceNumber: number, index: number): void => {
      this.CtiProxy.conferenceUnmute(conferenceNumber, index)
    }
    
    conferenceKick = (conferenceNumber: number, participant: Participant): void => {
      participant.kicked = true
      this.CtiProxy.conferenceKick(conferenceNumber, participant.index)
    }
    
    canMuteAll = (conference: Conference): boolean => {
      if(conference.currentUserRole == 'Organizer') {
        var nbUnmuted = _.filter(conference.participants, {'isMe': false, 'isMuted': false}).length
        if(nbUnmuted > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    }
    
    canUnmuteAll = (conference: Conference): boolean => {
      if(conference.currentUserRole == 'Organizer' && !this.canMuteAll(conference)) {
        var nbMuted = _.filter(conference.participants, {'isMe': false, 'isMuted': true}).length
        if(nbMuted > 0) {
          return true
        } else {
          return false
        }
      } else {
        return false
      }
    }
    
    displayParticipantActions = (conference: Conference, participant: Participant): boolean => {
      if(!participant.kicked && (participant.isMe || conference.currentUserRole == 'Organizer')) {
        return true
      } else {
        return false
      }
    }
    
    faClassFromState = (state: string): string => {
      switch (state) {
        case this.XucPhoneState.STATE_RINGING:
        case this.XucPhoneState.STATE_DIALING:
        return "fa-spinner fa-pulse fa-fw"
        case this.XucPhoneState.STATE_ONHOLD:
        return "xivo-pause-appel call-on-hold"
      }
      return "fa-exclamation-triangle"
    }
    
    isConferenceCapable = (): boolean => {
      return this.CtiProxy.isConferenceCapable()
    }
    
    showKeyboard = (): void => {
      this.keyboard.show()
    }
    
    canChat = (call: Call, username: string): boolean => {
      return (call.state === 'Established' || call.state === 'OnHold') && username != undefined
    }
    
    isConferenceAndIsMuted = (uniqueId: number): boolean => {
      return this.XucPhoneState.isConferenceAndIsMuted(uniqueId)
    }
    
    displayMicrophoneError = (): boolean => {
      if (this.disableMicrophoneBrokenMessage) return false
      return this.microphoneIsBroken
    }
    
    disableMicrophoneError = (): void => {
      this.disableMicrophoneBrokenMessage = true
    }
    
    muteWebRtcUser = (call: Call): boolean => {
      if (this.isWebRtcActive() && call.state === 'Established' && !call.conference) {
        return true
      } else {
        return false
      }
    }
    
    conferenceUndeaf = (conferenceNumber: number, participantIndex: number): void => {
      this.CtiProxy.conferenceUndeafen(conferenceNumber, participantIndex)
    }
  }  
