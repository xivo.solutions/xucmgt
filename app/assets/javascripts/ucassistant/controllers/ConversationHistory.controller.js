export default class ConversationHistoryController {

  constructor($scope, $state) {
    $scope.gotoConversation = (remoteParty) => {
      $state.go("interface.conversation", {"remoteParty": remoteParty});
    };
  }
}
