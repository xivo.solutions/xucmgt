import _ from "lodash"
import "angular-ui-bootstrap"
import { IHttpResponse, ILogService, IScope } from "angular"
import MeetingRoomService, { MeetingRoom } from "ucassistant/services/meetingRoom.service"
import { StateService } from "@uirouter/core"
import { RichWindow } from "RichWindow"

class AjaxRequest {
  constructor(public ongoing: boolean = false, public error?: string) {}
}

class ErrorResponse {
  constructor(public error: string) {}
}

class PersonalMeetingRoom extends MeetingRoom {
  public roomType = "personal"
  constructor(
    public id: number | undefined,
    public displayName: string,
    public userPin?: string,
    public number?: string,
    public uuid?: string
  ) {
    super(id, displayName, userPin, number, uuid)
  }
}

export class MeetingRoomForm {
  constructor(
    public displayName: string = "",
    public userPin?: string
  ) {}
}

export default class MeetingRoomController {
  public favorite: boolean = false
  public ajaxRequest: AjaxRequest
  public form: MeetingRoomForm
  public currentMeetingroom: MeetingRoom | undefined
  public numberPattern: RegExp = /^[0-9]+$/

  constructor(
    private $scope: IScope,
    private $window: RichWindow,
    private $state: StateService,
    private $uibModal: ng.ui.bootstrap.IModalService,
    private $uibModalStack: ng.ui.bootstrap.IModalStackService,
    private $log: ILogService,
    private MeetingRoomService: MeetingRoomService,
    private XucDirectory: any,
    public roomId: string
  ) {
    this.ajaxRequest = new AjaxRequest()
    if (this.roomId) {
      this.MeetingRoomService.get(this.roomId).then((response: IHttpResponse<MeetingRoom>) => {
        this.currentMeetingroom = response.data
        this.populateForm(response)
      })
    }
    this.form = new MeetingRoomForm()
  }

  populateForm(resp: IHttpResponse<MeetingRoom>) {
    this.favorite = this.isFavorite()
    this.form.displayName = resp.data.displayName
    this.form.userPin = resp.data.userPin
  }

  isFavorite() {
    return _.some(this.XucDirectory.getFavoriteContactSheet(), c => {
      const maybeMeetingRoomId = c.sources.find((source: any) => {
        return source.name === 'xivo_meetingroom';
      });
      if (maybeMeetingRoomId && maybeMeetingRoomId.id) {
        return maybeMeetingRoomId.id === this.roomId;
      } else {
        return false;
      }
    });
  }

  isValidForm() {
    return !(_.isEmpty(this.form.displayName))
  }

  saveForm() {
    let mr: PersonalMeetingRoom = new PersonalMeetingRoom(
      parseInt(this.roomId) || undefined,
      this.form.displayName,
      this.form.userPin != "" ? this.form.userPin : undefined,
      this.currentMeetingroom?.number
    )
    this.save(mr).catch(this.catchMeetingRoomError.bind(this))
  }

  async save(meetingroom: PersonalMeetingRoom) {

    this.ajaxRequest.ongoing = true

    try {
      const meetingRoomResponse: IHttpResponse<MeetingRoom> = this.roomId
      ? await this.MeetingRoomService.update(meetingroom)
      : await this.MeetingRoomService.add(meetingroom)

      if (this.favorite && !this.isFavorite()) {
        this.$window.Cti.addFavorite(meetingRoomResponse.data.id?.toString(), "xivo_meetingroom")
      }
      if (!this.favorite && this.isFavorite()) {
        this.$window.Cti.removeFavorite(meetingRoomResponse.data.id?.toString(), "xivo_meetingroom")
      }
  
      this.close("interface.favorites")
    }
    finally {
      this.ajaxRequest.ongoing = false
    }
  }

  catchMeetingRoomError(error: IHttpResponse<ErrorResponse>) {
    this.$log.error(error.data)
    this.ajaxRequest.error = "error" + error.data.error
  }

  remove() {
    this.MeetingRoomService.remove(this.roomId).then(() => {
      this.close("interface.favorites")
    })
  }

  close(dest: string) {
    this.$state.go(dest)
    this.$uibModalStack.dismissAll()
  }

  dismiss() {
    this.roomId
      ? this.close("interface.favorites")
      : this.close("interface.menu")
  }

  confirmDelete() {
    this.$uibModal.open({
      templateUrl: "confirmDialog.html",
      size: "sm",
      scope: this.$scope,
      controller: "ConfirmationModalController",
      controllerAs: "modalCtrl",
      resolve: {
        params: () => {
          return { item: "MEETING_ROOM" }
        },
      },
    })
  }
}
