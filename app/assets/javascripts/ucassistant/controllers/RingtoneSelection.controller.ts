import {IOnDestroy} from "angular";

export default class RingtoneSelectionController implements IOnDestroy {
    static $inject = [
        "$translate", "webRtcAudio"
    ]

    public ringtones = [
        {
            id: 1,
            name: this._translate.instant('DEFAULT_RINGTONE_NAME'),
            file: 'incoming_call.mp3',
            title: this._translate.instant('DEFAULT_RINGTONE_NAME'),
            author: ''
        },
        {
            id: 2,
            name: 'The Return',
            file: 'the_return.mp3',
            title: 'The Return',
            author: 'Alexander Nakarada'
        },
        {
            id: 3,
            name: 'Keys of Moon - The Success',
            file: 'keys_of_moon_the_success.mp3',
            title: 'Keys of Moon - The Success',
            author: 'Alexander Nakarada'
        },
        {
            id: 4,
            name: 'Another Happy Ukulele Song',
            file: 'another_happy_ukulele_song.mp3',
            title: 'Another Happy Ukulele Song',
            author: ''
        },
        {
            id: 5,
            name: 'Cordless Phone Ring',
            file: 'cbakos_cordless_phone_ring.wav',
            title: 'Cordless Phone Ring',
            author: 'Cbakos'
        },
        {
            id: 6,
            name: 'Fekete Rigo',
            file: 'sandor_molnar_fekete_rigo.mp3',
            title: 'Fekete Rigo',
            author: 'Sandor Molnar'
        },
        {
            id: 7,
            name: 'Pop Ringtone',
            file: 'imthemap_pop_ringtone.wav',
            title: 'Pop Ringtone',
            author: 'Imthemap'
        },
        {
            id: 8,
            name: 'British Telephone',
            file: 'inchadney_british_telephone.wav',
            title: 'British Telephone',
            author: 'Inchadney'
        },
        {
            id: 9,
            name: 'Adventure',
            file: 'adventure.mp3',
            title: 'Adventure',
            author: 'Alexander Nakarada'
        },
        {
            id: 10,
            name: 'Anthem of the united sloths',
            file: 'anthem_of_the_united_sloths.mp3',
            title: 'Anthem of the united sloths',
            author: 'Alexander Nakarada'
        },
        {
            id: 11,
            name: 'Armageddon',
            file: 'armageddon.mp3',
            title: 'Armageddon',
            author: 'Alexander Nakarada'
        },
        {
            id: 12,
            name: 'Banjo fever',
            file: 'banjo_fever.mp3',
            title: 'Banjo fever',
            author: 'Alexander Nakarada'
        },
        {
            id: 13,
            name: 'Chronos',
            file: 'chronos.mp3',
            title: 'Chronos',
            author: 'Alexander Nakarada'
        },
        {
            id: 14,
            name: 'Coconut caprice',
            file: 'coconut_caprice.mp3',
            title: 'Coconut caprice',
            author: 'Alexander Nakarada'
        },
        {
            id: 15,
            name: 'Regrock',
            file: 'regrock.mp3',
            title: 'Regrock',
            author: 'Alexander Nakarada'
        },
        {
            id: 16,
            name: 'Sunny rasta',
            file: 'sunny_rasta.mp3',
            title: 'Sunny rasta',
            author: 'Alexander Nakarada'
        }
    ];

    selectedRingtone: any;
    listeningRingtone = this.ringtones[0];
    ringtoneListener: HTMLElement | null = null;
    playPromise = undefined;

    constructor(private _translate: any, private _webRtcAudio: any) {
        let user_ringtone = window.localStorage.getItem('user_ringtone');
        if (user_ringtone !== null) {
            this.selectedRingtone = JSON.parse(user_ringtone);
        } else {
            this.selectedRingtone = this.ringtones[0];
        }
        this.listeningRingtone = this.selectedRingtone;
    }

    changeRingtone = (model: any) => {
        this.selectedRingtone = model;
        window.localStorage.setItem('user_ringtone', JSON.stringify(this.selectedRingtone));
        this._webRtcAudio.changeRingtone(model);
    };

    listenToRingtone(ringtone: any, event: any) {
        event.stopPropagation();
        if (this.listeningRingtone !== null && ringtone.name !== this.listeningRingtone.name && this.playPromise !== undefined) {
            this.stopAudio(this.ringtoneListener)
        }

        if (this.playPromise === undefined) {
            this.listeningRingtone = ringtone;
            this.startAudio(this.ringtoneListener)
        } else {
            this.stopAudio(this.ringtoneListener)
        }
    }

    stopAudio(element: any) {
        element.pause();
        element.currentTime = 0;
        this.playPromise = undefined;
    };

    startAudio(element: any) {
        element.src = 'assets/audio/pratix/' + this.listeningRingtone.file;
        element.currentTime = 0;
        this.playPromise = element.play();
    }

    onViewInit() {
        this.ringtoneListener = document.getElementById('ringtone-listener');
    }

    $onDestroy(): void {
        if (this.playPromise !== undefined) {
            this.stopAudio(this.ringtoneListener)
        }
    }

}