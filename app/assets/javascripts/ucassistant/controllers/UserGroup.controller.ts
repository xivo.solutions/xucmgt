import { IScope } from "angular";

import { StateService } from "@uirouter/core";
import {UserGroupService} from "../services/UserGroup.service";
import { MembershipStatus, UserGroup } from "xchelper/models/UserGroup.model";
import { RichWindow } from "RichWindow";

export class UserGroupController {
  static $inject = [
    "$scope",
    "$rootScope",
    "$state",
    "$translate",
    "UserGroupService",
    "debounce"
  ];

  groups: UserGroup[] | undefined =  undefined;

  constructor(
    private $scope: IScope,
    private $rootScope: IScope,
    private $state: StateService,
    private $translate: any,
    private UserGroupService: UserGroupService,
    private debounce: any
  ) {
    this.initializeGroups();
    this.$rootScope.$on('userGroupUpdate', () => {

      this.initializeGroups();
      if(!this.groups || this.groups.length == 0) {
        this.$state.go('interface.favorites')
      }
    }
  );
  }

  canShowPauseActions(): Boolean {
    return this.UserGroupService.canShowPauseActions();
  }

  toggleClickAnimation(elementId: string, className: string, duration: number): void {
    const element = document.getElementById(elementId);
    if (!element) return;

    element.classList.add(className);
    setTimeout(() => element.classList.remove(className), duration);
  }

  toggleGroupMembership = this.debounce((group: UserGroup): void => {
    const isPaused = group.membershipStatus === 'Paused';
    const action = isPaused ? 'join' : 'leave';

    this.toggleClickAnimation(`${action}Group-${group.groupId}`, 'clicked', 10000);
    (isPaused ? this.unpauseGroup : this.pauseGroup)(group.groupId);
  });

  callGroup = this.debounce((groupNumber: number): void => {
    this.UserGroupService.callGroup(groupNumber)
  });

  pauseGroup = this.debounce((groupId: number): void => {
    this.UserGroupService.pauseGroup(groupId)
  });

  unpauseGroup = this.debounce((groupId: number): void => {
      this.UserGroupService.unpauseGroup(groupId)
  });

  private initializeGroups(): void {
    this.groups = this.UserGroupService.userGroups?.sort((a, b) => {
      if (a.membershipStatus === MembershipStatus.Available && b.membershipStatus !== MembershipStatus.Available) {
        return -1;
      }
      if (b.membershipStatus === MembershipStatus.Available && a.membershipStatus !== MembershipStatus.Available) {
        return 1;
      }

      return a.groupName.localeCompare(b.groupName);
    });
  }
}
