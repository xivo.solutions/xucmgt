package controllers.helpers

import com.googlecode.htmlcompressor.compressor.HtmlCompressor
import javax.inject.Inject
import play.api.{Environment, Mode}
import play.twirl.api.Html

class PrettyController @Inject() (env: Environment) {

  def prettify(html: Html): Html = {
    val compressor = new HtmlCompressor()
    val cleanBody  = html.body.trim()

    if (env.mode == Mode.Dev) {
      compressor.setPreserveLineBreaks(true)
      compressor.setRemoveIntertagSpaces(false)
      compressor.setRemoveComments(false)
    }

    val compressedBody = compressor.compress(cleanBody)

    Html(compressedBody)

  }

}
