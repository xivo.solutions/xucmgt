package controllers.helpers

import configuration.HeadsetConfig
import play.api.mvc.InjectedController

trait HeadsetController extends InjectedController {

  def getJsonConfigs(config: HeadsetConfig) =
    Action {
      Ok(config.getJsonConfigs("conf/headsets"))
    }
}
