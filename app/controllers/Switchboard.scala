package controllers

import configuration.CcConfig
import controllers.helpers.PrettyController

import javax.inject.Inject
import play.api.Logger
import play.api.mvc.ControllerComponents

class Switchboard @Inject() (
    config: CcConfig,
    prettyCtrl: PrettyController,
    cc: ControllerComponents
) extends MainController(cc) {
  override val log     = Logger(getClass.getName)
  override val isAgent = true
  val title            = "XiVO Switchboard"

  override def connect =
    Action { implicit request =>
      log.debug(s"New connection to Switchboard from ${request.remoteAddress}")
      Ok(
        prettyCtrl.prettify(
          views.html.switchboard.index(title, config)(request.lang, request)
        )
      )
    }

}
