package controllers

import java.net.URLDecoder
import javax.inject.Inject
import play.api.mvc.{
  AbstractController,
  Action,
  AnyContent,
  ControllerComponents
}
import play.api.{Environment, Logger}

import scala.concurrent.ExecutionContext

class WindowsInstaller @Inject() (
    env: Environment,
    ec: ExecutionContext,
    cc: ControllerComponents
) extends AbstractController(cc) {
  val log: Logger = Logger(getClass.getName)

  def latest: Action[AnyContent] =
    Action { _ =>
      val folder = env.getFile("/updates/win64")

      if (folder.exists) {
        val latestSetup = folder
          .listFiles()
          .filter(f => f.getName.endsWith(".exe"))
          .sortBy(f => f.lastModified())
          .reverse
          .headOption

        latestSetup match {
          case Some(f) => Redirect("/updates/win64/" + f.getName)
          case None    => NotFound
        }
      } else NotFound
    }

  def serveFile(file: String): Action[AnyContent] =
    Action { _ =>
      val path = URLDecoder.decode(file, "UTF-8")
      if (!path.contains("..")) {
        val f = env.getFile("/updates/" + path)

        if (f.exists) {
          Ok.sendFile(f, inline = true)(ec, cc.fileMimeTypes)
        } else {
          NotFound
        }
      } else {
        NotFound
      }
    }
}
