package controllers

import configuration.UcConfig
import play.api.mvc.{Action, AnyContent, ControllerComponents}

import javax.inject.Inject

class UcAssistant @Inject() (config: UcConfig, cc: ControllerComponents)
    extends MainController(cc) {

  val loginImage: String =
    routes.Assets.versioned("images/avencallbox.jpg").toString
  override val isAgent = false
  val title            = "XiVO UC Assistant"

  override def connect: Action[AnyContent] =
    Action(implicit request => {
      log.debug(s"New connection to UC assistant from ${request.remoteAddress}")
      Ok(views.html.ucassistant.index(title, config)(request.lang, request))
    })
  def loginPage: Action[AnyContent] =
    Action(implicit request =>
      Ok(
        views.html.ucassistant.login("ucassistant Assistant Login", loginImage)
      )
    )
  def mainPage: Action[AnyContent] =
    Action(implicit request => Ok(views.html.ucassistant.main()))
  def contacts: Action[AnyContent] =
    Action(implicit request => Ok(views.html.ucassistant.contacts()))
  def callcontrol: Action[AnyContent] =
    Action(implicit request => Ok(views.html.ucassistant.callControl()))
  def history: Action[AnyContent] =
    Action(implicit request => Ok(views.html.ucassistant.history()))
  def userGroup: Action[AnyContent] =
    Action(implicit request => Ok(views.html.ucassistant.userGroup()))
  def menu: Action[AnyContent] =
    Action(implicit request => Ok(views.html.ucassistant.menu()))
  def personalcontact: Action[AnyContent] =
    Action(implicit request => Ok(views.html.ucassistant.personalContact()))
  def ringtoneSelection: Action[AnyContent] =
    Action(implicit request => Ok(views.html.ucassistant.ringtoneSelection()))
  def createmeetingroom: Action[AnyContent] =
    Action(implicit request => Ok(views.html.ucassistant.meetingRooms()))
  def sso: Action[AnyContent] =
    Action(implicit request =>
      request.getQueryString("token") match {
        case None =>
          Redirect(
            s"http://${config.hostAndPort}/xuc/sso?orig=http://${request.host}${request.path}"
          )
        case Some(token) =>
          Ok(
            views.html.ucassistant.index(title, config, Some(token))(
              request.lang,
              request
            )
          )
      }
    )

}
