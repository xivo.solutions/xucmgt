package controllers

import configuration.UcConfig
import controllers.helpers.{EmailTemplate, HeadsetController}
import play.api.Environment
import play.api.libs.json.Json
import play.api.mvc.{ControllerComponents, InjectedController}

import javax.inject.Inject

class ClientConfiguration @Inject() (
    config: UcConfig,
    env: Environment,
    cc: ControllerComponents
) extends InjectedController
    with EmailTemplate
    with HeadsetController {
  setControllerComponents(cc)

  def getVersion() =
    Action {
      Ok(Json.obj("value" -> config.appVersion))
    }

  def getEmailTemplate() = get(config)(env)

  def getNestedConfig(path: String) = {
    val nestedConfig = config.getNestedApplicationConf(
      path.stripSuffix("/").replace("/", "."),
      "client"
    )
    Action {
      nestedConfig match {
        case Some(nestedConfig) => Ok(Json.obj("value" -> nestedConfig))
        case None               => NotFound
      }
    }
  }

  def getHeadsetsConfig() = getJsonConfigs(config)

}
