package controllers

import com.google.inject.Inject
import models.{
  KerberosAuthentication,
  TokenAuthentication,
  XivoAuthentication,
  XucUser
}
import play.api.Logger
import play.api.data.Form
import play.api.data.Forms.*
import play.api.i18n.I18nSupport
import play.api.mvc.{Action, ControllerComponents, InjectedController}

abstract class MainController @Inject() (cc: ControllerComponents)
    extends InjectedController
    with I18nSupport {
  setControllerComponents(cc)

  val log = Logger(getClass.getName)

  val isAgent: Boolean

  def connect: Action[_]

  def secondStepValidation[R](
      user: XucUser,
      form: Form[XucUser]
  )(hasErrors: Form[XucUser] => R, success: XucUser => R): R = {
    user.authenticationMode match {
      case XivoAuthentication =>
        if (user.ctiPassword == null || user.ctiPassword.length < 3)
          hasErrors(form.withError("ctiPassword", "error.minLength", 3))
        else success(user)
      case KerberosAuthentication => success(user)
      case TokenAuthentication    => success(user)
    }
  }

  val loginForm = Form(
    mapping(
      "ctiUsername"        -> nonEmptyText,
      "ctiPassword"        -> text,
      "phoneNumber"        -> optional(text),
      "authenticationMode" -> optional(text)
    )(XucUser.applyForm)(XucUser.unapplyForm)
  )

}
