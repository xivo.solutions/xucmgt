package controllers

import java.util.UUID
import javax.inject.Inject
import play.api.cache.SyncCacheApi
import play.api.libs.json.{JsValue, Json}
import play.api.mvc.{Action, AnyContent, InjectedController}

import scala.concurrent.duration.*
import scala.language.postfixOps

class ThirdPartySample @Inject() (cache: SyncCacheApi)
    extends InjectedController {

  def ws: Action[AnyContent] =
    Action { implicit request =>
      request.body.asJson match {
        case None => BadRequest("No JSON found")
        case Some(json) =>
          val uuid = UUID.randomUUID().toString
          cache.set(uuid, json, 5 minutes)
          Ok(
            Json.parse(
              s"""{"action": "open", "event":"EventReleased", "url": "/thirdparty/open/$uuid", "autopause":true, "autopauseReason":"testReason", "title": "Third Party Sample"}"""
            )
          )
      }
    }

  def wsPopup: Action[AnyContent] =
    Action { implicit request =>
      request.body.asJson match {
        case None => BadRequest("No JSON found")
        case Some(json) =>
          val uuid = UUID.randomUUID().toString
          cache.set(uuid, json, 5 minutes)
          Ok(
            Json.parse(
              s"""{"action": "popup", "event":"EventEstablished", "url": "/thirdparty/open/$uuid", "autopause":false, "title": "Third Party Sample", "multitab": true}"""
            )
          )
      }
    }

  def open(uuid: String): Action[AnyContent] =
    Action { implicit request =>
      {
        cache.get[JsValue](uuid) match {
          case Some(json) =>
            Ok(
              views.html.sample.thirdPartyView(json)(request)
            )
          case None => BadRequest("No such pending request")
        }
      }
    }

  def logininfo(
      login: Option[String],
      token: Option[String],
      xivoccHost: Option[String]
  ): Action[AnyContent] =
    Action { implicit request =>
      {
        Ok(
          views.html.sample.loginCallback(
            s"The login callback was called with login: ${login.getOrElse("EMPTY")}, token: ${token
              .getOrElse("EMPTY")}, xivoccHost: ${xivoccHost.getOrElse("EMPTY")}"
          )
        )
      }
    }
}
