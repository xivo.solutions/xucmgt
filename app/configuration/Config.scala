package configuration

import play.api.Configuration
import play.api.Environment
import play.api.libs.json.{JsArray, JsValue, Json}

import java.io.File
import javax.inject.*
import scala.io.Source

trait Config {
  val hostAndPort: String
  val host: String
  val useSsl: Boolean
  val useSso: Boolean
  val casServerUrl: Option[String]
  val casLogoutEnable: Boolean
  val openidServerUrl: Option[String]
  val openidClientId: Option[String]
  val openidLogoutEnable: Boolean
}

@Singleton
class XucConfig @Inject() (configuration: play.api.Configuration)
    extends Config {
  val appVersion = server.info.BuildInfo.version
  val hostAndPort =
    s"${configuration.get[String]("xuc.host")}:${configuration.get[String]("xuc.port")}"
  val host: String    = configuration.get[String]("xuc.host")
  val useSsl: Boolean = configuration.get[Boolean]("xuc.useSsl")
  val useSso: Boolean = configuration.get[Boolean]("xuc.useSso")
  val casServerUrl: Option[String] =
    configuration.getOptional[String]("xuc.casServerUrl")
  val casLogoutEnable: Boolean =
    configuration.getOptional[Boolean]("xuc.casLogoutEnable").getOrElse(false)
  val openidServerUrl: Option[String] =
    configuration.getOptional[String]("xuc.openidServerUrl")
  val openidClientId: Option[String] =
    configuration.getOptional[String]("xuc.openidClientId")

  val internalHostAndPort =
    s"${configuration.get[String]("xuc.internalHost")}:${configuration.get[String]("xuc.port")}"

  val openidLogoutEnable: Boolean =
    configuration
      .getOptional[Boolean]("xuc.openidLogoutEnable")
      .getOrElse(false)

  def getNestedApplicationConf(
      path: String,
      section: String
  ): Option[String] =
    configuration.getOptional[String](s"$section.$path")
}

trait EmailConfig {
  def getEmailTemplate()(implicit env: Environment): Option[JsValue] = {
    env.resourceAsStream("custom/email.tpl") orElse env.resourceAsStream(
      "email.tpl"
    ) map Json.parse
  }
}

trait HeadsetConfig {
  def getJsonConfigs(directory: String): JsArray = {
    val dir       = new File(directory)
    val jsonFiles = dir.listFiles.filter(_.getName.endsWith(".json"))

    val jsons = jsonFiles.map { file =>
      val source = Source.fromFile(file)
      try {
        Json.parse(source.mkString)
      } finally {
        source.close()
      }
    }
    JsArray(jsons)
  }
}

@Singleton
class CcConfig @Inject() (configuration: play.api.Configuration)
    extends XucConfig(configuration)
    with EmailConfig
    with HeadsetConfig {
  // CC Agent
  def showRecordingControls: Boolean =
    configuration.get[Boolean]("agent.showRecordingControls")
  def showCallbacks: Boolean = configuration.get[Boolean]("agent.showCallbacks")
  def showQueueControls: Boolean =
    configuration.get[Boolean]("agent.showQueueControls")
  def logoffOnExit: Boolean = configuration.get[Boolean]("agent.logoffOnExit")
  def getAgentConf(param: String): Option[String] =
    configuration.getOptional[String]("agent." + param)

  // CC Manager
  val enforceManagerSecurity: Boolean =
    configuration.get[Boolean]("manager.enforceSecurity")
  def getManagerConf(param: String): Option[String] =
    configuration.getOptional[String]("manager." + param)
}

@Singleton
class UcConfig @Inject() (configuration: play.api.Configuration)
    extends XucConfig(configuration)
    with EmailConfig
    with HeadsetConfig {
  def keys(): Set[String] =
    configuration
      .get[Configuration]("client")
      .keys
}
