package system

import configuration.{CcConfig, UcConfig}
import javax.inject._
import play.api.Logger
import play.api.inject.ApplicationLifecycle

import scala.concurrent.Future

@Singleton
class ApplicationStart @Inject() (
    lifecycle: ApplicationLifecycle,
    config: CcConfig,
    configUC: UcConfig
) {

  val log: Logger = Logger(getClass.getName)
  dumpConfiguration()

  lifecycle.addStopHook { () =>
    Future.successful(log.info("Stopping xucmgt"))
  }

  private def dumpConfiguration(): Unit = {
    log.info("Starting xucmgt")
    log.info("---------- Server Configuration----------")
    log.info(s"Xuc Host  : ${config.hostAndPort}")
    log.info(s"Use SSL   : ${config.useSsl}")

    log.info("---------- SSO Configuration----------")
    log.info(s"--- Kerberos SSO")
    log.info(s"Use SSO           : ${config.useSso}")
    log.info(s"--- CAS SSO")
    log.info(s"CAS URL           : ${config.casServerUrl.getOrElse("")}")
    log.info(s"CAS Logout Enable : ${config.casLogoutEnable}")
    log.info(s"--- OpenID SSO")
    log.info(s"OIDC URL          : ${config.openidServerUrl.getOrElse("")}")
    log.info(s"OIDC Client ID    : ${config.openidClientId.getOrElse("")}")
    log.info(s"OIDC Logout       : ${config.openidLogoutEnable}")

    log.info("---------- CC Configuration----------")
    log.info(s"Logoff on exit          : ${config.logoffOnExit}")
    log.info(s"Show queue controls     : ${config.showQueueControls}")
    log.info(s"Show recording controls : ${config.showRecordingControls}")
    log.info(s"Show callbacks          : ${config.showCallbacks}")
    log.info(s"Enforce login rights    : ${config.enforceManagerSecurity}")

    log.info("---------- Client Configuration----------")

    configUC
      .keys()
      .map(k =>
        (
          k.padTo(18, ' '),
          config.getNestedApplicationConf(k, "client").getOrElse("<not set>")
        )
      )
      .foreach { case (k, v) => log.info(s"$k\t: $v") }
  }
}
