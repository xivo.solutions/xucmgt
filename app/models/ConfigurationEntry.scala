package models

import play.api.libs.json.{Json, Writes}

case class ConfigurationEntry(name: String, value: String)

object ConfigurationEntry {
  implicit val writes: Writes[ConfigurationEntry] = (o: ConfigurationEntry) =>
    Json.obj(
      "name"  -> o.name,
      "value" -> o.value
    )
}
