#!/usr/bin/env bash
set -ex

main() {
    sbt clean 'scalafix --check' 'scalafmtCheck' test docker:publishLocal
}

main "${@}"
