import { defineConfig } from "cypress";
import * as dotenv from "dotenv";

const webpackPreprocessor = require('@cypress/webpack-preprocessor');

dotenv.config();

export default defineConfig({
  e2e: {
    specPattern: "./cypress/tests",
    supportFile: "./cypress/support/e2e.ts",
    baseUrl: process.env.BASE_URL || "http://localhost:8070",
  },
  setupNodeEvents(on, config) {
    on('file:preprocessor', webpackPreprocessor());

    config.env.CONFIG_API_URL = process.env.CONFIG_API_URL;
    config.env.CONFIG_API_KEY = process.env.CONFIG_API_KEY;

    return config;
  },
  env: {
    CONFIG_API_URL: process.env.CONFIG_API_URL,
    CONFIG_API_KEY: process.env.CONFIG_API_KEY,
  }
});
